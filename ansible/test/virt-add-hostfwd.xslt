<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:qemu="http://libvirt.org/schemas/domain/qemu/1.0"
  >
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:param name="ip">127.0.42.1</xsl:param>
<xsl:param name="port">22022</xsl:param>
<xsl:strip-space elements="*"/>

<!-- identity transform -->
<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

<xsl:variable name="hostfwd-args">
  <qemu:arg value="-netdev"/>
  <qemu:arg value="user,id=virtnet.0,net=10.0.10.0/24,hostfwd=tcp:{$ip}:{$port}-:22"/>
  <qemu:arg value="-device"/>
  <qemu:arg value="virtio-net-pci,netdev=virtnet.0"/>
</xsl:variable>

<xsl:template match="domain[not(qemu:commandline)]">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
    <qemu:commandline>
      <xsl:copy-of select="$hostfwd-args"/>
    </qemu:commandline>
  </xsl:copy>
</xsl:template>

<xsl:template match="commandline">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
    <xsl:copy-of select="$hostfwd-args"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
