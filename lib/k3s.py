import distutils.util
import os.path
import random
import subprocess
import sys
import terraform
import logging
import string
import tempfile
import yaml
import logging
import _kubectl

kubectl = _kubectl.Kubectl(k3s=True)

MON_DIR=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..','infra', 'mon', 'k3s')
NETWORK_POLICY_DIR=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..','infra', 'kubernetes-deploy', 'network', 'k3s')

# k3s can't reuse the same port numbers for challenges since its on the same local ip
reuse_ports = False

CTFD_NAMESPACE='ctfd'
CHALLENGE_NAMESPACE='challenges'

class Kubectl(_kubectl.Kubectl):
    def __init__(self):
        super(_kubectl.Kubectl, self).__init__(k3s=True)

# Use the right input on Python 2
try:
    input = raw_input
except NameError:
    pass

# XXX: This is a k8s YAML from the internet so we should warn the user before
# trying to use it!
LOCAL_PATH_STORAGECLASS_YAML = (
    "https://raw.githubusercontent.com/rancher/local-path-provisioner"
    "/master/deploy/local-path-storage.yaml"
)


def prompt(query):
   sys.stdout.write('%s [y/n]: ' % query)
   val = input()
   try:
       ret = distutils.util.strtobool(val)
   except ValueError:
       sys.stdout.write('Please answer with a y/n\n')
       return prompt(query)
   return ret

def rollout_local_path_storage():
    if not prompt("Can I use a YAML from the net for local-path storage?"):
        try:
            kubectl.wait_for_deployment(
                "local-path-provisioner", ns="local-path-storage"
            )
        except subprocess.CalledProcessError as exc:
            raise Exception("Okay I won't use it then") from exc
        else:
            logging.info("You already have one running, I'll use that")
            return
    logging.warn("This will pollute your system at /opt/local-path-provisioner")
    kubectl.create_or_replace(LOCAL_PATH_STORAGECLASS_YAML)
    logging.info("local-path storage class system is being deployed")
    kubectl.wait_for_deployment("local-path-provisioner", ns="local-path-storage")

def start_ctfd(deployment_dir, config, verbose=False):
    # local deployment. Setting the hostname if not set will stop us from using the default DNS as our hosts in the challenge descriptions.
    if 'hostname' not in config:
        config['hostname'] = None

    # We have to spin up a `local-path` storage class to make persistent volume
    # claims work in k3s
    if not kubectl.deployment_exists('local-path-provisioner', 'kube-system'):
        rollout_local_path_storage()
    kubectl.ensure_namespace(CTFD_NAMESPACE)

    tls = kubectl.has_secret('ctf-tls', CTFD_NAMESPACE)

    # Do the actual deployment after applying some fixups to the `deploy.yml`
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as mod_yml:
        # We have to modify the CTFd deployment.yml to make it work in k3s
        with open(os.path.join(deployment_dir, "deploy.yml")) as orig_yml:
            deployment_docs = tuple(yaml.load_all(orig_yml.read(), Loader=yaml.SafeLoader))
        for doc in deployment_docs:
            # We need PVCs to use the local-path storage class since k3s has no
            # default which it'll use
            if doc["kind"] == "PersistentVolumeClaim":
                doc["spec"]["storageClassName"] = "local-path"
            # We need load balancers to not try to bind port 80 or 443 because
            # k3s' kubes services listen on those ports
            if (
                doc["kind"] == "Service" and
                doc["spec"]["type"] == "LoadBalancer"
            ):
                for port_entry in doc["spec"]["ports"]:
                    if port_entry["port"] in (80, 443):
                        # Pick a random IANA ephemeral port - good enough
                        port_entry["port"] = random.randrange(
                            2**15 + 2**14, 2**16 - 1
                        )
                        if not tls:
                            port_entry["targetPort"] = 8000

            if not tls:
                if (
                doc["kind"] == "Deployment" and
                doc["metadata"]["name"] == "ctfd-web"
                ):
                    logging.info("No TLS cert available so removing TLS container")
                    for container in doc["spec"]["template"]["spec"]["containers"]:
                        if container["name"] in {"nginx", "tls-sidecar"}:
                            doc["spec"]["template"]["spec"]["containers"].remove(container)
                    for volume in doc["spec"]["template"]["spec"]["volumes"]:
                        if volume["name"] == "tls-secret-volume":
                            doc["spec"]["template"]["spec"]["volumes"].remove(volume)
            if (
                doc["kind"] == "Deployment" and
                doc["metadata"]["name"] == "ctfd-web"
                ):
                for container in doc["spec"]["template"]["spec"]["containers"]:
                    if container["name"] == "frontend":
                        # 32 workers will likely bring your rig to a standstill
                        for env in container["env"]:
                            if env['name'] == 'WORKERS':
                                env['value'] = '4'
                                break
                        # set SECRET_KEY
                        container["env"].append({'name': 'SECRET_KEY', 'value': ''.join(random.choice(string.ascii_letters) for i in range(32))})

        # Write the mutated YAML out to the tempfile
        dumped_yml = yaml.dump_all(deployment_docs)
        mod_yml.write(dumped_yml)
        mod_yml.flush()
        # Now we can create the CTFd deployment using the modified deployment.
        # We can't replace certains parts of the CTFd deployment so we might
        # fail and should just move on to see if we can discover the service.
        kubectl.create(mod_yml.name, can_fail=True)
    # We need to add a secret to the ctfd namespace - this prompts the user
    if 'private' in config['gitlab_group']:
        kubectl.find_or_prompt_docker_secret("regcred", ns=CTFD_NAMESPACE)
    # We should be able to wait for the rollout now
    kubectl.wait_for_deployment("ctfd-cache", ns=CTFD_NAMESPACE)
    kubectl.wait_for_deployment("ctfd-db", ns=CTFD_NAMESPACE)
    kubectl.wait_for_deployment("ctfd-web", ns=CTFD_NAMESPACE)
    # And now pull the external service address
    return "{}://{}/".format('https' if tls else 'http', kubectl.get_service_address("ctfd-web", ns=CTFD_NAMESPACE))

def stop_ctfd(deployment_dir, config, verbose=False):
    # XXX: Need to work out how to take it down and then back up...
    purge_ctfd(deployment_dir, config)

def purge_ctfd(deployment_dir, config, verbose=False):
    kubectl.delete(os.path.join(deployment_dir, "deploy.yml"), quiet=False)
    try:
        subprocess.check_call(("sudo", "k3s", "crictl", "rmi", "prune",), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError as exc:
        pass

def start_challenges(config, verbose=False):
    # local deployment. Setting the DNS to None will stop us trying to set DNS and use hostname for challenge text
    config['chal_domain'] = None
    # This tells manifest_to_k8s_deploy to not include the externaldns annotation in challenges
    config['no_externaldns'] = True
    # This tells manifest_to_k8s_deploy to not use horizontal autoscaling
    config['no_horizontal_autoscaler'] = True
    # This tells manifest_to_k8s_deploy to not use windows support
    config['no_windows_support'] = True
  
    kubectl.ensure_namespace(CHALLENGE_NAMESPACE)
    # We need to add a secret to the challenges namespace - this prompts the user
    if 'private' in config['gitlab_group']:
        kubectl.find_or_prompt_docker_secret("regcred", ns=CHALLENGE_NAMESPACE)
    if 'no_monitoring' not in config:
        terraform.apply(MON_DIR, config['filename'], vars=config['vars'], verbose=verbose, sudo=True)
    
    terraform.apply(NETWORK_POLICY_DIR, config['filename'], vars=config['vars'], verbose=verbose, sudo=True)
    
def stop_challenges(config, verbose=False):
    terraform.destroy(NETWORK_POLICY_DIR, config['filename'], vars=config['vars'], sudo=True)
    terraform.destroy(MON_DIR, config['filename'], vars=config['vars'], sudo=True)
    try:
        subprocess.check_call(("sudo", "k3s", "crictl", "rmi", "prune",), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError as exc:
        pass
