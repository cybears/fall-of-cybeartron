import os.path
import _kubectl

kubectl = _kubectl.Kubectl()

def start_ctfd(deployment_dir, config, verbose=False):
    kubectl.create(os.path.join(deployment_dir, "deploy.yml"), can_fail=True)
    # We need to add a secret to the ctfd namespace - this prompts the user
    if 'private' in config['gitlab_group']:
        kubectl.find_or_prompt_docker_secret("regcred", ns="ctfd")
    # We should be able to wait for the rollout now
    kubectl.wait_for_deployment("ctfd-cache", ns="ctfd")
    kubectl.wait_for_deployment("ctfd-db", ns="ctfd")
    kubectl.wait_for_deployment("ctfd-web", ns="ctfd")
    # And now pull the external service address - we always use HTTPS when we
    # deploy on kubernetes thanks to the nginx sidecar container
    return "https://{}/".format(kubectl.get_service_address("ctfd-web", ns="ctfd"))

def stop_ctfd(deployment_dir, config, verbose=False):
    # XXX: Need to work out how to take it down and then back up...
    purge_ctfd(deployment_dir, config, verbose)

def purge_ctfd(deployment_dir, config, verbose=False):
    # Tell the k8s module that it'll be using k3s as an intermediary
    kubectl.delete(os.path.join(deployment_dir, "deploy.yml"), quiet=not verbose)
