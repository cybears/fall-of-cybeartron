import json
import logging
import os.path
import pathlib
import re
import time

import dateutil.parser
import passgen
import requests

import ctfd_admin_pass
from ctfd_api import CTFdAuthenticatedAPISession, extract_csrf_nonce

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BASE_DIR=os.path.normpath(os.path.join(SCRIPT_DIR, '..'))

import ctfd_upload

def wait_for_ctfd(uri):
    logging.info("Waiting for CTFd - this may take a while if the DB is new")
    for i in range(1, 31, 3):
        try:
            response = requests.get(uri)
            if response.status_code not in [500, 502, 504] and b"CTFd" in response.content:
                logging.info("CTFd is ready!")
                break
        except requests.exceptions.ConnectionError as exc:
            logging.warn(exc)
        time.sleep(i)

def try_setup_ctfd(root_uri, state_dir, config):
    client = requests.session()     # To maintain the session for CSRF
    root_get_r = client.get(root_uri)
    if not root_get_r.url.endswith("/setup"):
        logging.info("Existing CTFd doesn't need setup")
        return False
    else:
        logging.info("New CTFd - setting it up!")
        csrf_nonce = extract_csrf_nonce(root_get_r.content)
        admin_pass = ctfd_admin_pass.get_pass()
        setup_data = {
            "ctf_name": config['ctf_name'],
            "name": config['ctf_host'],
            "email": config['ctf_host_email'],
            "user_mode": "teams",   # Make sure this is correct or CTFd will
                                    # just 500 error all over the place!
            "password": admin_pass,
            "nonce": csrf_nonce,
        }
        setup_post_r = client.post(root_get_r.url, data=setup_data)
        if setup_post_r.url != root_uri and setup_post_r.url != root_uri[:-1]+ ":443/":
            raise Exception(
                "Redirect to home page after setup failed! "
                "Ended up at {!r} instead".format(setup_post_r.url)
            )
        return True

def run_extra_config(api_session, config):
    # Patch the config to set the theme and start/end times. We do this here so
    # that we can forcibly set this stuff up after first setup has been done.
    extra_config = {
        "ctf_theme": config["ctfd_theme"],
        **{
            k.removeprefix("ctf_"): int(dateutil.parser.isoparse(v).timestamp())
            for k, v in config.items() if k in {"ctf_start", "ctf_end"}
        },
    }
    config_patch_r = api_session.patch("configs", json=extra_config)
    assert config_patch_r.status_code == 200, config_patch_r.status_code
    try:
        with open(os.path.join(BASE_DIR, config['ctfd_dir'], 'pages.json')) as pages_json:

            pages_list = json.loads(pages_json.read())
            # Upload the pages
            for page in pages_list:
                ctfd_upload.upload_page(api_session, page, config['ctfd_dir'])
    except FileNotFoundError:
        logging.warning('No pages.json found, not adding any custom pages to CTFd.')

    if 'favicon' in config:
        logging.info("Setting favicon")
        with open(os.path.join(BASE_DIR, config['favicon']), "rb") as favicon:
            file_post_r = api_session.post(
                "files",
                # We use form data here to pack in the files to upload
                data={
                    "nonce": api_session.nonce,
                    "type": "standard",
                },
                files={"file": favicon}
            )
            assert file_post_r.status_code == 200, file_post_r.content
            response_data = json.loads(file_post_r.content)
            remote_file = response_data['data'][0]['location']
            logging.info(f"Favicon saved at {remote_file}")
            config_patch_r = api_session.patch(
                "configs",
                json={
                    "ctf_small_icon": remote_file
                }
            )
            assert config_patch_r.status_code == 200, config_patch_r.status_code

    # Add a custom team field to mark delegates - without changes to CTFd this
    # may be user settable when teams are created
    fields_post_r = api_session.post(
        "configs/fields",
        json={
            "type": "team",
            "field_type": "boolean",
            "name": "delegate",
            "description": "BSides Delegate",
            # We have a change which makes field with `editable: false` not
            # settable during team creation
            "editable": False,
            "required": False,
            "public": True
        }
    )
    # This will fail if CTFd does not have the config, so don't assert here.
    #assert fields_post_r.status_code == 200, fields_post_r.status_code


def do_config(ctfd_uri, state_dir, config, force=False, hostname=None):
    wait_for_ctfd(ctfd_uri)

    # Try setup will return False if ctfd is already set up
    new_instance = try_setup_ctfd(ctfd_uri, state_dir, config)

    # We should have a password we can use for API sessions now
    admin_pass = ctfd_admin_pass.get_pass()
    api_session = CTFdAuthenticatedAPISession(ctfd_uri, admin_pass)

    if new_instance:
        # Old CTFd versions needed the admin to be in a team before being able to use the API
        team_id = ctfd_upload.create_team(api_session, 'cybears', admin_pass, hidden=True)
        ctfd_upload.join_team(api_session, ctfd_uri, team_id, '1', 'cybears', admin_pass)

    if new_instance or force:
        # And we can run any extra configuration and setup we need
        run_extra_config(api_session, config)

def push_chals(ctfd_uri, chal_paths, hostname=None):
    # Get an API session
    admin_pass = ctfd_admin_pass.get_pass()
    api_session = CTFdAuthenticatedAPISession(ctfd_uri, admin_pass)
    # And push the challenges with it
    ctfd_upload.add_challenges(chal_paths, api_session, False, hostname)
