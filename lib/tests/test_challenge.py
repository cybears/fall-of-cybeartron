import pytest
import os
from . import SCRIPT_DIR

from .challenge_fixtures import required_challenge, required_deployment, complete_challenge, temp_files
from challenge import Challenge, InvalidChallengeManifest


def test_good(required_challenge):
    c = Challenge(SCRIPT_DIR, attrs=required_challenge)
    assert c

def test_missing_required(required_challenge):
    required_challenge.pop('name')
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_challenge)

def test_complete(complete_challenge):
    c = Challenge(os.path.basename(complete_challenge['deployment']['template']), attrs=complete_challenge)
    assert c

def test_port_list(required_deployment):
    required_deployment['deployment']['target_port'] = [3000, 3001, 3004]
    c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    assert c

def test_bad_category(required_challenge):
    required_challenge['category'] = 'does_not_exist'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_challenge)

def test_bad_value(required_challenge):
    required_challenge['value'] = 'd'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_challenge)

def test_bad_flag(required_challenge):
    required_challenge['flags'][0]['type'] = 'not-real'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_challenge)
    required_challenge['flags'][0]['type'] = 'regex'
    required_challenge['flags'][0]['data'] = 'rando'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_challenge)
    required_challenge['flags'][0].pop('data')
    required_challenge['flags'][0].pop('content')
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_challenge)

def test_deployment(required_deployment):
    c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    assert c

def test_deployment_missing_required(required_deployment):
    required_deployment['deployment'].pop('solve_script')
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_dns(required_deployment):
    required_deployment['deployment']['target_dns_subdomain'] = 'test_domain'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_image_name(required_deployment):
    required_deployment['deployment']['container_image'] = '_image'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['container_image'] = 'image?fd'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_cpu_requests(required_deployment):
    required_deployment['deployment']['cpu_request'] = '34f'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['cpu_request'] = 'm'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_cpu_limits(required_deployment):
    required_deployment['deployment']['cpu_limit'] = 'sdf'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['cpu_limit'] = 'm'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
def test_deployment_bad_cpu_limits(required_deployment):
    required_deployment['deployment']['cpu_limit'] = '34f'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['cpu_limit'] = 'm'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_healthcheck_cpu_requests(required_deployment):
    required_deployment['deployment']['healthcheck_cpu_request'] = '34f'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['healthcheck_cpu_request'] = 'm'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_healthcheck_cpu_limits(required_deployment):
    required_deployment['deployment']['healthcheck_cpu_limit'] = '34f'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['healthcheck_cpu_limit'] = 'm'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_mem_requests(required_deployment):
    required_deployment['deployment']['mem_request'] = '34f'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['mem_request'] = 'm'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_mem_limits(required_deployment):
    required_deployment['deployment']['mem_limit'] = '34f'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['mem_limit'] = 'm'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_healthcheck_mem_requests(required_deployment):
    required_deployment['deployment']['healthcheck_mem_request'] = '34f'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['healthcheck_mem_request'] = 'm'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_healthcheck_mem_limits(required_deployment):
    required_deployment['deployment']['healthcheck_mem_limit'] = '34f'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['healthcheck_mem_limit'] = 'm'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_replica_target_cpu_percentage(required_deployment):
    required_deployment['deployment']['replica_target_cpu_percentage'] = '34f'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)
    required_deployment['deployment']['replica_target_cpu_percentage'] = 400
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_isolation(required_deployment):
    required_deployment['deployment']['isolation'] = 'random'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_deployment_bad_privileged(required_deployment):
    required_deployment['deployment']['privileged'] = 'random'
    with pytest.raises(InvalidChallengeManifest):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_handout(complete_challenge):
    c = Challenge(SCRIPT_DIR, attrs=complete_challenge)
    for handout in c.handouts:
        assert handout.file
        assert handout.path

def test_bad_handout(required_deployment):
    required_deployment['handouts'] = ['/file/does/not/exist']
    with pytest.raises(FileNotFoundError):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)

def test_bad_template_file(required_deployment):
    required_deployment['deployment']['template'] = '/file/does/not/exist'
    with pytest.raises(FileNotFoundError):
        c = Challenge(SCRIPT_DIR, attrs=required_deployment)