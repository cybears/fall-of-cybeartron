import pytest
import os
from utils import discover_manifests
from . import BSIDES_CHALLENGE_DIR, TEST_CHALLENGE_DIR

BSIDES_2019 = os.path.join(BSIDES_CHALLENGE_DIR, "2019")
BSIDES_2021 = os.path.join(BSIDES_CHALLENGE_DIR, "2020")
BSIDES_NEXT = os.path.join(BSIDES_CHALLENGE_DIR, "next")

CHALLENGE_DIRS = [TEST_CHALLENGE_DIR, BSIDES_2019, BSIDES_2021, BSIDES_NEXT]

discover_manifests

def test_discover_manifests():
    for manifest in discover_manifests(CHALLENGE_DIRS, challenge_obj=False, need_handouts=False):
        assert os.path.exists(manifest)

def test_discover_manifests_chal_obj():
    for chal in discover_manifests(CHALLENGE_DIRS, challenge_obj=True, need_handouts=False):
        assert chal.name

def test_discover_manifests_chal_obj_where_handouts_dont_exist():
    with pytest.raises(FileNotFoundError):
        for chal in discover_manifests(CHALLENGE_DIRS, challenge_obj=True, need_handouts=True):
            assert chal.name