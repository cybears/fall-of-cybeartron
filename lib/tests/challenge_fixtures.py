import pytest

@pytest.fixture
def required_challenge():
    return {
        'name': 'test',
        'category': 'crypto',
        'value': 40,
        'tags': ['testflag1', 'testflag2'],
        'description': 'a challenge description',
        'workflow': {
            'concept': True,
            'workup': True,
            'playtest': 3,
            'ci': True,
            'deploy': True,
            'review': True,
            'theme': True
        },
        'flags': [
            {
                'type': 'static',
                'content': 'flag{flag_flag_flag}'
            }
        ],

    }

@pytest.fixture
def required_deployment(required_challenge):
    required_challenge['deployment'] = {
        'deploy_name': 'test-deployment',
        'container_image': 'test-image',
        'healthcheck_image': 'test-image-healthcheck',
        'solve_script': 'python3 ./solve.py --hostname localhost --port 1337 -v',
        'target_port': 3000,
        'target_dns_subdomain': 'test-domain'
    }
    return required_challenge

@pytest.fixture(scope="session")
def temp_dir(tmp_path_factory):
    fn = tmp_path_factory.mktemp("data")
    return fn

@pytest.fixture(scope="session")
def temp_files(tmp_path_factory):
    fn = tmp_path_factory.mktemp("data")
    template_file = fn / 'template.yml'
    handout1 = fn / 'handout1'
    handout2 = fn / 'handout2'
    template_file.write_text('something here')
    handout1.write_text('some data')
    handout2.write_text('more tests')
    return {
        'template_file': template_file,
        'handout1': handout1,
        'handout2': handout2,
    }

@pytest.fixture
def complete_challenge(required_deployment, temp_files):
    required_deployment['type'] = 'dynamic'
    required_deployment['use'] = True
    required_deployment['handouts'] = [temp_files['handout1'], temp_files['handout2']]
    required_deployment['deployment']['template'] = str(temp_files['template_file'])
    required_deployment['deployment']['container_image_os'] = 'windows'
    required_deployment['deployment']['healthcheck_interval'] = 300
    required_deployment['deployment']['min_replicas'] = 2
    required_deployment['deployment']['max_replicas'] = 19
    required_deployment['deployment']['replica_target_cpu_percentage'] = 75
    required_deployment['deployment']['cpu_request'] = '1'
    required_deployment['deployment']['mem_request'] = '500Mi'
    required_deployment['deployment']['cpu_limit'] = 1.5
    required_deployment['deployment']['mem_limit'] = '1Gi'
    required_deployment['deployment']['healthcheck_cpu_request'] = '200m'
    required_deployment['deployment']['healthcheck_mem_request'] = '1G'
    required_deployment['deployment']['healthcheck_cpu_limit'] = '1.6'
    required_deployment['deployment']['healthcheck_mem_limit'] = '1G'
    required_deployment['deployment']['privileged'] = False
    required_deployment['deployment']['isolation'] = 'call-out'
    return required_deployment

@pytest.fixture
def valid_challenge_template(complete_challenge, temp_files):
    complete_challenge['deployment'].pop('template')
    return complete_challenge