import os
import sys

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
LIB_DIR=os.path.normpath(os.path.join(SCRIPT_DIR, '..'))
BASE_DIR=os.path.normpath(os.path.join(LIB_DIR, '..'))
CHALLENGE_DIR=os.path.normpath(os.path.join(BASE_DIR, 'challenges'))
TEST_CHALLENGE_DIR = os.path.normpath(os.path.join(CHALLENGE_DIR, "test"))
BSIDES_CHALLENGE_DIR = os.path.normpath(os.path.join(CHALLENGE_DIR, "bsides"))
sys.path.append(LIB_DIR)