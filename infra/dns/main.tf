resource "aws_route53_zone" "ctf" {
  name = var.ctf_domain
}

resource "aws_route53_zone" "chal" {
  name = var.chal_domain
}

terraform {
  backend "s3" {
    bucket         = "terraform-20191116042935470700000001"
    key            = "dns/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "terraform-state-lock-dynamo"
    region         = "ap-southeast-2"
  }
}
