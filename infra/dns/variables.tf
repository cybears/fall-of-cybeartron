variable "aws_region" {
  default = "ap-southeast-2"
}

variable "ctf_domain" {
  default = "ctf.cybears.io"
}

variable "chal_domain" {
  default = "chal.cybears.io"
}
