terraform {
  required_version = ">= 0.14.4"
}
provider "kubernetes" {
  config_path = "/etc/rancher/k3s/k3s.yaml"
  host        = "https://127.1.1.1:6443"
}

provider "helm" {
  kubernetes {
    config_path = "/etc/rancher/k3s/k3s.yaml"
    host        = "https://127.1.1.1:6443"
  }
}

module "calico" {
  source       = "../calico"
}
