#tfsec:ignore:kubernetes-network-no-public-egress
#tfsec:ignore:kubernetes-network-no-public-ingress
resource "kubernetes_network_policy" "none" {
  metadata {
    name      = "no-isolation"
    namespace = "challenges"
  }
  spec {
    pod_selector {
      match_labels = {
        networkIsolation = "none"
      }
    }
    egress {}
    ingress {}

    policy_types = ["Ingress", "Egress"]
  }
}