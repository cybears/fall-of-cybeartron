#tfsec:ignore:kubernetes-network-no-public-egress
#tfsec:ignore:kubernetes-network-no-public-ingress
resource "kubernetes_network_policy" "call-out" {
  metadata {
    name      = "allow-call-out"
    namespace = "challenges"
  }
  spec {
    pod_selector {
      match_labels = {
        networkIsolation = "call-out"
      }
    }
    egress {
      to {
        ip_block {
          cidr = "0.0.0.0/0"
          except = [
            "10.0.0.0/8",
            "172.16.0.0/12",
            "192.168.0.0/16",
          ]
        }
      }
    }

    policy_types = ["Egress"]
  }
}