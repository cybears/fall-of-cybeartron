resource "helm_release" "calico" {
  name       = "calico"
  chart      = "https://github.com/projectcalico/calico/releases/download/v3.18.4/tigera-operator-v3.18.4-1.tgz"
}