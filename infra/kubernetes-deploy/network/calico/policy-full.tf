#tfsec:ignore:kubernetes-network-no-public-ingress
#tfsec:ignore:kubernetes-network-no-public-egress
resource "kubernetes_network_policy" "full" {
  metadata {
    name      = "full-isolation"
    namespace = "challenges"
  }
  spec {
    pod_selector {
      match_labels = {
        networkIsolation = "full"
      }
    }
    egress {}

    policy_types = ["Egress"]
  }
}