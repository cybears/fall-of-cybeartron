#tfsec:ignore:AWS017
#tfsec:ignore:AWS002
resource "aws_s3_bucket" "remote_state" {
  acl = "private"
  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = false
  }

  tags = {
    Name = "S3 Remote Terraform State Store"
  }

  dynamic "server_side_encryption_configuration" {
    for_each = var.s3_encryption_key_arn != "" ? [1] : []
    content {
      rule {
        apply_server_side_encryption_by_default {
          kms_master_key_id = var.s3_encryption_key_arn
          sse_algorithm     = "aws:kms"
        }
      }
    }
  }

  dynamic "logging" {
    for_each = var.log_bucket != "" ? [1] : []
    content {
      target_bucket = var.log_bucket
      target_prefix = "logs/"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "remote_state" {
  bucket                  = aws_s3_bucket.remote_state.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# create a dynamodb table for locking the state file
#tfsec:ignore:aws-dynamodb-table-customer-key
resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name           = "terraform-state-locks"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = "DynamoDB Terraform State Lock Table"
  }

  point_in_time_recovery {
    enabled = true
  }
  #tfsec:ignore:AWS092
  dynamic "server_side_encryption" {
    for_each = var.dynamodb_encryption_key_arn != "" ? [1] : []
    content {
      enabled     = true
      kms_key_arn = var.dynamodb_encryption_key_arn
    }
  }
}
