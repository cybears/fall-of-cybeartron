
output "remote_state_s3" {
  description = "S3 bucket for terraform remote remote_state."
  value       = aws_s3_bucket.remote_state.id
}
output "remote_state_lock" {
  description = "dynamodb used for remote_state lock."
  value       = aws_dynamodb_table.dynamodb-terraform-state-lock.id
}