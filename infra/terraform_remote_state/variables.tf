variable "aws_region" {
  default = "ap-southeast-2"
}

variable "s3_encryption_key_arn" {
  type        = string
  description = "Encryption key for use with S3 bucket at-rest encryption. Unencrypted if this is empty."
  default     = ""
}
variable "dynamodb_encryption_key_arn" {
  type        = string
  description = "Encryption key for use with DynamoDB at-rest encryption. Unencrypted if this is empty."
  default     = ""
}
variable "log_bucket" {
  type        = string
  description = "Bucket for log data. Logging disabled if empty"
  default     = ""
}