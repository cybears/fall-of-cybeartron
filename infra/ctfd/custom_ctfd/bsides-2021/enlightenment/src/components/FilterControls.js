import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import Tags from '../components/Tags'

const FilterControls = props => {
    const {
        tags,
        includeFilters,
        excludeFilters,
        handleTagClick,
        search,
        sortByPoints,
        sortDescending,
        setSortByPoints,
        setSortDescending,
        setSearch,
        clearFilters
    } = props

    return (
        <div className="filter-container">
            <div className="flex-container">
                <div className="tag-container">
                    <Tags
                        tags={tags}
                        includeFilters={includeFilters}
                        excludeFilters={excludeFilters}
                        onClick={handleTagClick}
                    />
                    <form>
                        <input type="search" placeholder="Challenge name" onChange={(e) => setSearch(e.target.value)} value={search} />
                    </form>
                </div>
            </div>
            <div className="filter-controls">
                <Form>
                    <Form.Switch id="sort-method" label="Sort by points" onChange={() => setSortByPoints(!sortByPoints)} />
                    <Form.Switch id="sort-order" label="Sort descending" onChange={() => setSortDescending(!sortDescending)} />
                    <Button onClick={clearFilters}>Clear Filters</Button>
                </Form>
            </div>
        </div>
    )
}

export default FilterControls
