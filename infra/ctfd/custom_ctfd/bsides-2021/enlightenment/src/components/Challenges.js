import Challenge from './Challenge'

const Challenges = props => {
    const { challenges, includeFilters, excludeFilters, activateChallenge, sortByPoints, sortDescending } = props

    const sortByName = (a, b) => {
        // Special case for "hidden" challenges -> bottom of the sort stack with you!
        if (a.name === "???") return 1
        else if (a.name === "???" && b.name === "???") return 0
        else if (b.name === "???") return -1

        const options = { numeric: true, sensitivity: 'base' }
        if (sortDescending) {
            return b.name.localeCompare(a.name, undefined, options)
        }
        return a.name.localeCompare(b.name, undefined, options)
    }

    const sortByScore = (a, b) => {
        if (sortDescending) {
            return b.value - a.value
        }
        return a.value - b.value
    }

    const sortByFunction = sortByPoints ? sortByScore : sortByName

    const displayedChallenges = challenges.slice()
        .filter(challenge => includeFilters.every(f => challenge.tags.some(tag => tag.value === f)))
        .filter(challenge => excludeFilters.every(f => !challenge.tags.some(tag => tag.value === f)))
        .sort(sortByFunction)

    return (
        <>
            {displayedChallenges.map(challenge => (
                <Challenge
                    key={challenge.id}
                    challenge={challenge}
                    onClick={() => activateChallenge(challenge.id)} />
            ))}
        </>
    )
}

export default Challenges
