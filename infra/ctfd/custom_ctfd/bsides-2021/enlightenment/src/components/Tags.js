import Tag from './Tag'

const Tags = props => {
    const { tags, onClick, includeFilters, excludeFilters } = props
    const onlyUnique = (value, index, self) => {
        return self.indexOf(value) === index
    }
    const uniqueTags = tags.filter(onlyUnique).sort()

    return uniqueTags.map(tag => {
        const additionalClasses = []
        if (includeFilters.includes(tag)) additionalClasses.push("filter-include")
        if (excludeFilters.includes(tag)) additionalClasses.push("filter-exclude")
        return <Tag
            key={tag}
            value={tag}
            onClick={() => onClick(tag)}
            additionalClasses={additionalClasses} />
    })
}

export default Tags
