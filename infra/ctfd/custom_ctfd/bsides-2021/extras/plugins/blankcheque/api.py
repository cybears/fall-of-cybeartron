import flask
import flask_restx
import wtforms
import wtforms.validators

from ... import api as ctfd_api
from ... import cache as ctfd_cache
from ... import forms as ctfd_forms
from ... import models as ctfd_models
from ...api.v1 import challenges as api_chals
from ...utils import config as ctfd_config
from ...utils import dates as ctfd_dates
from ...utils import decorators as ctfd_decs
from ...utils import logging as ctfd_log
from ...utils import user as ctfd_user_utils
from .. import challenges as plugin_chals

API_ENDPOINT_TAIL = "attempt_any"
MAX_FAILS_PER_MINUTE = 3


class SubmissionAttemptAnyForm(ctfd_forms.BaseForm):
    submission = wtforms.StringField(
        "Flag", validators=[wtforms.validators.InputRequired()]
    )
    submit = ctfd_forms.fields.SubmitField("Submit")
ctfd_forms.submissions.SubmissionAttemptAnyForm = SubmissionAttemptAnyForm


@api_chals.challenges_namespace.route("/attempt_any")
class ChallengeAttemptAny(flask_restx.Resource):
    @ctfd_decs.during_ctf_time_only
    @ctfd_decs.require_verified_emails
    def post(self):
        if not ctfd_user_utils.authed():
            return {
                "success": True,
                "data": {"status": "authentication_required"}
            }, 403

        user_obj = ctfd_user_utils.get_current_user()
        team_obj = ctfd_user_utils.get_current_team()
        if ctfd_config.is_teams_mode() and team_obj is None:
            return {
                "success": True,
                "data": {
                    "status": "not authorised",
                    "message": "you must be in a team to submit flags",
                }
            }, 403
        if ctfd_dates.ctf_paused() and not ctfd_user_utils.is_admin():
            return {
                "success": True,
                "data": {
                    "status": "paused",
                    "message": "{} is paused".format(ctfd_config.ctf_name()),
                },
            }, 403

        request = flask.request
        admin_view = (
            ctfd_user_utils.is_admin() and
            request.args.get("preview", False)
        )
        # We respect global submission rate limiting at a reduced rate. Note
        # that since this endpoint does not record failures since we have no
        # concrete challenge ID to do so against, this is a weak rate limit :(
        kpm = ctfd_user_utils.get_wrong_submissions_per_minute(
            user_obj.account_id
        )
        if kpm > MAX_FAILS_PER_MINUTE:
            result = dict(
                code=429, status="ratelimited", log="TOO FAST",
                message="You're submitting flags too fast. Slow down.",
            )
        else:
            # Get the submission data
            data = request.form or request.get_json()
            submission = data.get("submission")
            if submission:
                # Get the challenges we'll check against. We exclude any
                # challenges with a maximum number of attempts since this
                # endpoint doesn't record failures in the submissions table
                chals_q = ctfd_models.Challenges.query.filter(
                    ctfd_models.Challenges.max_attempts == 0,
                )
                if not admin_view:
                    chals_q = chals_q.filter(
                        ctfd_models.Challenges.state.notin_(
                            ["hidden", "locked"]
                        ),
                    )
                # Filter submission candidates for things the DB can't check
                # like challenge prerequisites
                candidates = set()
                user_solves = {
                    chal_id for (chal_id, ) in
                    ctfd_models.db.session
                    .query(ctfd_models.Solves.challenge_id)
                    .filter(
                        ctfd_models.Solves.account_id == user_obj.account_id
                    )
                }
                for chal_obj in chals_q:
                    # Check that the challenge is not already solved
                    if chal_obj.id in user_solves:
                        continue
                    # Check that any prerequisites are met
                    if chal_obj.requirements:
                        prereqs = set(
                            chal_obj.requirements.get("prerequisites", [])
                        )
                        if not prereqs.issubset(user_solves):
                            continue
                    # If we reach this point then we can attempt this one
                    candidates.add(chal_obj)
                # Now we can attempt each candidate challenge
                newly_solved = set()
                for chal_obj in candidates:
                    chal_class = plugin_chals.get_chal_class(chal_obj.type)
                    result, _ = chal_class.attempt(chal_obj, request)
                    if result:
                        if not admin_view:
                            chal_class.solve(
                                user=user_obj, team=team_obj,
                                challenge=chal_obj, request=request,
                            )
                            ctfd_cache.clear_standings()
                        newly_solved.add(chal_obj.id)
                # Prepare the response
                result = dict(
                    code=200,
                    status="correct" if newly_solved else "incorrect",
                    log="CORRECT" if newly_solved else "INCORRECT",
                    message="Solved {} new challenge(s)".format(len(newly_solved)),
                    solved=sorted(newly_solved),
                )
            else:
                result = dict(
                    code=400, status="invalid", log="INVALID",
                    message="No submission data specified",
                )
        # Log the submissions and result, and return the response
        ctfd_log.log(
            "submissions",
            "[{date}] {name} submitted {submission!r} with kpm {kpm} " +
            "and solved {solved} [{result}]",
            name=user_obj.name, submission=submission, kpm=kpm,
            solved=result.get("solved") or "nothing", result=result["log"],
        )
        return {
            "success": True,
            "data": {
                k: v for k, v in result.items()
                if k in {"status", "message", "solved", }
            }
        }, result.get("code", 200)


def register():
    ns_path = ctfd_api.CTFd_API_v1.ns_paths[api_chals.challenges_namespace]
    ep_path = "{}/{}".format(ns_path, API_ENDPOINT_TAIL)
    ctfd_api.CTFd_API_v1.register_resource(
        api_chals.challenges_namespace, ChallengeAttemptAny, ep_path
    )
