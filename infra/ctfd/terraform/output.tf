output "ctfd_domain" {
  value = "https://${var.ctf_domain}/"
}

output "lb_dns_name" {
  value = module.ctfd.lb_dns_name
}
resource "random_password" "ctfd_password" {
  length = 64
}
output "ctfd_password" {
  value = random_password.ctfd_password
}
