
import { group, sleep, check, fail } from 'k6';
import http from 'k6/http';
import { Rate } from "k6/metrics";

// Version: 1.2
// Creator: WebInspector


export let errorRate = new Rate("errors");

export let options = {
	maxRedirects: 0,
	thresholds: {
		errors: ["rate<0.1"] // <10% errors
	  }
};

let host =  `${__ENV.HOSTNAME}`;
let origin = "http://" + host;
let base_url = origin + "/";
let result;
export default function main() {

	let username = Math.random().toString(36).substring(2);
	let newteam = Math.random().toString(36).substring(2);
	let password = Math.random().toString(36).substring(7);
	let session = "";
	let nonce = "";
	let team_nonce = "";
	let io = "";
	let sid = "";
	let counter = 0;
	group("Get vars", function() {

		let res = http.get(base_url,{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get status was 200': res => res.status === 200,
		})) {
			session = res.cookies['session'];
            if(res.body.includes('csrf_nonce')) {
                nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
            } else {
                nonce = res.body.split('csrfNonce\': "')[1].split('"')[0];
            }
			
		} else {
			fail("status code was *not* 200");
		}

	});

	group("Register User", function() {

		let res = http.get(base_url + "register",{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get register page status was 200': res => res.status === 200,
		})) {
			if(res.body.includes('csrf_nonce')) {
                nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
            } else {
                nonce = res.body.split('csrfNonce\': "')[1].split('"')[0];
            }
		} else {
			fail("get register page status was *not* 200");
		}

		res = http.post(base_url + 'register',
            {
                "name": username,
                "email": username + "@mail.com",
                "password": password,
                "nonce": nonce,
                "_submit": "Submit"
            },
            {
                "headers": {
                    "Host": host,
                    "Connection": "keep-alive",
                    "Upgrade-Insecure-Requests": "1",
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                    "Sec-Fetch-Dest": "document",
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                    "Sec-Fetch-Site": "none",
                    "Sec-Fetch-Mode": "navigate",
                    "Sec-Fetch-User": "?1",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "en-US,en;q=0.9"
                }
            }
		);
		if (!check(res, {
			'User registration status was 302': res => res.status === 302,
		})) {
			fail("status code was *not* 302");
		}

	});

	group("Register Team", function() {

		let res = http.get(base_url + "teams/new",{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get new team page status was 200': res => res.status === 200,
		})) {
			if(res.body.includes('csrf_nonce')) {
                team_nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
            } else {
                team_nonce = res.body.split('csrfNonce\': "')[1].split('"')[0];
            }
		} else {
			fail("get new team page status was *not* 200");
		}

		res = http.post(base_url + 'teams/new',
            {
                "name": newteam,
                "password": password,
                "nonce": team_nonce,
                "_submit": "Create"
            },
            {
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (!check(res, {
			'team registration status was 302': res => res.status === 302,
		})) {
			fail("team registration status *not* 302");
		}

	});
  let response;

  group("page_8 - http://127.0.0.1:8000/scoreboard", function () {
    response = http.get(base_url + "scoreboard", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-User": "?1",
        "Sec-Fetch-Dest": "document",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/fonts.min.css?d=7b8953da",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/main.min.css?d=7b8953da",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/core.min.css?d=7b8953da",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/vendor.bundle.min.js?d=7b8953da",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/core.min.js?d=7b8953da",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/helpers.min.js?d=7b8953da",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/pages/scoreboard.min.js?d=7b8953da",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/echarts.bundle.min.js?d=7b8953da",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-brands-400.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6uyw4BMUTPHjx4wXg.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwiPGQ.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/raleway/v19/1Ptug8zYS_SKggPNyC0ITw.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/img/favicon.ico?d=7b8953da",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(base_url + "api/v1/scoreboard/top/10", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
  });

  group("page_7 - http://127.0.0.1:8000/challenges", function () {
    response = http.get(base_url + "challenges", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-User": "?1",
        "Sec-Fetch-Dest": "document",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(
      base_url + "themes/core/static/css/fonts.min.css?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "themes/core/static/css/main.min.css?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "themes/core/static/css/core.min.css?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "themes/core/static/css/challenge-board.min.css?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "themes/core/static/js/vendor.bundle.min.js?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "themes/core/static/js/core.min.js?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "themes/core/static/js/helpers.min.js?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "themes/core/static/js/pages/challenges.min.js?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-brands-400.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6uyw4BMUTPHjx4wXg.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwiPGQ.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      "https://fonts.gstatic.com/s/raleway/v19/1Ptug8zYS_SKggPNyC0ITw.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "themes/core/static/img/favicon.ico?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(base_url + "api/v1/teams/me/solves", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "text/event-stream",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/challenges", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/teams/me/solves", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/challenges/1", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(
      base_url + "plugins/dynamic_challenges/assets/view.js?_=1613466567028",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Accept:
            "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          "X-Requested-With": "XMLHttpRequest",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "cors",
          "Sec-Fetch-Dest": "empty",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(
      base_url + "plugins/dynamic_challenges/assets/view.html",
      {
        headers: {
          Accept: "*/*",
          Referer: base_url + "challenges",
          "X-Requested-With": "XMLHttpRequest",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(base_url + "api/v1/challenges/1/solves", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(
      base_url + "themes/core/static/img/favicon.ico?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.post(
      base_url + "api/v1/challenges/attempt",
      '{"challenge_id":1,"submission":"sdfsdfa"}',
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Accept: "application/json",
          "CSRF-Token":
            team_nonce,
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          "Content-Type": "application/json",
          "Sec-GPC": "1",
          Origin: "http://127.0.0.1:8000",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "cors",
          "Sec-Fetch-Dest": "empty",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(base_url + "api/v1/challenges", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/teams/me/solves", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.post(
      base_url + "api/v1/challenges/attempt",
      '{"challenge_id":1,"submission":"cybears{t3st_fl4g_please_1gnor3}"}',
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          Accept: "application/json",
          "CSRF-Token":
            team_nonce,
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          "Content-Type": "application/json",
          "Sec-GPC": "1",
          Origin: "http://127.0.0.1:8000",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "cors",
          "Sec-Fetch-Dest": "empty",
          Referer: base_url + "challenges",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }

    response = http.get(base_url + "api/v1/challenges", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(base_url + "api/v1/teams/me/solves", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });

    response = http.get(
      base_url + "themes/core/static/img/favicon.ico?d=1add12e6",
      {
        headers: {
          Referer: base_url + "challenges",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
  });

  group("page_6 - http://127.0.0.1:8000/scoreboard", function () {
    response = http.get(base_url + "scoreboard", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-User": "?1",
        "Sec-Fetch-Dest": "document",
        Referer: base_url + "teams",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/core/static/css/fonts.min.css?d=7b8953da",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "style",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/main.min.css?d=7b8953da",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "style",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/core.min.css?d=7b8953da",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "text/css,*/*;q=0.1",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "style",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/vendor.bundle.min.js?d=7b8953da",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/core.min.js?d=7b8953da",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/helpers.min.js?d=7b8953da",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/pages/scoreboard.min.js?d=7b8953da",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/echarts.bundle.min.js?d=7b8953da",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-brands-400.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6uyw4BMUTPHjx4wXg.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwiPGQ.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/raleway/v19/1Ptug8zYS_SKggPNyC0ITw.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/img/favicon.ico?d=7b8953da",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "image",
          Referer: base_url + "scoreboard",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Referer: base_url + "scoreboard",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(base_url + "api/v1/scoreboard/top/10", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "application/json",
        "CSRF-Token":
          team_nonce,
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Content-Type": "application/json",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "text/event-stream",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "scoreboard",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
  });

  group("page_5 - http://127.0.0.1:8000/teams", function () {
    response = http.get(base_url + "teams", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-User": "?1",
        "Sec-Fetch-Dest": "document",
        Referer: base_url + "users",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/core/static/css/fonts.min.css?d=3b063c07",
      {
        headers: {
          Referer: base_url + "teams",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/main.min.css?d=3b063c07",
      {
        headers: {
          Referer: base_url + "teams",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/core.min.css?d=3b063c07",
      {
        headers: {
          Referer: base_url + "teams",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/vendor.bundle.min.js?d=3b063c07",
      {
        headers: {
          Referer: base_url + "teams",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/core.min.js?d=3b063c07",
      {
        headers: {
          Referer: base_url + "teams",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/helpers.min.js?d=3b063c07",
      {
        headers: {
          Referer: base_url + "teams",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/pages/main.min.js?d=3b063c07",
      {
        headers: {
          Referer: base_url + "teams",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-brands-400.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6uyw4BMUTPHjx4wXg.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwiPGQ.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/raleway/v19/1Ptug8zYS_SKggPNyC0ITw.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Referer: base_url + "teams",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/img/favicon.ico?d=3b063c07",
      {
        headers: {
          Referer: base_url + "teams",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "text/event-stream",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "teams",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
  });

  group("page_4 - http://127.0.0.1:8000/users", function () {
    response = http.get(base_url + "users", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-User": "?1",
        "Sec-Fetch-Dest": "document",
        Referer: base_url + "",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/core/static/css/fonts.min.css?d=b6c10089",
      {
        headers: {
          Referer: base_url + "users",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/main.min.css?d=b6c10089",
      {
        headers: {
          Referer: base_url + "users",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/core.min.css?d=b6c10089",
      {
        headers: {
          Referer: base_url + "users",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/vendor.bundle.min.js?d=b6c10089",
      {
        headers: {
          Referer: base_url + "users",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/core.min.js?d=b6c10089",
      {
        headers: {
          Referer: base_url + "users",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/helpers.min.js?d=b6c10089",
      {
        headers: {
          Referer: base_url + "users",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/pages/main.min.js?d=b6c10089",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "users",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-brands-400.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6uyw4BMUTPHjx4wXg.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwiPGQ.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/raleway/v19/1Ptug8zYS_SKggPNyC0ITw.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Referer: base_url + "users",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/img/favicon.ico?d=b6c10089",
      {
        headers: {
          Referer: base_url + "users",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "text/event-stream",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "users",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
  });

  group("page_3 - http://127.0.0.1:8000/", function () {
    response = http.get(base_url + "", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-User": "?1",
        "Sec-Fetch-Dest": "document",
        Referer: base_url + "challenges",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/core/static/css/fonts.min.css?d=1add12e6",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/main.min.css?d=1add12e6",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/css/core.min.css?d=1add12e6",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/img/logo.png",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept:
            "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "image",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/vendor.bundle.min.js?d=1add12e6",
      {
        headers: {
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Site": "same-origin",
          "Accept-Encoding": "gzip, deflate, br",
          Host: host,
          "Accept-Language": "en-US,en;q=0.9",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "challenges",
          Connection: "keep-alive",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/core.min.js?d=1add12e6",
      {
        headers: {
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Site": "same-origin",
          "Accept-Encoding": "gzip, deflate, br",
          Host: host,
          "Accept-Language": "en-US,en;q=0.9",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "challenges",
          Connection: "keep-alive",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/helpers.min.js?d=1add12e6",
      {
        headers: {
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Site": "same-origin",
          "Accept-Encoding": "gzip, deflate, br",
          Host: host,
          "Accept-Language": "en-US,en;q=0.9",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "challenges",
          Connection: "keep-alive",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/js/pages/main.min.js?d=1add12e6",
      {
        headers: {
          Host: host,
          Connection: "keep-alive",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          Accept: "*/*",
          "Sec-GPC": "1",
          "Sec-Fetch-Site": "same-origin",
          "Sec-Fetch-Mode": "no-cors",
          "Sec-Fetch-Dest": "script",
          Referer: base_url + "",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-solid-900.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6uyw4BMUTPHjx4wXg.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwiPGQ.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://fonts.gstatic.com/s/raleway/v19/1Ptug8zYS_SKggPNyC0ITw.woff2",
      {
        headers: {
          Origin: "http://127.0.0.1:8000",
          Referer: "https://fonts.googleapis.com/",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      "https://use.fontawesome.com/releases/v5.9.0/webfonts/fa-brands-400.woff2",
      {
        headers: {
          origin: "http://127.0.0.1:8000",
          "user-agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
          accept: "*/*",
          "sec-gpc": "1",
          "sec-fetch-site": "cross-site",
          "sec-fetch-mode": "cors",
          "sec-fetch-dest": "font",
          referer: "https://use.fontawesome.com/releases/v5.9.0/css/all.css",
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en-US,en;q=0.9",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(
      base_url + "themes/core/static/sounds/notification.webm",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
    response = http.get(base_url + "events", {
      headers: {
        Host: host,
        Connection: "keep-alive",
        Accept: "text/event-stream",
        "Cache-Control": "no-cache",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        "Sec-GPC": "1",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Dest": "empty",
        Referer: base_url + "",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
      },
    });
    response = http.get(
      base_url + "themes/core/static/img/favicon.ico?d=1add12e6",
      {
        headers: {
          Referer: base_url + "",
          "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
        },
      }
    );
 if (
    !check(response, {
      'status code MUST be 200': (res) => res.status == 200,
    })
  ) {
      console.log(response.body)
    fail('status code was *not* 200');
  }
  });

  // Automatically added sleep
  sleep(1);
}
