terraform {
  required_version = ">= 0.14.4"
}
provider "kubernetes" {
  config_path = "/etc/rancher/k3s/k3s.yaml"
  host        = "https://127.1.1.1:6443"
}

provider "helm" {
  kubernetes {
    config_path = "/etc/rancher/k3s/k3s.yaml"
    host        = "https://127.1.1.1:6443"
  }
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

locals {
  service = <<EOT
    type: LoadBalancer
    port: 8000
    targetPort: 3000
  EOT
}
module "prometheus" {
  source    = "../prometheus_stack"
  namespace = kubernetes_namespace.monitoring.metadata[0].name
  service   = local.service
}
