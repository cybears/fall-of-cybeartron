output "grafana_password" {
  value     = module.prometheus.grafana_password
  sensitive = false
}