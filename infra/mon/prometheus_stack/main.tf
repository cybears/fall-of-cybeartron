resource "random_password" "grafana_password" {
  length  = 32
  special = true
}

output "grafana_password" {
  value     = random_password.grafana_password.result
  sensitive = false
}

resource "helm_release" "kube-prometheus-stack" {
  name       = "mon"
  chart      = "kube-prometheus-stack"
  repository = "https://prometheus-community.github.io/helm-charts"
  namespace  = var.namespace

  values = [
    templatefile("${path.module}/values.yml", { CYBEARS_JSON = indent(10, file("${path.module}/cybears.json")),
      ADMIN_PASSWORD                                         = random_password.grafana_password.result,
      NAMESPACE                                              = var.namespace,
      ANNOTATIONS                                            = var.annotations,
      SERVICE                                                = var.service,
      SERVICE_ANNOTATIONS                                    = var.service_annotations,
      EXTRA_PATHS                                            = var.extra_paths,
    HOST = var.host })
  ]
}