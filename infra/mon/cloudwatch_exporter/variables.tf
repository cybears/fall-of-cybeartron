
variable "aws_secret_access_key" {
  type        = string
  description = "AWS_SECRET_ACCESS_KEY for authentication to cloudwatch api"
}

variable "aws_access_key_id" {
  type        = string
  description = "AWS_ACCESS_KEY_ID for authentication to cloudwatch api"
}

variable "namespace" {
  type        = string
  description = "Kubernetes namespace to deploy into"
  default     = "default"
}