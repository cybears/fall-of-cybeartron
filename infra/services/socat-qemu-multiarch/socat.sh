#!/usr/bin/env bash
SOCAT_TCPLFLAGS=",nodelay,reuseaddr,fork,${SOCAT_FLAGS}"
SOCAT_EXECFLAGS=",stderr,${SOCAT_EXECFLAGS}"

# Work out if the `noob` (or otherwise named) user actually exists
if id "${SANDBOX_USER:=noob}" &>/dev/null; then
    SOCAT_TCPLFLAGS="${SOCAT_TCPLFLAGS},su=${SANDBOX_USER}"
else
    echo "WARNING: USER ${SANDBOX_USER} NOT PRESENT. Running as ${USER} instead"
fi

# Actually run the socat loop
while true; do
    socat -d                                                                \
        "TCP-LISTEN:${SOCAT_TCPLPORT:-2323},${SOCAT_TCPLFLAGS}"             \
        "EXEC:${BINARY},${SOCAT_EXECFLAGS}"
done
