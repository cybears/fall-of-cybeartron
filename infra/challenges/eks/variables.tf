variable "aws_region" {
  default = "ap-southeast-2"
}

variable "cloud_provider" {
  default = "aws"
}

variable "challenge_eks_cluster_name" {
  description = "challenges eks cluster name"
}

variable "vpc_cidr_block" {
  description = "The top-level CIDR block for the VPC."
  default     = "10.0.0.0/16"
}


# eks autoscaling
variable "eks_autoscaling_group_min_size" {
  default = 1
}

variable "eks_autoscaling_group_desired_capacity" {
  default = 1
}

variable "eks_autoscaling_group_max_size" {
  default = 2
}

variable "eks_instance_type" {
  default = "m5.large"
}

# eks autoscaling for windows
variable "eks_autoscaling_group_windows_min_size" {
  description = "Minimum number of Windows nodes for the EKS. Set to 0 for no WIndows support"
}

variable "eks_autoscaling_group_windows_desired_capacity" {
  description = "Desired capacity for Windows nodes for the EKS. Set to 0 for no WIndows support"
}

variable "eks_autoscaling_group_windows_max_size" {
  description = "Maximum number of Windows nodes for the EKS. Set to 0 for no WIndows support"
}

variable "eks_users" {
  description = "Additional AWS users to add to the EKS aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = []
}
