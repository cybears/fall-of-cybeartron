# policy for external-dns to be able to set records.
#tfsec:ignore:aws-iam-no-policy-wildcards
data "aws_iam_policy_document" "route53_change_records" {
  statement {
    actions = [
      "route53:ChangeResourceRecordSets",
    ]
    resources = [
      "arn:aws:route53:::hostedzone/*"
    ]
  }
  statement {
    actions = [
      "route53:ListHostedZones",
      "route53:ListResourceRecordSets"
    ]

    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "route53_change_records" {
  name   = "route53_change_records"
  path   = "/"
  policy = data.aws_iam_policy_document.route53_change_records.json
}


resource "aws_iam_role_policy_attachment" "external_dns_route53_change_records" {
  policy_arn = aws_iam_policy.route53_change_records.arn
  role       = data.aws_iam_role.worker_role.name
}

# policy for cluster-ausoscaler to be able to scale new worker nodes.
resource "aws_iam_role_policy_attachment" "worker_role_cluster_autoscaler" {
  policy_arn = aws_iam_policy.cluster_autoscaler.arn
  role       = data.aws_iam_role.worker_role.name
}

resource "aws_iam_policy" "cluster_autoscaler" {
  name_prefix = "cluster-autoscaler"
  description = "EKS cluster-autoscaler policy for cluster ${module.eks.cluster_id}"
  policy      = data.aws_iam_policy_document.cluster_autoscaler.json
}

#tfsec:ignore:aws-iam-no-policy-wildcards
data "aws_iam_policy_document" "cluster_autoscaler" {
  statement {
    sid    = "clusterAutoscalerAll"
    effect = "Allow"

    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:DescribeTags",
      "ec2:DescribeLaunchTemplateVersions",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "clusterAutoscalerOwn"
    effect = "Allow"

    actions = [
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
      "autoscaling:UpdateAutoScalingGroup",
    ]

    resources = ["*"]

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/kubernetes.io/cluster/${module.eks.cluster_id}"
      values   = ["owned"]
    }

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/k8s.io/cluster-autoscaler/enabled"
      values   = ["true"]
    }
  }
}

# policy for cloudwatch_exporter to be able to get metrics.
#tfsec:ignore:aws-iam-no-policy-wildcards
data "aws_iam_policy_document" "cloudwatch_exporter" {
  statement {
    actions = [
      "cloudwatch:ListMetrics",
      "cloudwatch:GetMetricStatistics",
      "tag:GetResources"
    ]

    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "cloudwatch_exporter" {
  name   = "cloudwatch_exporter"
  path   = "/"
  policy = data.aws_iam_policy_document.cloudwatch_exporter.json
}

resource "aws_iam_role_policy_attachment" "worker_role_cloudwatch_exporter" {
  policy_arn = aws_iam_policy.cloudwatch_exporter.arn
  role       = data.aws_iam_role.worker_role.name
}