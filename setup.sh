#!/bin/bash

# If we are a root in a Docker container and `sudo` doesn't exist
# lets overwrite it with a function that just executes things passed to sudo
# (yeah it won't work for sudo executed with flags)
if [ -f /.dockerenv ] && ! hash sudo 2>/dev/null && whoami | grep root; then
    sudo() {
        $*
    }
fi

# Helper functions
linux() {
    uname | grep -i Linux &>/dev/null
}

install_k3s() {
  if ! [ -x "$(command -v k3s)" ]; then
    curl -sfL https://get.k3s.io | sudo sh -
  fi 
}

install_terraform() {
  [[ -f /usr/local/bin/terraform ]] && echo "`/usr/local/bin/terraform version` already installed" && return 0
  LATEST_URL=$(curl https://releases.hashicorp.com/terraform/index.json | jq -r '.versions[].builds[].url | select(.|test("alpha|beta|rc")|not) | select(.|contains("linux_amd64"))' | sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | tail -1)
  curl ${LATEST_URL} > /tmp/terraform.zip
  (cd /tmp && unzip /tmp/terraform.zip && chmod +x /tmp/terraform && sudo mv /tmp/terraform /usr/local/bin/)
}

install_kubectl() {
    sudo snap install kubectl --classic
}

install_prereq_apt() {
    sudo apt-get update || true
    sudo apt-get -y install git python3 python3-dev python3-pip jq openssl unzip
}

install_aws() {
    [[ -f /usr/local/bin/aws ]] && echo "`/usr/local/bin/aws --version` already installed" && return 0
    curl -o /tmp/awscliv2.zip "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"
    (cd /tmp && unzip /tmp/awscliv2.zip && sudo ./aws/install --update && rm -rf /tmp/aws*)
}

setup_ctf_secret_key() {
    [[ -f infra/ctfd/terraform/.ctfd_secret_key ]] && echo "infra/ctfd/terraform/.ctfd_secret_key already set" && return 0
    python3 -c "import os; f=open('infra/ctfd/terraform/.ctfd_secret_key', 'ab+'); f.write(os.urandom(64)); f.close()"
}

python_requirements() {
    sudo python3 -m pip install -r requirements-infra.txt
}

setup_aws_credentials() {
    [[ -f ~/.aws/credentials ]] && echo "~/.aws/credentials already set" && return 0
    aws configure
}

setup_aws_session_manger() {
    if session-manager-plugin &> /dev/null
    then
        echo "session-manager-plugin already installed" && return 0
    else
        curl -o "/tmp/session-manager-plugin.deb" "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb"
        (cd /tmp/ && sudo dpkg -i session-manager-plugin.deb && rm /tmp/session-manager-plugin.deb)
    fi
}


if linux; then
    distro=$(grep "^ID=" /etc/os-release | cut -d'=' -f2 | sed -e 's/"//g')

    case $distro in
        "ubuntu")
            echo "This script installs a bunch of programs from the internet. Please choose wisely if you trust this."
            read -r -p "Are you sure you want to continue? [Y/n] " response
            response=${response,,} # tolower
            if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then  
                install_prereq_apt
                install_k3s
                install_kubectl
                install_terraform
                setup_ctf_secret_key
                python_requirements
                install_aws
                setup_aws_credentials
                setup_aws_session_manger
            fi
            ;;
     *) # we can add more install command for each distros.
            echo "\"$distro\" is not supported distro."
            ;;
    esac
else
    echo "Only linux is supported"
fi   