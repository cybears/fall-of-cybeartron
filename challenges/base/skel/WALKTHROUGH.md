# <name> walkthrough

A general synopsis of the challenge. What is the goal? What is the central
theme/idea of the challenge?

## Hints

A list of hints might be nice. This helps game masters on the day decide what
might be an approriate hint if many people are having issues with a challenge.

1. First hint
<details>
<summary>Spoiler warning</summary>
Check out the following hint!

```bash
sudo -s
```
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>
Have you tried reading the flag?

```bash
cat /flag
```
</details>

## Steps

A guide through the challenge in as much detail as you want. Feel free to
include images and diagrams or source snippets.

If you want to make diagrams you can embed images or check out
[Mermaid](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts-using-mermaid).
In general [Gitlab markdown](https://docs.gitlab.com/ee/user/markdown.html) has
a lot of cool features that can help you out here.

Also try using collapsing sections and spoiler warnings, though generally
people are here for spoilers.

<details>
<summary>Spoiler warning</summary>

Spoiler text. Note that it's important to have a space after the summary tag.
You should be able to write any markdown you want inside the `<details>` tag...
just make sure you close `<details>` afterward.

```javascript
console.log("I'm a code block!");
```
</details>

It's useful to think about the logical conclusions people should draw from each
step.  What information do they get and how it should help them reach the next
step. Writing this should help you refine your challenge and reveal logical
leaps.

## Sources

Put links to any websites, papers, books, videos, etc that are useful
background reading for the challenge.  Maybe some real world uses of an attack
class, or links to how to exploit the flaw you're playing with?

- [A cool link](https://trailofbits.github.io/ctf/vulnerabilities/source.html)
