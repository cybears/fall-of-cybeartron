---
include:
    - "/.gitlab/ci/base.yml"

variables:
    CHALLENGE_IMAGE_TAG: "<category>-<name>"
    BUILD_IMAGE_TAG: "<category>-<name>-builder"
    HEALTHCHECK_IMAGE_TAG: "<category>-<name>-healthcheck"
    CHALLENGE_HOST: "<name>.ctf.cybears.io"
    CHALLENGE_PORT: "2323"

# Build the container that will compile the challenge
builder:<category>/<name>:
    extends:
        - .build_container_template
    stage: builder
    variables:
        DOCKERFILE_RELPATH: "Dockerfile.builder"
        TAG: ${BUILD_IMAGE_TAG}
    needs: []

# Use the container we built above to compile the challenge
compile:<category>/<name>:
    extends:
        # This moves into the directory this file is in and
        # exposes the CBCI_JOB_PATH variable which we can use
        # in the artifacts definition below.
        - .compile_template
    image: ${CI_REGISTRY_IMAGE}/${BUILD_IMAGE_TAG}
    artifacts:
        # We'll just scoop everything that isn't tracked by git
        # this is the most compatible, but the slowest option.
        # Using this means we'll spend a while zipping, uploading
        # in this job, then downloading and unzipping in any job
        # that `needs` this job amd its artifacts
        untracked: true

    script:
        # One of these should work!
        - ${CHALLENGE_MANAGER} --local build || make build || ./build.py || ./build.sh
    needs:
        - job: builder:<category>/<name>
          artifacts: false

# Take the compiled output from the compile stage and put it into a container
# that will run the challenge
contain:<category>/<name>:
    extends:
        - .build_container_template
    variables:
        TAG: ${CHALLENGE_IMAGE_TAG}
        DOCKERFILE_RELPATH: "Dockerfile.challenge"
    needs:
        - job: compile:<category>/<name>
          artifacts: true

# Build a container to solve the challenge
contain:<category>/<name>-healthcheck:
    extends:
        - .build_container_template
    variables:
        TAG: "${HEALTHCHECK_IMAGE_TAG}"
        DOCKERFILE_RELPATH: Dockerfile.healthcheck
    needs:
        - job: compile:<category>/<name>
          artifacts: true

# Spin up the challenge container and the healthcheck container
# and test the challenge
test:<category>/<name>:
    image: ${CI_REGISTRY_IMAGE}/${HEALTHCHECK_IMAGE_TAG}
    stage: test
    extends:
        - .base_job
        - .cd_here
        # If you're having trouble with your CI
        # you can extend .ci-debug to print some
        # verbose information on env vars and other
        # things.
        # - .ci-debug
    needs:
        - job: contain:<category>/<name>
          artifacts: false
        # The compile clause should only be needed if you do
        # a local test of your challenge/handouts
        - job: compile:<category>/<name>
          artifacts: true
        - job: contain:<category>/<name>-healthcheck
          artifacts: false
    services:
        - name: ${CI_REGISTRY_IMAGE}/${CHALLENGE_IMAGE_TAG}
          alias: ${CHALLENGE_HOST}
    variables:
        TERM: 'xterm'
        TERMINFO: '/etc/terminfo'
    script:
        # One of the below should work if you use the skeleton
        # The skeleton uses the CHALLENGE_HOST and CHALLENGE_PORT
        # environment variables to select where to point the solver
        - ${CHALLENGE_MANAGER} --local test || make test || ./solve.py || ./solve.sh || ./doit.py || ./doit.sh
