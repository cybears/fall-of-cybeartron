# Certs

This is a standalone challenge where players need to parse an X.509 format. The flag is in the subject field of the attached certificate and it is base64 encoded.

## Hints
1. Hint
<details>
Cyberchef can parse X.509 certificates
</details>

## Steps - Official Walkthrough

<details>
<summary>Spoiler warning</summary>

1. Cyberchef
Cyberchef (https://gchq.github.io/CyberChef) has a recipe called "Parse X.509 Certificates". Pasting the text version of the certificate in will break out the fields.

The CN (Common Name) component of the subject field has a base64 blob in it. Copy and paste this into a new Cyberchef window and apply the base-64 decode option to get the flag

2. Openssl
`openssl x509 -in handout/CybearsTest.crt -text -noout | grep Issuer | cut -d"=" -f 6 | tr -d " " | base64 -d`
This line will use the openssl x509 tool to output the text fields, it will then look for a line that has "Issuer" in it, split the field on the delimiter `=` and then base64 decode the appropriate field.

</details>

