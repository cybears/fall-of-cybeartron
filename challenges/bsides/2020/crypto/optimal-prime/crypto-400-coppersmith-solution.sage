# run as
# sage crypto-400-coppersmith-solution.sage

import json
import Crypto.Util.number as nt

with open("./out.json", "r") as f:
    d = f.read()

j = json.loads(d)

p_approx = nt.bytes_to_long("cybearscybearscybearscybearscybearscybearscybearscybearscybearsc")
q_approx = nt.bytes_to_long("decepticomtssdecepticomtssdecepticomtssdecepticomtssdecepticomts")

message1 = nt.bytes_to_long("Theres a thin line between being a hero and being a memory")
message2 = nt.bytes_to_long("Bah weep gragnah weep nini bong")
message3 = nt.bytes_to_long("Even the wisest of men and machines can be in error")

e = 65537

## Recover N

mm1 = pow(message1, e) - j['cipher1']
mm2 = pow(message2, e) - j['cipher2']
mm3 = pow(message3, e) - j['cipher3']

put_n1 = gcd(mm1, mm2)

if pow(message1, e, put_n1) == j['cipher1']:
    n = put_n1
    print("Found n (first): " + str(n))
else:
    put_n2 = gcd(mm2, mm3)
    n = gcd(put_n1, put_n2)
    print("Found n (second): " + str(n))

## Find P
F.<x>=PolynomialRing(Zmod(n), implementation='NTL')
#check whether p is greater than, or less than p_approx - check both
r = (p_approx + x).small_roots(2**160, 0.4)[0] #creates an integer too large - would need to look at negative mod N?
p = p_approx + r
print("r: " + str(r))
print("p: " + str(p))
if r > 2**170:
    print("r > 2**170")

    #do this instead
    r = (x-p_approx ).small_roots(2**160, 0.4)[0]
    p = p_approx - r
    print("r: " + str(r))
    print("p: " + str(p))

if (n % p) == 0:
    print("n mod p is zero")
    q = n//int(p)
    print("q: " + str(q))
    phi = (p-1)*(q-1)
    d = inverse_mod(e, phi)
    flag = pow(j['enc_flag'], d, n)

    print("SUCCESS! Found flag: " + str(nt.long_to_bytes(flag)))

    exit(0)

else:
    print("Problem finding p")
    exit(-1)






