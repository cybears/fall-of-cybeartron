#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <string.h>

#include <openssl/rand.h>
#include <openssl/evp.h>

#define FLAG_LEN 84
#define PRIME_LEN 512/8
#define SHA256_LEN 256/8
#define SHA1_LEN 160/8
#define RET_ERROR -1
#define RET_OK 0

// use as
// DEBUG_PRINT(("var1: %d; var2: %d; str: %s\n", var1, var2, str));

#ifdef DEBUG
# define DEBUG_PRINT(x) printf x
# define GMP_DEBUG_PRINT(x) gmp_printf x
#else
# define DEBUG_PRINT(x) do {} while (0)
#endif


/*
   gcc -I/anaconda/pkgs/openssl-1.0.1h-1/include -L/usr/local/Cellar/openssl/1.0.2q/lib ./genkey.c -lgmp -lssl -lcrypto -DDEBUG && ./a.out
   */

int encrypt(mpz_t rop, mpz_t plain, mpz_t e, mpz_t n)
{
    mpz_powm(rop, plain, e, n); // r = plain^e mod n
    return RET_OK;
}

/*
   generatePrime()
   - in: bitlen - bitlength of primes
   - out: out - mpz to store prime in
   - in: inbuf - mpz to use as base number to start of random prime generation

   This prime generation routine will take a base number and then generate and xor a random low 160-bits to create a prime number
   */

int generatePrime(int bitlen, mpz_t out, mpz_t inbuf)
{
    int ret=RET_OK;
    unsigned char randbuf[PRIME_LEN]={0};
    mpz_t randx;

    EVP_MD_CTX *mdctx;
    const EVP_MD *md;
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;

    ret = RAND_bytes(randbuf, PRIME_LEN);

    md = (EVP_MD *)EVP_sha1();
    mdctx = EVP_MD_CTX_create();

    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, randbuf, PRIME_LEN);
    EVP_DigestFinal_ex(mdctx, md_value, &md_len);
    EVP_MD_CTX_destroy(mdctx);

    mpz_init(randx);

    mpz_import(randx,SHA1_LEN, 1/*most sign word first*/, sizeof(char), 1 /*Most Sig Byte First*/, 0 /*full words*/, (void *)md_value);

    //check if inbuf is NULL
    if (NULL == inbuf)
    {
        ret = RET_ERROR;
        return ret;
    }

    //XOR buffer to rand
    mpz_xor(inbuf, inbuf, randx);
    mpz_nextprime(out, inbuf);

    return ret;
}

int main()
{

    mpz_t  r, n, e, plain1, plain2, plain3, p, q;
    mpz_t i_cybears, i_decepticomtss;
    mpz_t i_flag;

    char * filename = "flag.txt";
    FILE *fp;
    char flag[100] = {0};

    char *outfile = "out.json";
    FILE *out;

    unsigned char randbuf[PRIME_LEN]={0};
    int ret=RET_OK,i;

    // Initialise all needed gmp integers
    mpz_init(r);
    mpz_init(p);
    mpz_init(q);
    mpz_init(n);
    mpz_init_set_ui(e, 65537);

    mpz_init(i_cybears);
    mpz_init(i_decepticomtss);

    EVP_MD_CTX *mdctx;
    const EVP_MD *md;
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;


    char *cybears = "cybearscybearscybearscybearscybearscybearscybearscybearscybearsc";
    char *decepticomtss = "decepticomtssdecepticomtssdecepticomtssdecepticomtssdecepticomts";

    char * message1 = "Theres a thin line between being a hero and being a memory";
    char * message2 = "Bah weep gragnah weep nini bong";
    char * message3 = "Even the wisest of men and machines can be in error";

    //open flag txt file and read in string -> encrypt this string and output
    fp = fopen(filename, "r");
    if (NULL == fp)
    {
        printf("ERROR: flag file not found\n");
        return RET_ERROR;
    }
    fread(flag, sizeof(char), FLAG_LEN*sizeof(char), fp);
    fclose(fp);

    out = fopen(outfile, "w");

    //Load the cybears and decepticomtss strings and load them into gmp integers
    mpz_import(i_cybears, strlen(cybears), 1/*most sign word first*/, sizeof(char), 1 /*Most Sig Byte First*/, 0 /*full words*/, (void *)cybears);
    mpz_import(i_decepticomtss, strlen(decepticomtss), 1/*most sign word first*/, sizeof(char), 1 /*Most Sig Byte First*/, 0 /*full words*/, (void *)decepticomtss);

    gmp_printf ("p_approx = %Zd\n", i_cybears);
    gmp_printf ("q_approx = %Zd\n", i_decepticomtss);

    //Generating first prime
    DEBUG_PRINT(("#GENERATING PRIME 1\n"));
    generatePrime(PRIME_LEN, p, i_cybears);
    gmp_printf ("#generatePrime(cybears):\np = %Zd\n", p);

    //Generating second prime
    DEBUG_PRINT(("#GENERATING PRIME 2\n"));
    generatePrime(PRIME_LEN, q, i_decepticomtss);
    gmp_printf ("#generatePrime(dtss):\nq = %Zd\n", q);

    //Initialise gmp integers for plain text
    mpz_init(plain1);
    mpz_import(plain1, strlen(message1), 1/*most sign word first*/, sizeof(char), 1 /*Most Sig Byte First*/, 0 /*full words*/, (void *)message1);
    mpz_init(plain2);
    mpz_import(plain2, strlen(message2), 1/*most sign word first*/, sizeof(char), 1 /*Most Sig Byte First*/, 0 /*full words*/, (void *)message2);
    mpz_init(plain3);
    mpz_import(plain3, strlen(message3), 1/*most sign word first*/, sizeof(char), 1 /*Most Sig Byte First*/, 0 /*full words*/, (void *)message3);

    //Generate RSA modulus
    mpz_mul(n, p, q);
    gmp_printf("n = 0x%Zx\n", n);

    //printf("F.<x>=PolynomialRing(Zmod(n), implementation='NTL')\n");

    DEBUG_PRINT(("#Encrypted message\n"));
    gmp_fprintf(out, "{\n");

    //Encrypt plaintext 1
    gmp_printf("plain1 = %Zd\n", plain1);
    encrypt(r, plain1, e, n); // r = plain^e mod n
    gmp_printf ("cipher1 = 0x%Zx\n", r);
    gmp_fprintf(out, "\t\"cipher1\" : %Zd,\n", r);

    //Encrypt plaintext 2
    gmp_printf("plain2 = %Zd\n", plain2);
    encrypt(r, plain2, e, n); // r = plain^e mod n
    gmp_printf ("cipher2 = 0x%Zx\n", r);
    gmp_fprintf(out, "\t\"cipher2\" : %Zd,\n", r);

    //Encrypt plaintext 3
    gmp_printf("plain3 = %Zd\n", plain3);
    encrypt(r, plain3, e, n); // r = plain^e mod n
    gmp_printf ("cipher3 = 0x%Zx\n", r);
    gmp_fprintf(out, "\t\"cipher3\" : %Zd,\n", r);

    //Encrypt flag
    mpz_init(i_flag);
    mpz_import(i_flag, FLAG_LEN, 1, sizeof(char), 1, 0, (void *)flag);
    gmp_printf("i_flag = %Zd\n", i_flag);
    encrypt(r, i_flag, e, n);
    gmp_printf("enc_flag = 0x%Zx\n", r);
    gmp_fprintf(out, "\t\"enc_flag\" : %Zd\n", r);

    gmp_fprintf(out, "}\n");
    fclose(out);

    return 0;
}
