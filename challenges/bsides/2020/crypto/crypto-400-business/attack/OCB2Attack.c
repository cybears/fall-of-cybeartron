/* jacekn@react0r.com */

/* Breaking OCB2 ISO/IEC 19772:2009 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include "ocb.h"
#include "rijndael-alg-fst.h"

#define EXPORT __attribute__((visibility("default")))

block K;

int EXPORT DecryptionOracle(byte *C, byte *T, byte *N, byte *A, uint32_t lenA, byte *M, uint32_t n) {

 printf("Decryption Oracle\n");
 
 ocb_state* state = ocb_init((byte *)K,16,16,AES128);
 ocb_provide_header(state,A,lenA); 
 int res = ocb_decrypt(state,N,C,n,T,M);
 ocb_zeroize(state);

return res;
}

void EXPORT EncryptionOracle(byte *C, byte *T, byte *N, byte *A, uint32_t lenA, byte *M, uint32_t n) { 

 printf("Encryption Oracle\n");

 ocb_state *state = ocb_init((byte *)K,16,16,AES128); 
 ocb_provide_header(state,A,lenA);
 ocb_encrypt(state,N,M,n,C,T);
}

void EXPORT RandomBytes(uint8_t *buf, uint64_t n) {

 uint8_t byte;

 FILE *fd = fopen("/dev/urandom", "r");

 uint8_t k;
 for(k=0;k<n;k=k+1) {
  fread(&byte, sizeof(byte), 1, fd);
  buf[k] = byte;
 }
 
 fclose(fd);
}

int EXPORT InoueMinematsuMinimalAttack(byte *C, byte *Mp) {
 
 printf("Inoue Minematsu Minimal Attack\n");

 /* 1. Fix any N != N* and M[2] in {0,1}^n and let M = (len(0^n) || M[2]) */

 block N;
 RandomBytes(N,16);

 block len; memset(len,0,16);
 len[15] = 16*8; // 128 
 byte M[32]; memset(M,0,32);
 memcpy(M,len,16);

 /* 2. Make an encryption query C  = E(N,e,M) */
 
 pbuf(N,16, "N"); 
 pbuf(M,32, "M"); 
 block T,Tp;
 byte A[0]; //empty
 EncryptionOracle(C,T,N,A,0,M,32); 
 pbuf(C,32, "C"); 
 pbuf(T,16, "T"); 
 
 /* 3. Let C' = (C[1] + len(0^n) || M[2] + C[2]) */

 byte Cp[16]; memset(Cp,0,16);
 memcpy(Cp,C,16);
 memcpy(C,C,32);
 xor_block(Cp,Cp,len);
 pbuf(N,16,"N'");
 pbuf(Cp,16,"C'");

 memcpy(Tp,M+16,16);
 xor_block(Tp,Tp,C+16);
 pbuf(Tp,16,"T'");

 /* 4. Make a decryption query M' = D(N,e,C') */
 
 int res = DecryptionOracle(Cp,Tp,N,A,0,Mp,16);

return res;
}

void GF2128DivideByTwo(block A, block B) { // B = A*2^{-1} 
 
 uint8_t bottom_right = B[15] & 1; /* low bit of A */
 
 A[0] = (B[0] >> 1) ^ (bottom_right * 0x80);

 uint8_t i;
 for (i = 0; i < 15; i++) {
   A[i+1] = (B[i+1] >> 1) | (B[i] << 7);
 }

 if(bottom_right) {
  A[15] ^= 0x43; /* mod 2^{-1} = 1100010 || 0^120 || 1 */
 }
}

int EXPORT IwataPlaintextRecoveryAdversary(byte *Ns, byte *As, uint32_t lenA, byte *Cs, byte *Ts) {

 printf("Iwata Plaintext Recovery Attack\n");

 /* 0. To recover L* = E(N*) perform Inoue-Minematsu Minimal Attack as per [5, §4.1] */

 byte Mp[16]; memset(Mp,0,16);
 byte Cp[32]; memset(Cp,0,32);

 InoueMinematsuMinimalAttack(Cp,Mp);

 pbuf(Mp,16,"M'"); 
 pbuf(Cp,32,"C'"); 

 /* 4. Obtain L = (M' + len(0^n)) / 2 */ 

 printf("Recover L\n");

 block len; memset(len,0,16);
 len[15] = 16 * 8; // 128
 xor_block(Mp,Mp,len);

 block L; memset(L,0,16); 
 GF2128DivideByTwo(L,Mp);

 /* 5. For i in {1,...,m-1} define (X[i],Y[i]) in {{0,1\}^n}^2 such that E(X[i]) = Y[i]\) as follows: */

 byte Cp1[16];
 byte Cp2[16];
 memcpy(Cp1,Cp,16);
 memcpy(Cp2,Cp+16,16);

 /* (X[i],Y[i])  = (M[i] + 2^i*L, C[i] + 2^i*L) */

 /* (X[m],Y[m])  = (len(M[m]) + 2^m*L, Pad) */

 block X1,Y1,_2L,_4L; 

 two_times(_2L,L);
 two_times(_4L,_2L);

 xor_block(X1,_2L,len); // M[1] = len(0^n); 
 xor_block(Y1,_2L,Cp1); // C[1] =    

 block Xm,Ym;
 xor_block(Xm,len,_4L);
 memcpy(Ym,Cp2,16);

 /* 6. Let (N'', L'') be one of m derived internal input-output pairs (X[i],Y[i]) */

 block Npp,Lpp;
 memcpy(Npp,Xm,16);
 memcpy(Lpp,Ym,16);
 pbuf(Npp,16,"N''"); 
 pbuf(Lpp,16,"L''");
 
 /* 7. Fix any A'' and M''[2] in {0,1}^n and let M'' = (N* + 2L'' || M''[2]) */

// byte App[0]; // empty
 byte *App; // 
 App = (byte*) malloc(lenA);

 byte Mpp[32]; memset(Mpp,0,32);
 byte Mpp1[16]; memset(Mpp1,0,16);
 
 block _2Lpp;
 two_times(_2Lpp,Lpp); 
 xor_block(Mpp1,Ns,_2Lpp);
 memcpy(Mpp,Mpp1,16);
 pbuf(Mpp,32,"M''");

 block Tpp;
 byte Cpp[32]; memset(Cpp,0,32);

 /* 8. Make an encryption query (C'' = {E(N'',A'',M'') */

 EncryptionOracle(Cpp,Tpp,Npp,App,lenA, Mpp,32); 

 printf("Recover L*\n");	

 pbuf(Cpp,32,"C''");
 block Cpp1;
 memcpy(Cpp1,Cpp,16);

 /* 9. Let L* be (C''[1] + 2L'\) */

 block Ls;
 xor_block(Ls,Cpp1,_2Lpp);
 pbuf(Ls,16,"L*");

 /* 10. Modify C* as per [5, § 4.3] by fixing indices j,k in {1,...,m*-1} such that C^*[j] != C*[k] */

 block Cs1,Cs2,Csm; //j,k,i

 memcpy(Cs1,Cs,16); 
 memcpy(Cs2,Cs+16,16); 
 memcpy(Csm,Cs+32,16); 
 pbuf(Cs1,16,"C*[1]"); pbuf(Cs2,16,"C*[2]"); pbuf(Csm,16,"C*[3]");

 /* 12. Define C$ = (C$[1],...,C$[m*]) as follows: */

  /* C$[i]  = C*[i] for i in {1,...,m*} \ {j,k} */

  pbuf(Csm,16,"C$[i]");
 
  /* C$[j] = C*[k] + 2^kL* + 2^jL* */

  block _2Ls,_4Ls;
  two_times(_2Ls,Ls); 
  two_times(_4Ls,_2Ls); 
 
  block C$j, C$k; 
  block tmp; // 2^kL* + 2^jL* 
  xor_block(tmp,_4Ls,_2Ls);
  xor_block(C$j,Cs2,tmp); 
  pbuf(C$j,16,"C$[j]");

  /* C$[k] = C*[j] = 2^kL* + 2^jL* */
 
  xor_block(C$k,Cs1,tmp); 
  pbuf(C$k,16,"C$[k]");

  byte C$[48]; memset(C$,0,48);
  byte M$[48]; memset(M$,0,48);

  memcpy(C$,C$j,16);
  memcpy(C$+16,C$k,16);
  memcpy(C$+32,Csm,16);

  pbuf(C$,48,"C$");

 /* 13. Make a decryption query M$ = D(N*,A*,C$,T*) */
 
 int a = DecryptionOracle(C$,Ts,Ns,As,lenA, M$,48); 

 pbuf(M$,48,"M$"); 

 /* 14. Swap modified blocks k and j of M$ to obtain goal M* as follows: */

 block Msj,Msk,Msm;
 block M$k,M$j;

 /*  M*[i] = M$[i] for i in {1,...,m*} / {j,k} */

 memcpy(M$k,M$+16,16);
 memcpy(M$j,M$,16);
 memcpy(Msm,M$+32,16);

 /* M*[j] = M$[k] + 2^kL* + 2^jL* */

 xor_block(Msj,M$k,tmp); 
 pbuf(Msj,16,"! M*[1]"); 

 /* M*[k] = M^$[j] + 2^kL* + 2^jL* */ 

 xor_block(Msk,M$j,tmp); 
 pbuf(Msk,16,"! M*[2]"); 
 pbuf(Msm,16,"! M*[3]");

return a;
}

int main(int argc, char **argv) {

 if(argc > 2) {

 memcpy(K,argv[2],16);

 /* Let (C*,T*) be the encryption of (N*,A*,M*) */

  block Ns;
  RandomBytes(Ns,16);
  //byte As[0]; // empty
  pbuf(Ns,16,"N*");
  byte Ms[48]; memset(Ms,0,48);
  byte Cs[48]; memset(Cs,0,48);
  byte Ts[16]; memset(Ts,0,16); 
  memcpy(Ms,(byte *)"AAAAAAAAAAAAAAAA",16); 
  byte As[16]; memset(As,0,16); 
  memcpy(As,(byte *)"DDDDDDDDDDDDDDDD",16); 
  memcpy(Ms+16,(byte *)"BBBBBBBBBBBBBBBB",16); 
  memcpy(Ms+32,(byte *)"CCCCCCCCCCCCCCCC",16); 
  pbuf(Ms,48,"M*");

  EncryptionOracle(Cs,Ts,Ns,As,16,Ms,48);

  pbuf(Ns,16,"N*"); 
  pbuf(Cs,48,"C*"); 
  pbuf(Ts,16,"T*"); 
  pbuf(As,16,"A*"); 
 
   /* (N*,A*,C*,T*) is given to adversary. Goal is to recover (M*) */

   switch(atoi(argv[1])) {
    case 1: 
     IwataPlaintextRecoveryAdversary(Ns,As,16,Cs,Ts); // assumes 3 blocks of plaintext
     break;
   }
   return 1;
 }
return 0;
}
