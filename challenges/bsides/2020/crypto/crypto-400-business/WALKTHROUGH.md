# Ordinary Course of Business

This is a server based challenge where players are given a binary of a server that performs encryption and decryption with an unknown key

## Steps - Official Walkthrough

<details>
<summary>Spoiler warning</summary>

### Enable admin mode
* Reverse engineering shows there is a hidden menu option - "0" 
* Entering "0" gives a password prompt
* Reverse engineering shows that the users input password is xor encoded (starting at 0xAA and adding 1 each character) and then compared against a fixed byte string in the binary
* You can apply the same encoding to the byte string to recover `GreatSageEqualOfHeaven` as the password

### Encryption/Decryption and Test vectors
* This admin mode enables two additional options in the menu
	* Decryption oracle
	* Test vectors
* Reverse engineering shows that the flag.txt file is encrypted with the random key generated on start and then returned as a "test vector"
* The first thing to try would be to put the encrypted flag into the decryption oracle... but this is disallowed. 

### Block cipher mode
* Symbols show that the block cipher AEAD mode is called "OCB" with rijndael (AES) as its core block cipher
* There are three different types of OCB (1,2 and 3). OCB2 has a number of vulnerabilities and this is the mode that this binary is using

### Attacks
* There are two main attacks to implement given an encryption and decryption oracle (see the paper) 
	* The Inoue Minematsu Minimal Attack generates a valid ciphertext/tag pair
	* The Iwata plaintext recovery attack breaks confidentiality - given cipher, nonce, additional_data and tag, it will recover the message. 

* See references for more details and `attack/solve.py` for a working implementation. These are heavily based on the code samples in the references below, tweaked to include additional data (`cybearsctf`).

</details>

# References
* OCB2 attack paper: https://eprint.iacr.org/2019/311
* OCB2 attacks: 
	* https://blog.react0r.com/2019/12/06/cryptography-attacking-ocb2/ #includes plaintext recovery attack
	* https://gist.github.com/HAITI/50fb494e449531e51c172aa8e24a9cf3
	* https://github.com/AntonKueltz/ocb-forger #python authentication attack and ocb2


