import subprocess

from binascii import *
from Crypto.Protocol.SecretSharing import Shamir

in_docker=True
if in_docker == True: 
    qrwork_path = "/qrpicture/qrwork"
else:
    qrwork_path = "./qrwork"

cybears = 'Cybears'
insignias = ['Pigbot', 'Sanditron', 'Trypitakon', 'WuKongPrime']
suffix = "_BLK_93x93_bg.png"

hints = ['https://ctf.cybears.io', 'ssssh', 'https://www.youtube.com/watch?v=J-SUoHmpRdM', 'https://www.youtube.com/watch/dQw4w9WgXcQ']

flag = b'cybears{C@r1ng!}' #must be len==16
shares = Shamir.split(4,4, flag)

for idx, share in shares:
    print("Index #%d: %s" % (idx, hexlify(share)))


for idx, share in shares:
    qr_text = '(%d,%s)' % (idx, hexlify(share).decode('utf-8'))
    outfile = "out/out"+str(idx) + ".png"

    a= "--maxsalt=0 --outline=1 " +qr_text+ " " + "ctf/"+insignias[idx-1]+"_BLK_93x93_bg.png " + outfile
    subprocess.call([qrwork_path] + a.split(" "))

    outfile_372 = "out/out"+str(idx) + "_372x372.png"
    outfile_all = "out/print_"+str(idx) + "_all.png"

    #resize output QR code from 93x93 to 372x372
    a = "-resize 372x372 " + outfile + " " + outfile_372
    subprocess.call(["convert"] + a.split(" "))

    #combine the text and QR code into the final image
    a = "-size 1600x1000 xc:white " + outfile_372 + " -gravity center -geometry +0-200 -composite ctf/text.png -gravity center -geometry +0+200 -composite " + outfile_all
    subprocess.call(["convert"] + a.split(" "))


for h in hints:
    qr_text = '%s'%h
    tag = h[-1] #just use last character to differntiate strings
    outfile = "out/out_"+tag+ ".png"

    a= "--maxsalt=0 --outline=1 " +qr_text+ " " + "ctf/"+"Cybears_BLK_93x93_bg.png " + outfile
    subprocess.call([qrwork_path] + a.split(" "))

    outfile_372 = "out/out"+tag + "_372x372.png"
    outfile_all = "out/print_"+tag + "_all.png"

    #resize output QR code from 93x93 to 372x372
    a = "-resize 372x372 " + outfile + " " + outfile_372
    subprocess.call(["convert"] + a.split(" "))

    #combine the text and QR code into the final image
    a = "-size 1600x1000 xc:white " + outfile_372 + " -gravity center -geometry +0-200 -composite ctf/text.png -gravity center -geometry +0+200 -composite " + outfile_all
    subprocess.call(["convert"] + a.split(" "))




