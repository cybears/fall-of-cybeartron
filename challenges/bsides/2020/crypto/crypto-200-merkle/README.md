# Empty Vault

* _author_: cybears:cipher
* _title_: Empty Vault
* _points_: 200
* _tags_:  crypto

## Flags
* See MANIFEST.yml

## Challenge Text
* See MANIFEST.yml

## Attachments
* mt_flask.py

## References
* merkle tree implementation based on the excellent MerkleTools library https://github.com/Tierion/pymerkletools

## Notes - Playtester
* Build dockerfile
`docker build -t mt .` 
*run Docker image
`docker run -it --rm -p 31415:31415 mt`
*browse to localhost:31415
`curl http://localhost:31415`
* players will have mt_flask.py file as a handout

