
import Crypto.PublicKey.RSA as rsa
import Crypto.Cipher.PKCS1_v1_5 as pkcs
import Crypto.Util.number as nt
from binascii import *
import random
import json
from flag import FLAG1

FLAG2 = b'Thank you Mario! But our princess is in another castle!'
PLACEHOLDER = b'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
NUM_KEYS = 128

handout = []

## Generate NUM_KEYS-2 sets
for i in range(NUM_KEYS-2):
    #print("DEBUG: {} of {}".format(i, NUM_KEYS))
    r = rsa.generate(1024)
    o = pkcs.new(r)
    c = o.encrypt(PLACEHOLDER)
    j = {"modulus":r.n, "exponent":r.e, "cipher":hexlify(c).decode('utf-8')}
    handout.append(j)

## Generate RSA keys with shared primes
r1 = rsa.generate(1024)
o = pkcs.new(r1)
c = o.encrypt(FLAG1)
j = {"modulus":r1.n, "exponent":r1.e, "cipher":hexlify(c).decode('utf-8')}
handout.append(j)

#generate a new q, but reuse p
q = nt.getPrime(512)
n2 = r1.p * q
e = 65537
d = nt.inverse(e, (r1.p-1)*(q-1))

r2 = rsa.construct((n2,e,d))
o = pkcs.new(r2)
c = o.encrypt(FLAG2)
j = {"modulus":r2.n, "exponent":r2.e, "cipher":hexlify(c).decode('utf-8')}
handout.append(j)

## Shuffle set
random.shuffle(handout)

## export json
print(json.dumps(handout))
