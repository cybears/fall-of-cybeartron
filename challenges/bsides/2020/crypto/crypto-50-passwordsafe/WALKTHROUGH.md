# Password Safe walkthrough

This is a standalone challenge where players need to find the last 16-bits of a damaged/unknown AES key. Because there is no IV, it should hint that the mode of AES is ECB (Electronic Code Book).

The player may suspect that the underlying plaintext is printable ascii. As there are only 2^16 (or 65536) possible options for the AES key, it is quick to brute force. To check whether they have the right key, the player may:
* Check for the text `cybears` in each decrypt
* Check whether the decrypt contains only printable ascii
* perform statistical tests on the decrypt and check for non-randomness


