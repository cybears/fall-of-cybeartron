## https://crypto.stackexchange.com/questions/5791/why-is-it-important-that-phin-is-kept-a-secret-in-rsa

import Crypto.PublicKey.RSA as rsa
import Crypto.Cipher.PKCS1_v1_5 as pkcs
import Crypto.Util.number as nt
from binascii import *
import json
import gmpy2
import random
import argparse

from flag import FLAG

from pwn import *

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge", required=False)
args = parser.parse_args()

# 1. Given N, e, p, q, C -> M
# 2. Given N, phi(N) -> p
# 3. GIven N, p+q -> p
# 4. Given N, p-q -> p
# 5. Given N,e,d -> p

###############
## Challenge 1
###############
# Calculate the private exponent as
# d = mod_inv(e, (p-1)(q-1)
# Then decrypt as standard RSA as
# M = cipher ^ d mod N

def solve_one(j):
    d = nt.inverse( j['e'], (j['p']-1)*(j['q']-1))
#    m = pow(nt.bytes_to_long(unhexlify(j['cipher'])), d, j['N'])
#    return nt.long_to_bytes(m)
    r = rsa.construct((j['N'], j['e'], d))
    p = pkcs.new(r)
    plain = p.decrypt(unhexlify(j['cipher']), None)
    return plain


###############
## Challenge 2
###############
# phi satisfies p^2 -p*(N-phi+1) + N = 0
# A quadratic in p
# Solve with quadratic formula where
# a=1, b= N-phi+1, c = n
def solve_two(j):
    n = j['N']
    phi = j['phi']

    a = 1
    b = -(n-phi + 1)
    c = n

    disc = b*b - 4*a*c
    if gmpy2.is_square(disc) == False:
        print("ERROR discriminant not square")
        return None,None
    sq = gmpy2.isqrt(disc)
    x1 = int((-b + sq)//2)
    x2 = int((-b - sq)//2)

    if x1 < x2 :
        return x1,x2
    else:
        return x2,x1


###############
## Challenge 3
###############
# Note that s = p+q satisfies
# phi = N - s + 1
# recover phi and then use solve_two()
def solve_three(j):
    N = j['N']
    s = j['p+q']

    phi = N-s+1

    return solve_two({"N":N, "phi":phi})

###############
## Challenge 4
###############
# Note that if diff = q-p, sum = p+q, then
# p = (diff + sum)/2
# q = (sum - diff)/2
# N = pq = (diff + sum)/2 * (sum - diff)/2
# So 4N = sum^2 - diff^2

def solve_four(j):
    N = j['N']
    DIFF = j['q-p']
    SUM_SQ = 4*N + DIFF*DIFF

    if gmpy2.is_square(SUM_SQ) == False:
        print("ERROR Non-square found")
        return None,None

    SUM = gmpy2.isqrt(SUM_SQ)

    return solve_three({"N":N, "p+q": SUM})


###############
## Challenge 5
###############
# https://www.di-mgt.com.au/rsa_factorize_n.html
def solve_five(j):
    N = j['N']
    e = j['e']
    d = j['d']

    k = e*d - 1

    found = False

    while(found == False):

        t = k
        g = random.randint(2,N)
        #print("DEBUG: New g")

        while(t % 2 == 0):  # While t is even
            t = t//2
            x = pow(g,t,N)
            if x == 1 or x == N :
                continue

            y = nt.GCD(x-1, N)
            #print("DEBUG ({},{},{})".format(t,x,y))
            if y > 1 and y < N:
                found = True
                print("JACKPOT")
                break

    #print("DEBUG: y={}".format(y))
    x = N // y

    if x < y:
        return x,y
    else:
        return y,x



if __name__ == "__main__":
    context.log_level = "debug"
    if args.remote != None:
        host = args.remote.split(":")[0]
        port = int(args.remote.split(":")[1])
        p = remote(host, port)
    else:
        p = process(["python3" ,"FunWithPrimes_server.py"])

    ## CHallenge 1
    r1 = p.readuntil("Enter Response:")
    start = r1.find(b"{")
    end = r1.find(b"}") + 1
    j1 = json.loads(r1[start:end])
    a1 = solve_one(j1)
    p.sendline(a1)

    ## Challenge 2
    r2  = p.readuntil("Enter Response:")
    start = r2.find(b"{")
    end = r2.find(b"}") + 1
    j2 = json.loads(r2[start:end])
    a2 = solve_two(j2)
    p.sendline(str(a2[0]))

    ## Challenge 3
    r3  = p.readuntil("Enter Response:")
    start = r3.find(b"{")
    end = r3.find(b"}") + 1
    j3 = json.loads(r3[start:end])
    a3 = solve_three(j3)
    p.sendline(str(a3[0]))

    ## Challenge 4
    r4  = p.readuntil("Enter Response:")
    start = r4.find(b"{")
    end = r4.find(b"}") + 1
    j4 = json.loads(r4[start:end])
    a4 = solve_four(j4)
    p.sendline(str(a4[0]))

    ## Challenge 5
    r5  = p.readuntil("Enter Response:")
    start = r5.find(b"{")
    end = r5.find(b"}") + 1
    j5 = json.loads(r5[start:end])
    a5 = solve_five(j5)
    p.sendline(str(a5[0]))


    RESPONSE = p.readuntil("}")
    #print(RESPONSE)

    if FLAG in RESPONSE:
        print("SUCCESS! Flag found")
        exit(0)
    else:
        print("ERROR: Flag not found - received [{}]".format(RESPONSE))
        exit(-1)
