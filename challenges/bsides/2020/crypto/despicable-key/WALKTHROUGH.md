# Despicable Key Walkthrough

* In this challenge, players are provided a file that has been encrypted with AES GCM mode. Players are provided the key, and cipher, but not the IV or the 'tag' that was used (AES GCM is an authenticated encryption mode, the 'tag' is used to ensure that the plain and cipher have not been tampered with).
* We actually only need to recover the IV to solve the challenge, the tag is only used to assure that the cipher hasn't been modifed
* By the standard, a 12-byte IV is used, which is then appended with a counter
* `counter_0 = IV || 00000000h`
* `counter_n = counter_{n-1} + 1`
* If you know the IV and the key, the decryption part of AES GCM looks like AES in Counter Mode
* `cipher_n = plain_n XOR AES_ENC(counter_n, k)`
* If we know or can guess the first 16-bytes of plaintext under the cipher, we can decrypt the first block to recover the IV. Then, we can just step forward with counter mode.
* ie `plain_n = cipher_n XOR AES_ENC(counter_n, k)`
* From the file name (flag.png.enc), players should guess that the underlying data is a PNG file.
* PNG files have a fixed 8-byte 'magic' header, then a series 'chunks' made up of of 4-byte size fields, 4-byte chunk names and then variable length chunk data
* The first chunk must be the IHDR chunk, which is 13-bytes.
* With these two facts, we can determine the first 16-bytes of the PNG file, recover the 12-byte counter and then decrypt the flag!
* The counter/IV can be recovered as `counter_0 = AES_DEC(cipher_0 XOR plain_0, k)`
* Full solve script in dec.py
