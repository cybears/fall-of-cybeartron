# Vanity AES Walkthrough

This challenge is mostly an exercise in interacting with an opaque
crypto-system in order to determine how it can be abused.

The application which players interact with is simply a few flavour text menus
around a core function whereby an "emergency beacon" can be transmitted. When
valid hexadecimal data is passed to the beacon function, it will be encrypted
and displayed to the user as data to be "transmitted". By varying the amount of
beacon data sent to the application, the player should be able to see that
there are 16 bytes of overhead, enough for an IV for a 128 bit wide block
cipher. They can also observe that the returned ciphertext is not padded,
implying that a block mode implementing a stream cipher one is being used.

The first major hurdle the player needs to recognise is that the system
deliberately renders slowly to force them to interact with it using multiple
parallel connections. Once they begin interacting with the system in parallel,
they should be able to determine that passing in the same beacon data within
some window of time results in a repeated ciphertext message. By this point,
the player should be building a harness which allows them to pass in beacon
plaintext near simultaneously to continue experimenting.

By observing that changing a bit in the plaintext is reflected in the same bit
of ciphertext (or technically 128 bits/1 block advanced if one considers the IV
part of the ciphertext), the player should be able to narrow the candidate
block modes down to CFB or OFB mode (or something more exotic, which isn't the
case). By observing that changing a bit in the ciphertext (via the plaintext)
scrambles subsequent blocks of ciphertext, OFB mode is eliminated as a
candidate and the player's best guess is to attempt an abuse of CFB mode to
control the ciphertext data.

They can then proceed to weaponise this information by using the beacon
function to:

 1. Constructing an initial arbitrary plaintext blob of at least as many blocks
    as need to be controlled eventually (2 in our case)
 1. Encrypting that plaintext and recovering the ciphertext
 1. Constructing a new plaintext:
   * `P' = P xor C xor D`
   * where `P'` is the new plaintext and `P` is the previous plaintext
   * `C` is the recovered ciphertext
   * `D` is a desired ciphertext to work toward
 1. Repeating the encryption with the new plaintext
 1. Observe the desired ciphertext in the first block
 1. Repeat from step 2 to control subsequent blocks of ciphertext

A simple demonstration of the concept is shown in `demo.py` and a solver for
the challenge is in `solve.py`.
