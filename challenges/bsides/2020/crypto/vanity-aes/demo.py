"""
Demo the concept of CFB ciphertext watermarking.
"""
import binascii
import hexdump
from Cryptodome import Random
from Cryptodome.Cipher import AES

KEY = Random.get_random_bytes(AES.block_size)

def demo():
    # First ptext
    ptext = b"A" * (AES.block_size - 1) + b"A" + b"A" * AES.block_size * 2
    print("PLAINTEXT A - OBSERVE IV REUSE/CYCLE")
    hexdump.hexdump(ptext)
    # First IV
    iv = Random.get_random_bytes(AES.block_size)
    # Make two ciphertexts to confirm IV reuse
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    print("CIPHERTEXT A1")
    hexdump.hexdump(iv + ztext)
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    print("CIPHERTEXT A2 WITH REPEATED IV")
    hexdump.hexdump(iv + ztext)
    # Second IV
    iv = Random.get_random_bytes(AES.block_size)
    # Make another ciphertext to see that the IV changes periodically
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    print("CIPHERTEXT A3 WITH NEW IV")
    hexdump.hexdump(iv + ztext)
    # Second ptext
    ptext = (
        b"A" * (AES.block_size // 2 - 1)
        + b"@"  # Flips the bit to the "left" of the midpoint of a block
        + b"A" * (AES.block_size // 2)
        + b"A" * AES.block_size * 2
    )
    print("PLAINTEXT B - DEMONSTRATE BIT CONTROL")
    hexdump.hexdump(ptext)
    # One more ciphertext to confirm bit control
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    # First block shows that ztext = something XOR ptext
    # This implies that we're using CFB, OFB or something exotic
    # Second block shows that ztext is scrmabled by first block ptext change
    # implying that the cipher input is related to the preceding block's ztext
    # This eliminates OFB as a potential block mode
    print("CIPHERTEXT B1")
    hexdump.hexdump(iv + ztext)
    # Again for sanity
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ztext = cipher_obj.encrypt(ptext)
    print("CIPHERTEXT B2 WITH REPEATED IV - SAME AS B1")
    hexdump.hexdump(iv + ztext)
    # Now we can make the first block of controlled ztext
    desired_ztext = b"CYBEARS" * 3
    print("DESIRED CIPHERTEXT Cn")
    hexdump.hexdump(desired_ztext)
    blocks = (len(desired_ztext) - 1) // AES.block_size + 1
    print("CIPHERTEXT C0 == CIPHERTEXT B2 - IV REUSED FOR SUBSEQUENT ENCIPHER")
    for i in range(blocks):
        ptext = bytearray(
            p ^ z ^ d for p, z, d in zip(ptext, ztext, desired_ztext)
        )
        print("PLAINTEXT C%i" % i)
        hexdump.hexdump(ptext)
        # Make the controlled ztext
        cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
        ztext = cipher_obj.encrypt(ptext)
        print("CIPHERTEXT C%i" % i)
        hexdump.hexdump(iv + ztext)

if __name__ == "__main__":
    demo()
