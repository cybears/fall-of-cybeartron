#!/usr/bin/env python3
"""
Vanity AES solve script.
"""
import argparse
import binascii
import logging
import os.path

import hexdump
import pwn

DESIRED_CIPHERTEXT = b"CYBEARS" * 3
NUM_BLOCKS = int((len(DESIRED_CIPHERTEXT) - 1) / 16 + 1)

LOCAL_RADIO = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "radio.py"
)
LOCAL_RADIO_ARGS = (LOCAL_RADIO, )

def solve(factory, args):
    # Prepare the processes - we use twice as many as we need to get the
    # necessary blocks of vanity ciphertext in case the IV rotates while we're
    # interacting with the cryptosystem
    procs = tuple(
        factory(*args) for _ in range(NUM_BLOCKS * 2 + 1)
    )
    pwn.log.info("Reading preamble text from target processes")
    for p in procs:
        p.send("2\n")
    for p in procs:
        p.readuntil("beacon data >>> ")
    # We need to repeatedly pass candidate plaintext through the system to get
    # ciphertext and then construct the next candidate. We build the vanity
    # ciphertext up block by block which is why we have to do this repeatedly.
    ptext = bytearray(32)
    for i, p in enumerate(procs):
        pwn.log.info("Sending plaintext %i", i)
        pwn.log.info(hexdump.hexdump(ptext, result="return"))
        # Send the current plaintext buffer
        p.send(binascii.hexlify(ptext).decode("iso-8859-1") + "\n")
        # Get the ciphertext dump back
        p.readuntil("Transmitting\n")
        iv_line, *ztext_lines = p.readlines(1 + NUM_BLOCKS)
        ztext_dump = "\n".join(l.decode("iso-8859-1") for l in ztext_lines)
        ztext = hexdump.restore(ztext_dump)
        pwn.log.info("Received ciphertext back")
        pwn.log.info(ztext_dump)
        # Check if we win and wait for the flag
        if DESIRED_CIPHERTEXT in ztext:
            pwn.log.success("Desired ciphertext completed on cycle %i", i)
            if not p.readuntil("VALIDATION FAILED RECEIVED DATA FOLLOWS:"):
                raise TimeoutError("Didn't get the flag when we won...")
            p.send("\0")
            flag_dump_lines = p.readuntil("Aborting")
            flag_dump = "\n".join(
                l.decode("iso-8859-1")
                for l in flag_dump_lines.splitlines()[2:-1]
            )
            flag = hexdump.restore(flag_dump)
            pwn.log.success("Recovered flag: %r", flag)
            break
        # If we didn't win, adjust the plaintext and loop around
        pwn.log.info("Making tweaks to plaintext")
        ptext = bytearray(
            p ^ z ^ d for p, z, d in zip(ptext, ztext, DESIRED_CIPHERTEXT)
        )

def pwn_args():
    parser = argparse.ArgumentParser("Vanity AES Solver")
    parser.add_argument(
        "-v", "--verbose", action="count", default=0,
        help="Turn up the logging",
    )
    parser.add_argument(
        "-q", "--quiet", action="count", default=0,
        help="Turn down the logging",
    )
    target = parser.add_mutually_exclusive_group(required=True)
    target.add_argument("-l", "--local", help="Test this local program")
    target.add_argument("-r", "--remote", help="Test a server at [addr:]port")
    args = parser.parse_args()
    log_level = max(
        logging.NOTSET + 1,
        logging.WARNING + (args.quiet - args.verbose) * 10,
    )
    pwn.context.log_level = log_level
    if args.local:
        assert os.access(args.local, os.X_OK)
        return pwn.process, (args.local, )
    else:
        try:
            address, port = args.remote.split(":")
        except ValueError:
            address, port = "localhost", args.remote
        return pwn.remote, (address, int(port))

if __name__ == "__main__":
    solve(*pwn_args())
