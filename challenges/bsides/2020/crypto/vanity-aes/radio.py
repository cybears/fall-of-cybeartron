#!/usr/bin/env python3
import binascii
import fcntl
import itertools
import os
import random
import select
import string
import struct
import sys
import time
import unittest.mock
import uuid

import hexdump
from Cryptodome import Random
from Cryptodome.Cipher import AES


###############################################################################
# Challenge flavour
###############################################################################
# The flag!
FLAG = b"cybears{fe3db4ck_1s_w3lc0m3ed}"

# Textual elements
PLAYER_PREAMBLE = """
Thanks for dialing in. We're at the abandoned radio outpost and need to get a
message out which our allies can recognise so we can begin work on establishing
comms back to the other side of the space bridge. But we can't get into the
control room to reconfigure anything at the moment and time is of the essence.

I'm connecting you to one of the serial lines hanging from the old security
terminal here. Work your magic and get a transmission out which contains the
word "CYBEARS" repeated three times, like "CYBEARSCYBEARSCYBEARS". That'll
demonstrate to our allies that we're in control of the radio outpost and it's
not some old ghost in the shell spitting out garbage data.

Dial in as many times as you need, I'll be here to hook you in.

>>>>>>>>>> BEGINNING SERIAL PASSTHROUGH
"""

SYSTEM_FAILURE = """
CRITICAL SYSTEM FAILURE ABORTING TRANSMISSION
"""

YOU_BROKE_IT = """<<<<<<<<<< SERIAL HANGUP

\t\t** bzzrt **

What the hell did you do? I just heard an electrical discharge from the control
room and I think I can smell some magic blue smoke in the air. Hopefully it
isn't permanently damaged!
"""

MENU_PREAMBLE = """
\tSECURE BROADCAST SYSTEM

Menu:
\t1. Secure Channel Transmission
\t2. Emergency Broadcast
\t0. Disconnect
"""

SECURE_MODE_FAILURE = """
Failed to negotiate secure channel with session controller.
\tHELLO TIMEOUT
"""

EMERGENCY_MODE_PREAMBLE = """
\tEMERGENCY BROADCAST MODE

Communications will be encrypted with the emergency broadcast key which is
securely held by all Communication Outposts. The system will broadcast a
specified beacon and then await responses indefinitely. To terminate waiting
for response, transmit any byte.
"""

EMERGENCY_INPUT_PREAMBLE = """
Enter beacon content in hexadecimal format. This will be decoded to bytes prior
to transmission.
"""

FLAG_PREAMBLE = """
RESPONSE DETECTED...

VALIDATION FAILED RECEIVED DATA FOLLOWS:
"""

def menu():
    slow_print(MENU_PREAMBLE)
    while True:
        mode = int(input("menu >>> "), 10)
        if mode == 1:
            slow_print("." * 10, 0.5)
            slow_print(SECURE_MODE_FAILURE)
        elif mode == 2:
            slow_print(EMERGENCY_MODE_PREAMBLE)
            run()
        elif mode == 0:
            slow_print("DISCONNECTING")
            slow_print("." * random.randint(2, 5), 0.5)
            return
        else:
            slow_print("INVALID SELECTION")

###############################################################################
# The actual challenge lives here
###############################################################################
# This needs to be predictable across challenge instances so we hardcode it
# based on randomness generated once on my box
KEY = binascii.unhexlify(
    "854323b27c5225303c939cb8b9dea71d3e64b0cfe20d430fe15f92f153d4d8d7"
)

# This granularity defines the interval between IV rotations. We can increase
# it if it turns out that the timing is too tight for players in prod. By
# default we provide a window of 1 second which should force players to
# instantiate multiple connections to the service since we forcibly block them
# for at least `IV_GRANULARITY / 2` seconds and the solve requires 3
# consecutive encryptions. It would be possible to predict the upcoming IVs as
# well if the players choose to understand how they vary over time rather than
# abuse the ability to make concurrent connections.
IV_GRANULARITY = 1.00
# We use a fixed `node` value for the UUID1 to avoid exposing MAC addresses
IV_NODE = int(b"69e564215ad3", 16)
# We use a fixed initial `clock_seq` value which counts upward to add a little
# more variability to the IVs while still maintaining predictability
IV_CLOCK_SEQ = itertools.count(2020)

# Cybear average response delay (seconds)
FLAG_MEAN_DELAY = 5

def run():
    # Get plaintext from the user and dehexlify it
    slow_print(EMERGENCY_INPUT_PREAMBLE)
    plaintext = binascii.unhexlify(input("beacon data >>> "))
    if not plaintext:
        slow_print("ABORTING NO INPUT")
        return
    slow_print(
        "Decoded beacon is {0} bytes long".format(len(plaintext)),
        interval=DEFAULT_FAST_INTERVAL
    )
    # We generate an IV from a UUID1 which is based on the number of 100ns
    # intervals since the UUID epoch (notably different to the UNIX epoch),
    # however we simulate a low resolution internal timer but mocking
    # `time.time_ns()` which is used by the generator logic in the `uuid`
    # module. We round to the nearest `IV_GRANULARITY` interval in nanoseconds
    # which should be observable by the low bytes of the resultant UUID
    # counting upward in an hopefully obvious way.
    #
    # We have to mock both `time.time()` and `time.time_ns()` which is used
    # from Python 3.8.
    real_time = time.time
    mock_time = lambda: IV_GRANULARITY * round(real_time() / IV_GRANULARITY)
    with unittest.mock.patch(
        "time.time", side_effect=mock_time
    ), unittest.mock.patch(
        "time.time_ns", create=True, side_effect=lambda: int(mock_time() * 1e9)
    ):
        iv = uuid.uuid1(node=IV_NODE, clock_seq=next(IV_CLOCK_SEQ)).bytes
    # Encrypt the plaintext with the generated IV in CFB mode. Note that we
    # need to specify a segment size equal to the AES block size so that we use
    # the full output of each cipher operation. It would be solvable with the
    # default segment size of 8 bits but it'd be much more difficult for the
    # player to work out what the opaque cryptosystem is.
    cipher_obj = AES.new(key=KEY, mode=AES.MODE_CFB, iv=iv, segment_size=128)
    ciphertext = cipher_obj.encrypt(plaintext)
    # Spit out the ciphertext for the player to analyse
    slow_print(
        "EMERGENCY BEACON SYSTEM: Transmitting\n" +
        hexdump.hexdump(iv + ciphertext, result="return"),
        # This needs to be a little faster to ensure the player can catch the
        # repeated IVs they need - this assumes they have prepared by reading
        # up to the beacon data prompt, they should be able to get 
        interval=DEFAULT_FAST_INTERVAL
    )
    # Wait for anything to pop up on stdin
    _i = (sys.stdin, )
    _n = tuple()
    flag_delay = FLAG_MEAN_DELAY - 1 + random.random()
    if not any(select.select(_i, _n, _n, flag_delay)):
        if "cybearscybearscybears" in ciphertext.decode("iso-8859-1").lower():
            slow_print(FLAG_PREAMBLE)
            slow_print(hexdump.hexdump(FLAG, result="return"))
    # Consume the rest of stdin so it doesn't break the menu
    if any(select.select(_i, _n, _n)):
        slow_print("Aborting beacon response loop")
        fl_orig = fcntl.fcntl(sys.stdin, fcntl.F_GETFL)
        fcntl.fcntl(sys.stdin, fcntl.F_SETFL, fl_orig | os.O_NONBLOCK)
        try:
            while sys.stdin.read():
                pass
        except TypeError:
            pass
        fcntl.fcntl(sys.stdin, fcntl.F_SETFL, fl_orig)
    # Block the player from returning to the menu for at least half of the IV
    # granularity period - works for granularity >= 1
    slow_print(
        "." * int(1 + random.random() * 3) * int(IV_GRANULARITY),
        interval=0.5
    )

###############################################################################
# Output helper functions
###############################################################################
# One 80 wide line per second
DEFAULT_SLOW_INTERVAL = 1.0 / 80
# THis is roughly fast enough locally to get 6 repeated IVs if used for the
# `slow_print()` calls in `run()`
DEFAULT_FAST_INTERVAL = 1.0 / 3200

def slow_print(text, interval=DEFAULT_SLOW_INTERVAL):
    for c in text:
        time.sleep(interval)
        sys.stdout.write(c)
        sys.stdout.flush()
    sys.stdout.write("\n")
    sys.stdout.flush()

CORRUPT_PROB = 1.0 / 8
CORRUPT_ALPHABET = tuple(set(string.printable) - set(string.whitespace))

def corrupt_print(text):
    corrupted_text = "".join(
        c if random.random() > CORRUPT_PROB
        else random.choice(CORRUPT_ALPHABET)
        for c in text
    )
    slow_print(corrupted_text, DEFAULT_SLOW_INTERVAL / 2)

###############################################################################
# Entrypoint
###############################################################################
# Print some flavour text and then punt them to the menu. This is where we
# suppress any traceback leaks to ensure the cryptosystem is opaque.
if __name__ == "__main__":
    print(PLAYER_PREAMBLE, end="\n")
    time.sleep(1)
    try:
        menu()
    except BaseException as exc:
        corrupt_print(SYSTEM_FAILURE)
        time.sleep(1)
        print(YOU_BROKE_IT, end="\n")
