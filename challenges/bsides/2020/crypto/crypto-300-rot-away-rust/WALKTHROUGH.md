# Rot Away Rust

This is a protocol based challenge where players are provided a protocol description and full source to the server, initiator and responder. A protocol diagram is also provided, detailing the steps needed to interact with the other clients and server to establish a session key.

## Steps - Official Walkthrough

<details>
<summary>Spoiler warning</summary>

### Solution

* The protocol is an implementation of the Otway-Rees Protocol
* The Otway-Rees protocol is susceptible to a type-confusion attack under some circumstances
* The encrypted message sent by Client B in step 2 is encrypted with K_BS (the responders server key)
* This encrypted message can be replayed to the reposnder in step 5
* In this implementation, because `session_id || ID_A || ID_B` is the same length as `K_AB` the shared session key, client B will mistake this message as being encrypted by the server and use `session_id || ID_A || ID_B` as the session key to encrypt a message.

## References
- https://en.wikipedia.org/wiki/Otway%E2%80%93Rees_protocol

</details>
