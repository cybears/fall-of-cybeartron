import EC
from conn import *
import json
import Crypto.Cipher.AES as AES
import hashlib
from binascii import *
import argparse

expected_flag = b"cybears{TheVirginiaStateFlagIsTheOnlyUSStateFlagToFeatureNudity}"

# socat -d TCP-LISTEN:3141,reuseaddr,fork EXEC:"python3 ./SuperCoolFactsServer.py"
# sage solve.sage -r hostname:port

def pad(data,blocksize):
    padlen = blocksize - (len(data)%blocksize)
    return data + (padlen.to_bytes(1,byteorder="little"))*padlen

def kdf(shared_point):
    s = str(shared_point.x) + str(shared_point.y)
    return hashlib.sha1(s.encode("utf-8")).digest()[0:16]

def encrypt_message(message, key):
    iv = os.urandom(16)
    pmessage = pad(message,16)

    a = AES.new(key, IV=iv, mode=AES.MODE_CBC)
    cipher = a.encrypt(pmessage)
    return {'iv':hexlify(iv).decode("utf-8"), 'cipher':hexlify(cipher).decode("utf-8")}

def decrypt_message(cipher, key, iv):
    a = AES.new(key, IV=iv, mode=AES.MODE_CBC)
    plain = a.decrypt(cipher)
    return plain

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge", required=True)
args = parser.parse_args()

#host = "localhost"
#port = 3141

host = args.remote.split(":")[0]
port = int(args.remote.split(":")[1])

sock = make_conn(host, port)
recvuntil(sock, b"value: ")
sendline(sock, b"0")
r = recvuntil(sock, b"value: ")
ra = r.decode("utf-8").split("\n")

#extract params
if " p = " in ra[4]:
    prime = int(ra[4].split("=")[1])
else:
    print("ERROR: could not extract prime")
    print("DEBUG: ra [{}]\n r [{}]".format(ra[4],r))
    exit(0)

if " a = " in ra[5]:
    a = int(ra[5].split("=")[1])
else:
    print("ERROR: could not extract a")
    exit(0)

if " b = " in ra[6]:
    b = int(ra[6].split("=")[1])
else:
    print("ERROR: could not extract b")
    exit(0)

if " gx = " in ra[9]:
    gx = int(ra[9].split("=")[1])
else:
    print("ERROR: could not extract gx")
    exit(0)

if " gy = " in ra[10]:
    gy = int(ra[10].split("=")[1])
else:
    print("ERROR: could not extract gy")
    exit(0)

#Generate EC
E  = EC.ec_curve(prime, a, b)
G = EC.ec_point(E, gx, gy)

##########################
###### BEGIN ATTACK ######
##########################

sE = EllipticCurve(GF(prime),[a,b])
sE_order = sE.order()

P = [1]  #primes
A = [0]  #values of private key modulo primes in P
temp_b = 1
bound = 65537

def send_point_get_cipher(sock, r):
    sendline(sock, b"1")
    pp = recvuntil(sock, b" your public point:")
    try:
        j = json.loads(pp.decode('utf-8').split("\n")[1])
    except Exception as e:
        print("ERROR: JSON Error occured {}".format(pp))
        exit(0)

    sendline(sock, json.dumps({"x": int(r.xy()[0]), "y": int(r.xy()[1])}).encode("utf-8") )
    recvuntil(sock, b"value: ")

    sendline(sock, b"2")

    c = recvuntil(sock, b"value: ")
    try:
        j = json.loads(c.decode("utf-8").split("\n")[0])
    except Exception as e:
        print("ERROR: JSON Error occured [{}]".format(c))
        exit(0)
    return j
    #print("DECRYPTED MESSAGE = {}".format(decrypt_message(unhexlify(j['cipher']), shared_key, unhexlify(j['iv']))))

def brute_key(cipher, iv, r, order_r):
    #turn sage EC point into python
    new_r = EC.ec_point(E, int(r.xy()[0]), int(r.xy()[1]))
    for i in range(0,order_r):

        putative_point = EC.ec_scalar(E, i, new_r)
        plain = decrypt_message(cipher, kdf(putative_point), iv)
        if plain.startswith(b"COOL FACT"):
            return i
    return -1

#while the product of the primes is less than the order of E, build relations
while (reduce((lambda a,b:a*b), P) < sE_order):
    print("DEBUG: P = {}".format(P))
    print("DEBUG: A = {}".format(A))

    #Generate a random b
    temp_b = temp_b+1
    #Generate an elliptic curve with b
    temp_E = EllipticCurve(GF(prime),[a,temp_b])
    #Factor this temp_EC
    temp_E_order = temp_E.order()
    f = factor(temp_E_order)
    print("DEBUG: factors {}, b = {}".format(f,temp_b))

    for (p,exponent) in f:
    #If there are factors f of order temp_EC which are a. non repeated, b. less than some bound
    #c. not already in P, then
        if (exponent == 1) and (p > 2) and (p not in P) and (p < bound):
            #generate a random point on temp_EC and make it order f
            r = temp_E.point((0,1,0))
            while (r.order() != p):
                temp_point = temp_E.random_point()
                r = (temp_E_order//p) * temp_point

            print("DEBUG: Found point of order {}".format(p))
            #print("DEBUG: ({},{})".format(r.xy()[0], r.xy()[1]))
            #send this point to the server (redo the handshake)
            #request a cool fact to determine order of private key mod p
            X = send_point_get_cipher(sock,r)
            #print("DEBUG: Received cipher - {}".format(X))
            m = brute_key(unhexlify(X['cipher']), unhexlify(X['iv']), r, p)

            #add this to P and A
            if (m == -1):
                print("ERROR could not recover value. p={}".format(p))
                continue
            else:
                print("DEBUG: JACKPOT! found (p,a) = ({},{})".format(p,m))
                P.append(p)
                A.append(m)


## CRT the values together and recover private key
print("DEBUG: P = {}".format(P))
print("DEBUG: A = {}".format(A))

recovered_secret_key = CRT(A,P)

print("SECRET KEY FOUND!: {}".format(recovered_secret_key))

##########################
###### END ATTACK ######
##########################

## send (hashed) private key to server to get flag

sendline(sock, b"3")
recvuntil(sock, b"digest()")

h = hashlib.sha1(str(recovered_secret_key).encode("utf-8")).hexdigest()

sendline(sock, h.encode("utf-8"))
resp = recvuntil(sock, b"}")

#print(resp)

if expected_flag in resp:
    print("SUCCESS! Flag found")
    exit(int(0))
else:
    print("ERROR, flag not found")
    print(resp)
    exit(int(1))
