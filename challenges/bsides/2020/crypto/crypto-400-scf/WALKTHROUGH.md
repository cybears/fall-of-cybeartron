# Super Cool Facts Walkthrough

* The cryptographic vulnerability challenge is the "invalid curve vulnerability" and it is based on the Cryptopals Set 8 challenge above. 
* For the invalid curve attack the elliptic curve implementation needs to a. ignore the "b" parameter in the elliptic curve addition, and b. Accept full (x,y) coordinates as points without checking that they lie on the valid curve. Further, there needs to be a way to perform multiple handshakes with the server with the same private key being used
* In this case, the EC implementation receives (x,y) coordinates and happily calculates the elliptic curve formulae. The player can continually request a new key/handshake as required. 
* Players need a way to check whether their exponent is the correct one - they can do this through brute force and checking whether the decrypt using the derived AES key contains "COOL FACT" in the first part of the message. If there wasn't a sterotypical start to messages, you could also just check for valid printable ascii characters. 
* The solve script builds relationships by randomly choosing a new "b" curve, factoring the order of the curve, then looking for factors that are smaller than some bound (that you're happy to brute force). Once it has used up all of these smal factors, it will choose another "b" and move on. 
* In this way, this solve script will work for any EC curve parameters, and any (static) private key. 

## References
* https://toadstyle.org/cryptopals/a0833e607878a80fdc0808f889c721b1.txt
