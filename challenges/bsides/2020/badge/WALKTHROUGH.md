# Badge Walkthroughs

This document contains walkthroughs for all of the badge flags. Hope you wanted
spoilers for all of them!

## `overflow`

This flag is a simple "overflow" crafted into the top level menu of the badge.
The menu item's name is a series of spaces so it is obviously highlighted if
the player move the selected item down enough times on that menu. Clicking the
middle (select/enter) button will open an application which has the flag
artfully arranged, like a bouquet of flowers.

## `573`

573 is a reference to the video game company Konami, using a form of Japanese
wordplay called goroawase The Konami code is a well known cheat where pressing
a sequence of inputs would unlock some secret in many of their games:
> up - up - down - down - left - right - left - right - B - A
However there is only a D-Pad-like input on the badge so the B - A part is
omitted.

Finding this flag is simply an exercise in exploring the interactive features
of the badge. If the player enters the Konami code in the correct menu (the
device's Status application under the About menu), the LEDs around the edge of
the badge will light up with each correct input. Once the full sequence of
directional inputs is entered, the flag will be shown on the eInk screen.

## `strhard`

This flag is a simple plaintext string hidden in the middle of the badge
firmware binary. The intention of this flag was for people to learn how to
connect their badge to a computer and dump the firmware, and hopefully sparking
some later interest in programming the badge!

The badge firmware can be dumped with `esptool.py` at which point the flag can
be found by simply running `strings` over the firmware blob, or `grep`ping for
`cybears{`.

## `its_dark`

This flag was intended to be a more annoying version of `strhard`. The flag was
once again a simple plaintext string but it was hidden way off the end of the
actual firmware code for the badge in the wilderness of the ESP32's flash
memory. Unfortunately, it seems that `esptool.py` dumps the entire flash by
default so this was basically about as hard as `strhard`.

## `strharder`

This flag can be extract in two different ways, one of which is significantly
more difficult than the other because I misjudged how hard it would be to find
"interesting" code in the firmware dump.

### The easy way

The idea of the easy way was that people who were playing with the badge and
monitoring the logging coming from the ESP32 over the debug UART would likely
notice an interesting log message which was printed toward the end of the boot
sequence, once all the ESP bootloader and FreeRTOS stuff had happened. The
actual message which gets spat out by the badge looks like so:

```
[WARN:1] CORRUPT MESSAGE
[2021-06-01 11:56:42] [monitor/esp32/INFO] [INFO:1] 63 1A 1B 07 04 13 01 08
[2021-06-01 11:56:42] [monitor/esp32/INFO] [INFO:1] 5D 0B 1C 5C 40 1D 7E 11
[2021-06-01 11:56:42] [monitor/esp32/INFO] [INFO:1] 0A 27 5F 18 1E 1F 1C 43
[2021-06-01 11:56:42] [monitor/esp32/INFO] [INFO:1] 16 7D 00 00 00 00 00 00
[2021-06-01 11:56:42] [monitor/esp32/INFO] [INFO:1] 00 00 00 00 00 00 00 00
[2021-06-01 11:56:42] [monitor/esp32/INFO] [INFO:1] 00 00 00 00 00 00 00 00
[2021-06-01 11:56:42] [monitor/esp32/INFO] [INFO:1] 00 00 00 00 00 00 00 00
```

The hexdump looks kind of like garbage but an observant reader might notice
that the first byte is an ASCII `c`, which is the first letter in the
`cybears{}` flag format. The idea is that by experimenting with some simple
encodings common in obfuscated code (e.g. virii), the player would realise that
the message is simply encoded using a rolling XOR where each encoded byte is
XORed with the previous plaintext one to recover the message.

### The hard way

I recommended this to a few players and definitely underestimated how annoying
it was to do with the final badge firmware blob. The intention was for players
to find the code which implemented the corrupt message hexdump through some
hunting for "interesting" code. In this case, `strharder` is a stack built
string which is the run through the same rolling XOR helper as `573` and
`overflow`. However, `strharder` uses this helper to **encode** the plaintext
stack string rather than **decoding** a normal string from `.rodata`.

The hope was that players would go hunting for the code which implemented the
display routines for `573` and `overflow`, with the assumption that similar
code might be used for another unknown flag. Additionally, I did try to hint
to some players that the name of this challenge implied that it would be like
`strhard` but a little different, ie. that it was a string in the firmware
which may have been stored in a different way during compilation.

Tearing apart the ESP32 firmware images is a bit annoying but not too difficult
to do with Ghidra and a few extra plugins, listed below. The blog post listed
below (3) has a decent if a little obtuse walkthrough of how to get an ESP32
firmware blob loaded into ghidra so you can start doing analysis.

The most direct way to find `strharder` would be to Just Know (duh) that there
probably aren't many occurences of a `mov $reg, '{'` instruction. This presumes
that you've already guessed that it's a stack built string and it must be in
the `cybears{}` flag format. Searching for instructions using ghidra turns up
something like 2 results for this pattern, one of which is in the middle of the
`strharder` stack string construction. So easy, right?

A more believable solution would have been to go hunting for the code where
either of `573` or `overflow` get decoded and displayed (`overflow` is probably
the easiest if you can work out the awful C++ indirection since it's a top
level badge application with a well known title (a bunch of spaces). By finding
that code, it would be possible to identify the rolling XOR routine which is
inlined into each of these bits of flag code. Once that has been discovered,
one could search for that same pattern of instructions across the rest of the
binary and the code at the tail of `strharder` should be one of those results.
Ghidra is basically magic.

1. `https://github.com/Ebiroll/ghidra-xtensa`
2. `https://github.com/Ebiroll/esp32_flash_loader`
3. `https://olof-astrand.medium.com/analyzing-an-esp32-flash-dump-with-ghidra-e70e7f89a57f`
