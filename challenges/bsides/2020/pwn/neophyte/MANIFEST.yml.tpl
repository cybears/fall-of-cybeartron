---
challenge:
  name: neophyte 1
  category: pwn
  value: 10
  description: |-
    Welcome to `neophyte`. This is a set of `11` challenges designed to walk you
    through the process of solving a `pwn` challenge. We will briefly touch on
    reverse engineering, vulnerability research and exploit development. Once you
    solve a challenge the next will appear when the challenge page `refreshes` or
    press `F5`.

    Like most `pwn` CTF challenges, you are given an executable binary. This one 
    runs on linux, and is an 
    [_Executable and Linkable Format_ (ELF) binary](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format).

    Binary files make sense to computers and operating systems, but are hard for 
    humans to understand. To aid this, we can parse the executable format 
    headers and convert machine instructions to assembly instructions. Assembly 
    is a low-level programming language heavily tied to underlying computer 
    architecture instructions. The process of converting machine code to 
    assembly instructions is called _disassembly_. Whilst assembly representation 
    allows a reverse engineer to understand a binary, in cases where the binary 
    was compiled from a higher level language like C, we can potentially aid the 
    reverse engineer further by decompilling the assembly code into high level 
    source code. This process is lossy, so the output is often called _pseudo code_.
    To analyse (reverse engineer) this binary, you could use a number of tools, 
    but we will focus on *Ghidra*. It's free, has both a decompiler and 
    disassembler, and is awesome. You can get Ghidra 
    [here](https://ghidra-sre.org/) and follow the 
    [installation guide](https://ghidra-sre.org/InstallationGuide.html). 
    The Ghidra homepage also has a quick start video, you should watch this if 
    you are not familiar with Ghidra.

    Once you have Ghidra installed, you need to create a new project (Ctrl + N) 
    and import the `neophyte` binary as a file (I). Accept the defaults for the 
    import. After clicking `OK` on the `Import Results Summary` you will be taken 
    back to the `Tool Chest`. Now you can disassemble and decompile the binary 
    by doing the following: 
    * Right click neophyte -> Open in default tool
    * Click `Yes` then `Analyze` to analyze

    Now you are presented with the code browser displaying information from the 
    neophyte binary. It can be a bit daunting, but Ghidra will open up with the 
    main window focussed on the start of the binary, which contains the ELF 
    headers as outlined in the 
    [Wikipedia article](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format).

    When the operating system executes a binary, one the of most important jobs 
    is to work out where to start execution instructions in the target binary. 
    It does this by reading the `e_entry` field in the ELF headers.

    The flag is the address pointed to by `e_entry`. 

    Hint: Use Ghidra to find this. Ghidra will helpfully name this location as a 
    function labeled `_start`.

    You could get this value by reading the bytes on this initial screen for the 
    `e_entry` field (be careful as x86 is 
    [little-endian](https://en.wikipedia.org/wiki/Endianness)). 
    Your other option is to double-click `start` and read the memory address 
    from the left hand side of the `Listing` window. This is the number in black 
    (for default color settings).

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  handouts:
    - ./handout/neophyte
  flags:
    - type: static
      content: "${e_entry}"
    - type: static
      content: "${e_entry_hex}"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 2
  category: pwn
  requirements:
    - neophyte 1
  anonymize: true
  value: 10
  description: |-
    Follow `_start` (double click in Ghidra) to look at the C runtime initialisation.
    This function will set up a call to `__libc_start_main`.

    Importantly a function parameter to `__libc_start_main` is the actual 
    applications `main` function. Ghidra will helpfully name this `main`.

    The flag is the memory address of the `main` function. 

    Hint: You can double click `main` in Ghidra then get the address from the 
    left hand side of the `Listing` window. This is the number in black (for 
    default color settings).

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  flags:
    - type: static
      content: "${main_addr}"
    - type: static
      content: "${main_addr_hex}"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 3
  category: pwn
  requirements:
    - neophyte 2
  anonymize: true
  value: 10
  description: |-
    Now its time to analyse the program for *vulnerabilities*.
    This is a very simple program, with just a couple of functions. We will 
    focus on the `main` function.

    In assembly, functions are called via the `CALL` 
    [instruction](https://www.felixcloutier.com/x86/call). 

    You will see a number of library function calls in the main function, one of 
    these is very [_unsafe_](https://rules.sonarsource.com/c/RSPEC-1081).

    The flag is the name of the unsafe function call.

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  flags:
    - type: static
      data: "case_insensitive"
      content: "gets"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 4
  category: pwn
  requirements:
    - neophyte 3
  anonymize: true
  value: 10
  description: |-
    So we have identified an unsafe library call.
    Now we need to work out if this is exploitable. Find the buffer that is 
    passed to the unsafe call you identified in the previous answer and 
    determine its allocated length.

    Ghidra will helpfully identify the length for you.

    The flag is the length of the buffer passed to the unsafe function call.

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  flags:
    - type: static
      content: "128"
    - type: static
      content: "0x80"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 5
  category: pwn
  requirements:
    - neophyte 4
  anonymize: true
  value: 10
  description: |-
    Ok. We have a vulnerable program. The vulnerability is called a _stack 
    buffer overflow_. We have a buffer _allocated_ on the program 
    [stack](https://en.wikibooks.org/wiki/X86_Disassembly/The_Stack) that is 
    of a set size, but we can populate this buffer with data of a length of *our 
    choosing*. This allows us to corrupt other data on the stack outside of the 
    bounds of this buffer.

    What is the `register` used to keep track of the stack location in the x86 
    instruction set? The flag is the `register` name.

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  flags:
    - type: static
      data: "case_insensitive"
      content: "esp"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 6
  category: pwn
  requirements:
    - neophyte 5
  anonymize: true
  value: 10
  description: |-
    Some more intel instruction set required knowledge.

    In the x86 instruction set, which `register` is used to indicate the 
    next instruction to be executed? The flag is the `register` name.

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  flags:
    - type: static
      data: "case_insensitive"
      content: "eip"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 7
  category: pwn
  requirements:
    - neophyte 6
  anonymize: true
  value: 10
  description: |-
    In order to exploit a stack buffer overflow we need to understand the usage 
    of the stack, including stack frames. The stack is, amongst other things, 
    used to pass function arguments, store local variables and importantly save 
    the caller address so a called function can return back to the caller and 
    continue execution when it finishes.

    We looked for `CALL` instructions previously to find a vulnerable function 
    call. `CALL` has one important difference to an unconditional jump or `JMP` 
    instruction - Basically it does two instructions: _something_ then `JMP`.

    The flag is the name of the _something_ (additional) instruction.

    Hint: It is described in the [intel instruction manual](https://www.felixcloutier.com/x86/call).

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  flags:
    - type: static
      data: "case_insensitive"
      content: "push eip"
    - type: static
      data: "case_insensitive"
      content: "push"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 8
  category: pwn
  requirements:
    - neophyte 7
  anonymize: true
  value: 10
  description: |-
    Nearly getting there...

    We have now learnt that the return address is stored on the stack, and this 
    is used for program control.

    We have also discovered that we can overflow a buffer on the stack with data 
    we control.

    If we send a large enough buffer the program will crash. This is because we 
    have overwritten the return address and the program as 'returned' to a 
    location of our control rather than the correct location.

    Assuming you ran the program and entered this as your buffer: 
    `AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' 
    what address would the instruction pointer `eip` be pointing to?

    Submit that value as the flag.

    Hint: if you have `gdb` installed, you can run the program in gdb and enter 
    the input above then read the value of `EIP` when it receives a `SIGSEGV` 
    signal.

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  flags:
    - type: static
      content: "0x41414141"
    - type: static
      content: "1094795585"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 9
  category: pwn
  requirements:
    - neophyte 8
  anonymize: true
  value: 10
  description: |-
    We can now overflow the return address on the stack and hijack execution. 
    However, in order to develop an exploit, we need to craft an input buffer to 
    overflow the return address with an address we specify (rather than a lot of 
    A's).

    For this, we don't need to know the input buffer length, but the distance 
    between the start of the overflowable buffer and the saved return address on 
    the stack.

    This can be calculated by knowledge of stack frames. Looking at the 
    disassembly window in Ghidra and calculating how the stack pointer `ESP` is 
    manipulated - or with the help of `gdb` and trial and error or _cyclic 
    buffers_. Using one of these techniques (or another), calculate the distance 
    between the start of the buffer and the start of the saved return address.

    The flag is this calculated value.

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  flags:
    - type: static
      content: "${return_address_offset}"
    - type: static
      content: "${return_address_offset_hex}"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 10
  category: pwn
  requirements:
    - neophyte 9
  anonymize: true
  value: 10
  description: |-
    Are we there yet?

    Nearly. We can now surgically overwrite the return address with an address 
    of our choosing. The remaining problem is to determine what address should 
    we overwrite the return address with?

    Earlier we noted that this binary had a couple of functions but then we only 
    analysed the `main` function.

    Find the name of the other function in this binary.

    The flag is the name of the _interesting_ function.

    Hint: Use Ghidra's Symbol Tree and look for pink functions listed in the 
    Functions sub-tree. Many of these (including all starting with '_') are part 
    of the c-runtime and initialisation so can be ignored. Also ignore 
    `frame_dummy`.

    `neophyte` questions do not use the `cybears{}` flag format. If you're
    having trouble submitting a flag which you think is correct, speak to
    an admin.
  flags:
    - type: static
      content: "readFlag"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
---
challenge:
  name: neophyte 11
  category: pwn
  requirements:
    - neophyte 10
  anonymize: true
  value: 10
  description: |-
    Have a look at what the function you identified in the previous question 
    does.

    It's a pretty interesting function isn't it? I think we might use that...

    For this final step you will need to get the address of this function and 
    use that as the value to overwrite the return address with.

    Use the template python solve script provided here, or your own if you are 
    comfortable, to complete the exploit and attack the competition server.

    You will need python and [pwntools](http://docs.pwntools.com/en/stable/install.html) 
    installed to use this script.

    `python3 solve.py -l [-v] [-d]`

    Once you have it working locally, point it at the challenge server to get 
    the flag!

    `python3 solve.py --hostname $target_dns --port $target_port`

    This particular `neophyte` question *does* use the `cybears{}` flag format.
  handouts:
    - ./handout/solve.py
  flags:
    - type: static
      content: "cybears{w3lc0m3_t0_th3_cyb3@rd0m3}"
  tags:
    - beginner
    - tutorial
    - pwn
  workflow:
    concept: true
    workup: true
    playtest: 1
    ci: true
    deploy: true
    review: false
    theme: false
  deployment:
    deploy_name: neophyte
    container_image: neophyte
    healthcheck_image: neophyte_healthcheck
    solve_script: python3 /neophyte/solve.py --hostname $target_dns --port $target_port
    target_port: 2323
    target_dns_subdomain: neophyte
    healthcheck_interval: 300
