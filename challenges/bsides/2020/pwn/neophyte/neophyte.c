#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#define FLAG_LEN 0x200

void readFlag() {
    char buffer[128] = {0};
    FILE * flag_file = fopen("flag.txt", "r");
    if (flag_file)
    {
        fread(buffer, 1, sizeof(buffer), flag_file);
        fclose(flag_file);
        printf("%s\n", buffer);
    }
    else
    {
        printf("Good start, you have got execution locally. Now try it against the competition server\n");
    }
}

#define NAME_LEN 0x80
int main(void)
{
    char input[NAME_LEN]; // = {0};
    char *readString;
    setvbuf(stdin, 0LL, 2LL, 0LL);
    setvbuf(stdout, 0LL, 2LL, 0LL);

    puts("So you want to learn to pwn?\nIt's actually not as hard as you might think.\nAll you need to do is fill my buffer.\nEnter your buffer here:"); 
    readString = gets(input);

    if(readString != NULL) {
        printf("Ok. I have your buffer, now lets see what happens....\n"); 
    }
    return 0;
}
