# Flux walkthrough

This is an AVR firmware challenge. I'm pretty sure everything should 
work on real hardware but have only tested it so far with the simulator.


## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>

Have you looked closely at the interrupts?
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>

Have you read the maunal carefully? What does the memory map look like?
</details>

## Steps

### 1. The bug
<details>
<summary>Spoiler warning</summary>

The main bug is that the UART read interrupt can over flow the buffer into the write pointer.
The pointer value check means that it should not be possible to move the pointer to a location
after the buffer.
</details>

### 2. Part One
<details>
<summary>Spoiler warning</summary>

There may be other possible solutions but the expected one is to move the pointer to one of the
memory mapped registers. In my solve script I wrote a fake stack into the user buffer and then
moved the pointer to the Stack Pointer location in memory and changed the SP to point to my fake
stack. The fake stack contained a ROP chain that printed out the EEPROM memory for the first flag.

</details>


### 3. Part Two
<details>
<summary>Spoiler warning</summary>

The second part requires the attacker to use the bootloader. This one is primarily a reversing 
challenge. The commands are fairly straightforward but the player needs to recognise the use of
a 16 bit CRC and identify the polynomial used.

Once they understand the bootloader the player will need to disable the WDT before jumping into the
bootloader. The WDT init function provides a single ROP gadget to do this.

The player will have to compile a simple program that reads the Flash data and writes it out
to the UART.

</details>

## Sources

- [ATtiny2313 Datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-2543-AVR-ATtiny2313_Datasheet.pdf)
- [AVR Libc reference](https://www.nongnu.org/avr-libc/user-manual/modules.html)
