
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wchar.h>
#include <locale.h>

#define MAX_KEY_LEN 250
#define MAX_DATA_LEN 0x1000
#define MAX_ALLOCATIONS 5
#define NEW_ITEMS 0x30

#define FORMAT(S) "%" #S "ls\n"
#define RESOLVE(S) FORMAT(S)

typedef struct item {
    struct item *next;
    struct item *prev;
    char key[(MAX_KEY_LEN + 1 ) * 2];
    size_t dataLen;
    uint8_t *data;
} item, *pItem;


pItem freeListHead = NULL;
void *allocations[MAX_ALLOCATIONS] = { 0 };
size_t allocationIndex = 0;

bool createNewItemSpace(size_t num) {
    bool status = false;
    pItem curr = NULL;
    pItem next = NULL;
    if(allocationIndex < MAX_ALLOCATIONS) {
        allocations[allocationIndex] = malloc(sizeof(item) * num);
        if(allocations[allocationIndex]) {
            memset(allocations[allocationIndex], 0, sizeof(item) * num);
            curr = (pItem)allocations[allocationIndex];
            freeListHead = curr;
            allocationIndex++;
            // Leave ->next NULL on last one. hence the -1
            for(int i = 0; i < num - 1; i++) {
                next = curr + 1;
                curr->next = next;
                next->prev = curr;
                curr = next;
            }
            status = true;
        }
    }
    return status;
}

void freeSpaceAllocations() {
    for(int i = 0; i < MAX_ALLOCATIONS; i++) {
        if(allocations[i] != NULL) {
            free(allocations[i]);
            allocations[i] = NULL;
        }
    }
}

pItem createNewItem() {
    pItem newItem = NULL;
    if( freeListHead == NULL) {
        createNewItemSpace(NEW_ITEMS);
    }
    if( freeListHead != NULL) {
        newItem = freeListHead;
        freeListHead = freeListHead->next;
        newItem->next = NULL;
        newItem->prev = NULL;
        newItem->data = NULL;
        newItem->dataLen = 0;
    }
    return newItem;
}

void deleteItem(pItem toRemove) {
    pItem currHead = freeListHead;
    freeListHead = toRemove;

    if( currHead == NULL) {
        toRemove->next = toRemove;
    } else {
        toRemove->next = currHead;
    }
    if(toRemove->data) {
        free(toRemove->data);
        toRemove->data = NULL;
    }
    toRemove->dataLen = 0;
    toRemove->prev = NULL;
}

static item itemHash[0xFF];

const size_t ITEM_SIZE = sizeof(struct item) + MAX_DATA_LEN;

static void initKey() {
    for(int index = 0; index < (sizeof(itemHash) / sizeof(itemHash[0])); index++) {
        memset(&itemHash[index], 0, sizeof(struct item));
        itemHash[index].next = &itemHash[index];
        itemHash[index].prev = &itemHash[index];
    }
}

static bool insertItem(pItem newItem) {
    bool status = false;
    pItem cur = NULL;
    if(newItem) {
        uint8_t keyIndex = (uint8_t)(newItem->key[0]);
        if(keyIndex >= 0 && keyIndex < (sizeof(itemHash) / sizeof(itemHash[0]))){
            
            cur = itemHash[keyIndex].prev;
            cur->next = newItem;
            newItem->prev = cur;
            newItem->next = &itemHash[keyIndex];
            itemHash[keyIndex].prev = newItem;
            status = true;
        }
    }
    return status;
}

static pItem findItem(wchar_t *key) {
    pItem cur = NULL;
    uint8_t keyIndex = (uint8_t)(key[0]);
    if(keyIndex >= 0 && keyIndex < (sizeof(itemHash) / sizeof(itemHash[0]))){
        cur = ((pItem)&itemHash[keyIndex]);
        do {
            if(wcsncmp((wchar_t *)cur->key, key, wcslen(key)) == 0) {
                return cur;
            }
            cur = cur->next;
        } while(cur != &itemHash[keyIndex]);
    }
    return NULL;
}

static bool removeItem(wchar_t *key) {
    bool result = false;
    pItem toRemove = findItem(key);
    if(toRemove != NULL) {
        toRemove->prev->next = toRemove->next;
        toRemove->next->prev = toRemove->prev;
        deleteItem(toRemove);
        result = true;
    }
    return result;
}

static bool readData(uint8_t *buffer, size_t max, size_t *readLength) {
    uint8_t *currBuff = buffer;
    bool result = true;

    for (*readLength = 0; (*readLength < max) && (result == true); currBuff++, (*readLength)++){
        if(fread(currBuff, 1, 1, stdin) == 0) {
            result = false;
        } else {
            if (*currBuff == '\n') {
                *currBuff = '\x00';
                break;
            }
        }
    }
    // read newline if we read max.
    if(*readLength == max) {
        (void)getchar();
    }
    return result;
}

static bool printItem(pItem item) {
    printf("%ls: ", (wchar_t *)item->key);
    for(int i = 0; i < item->dataLen; i++) {
        fwrite(&item->data[i], 1, 1, stdout);
    }
    puts("");

    return true;
}

static bool getKeyName(wchar_t **name) {
    bool result = false;
    size_t len = (MAX_KEY_LEN + 1) * sizeof(wchar_t);
    size_t readLen = 0;
    *name = (wchar_t *)malloc(len);
    memset(*name, 0, len);
    if(*name) {
        printf("key name: ");
        result = readData((uint8_t *)*name, len, &readLen);
    }
    return result;
}

static bool delete() {
    wchar_t *key = NULL;
    bool result = false;
    result = getKeyName(&key);
    if(result) {
        result = removeItem(key);
        free(key);
    }
    return result;
}

static bool clone() {
    wchar_t *clone_key = NULL;
    wchar_t *new_key = NULL;
    pItem newItem = NULL;
    bool result = false;

    printf("clone ");
    result = getKeyName(&clone_key);

    if(result) {
        pItem toClone = findItem(clone_key);
        if(toClone) {
            printf("new ");
            result = getKeyName(&new_key);
            if(result) {
                // blow away old version
                removeItem(new_key);
                newItem = createNewItem();
                newItem->dataLen = toClone->dataLen;
                newItem->data = malloc(newItem->dataLen);
                if(newItem->data) {
                    memcpy(newItem->data, toClone->data, toClone->dataLen);
                    memcpy(&newItem->key, new_key, wcslen(new_key) * sizeof(wchar_t));
                    result = insertItem(newItem);
                }
            }
            free(new_key);
        }
        free(clone_key);
    }
    return result;
}

static bool get() {
    wchar_t *key = NULL;
    bool result = false;
    result = getKeyName(&key);
    if(result) {
        pItem toPrint = findItem(key);
        if(toPrint) {
            result = printItem(toPrint);
        }
        free(key);
    }
    return result;
}

static bool set() {
    wchar_t *key = NULL;
    char newLine = '\0';
    int dataLen = 0;
    bool result = false;
    pItem newItem = NULL;

    result = getKeyName(&key);
    if(result) {
        newItem = findItem(key);
        if(!newItem){
           newItem = createNewItem();
        }
        if(newItem) {
            printf("data len: ");
            scanf("%d", &dataLen);
            scanf("%c", &newLine);
            if(dataLen < MAX_DATA_LEN) {
                if(dataLen > newItem->dataLen){
                    free(newItem->data);
                    newItem->data = NULL;
                    newItem->dataLen = 0;
                }
                if(newItem->data == 0 ) {
                    newItem->data = malloc(dataLen);
                }
                printf("data: ");
                result = readData(newItem->data, dataLen, &newItem->dataLen);
                if(result) {
                    dataLen = wcslen(key);
                    memcpy(&newItem->key, key, dataLen * sizeof(wchar_t));
                    result = insertItem(newItem);
                }
            } else {
                printf("Buffer to big");
            }
        }
        free(key);
    }
    return result;
}


void freeItems(void) {
    pItem cur = NULL;
    for(int keyIndex = 0; keyIndex < 0xFF; keyIndex++) {
        cur = ((pItem)&itemHash[keyIndex])->next;
        while(cur != &itemHash[keyIndex]) {
            free(cur->data);
            cur = cur->next;
        }
    }
}

void help() {
    puts("g\tGet a Value");
    puts("s\tSet a Value");
    puts("d\tDelete a Value");
    puts("c\tClone a Value");
    puts("q\tQuit");
}

void menu() {
    char input[4];

    printf("> ");

    (void)fgets(input, 3, stdin);

    while (input[0] != EOF && input[0] != 'q') {

      switch(input[0]) {
          case 'g':
            get();
            break;
          case 's':
            set();
            break;
          case 'd':
            delete();
            break;
          case 'c':
            clone();
            break;
          case 'h':
            help();
            break;
          default:
            break;
      }

      printf("menu > ");
      fgets(input, 3, stdin);
    }
}



int main(int argv, char *argc[])
{
    setvbuf(stdin, 0LL, 2LL, 0LL);
    setvbuf(stdout, 0LL, 2LL, 0LL);

    initKey();
    puts("Welcome to Memcrashd_beta, a less simple remote key value store. Now with dynamic memory and support for wide string key names.");
    help();
    menu();
    freeItems();
    freeSpaceAllocations();
    return 0;
}
