#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>


#define MAX_HEXSTRING 200
#define NO_PADDING 0
#define AES_BLOCK_LEN_HEX 16*2

unsigned char *test_plain = (unsigned char *)"abcdef0123456789";
//unsigned char *test_key256 = (unsigned char *)"\xff\xe4\x41\x41\x41\x41\x41\x41";
unsigned char *test_key256 = (unsigned char *)"c\xbc\xb2\xe1m\xa6\xfe\xa6u\x0fRa\x07\xb2P)\xe5\xb7\xfb\x9d\x07\xc9)\x96\xff\xe4\x17\xd5<\xca\xf3Y";
unsigned char *test_iv = (unsigned char *)"BBBBBBBBBBBBBBBB";
//unsigned char *key256 = (unsigned char *)"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB";
//unsigned char *iv = (unsigned char *)"BBBBBBBBBBBBBBBB";



/*
   encrypt and decrypt from https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
   */

//Dirty hack to get stack alignment correct for stack based overflow
typedef struct { unsigned int dec_len; unsigned int  i; unsigned char decrypt_buffer[16*10]; } decrypt_and_print_locals;

unsigned int is_hex_string(unsigned char * s)
{
    return s[strspn((const char *)s, "0123456789abcdefABCDEF")] == 0;
}

unsigned int is_odd_length(unsigned char *s)
{
    return (strlen((const char *)s) & 1) == 0;
}

unsigned int is_multiple_of_blocklen(unsigned char *s)
{
    return (strlen((const char *)s) % AES_BLOCK_LEN_HEX) == 0;
}

void convert_hex_to_bytes(unsigned char *s, unsigned char *output)
{
    unsigned int i=0, len;
    unsigned int temp, ret;

    len = strlen((const char *)s);
    //printf("DEBUG: strlen(s) was: %d\n", len);

    for(i=0; i<len/2; i++)
    {
        //ret = sscanf((const char *)s[2*i], "%02x", &temp);
        ret = sscanf((const char *)s+2*i, "%02x", &temp);
        //printf("DEBUG: sscanf returned: %d\n", ret);
        //printf("[%d] - %c%c = %d\n", i, s[2*i], s[2*i+1], temp);
        if (1!=ret)
        {
            printf("ERROR in hex conversion. Exiting...\n");
            return;
        }
        output[i] = (unsigned char) (temp &0xff);
    }

    output[i] = 0; //NULL terminate string

    return;
}

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
    unsigned char *iv, unsigned char *ciphertext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    /*
     * Initialise the encryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
        handleErrors();

    /*
     * Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        handleErrors();
    ciphertext_len = len;

    /*
     * Finalise the encryption. Further ciphertext bytes may be written at
     * this stage.
     */
    if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        handleErrors();
    ciphertext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}

int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
    unsigned char *iv, unsigned char *plaintext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int plaintext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    /*
     * Initialise the decryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
        handleErrors();

    EVP_CIPHER_CTX_set_padding(ctx, NO_PADDING); //always returns 1


    /*
     * Provide the message to be decrypted, and obtain the plaintext output.
     * EVP_DecryptUpdate can be called multiple times if necessary.
     */
    if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
        handleErrors();
    plaintext_len = len;

    /*
     * Finalise the decryption. Further plaintext bytes may be written at
     * this stage.
     */
    if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len))
        handleErrors();
    plaintext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return plaintext_len;
}

int decrypt_and_print_hex(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
    unsigned char *iv)
{
    //unsigned int dec_len;
    //unsigned char decrypt_buffer[16*10];
    //unsigned int i;

    decrypt_and_print_locals l;

    l.dec_len = decrypt(ciphertext, ciphertext_len, key, iv, l.decrypt_buffer);

    //printf("DEBUG: dec_len is [%d]\n", l.dec_len);
    printf("Decrypted (hex): [");
    for(l.i=0;l.i<l.dec_len;l.i++)
    {
        printf("%02x", l.decrypt_buffer[l.i]);
    }printf("]\n");


    return 0;
}


int self_test()
{
    unsigned int ret, i;

    /*
     * Buffer for ciphertext. Ensure the buffer is long enough for the
     * ciphertext which may be longer than the plaintext, depending on the
     * algorithm and mode.
     */
    unsigned char ciphertext[128];

    /* Buffer for the decrypted text */
    unsigned char decryptedtext[128];
    int decryptedtext_len, ciphertext_len;

    //Test encrypt and decrypt to ensure engine is working...
    ciphertext_len = encrypt (test_plain, 16, test_key256, test_iv,
        ciphertext);

    //length should be 32
    if(32!=ciphertext_len)
    {
        printf("ERROR: Error in self-test (encryption)... exiting\n");
        return -1;
    }

    /*
       printf("DEBUG: ciphertext_len = %d : cipher = [", ciphertext_len);
       for(i=0;i<ciphertext_len;i++)
       {
       printf("%02x", ciphertext[i]);
       }	printf("]\n");
       */

    decryptedtext_len = decrypt(ciphertext, ciphertext_len, test_key256, test_iv,
        decryptedtext);

    //length should be 32
    if(32!=decryptedtext_len)
    {
        printf("ERROR: Error in self-test (decryption)... exiting\n");
        return -1;
    }

    /*
       printf("DEBUG: decrypted_len = %d : plain = [", decryptedtext_len);
       for(i=0;i<decryptedtext_len;i++)
       {
       printf("%02x", decryptedtext[i]);
       }	printf("]\n");
       */

    //printf("DEBUG: Comparing memory = %d\n", memcmp(test_plain,decryptedtext, 16));
    //Checking decryption works
    if(0!=memcmp(test_plain,decryptedtext, 16))
    {
        printf("ERROR: Error in self-test (plaintext != decrypt)... exiting\n");
        return -1;
    }

    return 0;
}
