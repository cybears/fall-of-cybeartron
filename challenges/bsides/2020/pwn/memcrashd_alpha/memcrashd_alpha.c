
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

size_t readBuffer(char* buffer, size_t max) {
    char *currBuff = buffer;
    char newLine;
    size_t length;
    for (length = 0; length < max; currBuff++, length++){
        fread(currBuff, sizeof(char), 1, stdin);
        if (currBuff[0] == '\n') {
            break;
        }
    }
    if( length == max) {
        scanf("%c", &newLine);
    }
    return length;
}

#define KEY_LEN 250
#define DATA_LEN 0x250
#define NUM_KEYS 10

#define FORMAT(S) "%" #S "s"
#define RESOLVE(S) FORMAT(S)

typedef struct data_item
{
    size_t len;
    char buffer[DATA_LEN];
} data_item;

typedef struct data
{
    data_item data[NUM_KEYS];
} data;

typedef struct key_item
{
    char key[KEY_LEN];
    bool in_use;
} key_item;

typedef struct keys
{
    key_item data[NUM_KEYS];
} keys;

data *data_buffer = NULL;
keys *key_buffer = NULL;

int get_index(char *key, bool free_if_not_found)
{
    int index = -1;
    int free = -1;
    for(int i=0; i < NUM_KEYS; i++) {
        if(!key_buffer->data[i].in_use && free == -1 ) {
            free = i;
        }
        if(key_buffer->data[i].in_use &&
           strcmp(key, key_buffer->data[i].key) == 0) {
               index = i;
               break;
        }
    }
    if(index == -1 && free_if_not_found) {
        return free;
    }
    else {
        return index;
    }
}

void set() 
{
    char input[DATA_LEN+1];
    char key[KEY_LEN+1];
    int msgLen = 0;
    char newLine = 0;
    size_t readLen = 0;
    volatile int index = -1;

    printf("key name: ");
    scanf(RESOLVE(KEY_LEN), key);
    scanf("%c", &newLine);

    index = get_index(key, true);
    if(index < 0) {
        puts("no slots left."); 
        return;
    }

    printf("buffer length for data: ");
    scanf("%d", &msgLen);
    scanf("%c", &newLine);

    if (msgLen < 0) {
        // can't trust dodgyness
        puts("I dont trust negative numbers. assuming you want a positive one, converting now...");
        msgLen = -msgLen; 
    }

    if (msgLen > DATA_LEN) {
        puts("Thats to big for my buffer, rejected!");
        return;
    }
    printf("Length validated, Now enter your buffer: ");
    
    readLen = readBuffer((char *)&input, msgLen);
    if(readLen != 0) {
        memcpy(key_buffer->data[index].key, key, strlen(key));
        key_buffer->data[index].in_use = true;
        memcpy(data_buffer->data[index].buffer, input, readLen);
        data_buffer->data[index].len = readLen;
    }
}

void delete() 
{
    char key[KEY_LEN+1];
    int index = -1;
    char newLine;

    printf("key name: ");
    scanf(RESOLVE(KEY_LEN), key);
    scanf("%c", &newLine);

    index = get_index(key, false);
    if(index < 0) {
        puts("key not found."); 
        return;
    }
    memset(key_buffer->data[index].key, '\0', KEY_LEN);
    key_buffer->data[index].in_use = false;
    memset(data_buffer->data[index].buffer, '\0', DATA_LEN);
    data_buffer->data[index].len = 0;
}

void clone() {
    char clone_key[KEY_LEN+1];
    char new_key[KEY_LEN+1];
    int clone_index = -1;
    int new_index = -1;
    char newLine;

    printf("clone key name: ");
    scanf(RESOLVE(KEY_LEN), clone_key);
    scanf("%c", &newLine);

    clone_index = get_index(clone_key, false);
    if(clone_index < 0) {
        puts("key not found."); 
        return;
    }

    printf("new key name: ");
    scanf(RESOLVE(KEY_LEN), new_key);
    scanf("%c", &newLine);

    new_index = get_index(new_key, true);
    if(new_index < 0) {
        puts("No slots found."); 
        return;
    }
    memcpy(key_buffer->data[new_index].key, new_key, strlen(new_key));
    key_buffer->data[new_index].in_use = true;
    memcpy(data_buffer->data[new_index].buffer, data_buffer->data[clone_index].buffer, data_buffer->data[clone_index].len);
    data_buffer->data[new_index].len = data_buffer->data[clone_index].len;
}
void get() 
{
    char key[KEY_LEN+1];
    int index = -1;
    char newLine;

    printf("key name: ");
    scanf(RESOLVE(KEY_LEN), key);
    scanf("%c", &newLine);

    index = get_index(key, false);
    if(index < 0) {
        puts("key not found."); 
        return;
    }

    printf("%s: ", key);
    fwrite(data_buffer->data[index].buffer, sizeof(char), data_buffer->data[index].len, stdout);
    puts("");
}

void help() {
    puts("g\tGet a Value");
    puts("s\tSet a Value");
    puts("d\tDelete a Value");
    puts("c\tClone a Value");
    puts("q\tQuit");
}

void menu() {
    char input[4];
    char newline = '0';
    char *readString = NULL;

    printf("> ");

    readString = fgets(input, 3, stdin);

    while (input[0] != EOF && input[0] != 'q') {

      switch(input[0]) {
          case 'g':
            get();
            break;
          case 's':
            set();
            break;
          case 'd':
            delete();
            break;
          case 'c':
            clone();
            break;
          case 'h':
            help();
            break;
          default:
            break;
      }

      printf("menu > ");
      readString = fgets(input, 3, stdin);
    }
}


void main(void)
{

    data_buffer = malloc(sizeof(data));
    key_buffer = malloc(sizeof(keys));

    setbuf(stdin, NULL);
    setbuf(stdout, NULL);
    setbuf(stderr, NULL);

    puts("Welcome to Memcrashd_aplha, a simple remote key value store");
    help();
    menu();
    free(data_buffer);
    free(key_buffer);
}
