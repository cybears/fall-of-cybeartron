#!/usr/bin/env bash

# Put your commands to run your challenge here
# Since this is a simple binary challenge
# that reads on stdin and writes to stdout/stderr
# we can just use the socat script

# This script will spin up our challenge in a safe
# way so that players can't stuff around and cause
# problems.
# It will also make our simple challenge bind
# to port 2323 so you can talk to it with
# netcat!

# We just export the BINARY env var to point
# to the binary we want to run under socat
BINARY=${BINARY:="./dist/chal"}

if [ ! -f "$BINARY" ]; then
    echo "Binary ${BINARY} does not exist! Have you built the challenge?"
    exit 1
fi

# And then we invoke the socat.sh script!

# If we're in the docker container the socat script
# will be in the root
if [ -f /socat.sh ]; then
    /socat.sh
    exit 0
fi

# If we're on a local machine, it'll be in the socat
# container directory, which we can find with git
BASE_DIR=`git rev-parse --show-toplevel`
$BASE_DIR/infra/services/socat/socat.sh

# All done! We'll spin and listen forever on port 2323,
# restarting the binary when it exits and forking a new
# copy for each connection a player makes.
# If you're running locally and want to kill this, hit
# Ctrl + Z to background the script, then do
# kill -9 %
# to kill the server.
