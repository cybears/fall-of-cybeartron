# memcrashd walkthrough

The is an implementation of common(possibly?) pitfalls with C++.

It is a progression of memcrashd_beta.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>

We are using smart pointers

</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>

Are smart pointers failsafe? What is the expectation of a shared_ptr?

</details>

3. Third hint
<details>
<summary>Spoiler warning</summary>

How do shared_ptr's track usage? How show they be shared (or cloned :))?

</details>

## Steps

Follow the hints above.

<details>
<summary>Spoiler warning</summary>

So we now know that the clone function does not use the shared_ptr properly and instead creates a new shared_ptr with the raw pointer from the original.

This means that we have two shared_ptr's pointing to the same object.

Therefor if we delete the original key after cloning it, the shared_ptr reference count will decrement to `0` and the object will be deleted. We now have a reference to freed memory with the cloned key. If we crafted the key's buffer to be the same size as a DataItem(key) class then we can create a new key and have the c++ object take the free'd (but referenced) memory. At this point we can read (get) the cloned object and it will leak the data from the new DataItem object. 

From there you can set the cloned objects data which will actually corrupt the DataItem class for the new key. By setting the dataPtr and size's in this class we can then set/get on the new key to get an arbitraty read/write.

A traditional C++ execution technique is a vTable overwrite but we don't have virtual functions in this project. Also the got is not writable. C++ programs still use libc so we can abuse libc. One well known technique is to target `__malloc_hook` or `__free_hook` which are called when malloc and free are called respectively and can be overwritten at runtime. `__free_hook` is interesting to us as when a key is deleted the data buffer will be deleted first. This is a string we completely control, therefor we will call `free` with an address that points to a string we control.

If we overwrite `__free_hook` to point to `system` and have a key with a data buffer of `/bin/sh` then we we delete that key we will call `system("/bin/sh")` :).

</details>
