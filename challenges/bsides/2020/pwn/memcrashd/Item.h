#include <iostream>
#include <memory>
#include <string>
#include <limits>

using std::cout;
using std::cin;
using std::string;
using std::endl;
using std::shared_ptr;

class DataItem {
 public:
  DataItem() : allocLength(0), dataLength(0), dataPtr(nullptr) {}

  DataItem(const DataItem& rhs) :
    allocLength(rhs.allocLength),
    dataLength(rhs.dataLength),
    dataPtr(rhs.dataPtr.get())
    // correct would be: dataPtr = rhs.dataPtr;
    {}
  bool valid() { return this->dataLength > 0; }

  void print(void) {
    for (size_t i = 0; i < this->dataLength; i++) {
      cout << dataPtr[i];
    }
    cout << endl;
  }

  void set(void) {
    cout << "data len: ";
    size_t inputDataLength;
    if (cin >> inputDataLength) {
      // clear out any remaining data on the line
      cin.clear();
      cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      if (inputDataLength > this->allocLength) {
        this->allocLength = inputDataLength;
        dataPtr = shared_ptr<uint8_t[]>(new uint8_t[inputDataLength]);
      }
      this->dataLength = inputDataLength;
      cout << "data: " << std::flush;
      for (size_t i = 0; i < this->dataLength; i++) {
        cin >> std::noskipws >>dataPtr[i];
      }
      
    } else {
        cout << "Invalid length" << endl;
    }
    // clear out any remaining data on the line
    cin.clear();
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }

 private:
  size_t allocLength;
  size_t dataLength;
  shared_ptr<uint8_t[]> dataPtr;
};
