#!/bin/bash

mkdir -p export/handout

mkdir build-mingw
pushd build-mingw

cmake -DCMAKE_TOOLCHAIN_FILE=../x86_64-w64-mingw32.cmake ..

cmake --build .

if [ ! -f Blinkenlights.exe ]
then
    echo "Failed to compile"
    exit 1
fi

cp Blinkenlights.exe ../export/handout

popd

