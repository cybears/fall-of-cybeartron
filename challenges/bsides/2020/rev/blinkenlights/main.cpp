/**
 * Blinkenlights
 **/

#include <windows.h>
#include <stdint.h>
#include <tchar.h>

 // Generated headers
#include <resource.h>
#include "flag.h"

// Cheat mode
// #define CTF_DEBUG

#ifdef CTF_DEBUG
#define CTF_DEBUG_PRINT OutputDebugString
#else
#define CTF_DEBUG_PRINT
#endif

const TCHAR kWindowClass[] = TEXT("VPNClientClass");
const TCHAR kWindowTitle[] = TEXT("Cybears VPN");

// Message used for tray icon notifications
const uint32_t kTrayIconMessage = WM_APP + 1;

// State
bool g_Connected = false;
uint32_t g_PosBits = 0;

// ID of timer used for morse code
const uint32_t kTimer = 1;
const uint32_t kBlinkInterval = 350;

const uint32_t kMaxPasswordLength = 64;

HINSTANCE g_hInstance = NULL;

/**
 * Login dialog box proc
 **/
BOOL CALLBACK LoginDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
    BOOL handled = FALSE;
    TCHAR password[kMaxPasswordLength] = { 0 };
    static TCHAR * main_window_password = NULL;

    switch (Message)
    {
    case WM_INITDIALOG:
        main_window_password = (TCHAR*)lParam;
        handled = TRUE;
        break;
    case WM_COMMAND:
        switch (LOWORD(wParam))
        {
        case IDOK:
            if (GetDlgItemText(hwnd, IDC_PASSWORD, password, ARRAYSIZE(password) - 1))
            {
                _tcsncpy_s(main_window_password, kMaxPasswordLength, password, _tcsclen(password));
                EndDialog(hwnd, IDOK);
            }
            break;
        case IDCANCEL:
            EndDialog(hwnd, IDCANCEL);
            break;
        default:
            break;
        }
        handled = TRUE;
    default:
        break;
    }
    return handled;
}

/**
 * Main window procedure
 **/
LRESULT CALLBACK WindowProc(HWND hwnd,
    UINT uMsg,
    WPARAM wParam,
    LPARAM lParam
)
{
    LRESULT result = 0;
    static NOTIFYICONDATA nid = { 0 };
    static HICON off_icon = 0;
    static HICON on_icon = 0;
    static HICON disconnected_icon = 0;
    static HMENU menu = 0;
    static TCHAR title[32] = { 0 };
    static TCHAR password[kMaxPasswordLength] = { 0 };
    TCHAR stringres[32] = { 0 };

    switch (uMsg)
    {
    case WM_CREATE:

        // Load required resources
        off_icon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_CONNECTED));
        on_icon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_LEFT));
        disconnected_icon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_DISCONNECTED));
        menu = LoadMenu(g_hInstance, MAKEINTRESOURCE(IDR_MENU1));
        menu = GetSubMenu(menu, 0);
        LoadString(g_hInstance, IDS_CONNECTED, title, ARRAYSIZE(title) - 1);

        // create the tray icon
        nid.cbSize = sizeof(nid);
        nid.uID = 1;
        nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
        nid.hWnd = hwnd;
        nid.uCallbackMessage = kTrayIconMessage;

        nid.hIcon = disconnected_icon;

        _tcscpy_s(nid.szTip, sizeof(nid.szTip) / sizeof(nid.szTip[0]), title);
        Shell_NotifyIcon(NIM_ADD, &nid);
        break;
    case kTrayIconMessage:
        switch (lParam)
        {
        case WM_RBUTTONUP:
        {
            POINT current_pos;
            GetCursorPos(&current_pos);
            TrackPopupMenu(menu, 0, current_pos.x, current_pos.y, 0, hwnd, NULL);
        }
        break;
        default:
            break;
        }
        break;
    case WM_COMMAND:
        switch (wParam)
        {
        case ID_TRAYMENU_CONNECTTOVPN:
            if (g_Connected)
            {
                LoadString(g_hInstance, IDS_ALREADYCONNECTED, stringres, ARRAYSIZE(stringres) - 1);
                MessageBox(hwnd, stringres, title, MB_ICONINFORMATION);
            }
            else
            {
                UINT result = DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_LOGIN), hwnd, (DLGPROC) LoginDlgProc, (LPARAM)&password);
                if (IDOK == result)
                {
                    // Check password
                    if (_tcslen(password) == 5
                        && _T('1') == password[0]
                        && _T('2') == password[1]
                        && _T('3') == password[2]
                        && _T('4') == password[3]
                        && _T('5') == password[4])
                    {
                        g_Connected = true;
                        LoadString(g_hInstance, IDS_NOWCONNECTED, stringres, ARRAYSIZE(stringres) - 1);
                        MessageBox(hwnd, stringres, title, MB_ICONINFORMATION);
                        SetTimer(hwnd, kTimer, kBlinkInterval, NULL);
                    }
                    else
                    {
                        LoadString(g_hInstance, IDS_BADPASSWORD, stringres, ARRAYSIZE(stringres) - 1);
                        MessageBox(hwnd, stringres, title, MB_ICONERROR);
                    }
                }
            }
            break;
        case ID_TRAYMENU_EXITVPNCLIENT:
            SendMessage(hwnd, WM_CLOSE, 0, 0);
            break;
        default:
            break;

        }
        break;
    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;
    case WM_DESTROY:
        // remove the tray icon
        nid.cbSize = sizeof(nid);
        nid.uID = 1;
        Shell_NotifyIcon(NIM_DELETE, &nid);

        PostQuitMessage(0);
        break;
    case WM_TIMER:
    {
        // Get the current bit
        char this_char = g_Message[g_PosBits / 8] ^ (uint8_t)(password[2]);
        char this_bit = 1 & (this_char >> (7 - (g_PosBits % 8)));

        if (this_bit)
        {
            CTF_DEBUG_PRINT(TEXT("ONE\n"));
            nid.hIcon = on_icon;
        }
        else
        {
            CTF_DEBUG_PRINT(TEXT("ZERO\n"));
            nid.hIcon = off_icon;
        }
        g_PosBits++;
        if (g_PosBits >= g_MessageBits)
        {
            // Stop the timer
            KillTimer(hwnd, kTimer);
            g_PosBits = 0;
            g_Connected = false;

            // Reset icon
            nid.hIcon = disconnected_icon;

            // Display message box
            LoadString(g_hInstance, IDS_NOWDISCONNECTED, stringres, ARRAYSIZE(stringres) - 1);
            MessageBox(hwnd, stringres, title, MB_ICONINFORMATION);
        }

        Shell_NotifyIcon(NIM_MODIFY, &nid);
    }
    break;
    default:
        result = DefWindowProc(hwnd, uMsg, wParam, lParam);
        break;
    }

    return result;
}

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, TCHAR *lpCmdLine, int nCmdShow)
{
    WNDCLASSEX window_class = { 0 };
    g_hInstance = hInstance;

    // Create window class
    window_class.cbSize = sizeof(window_class);
    window_class.lpszClassName = kWindowClass;
    window_class.lpfnWndProc = WindowProc;
    window_class.style = CS_HREDRAW | CS_VREDRAW;                   // redraw if size changes
    window_class.hInstance = hInstance;                             // handle to instance
    window_class.hIcon = LoadIcon(NULL, IDI_APPLICATION);           // predefined app. icon
    window_class.hCursor = LoadCursor(NULL, IDC_ARROW);             // predefined arrow
    window_class.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);

    if (!RegisterClassEx(&window_class))
    {
        OutputDebugString(TEXT("Could not register window class"));
        return 1;
    }

    // Create window
    HWND main_window = CreateWindowEx(WS_EX_CLIENTEDGE, kWindowClass, kWindowTitle, WS_OVERLAPPEDWINDOW, 0, 0, 64, 64, NULL, NULL, hInstance, NULL);

    if (!main_window)
    {
        OutputDebugString(TEXT("Could not create window"));
        return 1;
    }

    //ShowWindow(main_window, SW_SHOW);
    //UpdateWindow(main_window);

    // Start message loop
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0) != 0)
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}
