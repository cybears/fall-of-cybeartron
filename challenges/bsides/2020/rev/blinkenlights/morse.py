#!/usr/bin/env python3
# Blinkenlights flag generator

import sys

DEFAULT_FLAG_MESSAGE = "SOS"
DEFAULT_DELIMITER = "/"
DEFAULT_XOR_KEY = 0

HEADER_TEMPLATE = r"""/**
 * Blinkenlights
 **/

#pragma once
#include <stdint.h>

// Flag: {1}
// XOR key: {2:x}
const uint8_t g_Message[] = {{ {0} }};
const uint32_t g_MessageBits = sizeof(g_Message) * 8;
const uint8_t g_XorKey = {2};
"""

CHAR_TO_MORSE = {
    "A": ".-",
    "B": "-...",
    "C": "-.-.",
    "D": "-..",
    "E": ".",
    "F": "..-.",
    "G": "--.",
    "H": "....",
    "I": "..",
    "J": ".---",
    "K": "-.-",
    "L": ".-..",
    "M": "--",
    "N": "-.",
    "O": "---",
    "P": ".--.",
    "Q": "--.-",
    "R": ".-.",
    "S": "...",
    "T": "-",
    "U": "..-",
    "V": "...-",
    "W": ".--",
    "X": "-..-",
    "Y": "-.--",
    "Z": "--..",
    "1": ".----",
    "2": "..---",
    "3": "...--",
    "4": "....-",
    "5": ".....",
    "6": "-....",
    "7": "--...",
    "8": "---..",
    "9": "----.",
    "0": "-----",
    ".": ".-.-.-",
    ",": "--..--",
    ":": "---...",
    ";": "-.-.-.",
    "!": "-.-.--",
    "?": "..--..",
    "'": ".----.",
    "\"": ".-..-.",
    "/": "-..-.",
    "-": "-....-",
    "+": ".-.-.",
    "(": "-.--.",
    ")": "-.--.-",
    "@": ".--.-.",
    "=": "-...-",
    "&": ".-...",
    "_": "..--.-",
    "$": "...-..-"
}


def string_to_morse(s, delimiter=DEFAULT_DELIMITER):
    """
    Convert a string to morse code
    """
    return delimiter.join(map(lambda c: CHAR_TO_MORSE[c.upper()], s))


def morse_to_bytes(m, delimiter=DEFAULT_DELIMITER):

    # A dash is 3x a dot
    # Letters of a word are separated by a space of duration 3x dots
    MORSE_TO_BITS_MAP = {
        "-": "111000",
        ".": "1000",
        delimiter: "00000000"
    }

    bits = "".join(map(lambda c: MORSE_TO_BITS_MAP[c], m))
    # align to eight bits
    bits += "0" * (8 - len(bits))

    # Convert to bytes
    bytes = []
    for offset in range(0, len(bits), 8):
        this_byte = bits[offset:offset+8]
        byte = int(this_byte, 2)
        bytes.append(byte)
    return bytes

def generate_flag_header():
    flag_message = DEFAULT_FLAG_MESSAGE

    if len(sys.argv) < 4:
        print("usage: %s <header> <flag> <xor-key>" % sys.argv[0])
        return
    else:
        output_filename = sys.argv[1]
        flag_message = sys.argv[2]
        xor_key = int(sys.argv[3], 16)

    print("Generating flag: %s" % flag_message)
    print("Writing result to: %s" % output_filename)
    flag_as_morse = string_to_morse(flag_message)

    print("Morse: %s" % flag_as_morse)

    morse_bytes = morse_to_bytes(flag_as_morse)

    morse_bytes_values = ", ".join(["0x%02x" % (x ^ xor_key) for x in morse_bytes])


    header = HEADER_TEMPLATE.format(morse_bytes_values, flag_message, xor_key)

    with open(output_filename, "w") as output_file:
        output_file.write(header)



if __name__ == "__main__":
    generate_flag_header()
