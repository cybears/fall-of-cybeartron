# Blinkenlights

A simple Windows reversing challenge. The binary looks like a VPN client: you enter a password to connect and the program creates a tray icon that looks like an old-school dial-up networking connection. It looks like it's connected, but is it?

## Build

Requirements:

- CMake 3.12+
- Python 3
- MinGW

The build script will use CMake with a toolchain file to build a 64-bit executable using mingw.
