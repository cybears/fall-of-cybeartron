import Foundation

enum ListDirContentType: String {
    case Directory = "📂"
    case File = "📄"
}

public struct ListDirectoryOptions: Codable {
    var path: String
    var maxSize: Int32
}

public struct ListDirectoryFileResult: Codable {
    public var path: String
    var size: size_t?
    var isDirectory: Bool?
    var isSymlink: Bool?
    var perms: Int32?
    var owner: String?
    var group: String?
    
    init(path: String) {
        self.path = path
    }
    
    init(path: URL) {
        self.path = path.path
    }
}

public struct ListDirectoryResult: Codable {
    public var path: String
    public var entries: [ListDirectoryFileResult]?
}

class ListDirectoryModule: Module {
    func run(_ options: ModuleOptions) -> ModuleReturnData {
        guard options.type == ModuleType.ListDirectory else {
            return .init(type: .ListDirectory, status: .BadType, data: nil)
        }
        let listdirOptions: ListDirectoryOptions
        do {
            try listdirOptions = PropertyListDecoder.init().decode(ListDirectoryOptions.self, from: options.data)
        } catch {
            return .init(type: .ListDirectory, status: .BadFormat, data: nil)
        }
        
        do {
            let rootPath = URL.init(fileURLWithPath: listdirOptions.path).resolvingSymlinksInPath().path
            var entries: [ListDirectoryFileResult] = .init()
            for name in try FileManager.default.contentsOfDirectory(atPath: rootPath) {
                let path = URL.init(fileURLWithPath: rootPath).appendingPathComponent(name)
                var isDir: ObjCBool = false
                if FileManager.default.fileExists(atPath: path.path, isDirectory: &isDir) {
                    var entry = ListDirectoryFileResult.init(path: path)
                    let attrs = try FileManager.default.attributesOfItem(atPath: entry.path)
                    entry.size = attrs[.size] as? size_t
                    entry.perms = attrs[.posixPermissions] as? Int32
                    entry.owner = attrs[.ownerAccountName] as? String
                    entry.group = attrs[.groupOwnerAccountName] as? String
                    if isDir.boolValue {
                        entry.isDirectory = true
                    }
                    entries.append(entry)
                }
            }
            
            let retData = try! self.getEncoder().encode(ListDirectoryResult.init(path: rootPath, entries: entries))
            return .init(type: .ListDirectory, status: .Success, data: retData)
        } catch {
            return .init(type: .ListDirectory, status: .Fail, data: nil)
        }
    }
}

let listDirModule = ListDirectoryModule.init()
