#define _GNU_SOURCE
#include <stdint.h>
#include <stdio.h>

#include <unistd.h>

#include <linux/fcntl.h>
#include <linux/memfd.h>

#include <sys/syscall.h>
#include <sys/mman.h>
#include <sys/wait.h>


// linux/fcntl.h and fcntl.h have conflicts -_-
extern int fcntl(int, int, ...);

int open_child_file(void);
int write_data(int, void *, size_t);
int run_child(int);

#if defined(STAGE_2) || defined(STAGE_3)
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

int decompress_data(int, void *, size_t, size_t);
#endif

#if defined(STAGE_1)
#include "archnemesis_2.h"

int run_stage(void) {
    int child_fd;
    size_t i;
    unsigned char key[8];

    printf("Hello from stage 1! Please enter the key: ");
    fflush(stdout);
    if (scanf("%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx",
              &key[0], &key[1], &key[2], &key[3],
              &key[4], &key[5], &key[6], &key[7]) != 8) {
        return 1;
    }

    for (i = 0; i < sizeof(archnemesis_2); i++) {
        archnemesis_2[i] ^= key[i % sizeof(key)];
    }

    if ((child_fd = open_child_file()) < 0) {
        return 1;
    }
    if (write_data(child_fd, archnemesis_2, sizeof(archnemesis_2))) {
        close(child_fd);
        return 1;
    }
    if (run_child(child_fd)) {
        close(child_fd);
        return 1;
    }

    while (wait(NULL) >= 0);

    close(child_fd);
    return 0;
}
#elif defined(STAGE_2)
#include "archnemesis_3.h"

int run_stage(void) {
    int child_fd;
    size_t i;
    unsigned char key[8];

    printf("Hello from stage 2! Please enter the key: ");
    fflush(stdout);
    if (scanf("%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx",
              &key[0], &key[1], &key[2], &key[3],
              &key[4], &key[5], &key[6], &key[7]) != 8) {
        return 1;
    }

    for (i = 0; i < sizeof(archnemesis_3); i++) {
        archnemesis_3[i] ^= key[i % sizeof(key)];
    }

    if ((child_fd = open_child_file()) < 0) {
        return 1;
    }

    if (decompress_data(child_fd, archnemesis_3, sizeof(archnemesis_3), ARCHNEMESIS_3_DECOMPRESSED_SIZE)) {
        close(child_fd);
        return 1;
    }

    if (run_child(child_fd)) {
        close(child_fd);
        return 1;
    }

    while (wait(NULL) >= 0);

    close(child_fd);
    return 0;
}
#elif defined(STAGE_3)
#include "archnemesis_4.h"

inline unsigned char lookup_index(char x) {
    if ('A' <= x && x <= 'Z') {
        return x - 'A';
    } else if ('a' <= x && x <= 'z') {
        return x - 'a' + 26;
    } else if ('0' <= x && x <= '9') {
        return x - '0' + 52;
    } else if (x == '+') {
        return 62;
    } else if (x == '/') {
        return 63;
    } else {
        return 255;
    }
}

int run_stage(void) {
    unsigned char x, y;
    int child_fd;
    size_t i;
    char key_in[12];
    unsigned char key[9];

    printf("Hello from stage 3! Please enter the key: ");
    fflush(stdout);
    if (scanf("%12c", key_in) != 1) {
        return 1;
    }

    for (i = 0; i < sizeof(key_in); i += 4) {
        // Byte 0
        x = lookup_index(key_in[i+0]);
        y = lookup_index(key_in[i+1]);
        if (x > 63 || y > 63) {
            return 1;
        }
        key[i * 3 / 4 + 0] = ((x & 0x3F) << 2) | ((y & 0x30) >> 4);

        // Byte 1
        if (key_in[i+2] == '=') {
            break;
        }
        x = y;
        y = lookup_index(key_in[i+2]);
        if (y > 63) {
            return 1;
        }
        key[i * 3 / 4 + 1] = ((x & 0x0F) << 4) | ((y & 0x3C) >> 2);

        // Byte 2
        if (key_in[i+3] == '=') {
            break;
        }
        x = y;
        y = lookup_index(key_in[i+3]);
        if (y > 63) {
            return 1;
        }
        key[i * 3 / 4 + 2] = ((x & 0x03) << 6) | ((y & 0x3F) >> 0);
    }
    for (i = 0; i < sizeof(archnemesis_4); i++) {
        archnemesis_4[i] ^= key[i % sizeof(key)];
    }

    if ((child_fd = open_child_file()) < 0) {
        return 1;
    }

    if (decompress_data(child_fd, archnemesis_4, sizeof(archnemesis_4), ARCHNEMESIS_4_DECOMPRESSED_SIZE)) {
        close(child_fd);
        return 1;
    }

    if (run_child(child_fd)) {
        close(child_fd);
        return 1;
    }

    while (wait(NULL) >= 0);

    close(child_fd);
    return 0;
}
#elif defined(STAGE_4)
int run_stage(void) {
    puts("Hello from stage 4! I have no more, you have defeated me!");
    puts("cybears{but_who_was_the_true_nemesis?}");
    fflush(stdout);
    return 0;
}
#endif

int open_child_file(void) {
    int child_fd;

    if ((child_fd = memfd_create("", MFD_ALLOW_SEALING)) < 0) {
#if EBUG
        perror("[-] failed to create in-memory file");
#endif
    }

    return child_fd;
}

int write_data(int child_fd, void *bin, size_t bin_size) {
    size_t pos;
    ssize_t written;

    pos = 0;

    while(pos < bin_size) {
        written = write(child_fd, &((char *)bin)[pos], bin_size - pos);
        if (written < 0) {
#if EBUG
            perror("[-] failed to write to memory");
#endif
            return 1;
        }
        pos += written;
    }

    return 0;
}

#if defined(STAGE_2) || defined(STAGE_3)
void *z_alloc(void *opaque, unsigned int items, unsigned int size) {
    return calloc((size_t)items, (size_t)size);
}

void z_free(void *opaque, void *address) {
    free(address);
}

int decompress_data(int child_fd, void *bin, size_t bin_size, size_t bin_out_size) {
    int ret;
    void *bin_out;
    z_stream z;

    if (ftruncate(child_fd, bin_out_size) < 0) {
#if EBUG
        perror("[-] failed to expand (truncate?) the file");
#endif
        return 1;
    }
    if ((bin_out = mmap(NULL, bin_out_size, PROT_WRITE, MAP_SHARED, child_fd, 0)) == MAP_FAILED) {
#if EBUG
        perror("[-] failed to map file into memory");
#endif
        return 1;
    }

    memset(&z, 0, sizeof(z_stream));
    z.zalloc = z_alloc;
    z.zfree = z_free;
    z.next_in = bin;
    z.avail_in = bin_size;
    z.next_out = bin_out;
    z.avail_out = bin_out_size;
    if ((ret = inflateInit2(&z, 15 + 16)) != Z_OK) {
        // 16 could be 32 for a bonus chal?
#if EBUG
        switch (ret) {
        case Z_MEM_ERROR:
            fprintf(stderr, "[-] ran out of memory during zlib init\n");
            break;
        case Z_VERSION_ERROR:
            fprintf(stderr, "[-] compiled against incompatible zlib version\n");
            break;
        case Z_STREAM_ERROR:
            fprintf(stderr, "[-] invalid parameters in header\n");
            break;
        default:
            fprintf(stderr, "[-] unknown error during zlib init: %d\n", ret);
            break;
        }
#endif
        munmap(bin_out, bin_out_size);
        return 1;
    }
    if ((ret = inflate(&z, Z_FINISH)) != Z_STREAM_END) {
#if EBUG
        switch (ret) {
        case Z_NEED_DICT:
            fprintf(stderr, "[-] need preset dict to inflate\n");
            break;
        case Z_DATA_ERROR:
            fprintf(stderr, "[-] error in inflate data: %s\n", z.msg);
            break;
        case Z_STREAM_ERROR:
            fprintf(stderr, "[-] error in the inflate stream\n");
            break;
        case Z_MEM_ERROR:
            fprintf(stderr, "[-] ran out of memory while inflating\n");
            break;
        case Z_BUF_ERROR:
            fprintf(stderr, "[-] ran out of output space while inflating\n");
            break;
        default:
            fprintf(stderr, "[-] unknown error while inflating: %d\n", ret);
            break;
        }
#endif
        inflateEnd(&z);
        munmap(bin_out, bin_out_size);
        return 1;
    }
    inflateEnd(&z);

    if (msync(bin_out, bin_out_size, MS_SYNC)) {
#if EBUG
        perror("[-] failed to sync file to disk");
#endif
        return 1;
    }
    munmap(bin_out, bin_out_size);

    return 0;
}
#endif

int run_child(int child_fd) {
    pid_t child_pid;
    char child_path[32]; // len("/proc/{32-bit pid}/fd/{32-bit fd}") -> 30 + NUL + 1 for good luck
    char *args[] = { "/noob/run", child_path, NULL };

#ifdef QEMU_MASTER
    // This requires commit 2bb963f in QEMU: still not released as of Mar 2021
    int err;

    err = fcntl(child_fd, F_ADD_SEALS,
                F_SEAL_SEAL | F_SEAL_WRITE | F_SEAL_SHRINK | F_SEAL_GROW);
    if (err < 0) {
#if EBUG
        perror("[-] failed to seal in-memory file");
#endif
        return 1;
    }
#endif

    child_pid = fork();
    if (child_pid < 0) {
#if EBUG
        perror("[-] fork-ing heck");
#endif
        return 1;
    } else if (child_pid == 0) {
        snprintf(child_path, sizeof(child_path), "/proc/%d/fd/%d", getpid(), child_fd);
        if (execve("/noob/run", args, environ)) {
#if EBUG
            perror("[-] failed to execve in child");
#endif
            return 1;
        }
    }

    return 0;
}

int main(int argc, char **argv) {
    if (run_stage()) {
        puts("Sorry, that was wrong.");
        fflush(stdout);
        return 1;
    }

    return 0;
}
