#import <Foundation/NSObject.h>

enum Move {
    SCISSORS=0,
    PAPER=1,
    ROCK=2
};

@interface Player: NSObject {
    unsigned int numMoves;
    unsigned int numGames;
    unsigned int numWins;
    char name[128];
    unsigned int prevMove;
    unsigned int moveMatrix[3][3];
}

-(void) setName: (char *) name;
-(enum Move) getComputerMove;
-(int) playMove:(enum Move)player computerMove:(enum Move)computer;
-(void) printStats;

@end
