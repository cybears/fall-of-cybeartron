#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "Player.h"

@implementation Player

-(void) setName: (char *) n {
    strcpy(name, n);
}

-(enum Move) getComputerMove {
    long r;
    unsigned int prevMoveSum;

    r = random();
    prevMoveSum = moveMatrix[prevMove][SCISSORS] + moveMatrix[prevMove][PAPER] + moveMatrix[prevMove][ROCK];
    if (prevMoveSum >= 2) {
        if (r < 2147483647ull * moveMatrix[prevMove][SCISSORS] / prevMoveSum) {
            return ROCK;
        } else if (r < 2147483647ull * (moveMatrix[prevMove][SCISSORS] + moveMatrix[prevMove][PAPER]) / prevMoveSum) {
            return SCISSORS;
        } else {
            return PAPER;
        }
    } else {
        if (r < 2147483647ull / 3) {
            return ROCK;
        } else if (r / 2 < 2147483647ull / 3) {
            return SCISSORS;
        } else {
            return PAPER;
        }
    }
}


-(int) playMove:(enum Move)playerMove computerMove:(enum Move)computerMove {
    if (numMoves >= 1) {
        moveMatrix[prevMove][playerMove]++;
    }
    prevMove = playerMove;
    numMoves++;

    // Tie: no result yet
    if (playerMove == computerMove) {
        return 0;
    }

    // We have a result
    numGames++;
    if ( (playerMove == SCISSORS && computerMove == PAPER   ) ||
         (playerMove == PAPER    && computerMove == ROCK    ) ||
         (playerMove == ROCK     && computerMove == SCISSORS) ) {
        numWins++;
        return 1;
    }

    return -1;
}

-(void) printStats {
    printf("Current player: %s\n", name);
    printf("%u moves, %u games, %u wins\n", numMoves, numGames, numWins);
    printf("Last Move: %s\n", prevMove == SCISSORS ? "Scissors" : prevMove == PAPER ? "Paper" : "Rock");
    printf("         |  Scissors  |    Paper   |    Rock   \n");
    printf("Scissors | %10u | %10u | %10u\n",
           moveMatrix[SCISSORS][SCISSORS],
           moveMatrix[SCISSORS][PAPER],
           moveMatrix[SCISSORS][ROCK]);
    printf("   Paper | %10u | %10u | %10u\n",
           moveMatrix[PAPER][SCISSORS],
           moveMatrix[PAPER][PAPER],
           moveMatrix[PAPER][ROCK]);
    printf("    Rock | %10u | %10u | %10u\n",
           moveMatrix[ROCK][SCISSORS],
           moveMatrix[ROCK][PAPER],
           moveMatrix[ROCK][ROCK]);
}

@end
