# Carton

Carton is a Microsoft Xbox game reverse engineering challenge. To solve the challenge, players need to enter a password to get the flag.

## Design

Carton has a number of features to make reverse engineering challenging:

* All dependencies are statically linked resulting in a large binary file (about 1.5MB)
* Lack of Xbox emulation options makes dynamic reversing difficult
* Object-oriented design and use of C++ virtual functions makes static reversing difficult
* Steganography is used to hide some components of the challenge

## Build

Carton is built using the [nxdk open-source toolchain for Xbox homebrew development](https://github.com/XboxDev/nxdk). It uses the following open-source projects:

* nxdk - build toolchain
* SDL 2.0 - provided by nxdk
* WjCryptLib - public domain encryption library

A Docker container is included that will set up the build environment and build the core nxdk libraries (eg. the CRT, pbkit, SDL) so these don't have to be built every time CI runs.

The challenge can also be built for Windows, as it is much easier to debug on Windows with Visual Studio than a real Xbox with no debugger.

### Manual install of dependencies

* Install all of the dependencies required for nxdk
* Clone the nxdk repo
* Update all of the submodules for the nxdk repo
* Install Python 3.6+
* (optional) Set up and enter a [virtualenv](https://docs.python-guide.org/dev/virtualenvs/)
* Install required Python modules (`pip install -r requirements.txt`)
* Set `NXDK_DIR` to the path to nxdk
* Set `EXTRACT_XISO` to `$NXDK_DIR/tools/extract-xiso/build/extract-xiso` (or path to a pre-built extract-xiso. This will be built as part of nxdk.)

### Build for Xbox

* Set `NXDK_DIR` to the path to a checkout of the nxdk repo (already done in the Docker container)
* Run `make`. This should produce the following outputs:
    * The `bin` directory containing the executable `default.xbe` and related asset files
    * The final ISO: `carton.iso`

### Build for Windows

The Windows build uses CMake to generate a Visual Studio project. The Hunter package manager is used to download and build the SDL2 graphics library.

```
mkdir build
cd build
cmake ..
cmake --build .
```

## Debugging

The challenge must run on a modded Xbox. The challenge files can be copied from the `bin` directory to the Xbox via FTP provided by most third-party Xbox dashboards.

The challenge cannot be debugged on a standard (retail) Xbox. The challenge will create a log file which may assist with diagnosing problems. Note that the log file is only created if running the challenge from the hard drive.

## Testing

An automated test script is included which will check that the final ISO contains the expected files, and that the files contain the code that validates the password.

The test script uses the [Unicorn emulator](https://www.unicorn-engine.org/) to execute the password shellcode and check that it works as expected.

To run the test, execute `python test.py path-to-carton.iso`.
