#pragma once

/**
 *  @brief Validate the user-provided password.
 */
int ValidatePassword(const char * password);
