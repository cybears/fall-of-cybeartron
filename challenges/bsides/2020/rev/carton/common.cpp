/**
 * Carton: Font and bitmap utilities
 **/
#include "game.h"
#include <cstring>

CFont * LoadFont(SDL_Renderer * renderer, const char * filename, const uint32_t cell_width, const uint32_t cell_height)
{
    CFont * new_font = nullptr;

    // Load the bitmap
    SDL_Surface * font_surface = SDL_LoadBMP(filename);
    if (font_surface == nullptr)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Loading font %s failed: %s", filename, SDL_GetError());
    }
    else
    {
        // Convert the bitmap into a 24-bit bitmap
        // BUG: This leaks the converted bitmap. This is intentional.
        SDL_PixelFormat * format = SDL_AllocFormat(SDL_PIXELFORMAT_RGB24);
        SDL_Surface * new_surface = SDL_ConvertSurface(font_surface, format, 0);
        SDL_FreeFormat(format);

        // Set transparent color
        SDL_SetColorKey(new_surface, TRUE, SDL_MapRGB(new_surface->format, 0, 0, 0));

        SDL_Texture * font_texture = SDL_CreateTextureFromSurface(renderer, new_surface);
        if (font_texture == NULL)
        {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Creating texture from font failed: %s", SDL_GetError());
        }
        else
        {
            // Create new font
            new_font = new CFont(renderer, font_texture, cell_width, cell_height);

            // Free the bitmap surface
        }

        // Free the original surface
        SDL_FreeSurface(font_surface);
    }

    return new_font;
}

CFont::CFont(SDL_Renderer * renderer, SDL_Texture * font_texture, const uint32_t cell_width, const uint32_t cell_height)
{
    this->Renderer = renderer;
    this->Texture = font_texture;
    this->CellWidth = cell_width;
    this->CellHeight = cell_height;
    this->BorderX = 0;
    this->BorderY = 0;

    uint32_t format;
    int access, w, h;
    if (SDL_QueryTexture(font_texture, &format, &access, &w, &h) == 0)
    {
        this->TextureHeight = h;
        this->TextureWidth = w;
    }
}

void CFont::SetBorder(const uint32_t border_x, const uint32_t border_y)
{
    BorderX = border_x;
    BorderY = border_y;
}

void CFont::Measure(const char * text, uint32_t * width, uint32_t * height)
{
    uint32_t longest_line = 0;
    uint32_t this_line = 0;
    uint32_t number_of_lines = 0;

    for (uint32_t i = 0; i < strlen(text); i++)
    {
        if (text[i] == '\r' || text[i] == '\n')
        {
            number_of_lines++;
            if (this_line > longest_line)
            {
                longest_line = this_line;
            }

            this_line = 0;
        }
        else
        {
            this_line++;
        }
    }

    if (this_line > longest_line)
    {
        longest_line = this_line;
    }

    *width = this->CellWidth * longest_line;
    *height = this->CellHeight * number_of_lines;
}

void CFont::Render(const char * text, const uint32_t x, const uint32_t y)
{
    SDL_Rect dest_rect;

    int renderer_width, renderer_height;
    SDL_GetRendererOutputSize(this->Renderer, &renderer_width, &renderer_height);

    renderer_width -= BorderX;
    renderer_height -= BorderY;

    dest_rect.x = x;
    dest_rect.y = y;
    dest_rect.w = this->CellWidth;
    dest_rect.h = this->CellHeight;

    for (int i = 0; i < strlen(text); i++)
    {
        bool wrap = false;

        char c = text[i];

        // Calculate position in source bitmap
        SDL_Rect src_rect;
        char font_sheet_pos = (c - ' ');
        if (font_sheet_pos >= 0)
        {
            int chars_wide = this->TextureWidth / this->CellWidth;
            int posx = font_sheet_pos % chars_wide;
            int posy = font_sheet_pos / chars_wide;

            src_rect.x = posx * this->CellWidth;
            src_rect.y = posy * this->CellHeight;
            src_rect.w = this->CellWidth;
            src_rect.h = this->CellHeight;

            SDL_RenderCopy(this->Renderer, this->Texture, &src_rect, &dest_rect);

            dest_rect.x += this->CellWidth;
            wrap = (dest_rect.x >= renderer_width);
        }
        else if (c == '\r' || c == '\n')
        {
            wrap = true;
        }

        // Handle wrapping
        if (wrap)
        {
            dest_rect.x = x;
            dest_rect.y += this->CellHeight;
            if (dest_rect.y >= renderer_height)
            {
                dest_rect.y = BorderY;
            }
        }
    }
}
