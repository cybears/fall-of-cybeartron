/**
 * Carton: Checking password state.
 **/
#include "game.h"
#include <cstdio>
#include "egghunter.h"

void CCheckingPasswordState::HandleInput(SDL_Keycode keycode)
{
    // do nothing

}

CGameState * CCheckingPasswordState::Update()
{
    frames++;

    if (frames >= 120)
    {
        if (ValidatePassword(password) != 0)
        {
            game.GetPlatform().FlashLED(LedColor::kGreen, LedColor::kOff, LedColor::kGreen, LedColor::kOff);
            return new CGoodPasswordState(this->game, this->password);
        }
        else
        {
            game.GetPlatform().FlashLED(LedColor::kRed, LedColor::kOff, LedColor::kRed, LedColor::kOff);
            return new CBadPasswordState(this->game);
        }
    }
    else
    {
        return nullptr;
    }
}

void CCheckingPasswordState::Render()
{
    this->game.GetDefaultFont().Render("Checking password...", 32, 32);
}