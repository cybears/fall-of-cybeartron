import argparse
import os
import binascii
import shutil
import subprocess
import itertools
import unicorn
import struct
import sys
from PIL import Image


def extract_iso(iso_file, output_dir):
    """ Use extract-xiso to extract files from an Xbox ISO file. """
    path_to_extract_xiso = os.environ.get("EXTRACT_XISO", "extract-xiso.exe")
    command = [
        path_to_extract_xiso,
        "-d",
        output_dir,
        "-x",
        iso_file,
    ]
    print(command)
    subprocess.check_call(command)


def find_eggs_in_buffer(buffer, egg_size, number_of_eggs):
    """ Simulate the egghunter in the challenge """

    found_eggs = []

    for i in range(0, len(buffer) - 4 - egg_size):
        if buffer[i:i + 3] == b'EGG':
            which_egg = int(buffer[i + 3] - ord('a'))
            if 0 <= which_egg < number_of_eggs:
                # Found one!
                found_eggs.append((which_egg, buffer[i + 4:i + 4 + egg_size]))

    return found_eggs


def load_anticheat_file(anticheat_filename):
    """ Simulate loading the hash file """

    hash_buffer = b''
    with open(anticheat_filename, "r") as anticheat_file:
        for line in anticheat_file:
            this_hash = binascii.unhexlify(line.strip())
            if len(this_hash) == 16:
                hash_buffer += this_hash

    return hash_buffer


def load_and_convert_font_bitmap(font_filename):
    """ Simulate the font loader in the challenge opening the 256-color bitmap and converting it to 24-bit. """
    test_bitmap = Image.open(font_filename)

    # Convert the bitmap to RGB 24-bit
    converted_bitmap = test_bitmap.convert("RGB")
    raw_data = bytearray(itertools.chain.from_iterable(converted_bitmap.getdata()))
    return raw_data


def test_extracted_iso(extracted_path):
    # Check that all of the files were found in the extracted ISO
    ANTICHEAT_FILE = os.path.join(extracted_path, "anticheat.txt")
    FONT_FILE = os.path.join(extracted_path, "barcade.bmp")
    XBE_FILE = os.path.join(extracted_path, "default.xbe")

    EXPECTED_FILES = [ANTICHEAT_FILE, FONT_FILE, XBE_FILE]
    for expected_file in EXPECTED_FILES:
        if not os.path.isfile(expected_file):
            raise Exception("ISO missing file: %s" % expected_file)

    # Try to find the shellcode eggs in the data files
    # TODO: sync this with challenge source
    egg_size = 23
    number_of_eggs = 2

    anticheat_data = load_anticheat_file(ANTICHEAT_FILE)
    anticheat_eggs = find_eggs_in_buffer(anticheat_data, egg_size, number_of_eggs)
    if len(anticheat_eggs) != 1:
        raise Exception("Shellcode egg not found in anticheat file")

    font_data = load_and_convert_font_bitmap(FONT_FILE)
    font_eggs = find_eggs_in_buffer(font_data, egg_size, number_of_eggs)

    if len(font_eggs) != 1:
        raise Exception("Shellcode egg not found in font file!")

    # Assemble the shellcode egg
    shellcode_egg = bytearray(b'\xCC' * egg_size * number_of_eggs)
    for egg_number, egg_data in itertools.chain(anticheat_eggs, font_eggs):
        shellcode_egg[egg_number * egg_size:(egg_number + 1) * egg_size] = egg_data

    # If we have a block of CCs, we probably missed an egg
    if b'\xcc\xcc' in shellcode_egg:
        print(shellcode_egg)
        raise Exception("Shellcode eggs missing? TODO: manually check this")

    # Try the shellcode with the valid password
    shellcode_result = emulate_shellcode(shellcode_egg, "DIRECTXBORKS")
    if shellcode_result != 1:
        raise Exception("Shellcode failed to identify correct password")

    # Try the shellcode with an invalid password
    shellcode_result = emulate_shellcode(shellcode_egg, "notthepassword")
    if shellcode_result != 0:
        raise Exception("Shellcode failed to identify incorrect password")

    print("Tests passed!")


def emulate_shellcode(shellcode_egg, password):
    # Emulate the shellcode egg
    emulator = unicorn.Uc(unicorn.UC_ARCH_X86, unicorn.UC_MODE_32)

    # Create a memory block to hold the code
    CODE_ADDRESS = 0xA000
    CODE_PAGE_SIZE = len(shellcode_egg) + (4096 - (len(shellcode_egg) % 4096))
    emulator.mem_map(CODE_ADDRESS, CODE_PAGE_SIZE, unicorn.UC_PROT_ALL)
    emulator.mem_write(CODE_ADDRESS, b'\xcc' * CODE_PAGE_SIZE)
    emulator.mem_write(CODE_ADDRESS, bytes(shellcode_egg))

    # Create a memory block to hold the password
    PASSWORD_ADDRESS = 0x2000
    emulator.mem_map(PASSWORD_ADDRESS, 0x1000, unicorn.UC_PROT_READ | unicorn.UC_PROT_WRITE)
    emulator.mem_write(PASSWORD_ADDRESS, password.encode("utf-8") + b'\x00')

    # Create a memory block to hold the stack
    STACK_TOP = 0x1000
    emulator.mem_map(0, 0x1000, unicorn.UC_PROT_READ | unicorn.UC_PROT_WRITE)

    # Set up stack:
    END_ADDRESS = CODE_ADDRESS + CODE_PAGE_SIZE - 1
    STACK_POINTER = STACK_TOP - 8
    emulator.mem_write(STACK_POINTER, struct.pack("<I", END_ADDRESS))  # return address
    emulator.mem_write(STACK_POINTER + 4, struct.pack("<I", PASSWORD_ADDRESS))  # arg 0: address of password

    # Set up registers
    emulator.reg_write(unicorn.x86_const.UC_X86_REG_EAX, 0xF0F0F0F0)
    emulator.reg_write(unicorn.x86_const.UC_X86_REG_EBX, 0)
    emulator.reg_write(unicorn.x86_const.UC_X86_REG_ECX, 0)
    emulator.reg_write(unicorn.x86_const.UC_X86_REG_EDX, 0)
    emulator.reg_write(unicorn.x86_const.UC_X86_REG_ESI, 0)
    emulator.reg_write(unicorn.x86_const.UC_X86_REG_EDI, 0)
    emulator.reg_write(unicorn.x86_const.UC_X86_REG_ESP, STACK_TOP - 8)
    emulator.reg_write(unicorn.x86_const.UC_X86_REG_EBP, STACK_TOP - 8)

    emulator.emu_start(CODE_ADDRESS, END_ADDRESS)

    return_value = emulator.reg_read(unicorn.x86_const.UC_X86_REG_EAX)
    return return_value


if __name__ == "__main__":
    if len(sys.argv) >= 2:
        iso_file = sys.argv[1]
    else:
        iso_file = ".\\carton.iso"

    iso_file = os.path.abspath(iso_file)
    print("Testing ISO: %s" % iso_file)

    # Extract the ISO
    test_dir = os.path.abspath(".\\testing")
    if os.path.isdir(test_dir):
        shutil.rmtree(test_dir)
    if not os.path.isdir(test_dir):
        os.mkdir(test_dir)
    extract_iso(iso_file, test_dir)

    # Test the extracted result
    test_extracted_iso(test_dir)
