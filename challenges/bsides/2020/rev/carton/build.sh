#!/bin/bash

echo NXDK: $NXDK_DIR
pushd $NXDK_DIR
NXDK_VERSION=`git log --pretty=format:'%h' -n 1`
popd
echo NXDK Version: $NXDK_VERSION

make

mkdir -p export/handout
mv carton.iso export/handout

md5sum bin/default.xbe
md5sum export/handout/carton.iso