import pygame
from spritesheet import Spritesheet


class NPC(pygame.sprite.Sprite):
    def __init__(self, image, frames):
        pygame.sprite.Sprite.__init__(self)
        self.load_frames(image, frames)
        self.orientation = 'Left'
        self.rect = self.idle_frames[0].get_rect()
        self.rect.midbottom = (1000, 239)
        self.current_frame = 0
        self.last_updated = 0
        self.current_image = self.idle_frames[0]
        self.box = pygame.Rect(self.rect.x, self.rect.y, self.rect.w * 2, self.rect.h)
        self.box.center = self.rect.center
        self.near_player = False

    def draw(self, display):
        display.blit(self.current_image, self.rect)

    def update(self):
        if self.near_player:
            self.orientation = 'Right'
        else:
            self.orientation = 'Left'

        self.animate()

    def animate(self):
        now = pygame.time.get_ticks()
        if now - self.last_updated > 200:
            self.last_updated = now
            self.current_frame = (self.current_frame + 1) % len(self.idle_frames)
            if self.orientation == 'Left':
                self.current_image = self.idle_frames_left[self.current_frame]
            else:
                self.current_image = self.idle_frames[self.current_frame]


    def load_frames(self, image, frames):
        npc_spritesheet = Spritesheet(image)
        self.idle_frames = []
        for i in range(frames):
            self.idle_frames.append(npc_spritesheet.parse_sprite(f'Idle-{i}.png'))
        self.idle_frames_left = []
        for frame in self.idle_frames:
            self.idle_frames_left.append(pygame.transform.flip(frame, True, False))