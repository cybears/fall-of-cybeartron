from twisted.internet import protocol, reactor, endpoints
from twisted.internet.task import LoopingCall
from clienthandler import ClientController
import json


class EnlightenProtocol(protocol.Protocol):

    def __init__(self):
        self.report_position = LoopingCall(self.sendPosition)

    def connectionMade(self):
        self.report_position.start(1)

    def sendPosition(self):
        cc = ClientController.getInstance()
        data = {'uid': str(cc.uid), "pos": cc.player_x}
        if ClientController.getInstance().player_x > 1400:
            self.transport.write(bytes(
                [0x47, 0x45, 0x54, 0x00, 0x45, 0x4e, 0x4c, 0x00, 0x49, 0x47, 0x48, 0x00, 0x54, 0x45, 0x4e, 0x45, 0x44]))
            return
        self.transport.write(bytes(json.dumps(data), encoding='utf-8'))


    def dataReceived(self, data: bytes):

        try:

            raw_data = json.loads(data.decode())
            if 0 in raw_data:
                ClientController.getInstance().blit_data.append(raw_data[0])
            else:
                ClientController.getInstance().handle_player_data(raw_data)

        except json.decoder.JSONDecodeError:
            return


class EnlightenFactory(protocol.ClientFactory):
    protocol = EnlightenProtocol

    def buildProtocol(self, addr):
        return EnlightenProtocol()
