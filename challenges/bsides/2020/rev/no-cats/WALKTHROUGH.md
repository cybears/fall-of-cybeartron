# Walkthrough

Defeat the game logic to allow your cat into the shrine.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>
Check out the following hint!

```
X MARKS THE SPOT.
```
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>
You can see your friends!

```
what details do you share with them?
```
</details>

## Steps

<details>
<summary>Spoiler warning</summary>

Your player has a number of attributes.

```
UID: a private identifier you provide to the server.
```
   ( The server also keeps track of you with a public uid, which it provides to the other players so that they cannot move you. )

```
Your X coordinate: The value that represents where your character is in the game world.
```
   The server does not do any validation of your characters location - this is the basis of the vulnerability.

Since you control the X coordinate, if you can find it in memory, you can change that value to control where your
character is. You can use the networking data to identify where your character is in space, and a memory editor like cheat engine to
find it and make those changes. The client game logic for preventing the user from moving is:
```python
# player.py
[...]
    def update(self):
      [...]
        if self.rect.x == 995 and (self.FACING_LEFT is False):
            self.velocity = 0
        elif self.rect.x < 250 and (self.FACING_LEFT is True):
            self.velocity = 0
        self.rect.x += self.velocity
        self.set_state()
        self.animate()
        [...]
```
This allows users to bypass the wall and still move around. If you move past the entry to the shrine, you'll trigger the game logic
to request the flag from the server. The flag appears as the window title to the game. 

