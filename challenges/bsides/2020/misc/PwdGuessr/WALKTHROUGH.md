# PwdGuessr walkthrough

This is a timing attack, with a twist.

In the classic timing attack, the attempts that return quickly are the
incorrect ones. Here it's the other way around, and you've got a fairly
strict time limit on each connection. Too many incorrect guesses and
you're toast!

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>

Be sure to actually look at what you can recover before the timeout.

Do you notice any patterns across multiple attempts?
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>

Recover the password one character at a time. If your wordlist has minor
differences from the challenge author's (e.g. in spelling) and you guess
a whole word at a time, you won't know where you're falling down.
</details>

## Steps

With the 'leaked' password check function, you should be able to derive
a simple algorithm for recovering the password:
* Try each one-character password until you find the one that doesn't
  incur the time penalty
* Now you know the first character of the password. Use that as a prefix
  for figuring out the second character of the password.
* Keep going until you have the whole password.

So let's try it out. When you access the challenge, you're presented
with a long-ish user ID, and a password prompt. You might try that
alogrithm, but the connection times out well before you complete the
password.

So you try again. But this time you get a different user ID. Trying what
you got last time as a prefix for further guesses doesn't work, so you
have to start again. By now though you should be comfortable that the
alphabet of possible password characters consists of lower-case letters
only.

But if you look at the partial passwords that you do recover, you notice
a pattern. The first half-dozen or so characters make up the name of a
day of the week. So maybe on your next attempt you start out guessing
only letters from the set {m, t, w, f, s}. When you have one or two
characters, you get the rest of that weekday for free.

So now you can look further into the password before the connection
times out. You find that the second component of each password is the
name of a Harry Potter or Star Wars film (with any numbers in the titles
spelled out in words). The third component is the name of a chemical
element. The fourth (and last) component is the letters of the NATO
phonetic alphabet - once you've figured out the first letter, you can
sail on through the remainder of the password with ease to recover the
flag.
