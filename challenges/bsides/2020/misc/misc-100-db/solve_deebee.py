import sqlite3
import argparse
from pwn import *

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge")
parser.add_argument('-l', '--local', help="The filename of the local tf.db")
args = parser.parse_args()


qA = "select year_introduced, sum(1) as s from toys group by year_introduced order by s desc limit 1;"
qB = "select toys.name, SUM(likes.like) as s from likes join toys on toys.id=likes.toyid group by toys.name order by s desc limit 1;"
qC = "select * from toys order by number desc limit 1;"

if args.remote:
        # Split out the host and port
        p = remote(*args.remote.split(':'))

        p.readuntil(b"sqlite> ")
        p.sendline(qA)
        dA = p.readuntil(b"sqlite> ")
        aA = dA.split(b"\n")[1].split(b"|")[0]

        p.sendline(qB)
        dB = p.readuntil(b"sqlite> ")
        aB = dB.split(b"\n")[1].split(b"|")[0]

        p.sendline(qC)
        dC = p.readuntil(b"sqlite> ")
        aC = dC.split(b"\n")[1].split(b"|")[1]

        answer = aA + aB + aC
        flag =  "cybears{" + answer.lower().replace(b" ", b"").replace(b"-", b"").decode("utf-8") + "}"

else:
        filename = args.local

        #c = sqlite3.connect("./handout/tf.db")
        c = sqlite3.connect(filename)
        cur = c.cursor()

        r = cur.execute(qA)
        AA = r.fetchone()[0]

        r = cur.execute(qB)
        AB = r.fetchone()[0]

        r = cur.execute(qC)
        AC = r.fetchone()[1]

        ans = str(AA)+AB+AC
        flag =  "cybears{" + ans.lower().replace(" ", "").replace("-", "") + "}"

EXPECTED_FLAG = "cybears{1990directhitoptimusprime}"

assert(flag == EXPECTED_FLAG)

print("SUCCESS! Flag found")

