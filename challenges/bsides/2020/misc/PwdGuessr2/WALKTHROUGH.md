# PwdGuessr2 walkthrough

Do the same as for PwdGuessr, but you have to be smart about choosing
your guesses.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>

Make sure the wordlists you're using are complete and correct. The lists
are the same between the two challenges.

It's not enough to know exactly what the possibilities are, you have to
make the guess that's most likely to be correct at every point. For
example, the first component (the day of the week) is twice as likely to
start with T or S as it is to start with any of the other letters.
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>

For the second part of the challenge, when you're given the password
lengths, rather than looking through each of the password component
lists to find what components have the right lengths to be able to
complete a password with a given prefix, it may pay to instead
pre-compute all passwords and store them by length.
</details>

## Steps

There's not a lot to have to discover here. The parameters have been
chosen such that simply choosing a letter at random from amongst the set
of possible next letters isn't efficient enough to get you past the
first part (although sometimes it does, which I don't understand).

So you might consider choosing a letter at random from amongst the
_list_ of possible next letters. Thus, a more-common next letter is more
likely to be chosen. This strategy is good enough to get you the first
(false) "flag", but not good enough for the second part when you are
given the length and far fewer guesses.

What you need to do is explicitly make guesses in order of most-likely
to least-likely. Combined with the information in the length of the
password which excludes many possibilities, that should be enough to
comfortably solve the challenge.

There is also a hidden flag, not referred to in any of the published
documentation, for solvers who manage to recover all of the passwords
without making any incorrect guesses. The only way to do this is to be
able to determine what the password is based only on the UUID that
you're given from the server. If you store the UUIDs across connections,
you may find that you get duplicates, and that these duplicates
correspond to the same password. So, you might try to hit the challenge
many times, storing the UUIDs that you see in the hope that you get a
batch of them all of which you've seen before. But that will take many
thousands of attempts. Instead, think about what's the dumbest thing the
challenge author could have done to generate a deterministic UUID for
each password - a hash. Indeed, if you study the UUIDs closely, you'll
see that they're malformed - a UUID should have certain bits fixed that
indicate what type of UUID it is, and these ones don't. So maybe you
just compare the MD5 hash of a password to that password's UUID - you
will find that they're byte-for-byte (modulo being formatted as a UUID)
identical. So now you can precompute all the possible passwords and
their UUIDs, and get a perfect 0-fail run.
