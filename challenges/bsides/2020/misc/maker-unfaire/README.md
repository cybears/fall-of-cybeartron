# maker-unfaire

How much can you do when all you can run is `make`?

The description/flavour text that will be shown to players is in the
[MANIFEST.yml](MANIFEST.yml) file. This README is a developer/post-event
focused description of the challenge.

You could include a walkthrough here but some people might not enjoy being
spoiled by the README, so we'd encourage you to include a separate
[WALKTHROUGH.md](Walkthrough) file instead.

## Post-event changes

Some changes were made to this challenge after BSides 2021 to block some
of the shortcuts that were found by players at that event. For posterity
the original files are kept in `src/` with suffix `_orig`, but future
instances should use the current files.

## Quick start

To build and test locally:
```bash
make test
```

To replicate the CI locally with docker:

```bash
make docker-test
```

This will:
- Build the containers
    - Dockerfile.builder - This knows how to compile the challenge
    - Dockerfile.challenge - This knows how to run the challenge
    - Dockerfile.healthcheck - This knows how to solve the challenge
- Build the challenge inside the builder container
- Create a network
- Spin up the challenge in a container
- Run the solver in a container against the challenge container

## Layout

There are a bunch of files here, it can be a bit scary at first!
So here is a walkthrough of the layout:

| Path | Description |
| ---- | ----------- |
| [Dockerfile.builder](Dockerfile.builder)       | This knows how to compile the challenge |
| [Dockerfile.challenge](Dockerfile.challenge)     | This knows how to run the challenge     |
| [Dockerfile.healthcheck](Dockerfile.healthcheck)   | This knows how to solve the challenge   |
| [Makefile](Makefile)                 | The standard Makefile for challenges    |
| [src](src)                      | This is where you put your code         |
| handout                  | This is where things you want to give to players go |
| dist                     | This is where server side things go     |
| build                    | This is where intermediate build bits go |
| README.md                | An internal README, not given to players until after the competition |
| [MANIFEST.yml](MANIFEST.yml)             | This is where all the info about the challenge goes, stuff like flags and the description given to players |
| [gitlab-ci.yml](gitlab-ci.yml)            | This is the CI configuration for this challenge, you shouldn't need to touch this. |
| [solve.py](solve.py) | An example challenge solver. You can rewrite this to solve your challenge |

## Default names

The CI system will try a few things to build and test your challenge:

To build a challenge it will try the following commands from within
the docker container it builds from [Dockerfile.builder](Dockerfile.builder).

- `./bin/chal.py --local build`
- `make build`
- `./build.py`
- `./build.sh`

The builder container is built in [gitlab-ci.yml](gitlab-ci.yml) in the
`builder:misc/maker-unfaire` job, and the compilation/building of maker-unfaire
happens in the `compile:misc/maker-unfaire` job. You may change this job
if you wish, but you should probably try to stick to changing these scripts
if you can. It'll just make it easier for you and others.

To test your challenge the CI will try to run the following in the container
it builds from [Dockerfile.healthcheck](Dockerfile.healthcheck):

- `./bin/chal.py --local test`
- `make test`
- `./solve.py`
- `./solve.sh`
- `./doit.py`
- `./doit.sh`

It will expose two environment variables, `${CHALLENGE_HOST}` and
`${CHALLENGE_PORT}` which will be filled in with the hostname and port
of an instance of [Dockerfile.challenge](Dockerfile.challenge) where your
challenge will be running. These varables should be used by your solver
to test the remote version of your challenge. See the default
[solve.py](solve.py) for an example of how to do this.
