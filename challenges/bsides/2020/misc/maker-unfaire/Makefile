all: local docker

local: clean build dist handout test
docker: docker-clean docker-build docker-test
build: dist handout

compile:
	# Compile/build any of the things you
	# need for the challenge
	mkdir -p build
# 	cp src/* build/

dist: compile
	mkdir -p dist
	# Copy things that are necessary on
	# the challenge server to dist
# 	cp build/* dist/
# 	cp flag.txt sshd_config requirements.txt dist/

handout: compile
	mkdir -p handout
	# Copy things you want to give to players
	# into the handout directory

run: dist
	# This runs the challenge, so we just need
	# stuff from dist.
	./run.sh

test: dist handout
	# Use the stuff in handout and dist to
	# solve the challenge
	./solve.py

# --- Docker releated items ---
CHALLENGE_NAME = maker-unfaire
CHALLENGE_CATEGORY = misc
CI_REGISTRY_IMAGE = registry.gitlab.com/cybears-private/fall-of-cybeartron
CHALLENGE_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}
BUILD_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}-builder
HEALTHCHECK_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}-healthcheck
REPO_DIR := $(shell git rev-parse --show-toplevel)
CHALLENGE_HOST = ${CHALLENGE_NAME}.ctf.cybears.io
CHALLENGE_PORT = 2323
CHALLENGE_NETWORK = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}

docker-build:
	# build the container that hosts the challenge
	docker build -t $(CI_REGISTRY_IMAGE)/$(CHALLENGE_IMAGE_TAG) -f Dockerfile.challenge .
	# build the container that solves the challenge
	docker build -t $(CI_REGISTRY_IMAGE)/$(HEALTHCHECK_IMAGE_TAG) -f Dockerfile.healthcheck .

docker-test: docker-clean docker-build
	# run the challenge in the background
	docker run -t -i --expose ${CHALLENGE_PORT} --hostname ${CHALLENGE_HOST} --name ${CHALLENGE_HOST} -d --rm ${CI_REGISTRY_IMAGE}/${CHALLENGE_IMAGE_TAG}
	# run the healthcheck
	docker run --rm --link ${CHALLENGE_HOST} --name ${HEALTHCHECK_IMAGE_TAG} -e CHALLENGE_HOST=${CHALLENGE_HOST} -e CHALLENGE_PORT=${CHALLENGE_PORT} ${CI_REGISTRY_IMAGE}/$(HEALTHCHECK_IMAGE_TAG)
	# Now we're done delete the bits we created
	docker rm -f ${CHALLENGE_HOST} || true

docker-run: docker-clean docker-build
	# Run the challenge in the foreground
	docker run -t -i --expose ${CHALLENGE_PORT} --hostname ${CHALLENGE_HOST} --name ${CHALLENGE_HOST} --rm ${CI_REGISTRY_IMAGE}/${CHALLENGE_IMAGE_TAG}
	docker rm -f ${CHALLENGE_HOST} || true

docker-clean: clean
	docker rm -f ${CHALLENGE_HOST} || true
	docker rm -f ${HEALTHCHECK_IMAGE_TAG} || true
	docker network rm ${CHALLENGE_NETWORK} || true

clean:
	rm -rf build/ dist/ handout/
