# This container is used to run your challenge
# It should run the challenge you build with the
# Dockerfile.builder container so that players
# can connect to it

# Here we set our default registry to point to gitlab
ARG CI_REGISTRY_IMAGE=registry.gitlab.com/cybears-private/fall-of-cybeartron
# And we use the socat container as our base, this can be any container you
# would like to base on.
FROM ${CI_REGISTRY_IMAGE}/socat-python

# Run commands to install your dependencies here
# Your "apt-get" and "apk add" commands should go here.
RUN apt-get update && apt-get install -y openssh-server
COPY src/sshd_config /etc/ssh/sshd_config
COPY src/requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# We make a directory that will contain our challenge binary
RUN mkdir -p /chal

# Copy in your assets you build in your builder container
# They should be in the build or dist directory
COPY src/flag.txt src/Makefile src/faux_shell.py /chal/

# chmod so everyone can read/execute the challenge
RUN chmod 555 /chal/faux_shell.py
# but make the Makefile and flag read-only
RUN chmod 444 /chal/Makefile /chal/flag.txt

# We want the challenge directory writable, but not readable
RUN chmod 733 /chal

# Add the user that folks will log in as.
RUN useradd cybears -d /chal -p '$6$xOteENTD$J1Q.Zvu/bUCml7cjVGOtCOzZlLGDLJzSV2COPhjgwNypP8Lh9mk9stFBOzBWmvwB3WNTULPZQAX3Zj9u5QF9X.'
#                               ^cybearsareawesome
RUN echo "Your password is cybearsareawesome" > /chal/usrpwd

RUN service ssh start
# I don't really understand why the CMD is not sufficient, it seems it needs the service start too.
CMD /usr/sbin/sshd -D