#!/usr/bin/env python3
import sys
import os
import shutil
import subprocess
import tempfile
import cmd2

CUSTOM_PROMPT = 'cybears@cybeartron:/home/ctf/challenge$ '


class OnlyMake(cmd2.Cmd):
    TARGETS = ['burninate', 'LoveNotWar', 'UpSexIsTheBest',
               'flagprereq', 'MeASandwich', 'flag', 'TheWorldABetterPlace']

    def __init__(self):
        # Disable the shortcuts (? = help, ! = shell etc)
        super().__init__(use_ipython=False, shortcuts={})
        self.prompt = CUSTOM_PROMPT
        # Disable all the builtins
        for command in ('do_shell', 'do_help', 'do_set', 'do_alias', 'do_edit', 'do_history', 'do_macro',
                        'do_py', 'do_run_pyscript', 'do_run_script', 'do__relative_run_script', 'do_shortcuts'):
            delattr(cmd2.Cmd, command)
        # Protect against CTRL-c
        self.quit_on_sigint = False
        # Disable redirection/piping to shell
        self.allow_redirection = False

    def run_cmd(self, *cmd):
        """
        Wrap a call to subprocess.run
        :param cmd:
        :return:
        """
        # res = subprocess.run(*cmd, capture_output=True)
        res = subprocess.run(*cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)  # Needed for python<3.7
        if res.stderr:
            self.perror(res.stderr.decode(), end='')
        if res.stdout:
            self.poutput(res.stdout.decode(), end='')
        # self.poutput(CUSTOM_PROMPT, end='')

    def do_make(self, args):
        if any([x in args for x in ('-f', '--file', '--makefile')]):
            self.poutput("Surely the only Makefile you'll ever need is right here!")
            # self.poutput(CUSTOM_PROMPT, end='')
            return
        self.run_cmd(args.argv)

    def help_make(self):
        self.run_cmd(('make', '-h'))

    def complete_make(self, text, line, begidx, endidx):
        # Only the targets can be completed - i.e. you can't go 'make -f <tab><tab>' to get a list of files.
        # I guess I could rewrite all of make's arguments into an argparser but I really don't want to have to do that.
        if not text:
            completions = self.TARGETS[:]
        else:
            completions = [t for t in self.TARGETS if t.startswith(text)]
        return completions

    def do_help(self, arg):
        # Implement custom help function for empty args - call out to super() for any args.
        if arg:
            super().do_help(arg)
        else:
            self.poutput('\nAvailable commands (type help <topic>)')
            self.poutput('======================================')
            self.poutput('help make quit\n')
        # self.poutput(CUSTOM_PROMPT, end='')

    def default(self, line):
        retval = cmd2.Cmd.default(self, line)
        self.poutput("Instead, here's a file listing:")
        self.run_cmd("ls")
        return retval


if __name__ == '__main__':
    # Do we have an SSH_ORIGINAL_COMMAND env var?
    if 'SSH_ORIGINAL_COMMAND' in os.environ:
        print("You appear to have invoked ssh with a command to run on the remote system.")
        print("Letting you continue will cause more problems than it solves.")
        sys.exit(2)
    # Set up a temporary directory
    with tempfile.TemporaryDirectory(dir='.') as d:
        # print('temp dir {}'.format(d))
        # Copy the makefile there
        shutil.copy2('Makefile', d)
        # cd there to run
        os.chdir(d)
        # print(CUSTOM_PROMPT, end='')
        # run the loop
        app = OnlyMake()
        app.cmdloop()
        # Move back up into the challenge directory so that the temp dir can be removed
        os.chdir('..')
