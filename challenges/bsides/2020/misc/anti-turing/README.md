# Anti-Turing

* _author_: Cybears:cipher
* _title_: Anti-Turing
* _points_: 200
* _tags_:  misc

## Flags
* `cybears{S0rry_1f_y0u_h@v3nt_h3@rd_@b0ut_R0k0s_B@s1l1sk}`

## Challenge Text

```markdown
BEEP BOOP Bzzzzzt We've had attempted infilitrations into our systems. Prove
that you're a robot and log in here BIP BIP BOOP

nc antituring.ctf.cybears.io 3141
```

## Attachments
* 

## Hints
* 
