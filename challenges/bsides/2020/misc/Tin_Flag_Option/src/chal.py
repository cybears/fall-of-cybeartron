#!/usr/bin/env python3

import random
from numpy import float32
from math import ldexp


def compute_distances(stride, targ, guess):
    f_dist_trav = float32(stride * guess)
    f_new_targ_pos = float32(f_dist_trav + targ)
    f_new_dist = f_new_targ_pos - f_dist_trav
    return f_new_dist, f_dist_trav


def check_guess(stride, targ, guess):
    new_targ_dist, dist_trav = compute_distances(stride, targ, guess)
    if new_targ_dist > 0:
        print("You move {0:1.8e} fp, but the light floats ahead of you, also moving {0:1.8e} fp, and it is now {1:1.8e} fp away. Perhaps you need to go farther.".format(dist_trav, new_targ_dist))
        return False
    else:  # We already checked that guess was positive
        assert new_targ_dist == 0
        # The number is big enough. If a smaller guess would also work, then the number is too big.
        if compute_distances(stride, targ, guess - 1)[0] > 0:
            return True
        else:
            print("Wow, that's a long way. You think you could probably catch the light, but you don't want to go any farther than necessary.")
            return False


def get_rand_float(range_start, range_stop):
    # Can't use the normal random functions, they don't take account of the distribution of floats themselves.
    mantissa = random.uniform(1, 1.9999)
    exponent = random.randint(range_start, range_stop - 1)
    return ldexp(mantissa, exponent)


def do_challenge(flg):
    for i, character in enumerate(flg):
        stride_len = float32(get_rand_float(-6, -5))  # ~0.5 to 1 meters in femtoparsecs
        targ_dist = float32(get_rand_float(-4, 2))  # ~1.8 to 120 meters in femtoparsecs
        intro_str = '''As you're walking your path, you are startled by the sudden appearance
of a glowing, floating point of light hovering {:1.8e} femtoparsecs in front of you.
Your stride length is {:1.8e} fp.'''.replace('\n', ' ')
        print(intro_str.format(targ_dist, stride_len))
        # Start the loop. For each character after the first, we'll break out after the first few iterations
        while True:
            # Get the guess, make sure it's valid. Set it to an invalid number at the start
            guess = -1
            prompt = "How many steps do you want to take? "
            while True:
                try:
                    guess = int(input(prompt))
                    if guess <= 0:
                        raise ValueError
                    break
                except ValueError:
                    print("You need to specify a positive integer")
                    continue

            if check_guess(stride_len, targ_dist, guess):
                print(
                    "You finally catch up with the light source, but no sooner have you done so than it briefly forms the shape of a '{}', before disappearing completely.".format(
                        character))
                break

            if i > 4:
                print("You're feeling tired, and decide to stop for the night.")
                return


# Load in the flag
with open('flag.txt', 'r') as f:
    flag = f.read().strip()

do_challenge(flag)
