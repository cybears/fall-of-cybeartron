# This is our container for compiling the challenge
# This should build everything we need for hosting or distributing
# the challenge, so that includes binaries, assets, and handouts

# This environment variable will contain the path to our
# container registry. It defaults to our private container registry
ARG REGISTRY_IMAGE=registry.gitlab.com/cybears/fall-of-cybeartron

FROM python:3-slim-buster

RUN apt-get update && apt-get install -y make && rm -rf /var/lib/apt/*

# Add commands here to install any dependencies you need to
# compile or build your stuff.
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
