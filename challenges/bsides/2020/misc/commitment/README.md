# Commitment

In this challenge we push a git tag, annotation or note up to the open source repo
on the day before the event containing the flag.

This challenge is not meant to be hard, it's just meant to advertise the open source
repo on Gitlab. 

## Commands to generate the tag

```sh
$ git checkout --orphan DO_NOT_PUSH_THIS_BRANCH
$ git reset     # --orphan stages all files from the start point
$ git commit --allow-empty --allow-empty-message -m ''
$ git tag -a commitment -m 'FLAG HERE'
$ git checkout commitment
$ git branch -D DO_NOT_PUSH_THIS_BRANCH
$ git push REMOTE commitment
```
