import binascii
import copy
import string

field_base = 8
fieldsize_y = field_base + 1
fieldsize_x = field_base * 2 + 1
#augmentation_string = " .o+=*BOX@%&#/^SE"
custom_alpha = " " + "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

def gen_randomart(digest_raw, augmentation_string=" .o+=*BOX@%&#/^SE"):
    aug_len = len(augmentation_string) - 1
    field = list()

    for x in range(fieldsize_x):
        field.append(list())
        for y in range(fieldsize_y):
            field[x].append(0)

    x = int(fieldsize_x / 2)
    y = int(fieldsize_y / 2)

    for digest_byte in digest_raw:
        for b in range(4):
            # Determine our movement in X
            if digest_byte & 0x1:
                x += 1
            else:
                x -= 1

            # Determine our movement in Y
            if digest_byte & 0x2:
                y += 1
            else:
                y -= 1

            # Don't go out of bounds
            x = max(x, 0)
            y = max(y, 0)
            x = min(x, (fieldsize_x - 1))
            y = min(y, (fieldsize_y - 1))

            # Augment the field
            if field[x][y] < (aug_len - 2):
                field[x][y] += 1
            digest_byte = digest_byte >> 2

    field[int(fieldsize_x / 2)][int(fieldsize_y / 2)] = aug_len - 1
    field[x][y] = aug_len

    output = ''
    for y in range(fieldsize_y):
        line = ''
        for x in range(fieldsize_x):
            line += augmentation_string[min(field[x][y], aug_len)]
        output += line + '\n'
    return output

def frame(art, top='RSA 2048', bottom='SHA256'):
    out = '+---[{}]----+\n'.format(top)
    for line in art.splitlines():
        out += '|' + line + '|\n'
    out += '+----[{}]-----+'.format(bottom)
    return out

if __name__ == '__main__':
    '''
    Generating public/private rsa key pair.
    Enter file in which to save the key (/Users/torgo/.ssh/id_rsa): ./test
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in ./test.
    Your public key has been saved in ./test.pub.
    The key fingerprint is:
    SHA256:Bxk9Qt02csEp1SO8Iw0R7CiK4YPBjplz0A83dYReLmE torgo@<REDACTED>.local
    The key's randomart image is:
    +---[RSA 2048]----+
    |       +++oBo+   |
    |      E +oO X o  |
    |..   + =o+ O + . |
    |o.+ o + o.o +    |
    |oB * o oS .. .   |
    |*.= o    .       |
    | o .             |
    |                 |
    |                 |
    +----[SHA256]-----+
    '''

    test_case = '''+---[RSA 2048]----+
    |       +++oBo+   |
    |      E +oO X o  |
    |..   + =o+ O + . |
    |o.+ o + o.o +    |
    |oB * o oS .. .   |
    |*.= o    .       |
    | o .             |
    |                 |
    |                 |
    +----[SHA256]-----+'''

    print("Test case")
    x = gen_randomart(binascii.a2b_base64('Bxk9Qt02csEp1SO8Iw0R7CiK4YPBjplz0A83dYReLmE='))
    print(frame(x))

    assert(frame(x) == test_case)


    print("Fieldsize of 8")
    field_base = 8
    fieldsize_y = field_base + 1
    fieldsize_x = field_base * 2 + 1
    print(frame(gen_randomart(b'cybears{asc11_4rt}', custom_alpha), 'CYBEARS', 'SIGIL'))


    print("Fieldsize of 30")
    field_base = 30
    fieldsize_y = field_base + 1
    fieldsize_x = field_base * 2 + 1
    print(frame(gen_randomart(b'cybears{asc11_4rt}', custom_alpha), 'CYBEARS', 'SIGIL'))
