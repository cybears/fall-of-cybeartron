# Get Ahead

[Waltkthough](walkthrough.md)

## Quick start

To build and test locally:
```bash
make
```

To replicate the CI locally with docker:

```bash
# For this to work, you’ll need to login with docker to pull the images
docker login registry.gitlab.com
# Once you’re logged in you can run the docker containers
make docker-test
```

This will:
- Build the containers
    - Dockerfile.challenge - This knows how to run the challenge
    - Dockerfile.healthcheck - This knows how to solve the challenge
- Create a network
- Spin up the challenge in a container
- Run the solver in a container against the challenge container, exporting
  environment to tell the solver where the challenge is hosted.
