# GetAHead walkthrough

This is a beginner web challenge based around incorrect handling of HEAD requests. The user is expected to examine the HTTP OPTIONS allowed by endpoints and then take advantage of the lack of authentication on HEAD requests.

## Hints

1. First Hint
<details>
<summary>Spoiler Warning</summary>
What HTTP OPTIONS are permitted?
</details>

2. Second Hint
<details>
<summary>Spoiler Warning</summary>
What can you discover about the token you're given? How do you think authentication is being performed?
</details>

3. Third Hint
<details>
<summary>Spoiler Warning</summary>
Are any HTTP methods often implemented as a subset of the spec? i.e., incorrectly?
</details>

## Steps
<details>
<summary>Spoiler warning</summary>
Examine endpoints and their possible HTTP methods. Notice that all POST requests require authentication. No GET or HEAD requests require authentication (No other methods permitted).

GET `/` to view available endpoints. GET `/login` to receive an auth token for "guest" user. Decode (b64) to notice it's in the format "username:signature". This signature is also base64 encoded binary data.

The player will hit `/display_flag` and learn that this page will display any flag given to it in a parameter.

Potentially hit the endpoint `/list_users` with a POST request (and valid guest token) to view the possible users. Notably, there is an admin user.

At this point we begin to try to login as the Admin user.

Craft a custom cookie, with content "admin:[BASE64]" The [Base64] content can be anything, so long as it's valid.

Use this cookie to try various methods against endpoints and see if you can authenticate as Admin. This will work for a HEAD request to `/login`, and the redirect location here will contain the flag in a url parameter.

This works, as `/login` allows GET and POST requests. The endpoint filter processes HEAD requests as a GET, so allows it to hit the endpoint. Within the endpoint itself, the code follows a
```py
if request.method == "GET":
  # GET request
else:
  # Must be a POSt request, as the route only allows GET and POST
```
to treat GET and POST requests differently. This means however that anything that is not a GET request is assumed to be an authenticated POST request (authentication is handled by a preprocessor) - flawed logic, as HEAD requests are allowed. This means HEAD requests can be used to bypass cookie authentication, and unauthenticated cookies can impersonate arbitrary users.


</details>
