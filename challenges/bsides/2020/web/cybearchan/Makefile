all: docker

docker: docker-clean docker-build docker-test

# --- Docker releated items ---
CHALLENGE_NAME = cybearchan
CHALLENGE_CATEGORY = web
CI_REGISTRY_IMAGE = registry.gitlab.com/cybears-private/fall-of-cybeartron
CHALLENGE_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}
HEALTHCHECK_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}-healthcheck
CHALLENGE_HOST = ${CHALLENGE_NAME}.ctf.cybears.io
CHALLENGE_PORT = 80
CHALLENGE_NETWORK = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}

docker-build:
	# build the container that hosts the challenge
	docker build -t $(CI_REGISTRY_IMAGE)/$(CHALLENGE_IMAGE_TAG) -f Dockerfile.challenge .
	# build the container that solves the challenge
	docker build -t $(CI_REGISTRY_IMAGE)/$(HEALTHCHECK_IMAGE_TAG) -f Dockerfile.healthcheck .

docker-test: docker-clean docker-build
	# run the challenge in the background
	docker run -t -i --expose ${CHALLENGE_PORT} --hostname ${CHALLENGE_HOST} --name ${CHALLENGE_HOST} -d --rm ${CI_REGISTRY_IMAGE}/${CHALLENGE_IMAGE_TAG}
	# run the healthcheck
	docker run --rm --link ${CHALLENGE_HOST} --name ${HEALTHCHECK_IMAGE_TAG} -e CHALLENGE_HOST=${CHALLENGE_HOST} -e CHALLENGE_PORT=${CHALLENGE_PORT} ${CI_REGISTRY_IMAGE}/$(HEALTHCHECK_IMAGE_TAG)
	# Now we're done delete the bits we created
	docker rm -f ${CHALLENGE_HOST} || true

docker-run: docker-clean docker-build
	# Run the challenge in the foreground
	docker run -t -i --expose ${CHALLENGE_PORT} --hostname ${CHALLENGE_HOST} --name ${CHALLENGE_HOST} --rm ${CI_REGISTRY_IMAGE}/${CHALLENGE_IMAGE_TAG}
	docker rm -f ${CHALLENGE_HOST} || true

docker-clean: 
	docker rm -f ${CHALLENGE_HOST} || true
	docker rm -f ${HEALTHCHECK_IMAGE_TAG} || true
	docker network rm ${CHALLENGE_NETWORK} || true

clean: docker-clean
