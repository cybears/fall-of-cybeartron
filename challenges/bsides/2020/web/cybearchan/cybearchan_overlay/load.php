<?php
$f = $_GET['img'];

if (
	(strpos($f, "http:") !== false) || 
	(strpos($f, "https:") !== false) || 
	(strpos($f, "ftp:") !== false) || 
	(strpos($f, "data:") !== false) || 
	(strpos($f, "php:") !== false)
) {
	echo "Error: I don't support remote files.\n";
	exit(1);
}
$contents = file_get_contents($f);
echo $contents;
?>
