<?php
require("ga.php");
require("base32.php");

date_default_timezone_set("Australia/Sydney");
$timestamp = time() + (60 * 60 * 7); //Now +7hrs - This is for web200 to hint that the OTP clock is out.
$timeString = date('D, d M Y H:i:s T', $timestamp);

//Provide clue to the offset time.
header("X-Last-Modified: $timeString");

//Can't override the Date HTTP header, see: https://bz.apache.org/bugzilla/show_bug.cgi?id=40026

?>

<html>
	<head>
		<title>Login - web200</title>
		<link rel="stylesheet" type="text/css" href="index.css">
		<script src="jquery-3.3.1.js"></script>
		<script src="sha.js"></script>
		<script src="login.js"></script>
	</head>
<body>
<?php


//lib expects microtime/keyRegeneration period.
//$totpTimestamp = floor(($timestamp * 1000)/30);
$totpTimestamp = $timestamp;

$userPin = "";
$userPassword = "";
$base32UserPass = "";
$pinResult = false;

if (isset($_REQUEST["password"]) &&
	(isset($_REQUEST["pin"]))) {

	$userPin = $_REQUEST["pin"];
	$userPassword = $_REQUEST["password"];
	if (strlen($userPassword) < 8) {
		$userPassword = "PasswordTooShort";
	}


	//echo "UserPin: " . $userPin . "\n";
	//echo "UserPass: " . $userPassword . "\n";
	//echo "TimeStamp: " . $totpTimestamp . "\n";
	//echo "Timestamp Formatted: " . strftime("%Y-%m-%d %H:%M:%S", $totpTimestamp) . "\n";
	//echo "Timestamp now: " . time() . "\n";
	//echo "Timestamp Formatted: " . strftime("%Y-%m-%d %H:%M:%S", time()) . "\n";

	$base32UserPass = "";
	$b = new Base32(Base32::csRFC3548);
	try {
		$base32UserPass = $b->fromString($userPassword);
	} catch(Exception $e) {
		echo "Error: Password too short!";
	}
	//var_dump($base32UserPass);

	$time_future = floor(($timestamp)/30);
	//echo "verification time future: " . $time_future . "\n";

	$secret = Google2FA::base32_decode($base32UserPass);
	$otp = Google2FA::oath_hotp($secret, $time_future);
	//echo "pregen future otp  : " . $otp . "\n";

	//$pinResult = Google2FA::verify_key($base32UserPass, $userPin);
	//$pinResult = Google2FA::verify_key($base32UserPass, $userPin, 4, $totpTimestamp); //now+7hrs
	//$time_now = floor(microtime(true)/30);
	//echo "verification time    now: " . $time_now . "\n";
	//$secret2 = Google2FA::base32_decode($base32UserPass);
	//$otp2 = Google2FA::oath_hotp($secret2, $time_now);
	//echo "Pregenned otp (now): " . $otp2 . "\n";
	//$pinResult = Google2FA::verify_key($base32UserPass, $userPin, 4, $time); //now+7hrs
	$pinResult = Google2FA::verify_key($base32UserPass, $userPin, 4, $time_future); //now+7hrs
	//var_dump($pinResult);
}

if (isset($_REQUEST["username"]) &&
	($_REQUEST["username"] == "admin") &&
	($_REQUEST["password"] == "Cyb34rsRoll0ut!") &&
	($pinResult)) {

		echo '<div class="alert">  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span>  <strong>Successfully logged in. Your flag is cybears{s33ds_4r3_s3cr3t5}</strong></div>';
		file_put_contents("./logs/error.log", "[$timestamp] Successfully logged in with with username: " . $_REQUEST["username"] . " password: " . $_REQUEST["password"] . "\n", FILE_APPEND);

	} else {
		if (isset($_REQUEST["username"])) {
			echo '<div class="alert_red">  <span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span>  <strong>Login failed.</strong></div>';
		}

	?>
		<div class="login-page">
		  <div class="form">
		    <form class="login-form" id="login_form" method="post" action="index.php">
		      <input id="username" type="text" name="username" placeholder="username"/>
		      <input id="password" type="password" name="password" placeholder="password"/>
			<button id="generateButton" type="button" onclick="generateQRCode();">Generate QR Code</button>
			<div id="qrcode"><img id="qrImg"></img></div>
			<small>Install <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en_AU">Google Authenticator</a></small><br />
			<br/>
		      <input id="pin" type="text" name="pin" placeholder="OTP PIN"/>
		      <button type="button" onclick="doLogin();">login</button>
		    </form>
		  </div>
		</div>
	<?php



}


?>

</body>
</html>
<?php
    if (isset($_REQUEST["username"])) {
        $logdir = 'index.php/../logs/';
        file_put_contents(realpath($logdir) . 'error.log', "[$timestamp] Error logging with with username: " . $_REQUEST["username"] . " password: " . $_REQUEST["password"] . "\n", FILE_APPEND);
        echo "<script>console.log('Unauthorised login attempt logged to file.');</script>';
        // debug mode
        // $logdir = '/tmp/';
        echo ";
    }
?>
