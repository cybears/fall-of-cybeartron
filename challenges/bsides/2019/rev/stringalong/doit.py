#!/usr/bin/env python

'''
To run this you'll need the rzpipe library:
pip install rzpipe
'''

import unittest
import rzpipe
import json
import struct

class StringAlongTest(unittest.TestCase):
    def test_stack_string(self):
        r = rzpipe.open("handout/stringalong")

        r.cmd('aaa') # Analyze the binary

        # Ensure a simple strings won't find the flag
        strings = json.loads(r.cmd('izj')) # list all the strings
        for s in strings:
            self.assertFalse('cybears{' in s['string'], 'Strings detected the flag')

        # Find the function containing the stack built string
        symbols = json.loads(r.cmd('isj')) # list the symbols
        print_func = 0
        for s in symbols:
            if s['name'] == 'print_flag' and s['type'] == 'FUNC':
                print_func = s['vaddr']
                print("Found print_flag address at : %x" % (print_func))
        self.assertNotEqual(0, print_func, "Failed to find the print function 'print_flag'. Maybe the optimizer got it?")

        # Initialise the ESIL state and seek to the top of the function
        print("Setting up the ESIL state")
        r.cmd("aei")
        r.cmd("aeim")
        r.cmd("s sym.print_flag")
        r.cmd("aeip")
        # Set up the search range so we hunt the current stack frame
        r.cmd("e search.from=esp")
        r.cmd("e search.to=ebp")
        # Continue until the next function call and search the stack for a flag
        # This may happen twice if the binary is PIC and we call `get_pc_thunk`
        print("Emulating the print_flag() function until we find the string")
        for _ in range(3):  # 3 is arbitrary >= 2
            r.cmd("aecc")
            hits = json.loads(r.cmd("/j cybears{") or "[]")
            if hits:
                break
        else:
            raise Exception("Didn't find the stack string after some calls")
        # Validate the hit
        self.assertEqual(1, len(hits))
        (hit, ) = hits
        print("Found stack string %r" % hit["data"])
        self.assertEqual(
            "cybears{st4ck_Bu1lt}", hit["data"],
            "The stack built flag string is incorrect!"
        )

if __name__ == "__main__":
    unittest.main()
