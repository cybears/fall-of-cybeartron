#!/usr/bin/python3

import sys
import zipfile
import os


def build_zip(flag_data, zip_file_name, file_name_in_zip="f"):
    with zipfile.ZipFile(zip_file_name, mode="w") as flag_zip:
        print(flag_zip.compression)
        metadata = zipfile.ZipInfo(filename=file_name_in_zip, date_time=(2019, 3, 16, 13, 37, 00))
        metadata.external_attr = 0o777 << 16  # full access
        flag_zip.writestr(metadata, bytes(flag_data), compress_type=zipfile.ZIP_DEFLATED)


def not_real_encryption(data, key, preserve_nulls=True):
    for x in range(len(data)):
        key_byte = ord(key[x % len(key)])
        if preserve_nulls and (data[x] == 0 or data[x] == key_byte):
            pass
        else:
            data[x] = data[x] ^ key_byte
    return data


def xorzip(data, key):
    # Build a ZIP file containing the flag, then XOR it
    temp_zip_file = "flag.zip"
    build_zip(flag_data=data, zip_file_name=temp_zip_file, file_name_in_zip="f")
    flag_zip_data = None
    with open(temp_zip_file, "rb") as flag_zip_file:
        flag_zip_data = bytearray(flag_zip_file.read())

    #os.unlink(temp_zip_file)

    # Pad the ZIP file with a plaintext string.
    # This will be XORed to be junk when the file is "decrypted", but the ZIP file
    # will still work.
    fake_flag = "The flag is cybears{--FLAG ENCRYPTED--}".encode("utf-8") + b'\x00'
    if len(fake_flag) % len(key) != 0:
        raise Exception("fake flag length must be a multiple of the key length")

    encoded_zip = not_real_encryption(flag_zip_data, key, preserve_nulls=True)

    return fake_flag + encoded_zip


def main():
    if len(sys.argv) < 4:
        print("usage: %s dest_file src_file key" % sys.argv[0])
        return
        
    _, dest_file, src_file, key = sys.argv[0:4]
    
    print("Encrypting %s to %s using key %s" % (src_file, dest_file, key))
    
    if len(key) != 8:
        raise Exception("key must be eight characters")
    
    src_data = bytearray(open(src_file, "rb").read())
    dest_data = xorzip(src_data, key)
   
    with open(dest_file, "wb") as dest_file_handle:
        dest_file_handle.write(dest_data)
    
    timestamp = os.environ.get("SOURCE_DATE_EPOCH")
    if timestamp:
        timestamp = int(timestamp)
        os.utime(dest_file, (timestamp, timestamp))


if __name__ == "__main__":
    main()