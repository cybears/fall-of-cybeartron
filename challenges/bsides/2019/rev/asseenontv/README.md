# As Seen On TV

## Build

To build As Seen On TV, you will need to install Microsoft Visual Basic 6.0.

CI builds are not yet implemented: "Set up a Gitlab CI runner that builds VB6 applications" is not a problem I want to solve in 2019.

## Solution

The binary is a Visual Basic 6 program compiled as P-Code (not native code). It can be disassembled/decompiled with any VB decompiler. Most packer identifiers (PEiD etc.) will detect that this is a VB binary.

The GUI interface prompts for an IP address string. Several checks are performed on the string using the "Like" operator (similar to a regex). If the checks pass, the program displays "ACCESS GRANTED" plus the flag. Otherwise it displays ACCESS DENIED.

Opening the binary with a VB decompiler (eg. VB Decompiler Lite) shows there are four forms: the main form (Form1), the progress bar (frmTrackingProgress), an Access Denied form (frmAccessDenied) and an Access Granted form (frmAccessGranted).

When the button is clicked on the main form, the IPAddress property is set on frmTrackingProgress, then the window is displayed. The tracking progress form has a timer that increments the progress bar until it reaches the end. Then it does the check to see which window to display.

The P-Code shows there are several checks that happen. The first two use the "Like" operator in VB to do regex-like checks against the string:

```
  loc_43C2DC: FFree1Ad var_88
  loc_43C2DF: FFree1Var var_98 = ""
  loc_43C2E2: BranchF loc_43C396
  loc_43C2E5: FLdPr arg_8
  loc_43C2E8: MemLdStr global_52
  loc_43C2EB: LitStr "[1I]* [i1]n*"
  loc_43C2EE: LikeStr
  loc_43C2F0: FLdPr arg_8
  loc_43C2F3: MemLdStr global_52
  loc_43C2F6: LitStr "I'm*"
  loc_43C2F9: LikeStr
```

The first check is effectively the regex `/^[1I].* [i1]n.*$/`, and the second check is `/^I'm.*/`. So, the phrase `I'm in` will pass both checks.

There is a final check that ensures the last character is an exclaimation point:
```
  loc_43C300: LitI4 1
  loc_43C305: FLdPr arg_8
  loc_43C308: MemLdRfVar from_stack_1.global_52
  loc_43C30B: CVarRef
  loc_43C310: FLdRfVar var_98
  loc_43C313: ImpAdCallFPR4  = Right(, )
  loc_43C318: FLdRfVar var_98
  loc_43C31B: LitVarStr var_B8, "!"
  loc_43C320: HardType
  loc_43C321: EqVar var_CC
```

So, passing the string `I'm in!` will cause the ACCESS GRANTED window to display, along with the flag.


## Flag

```
cybears{furious_typing_intensifies}
```

