VERSION 5.00
Begin VB.Form frmAccessGranted
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   Caption         =   "ACCESS GRANTED"
   ClientHeight    =   1875
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6600
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1875
   ScaleWidth      =   6600
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1
      Interval        =   500
      Left            =   5760
      Top             =   240
   End
   Begin VB.Label lblFlag
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Flag is: cybears{"
      ForeColor       =   &H0000FF00&
      Height          =   255
      Left            =   135
      TabIndex        =   1
      Top             =   1065
      Width           =   6360
   End
   Begin VB.Label Label1
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ACCESS GRANTED"
      BeginProperty Font
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   555
      Left            =   960
      TabIndex        =   0
      Top             =   555
      Width           =   4680
   End
   Begin VB.Shape Shape1
      BorderColor     =   &H0000FF00&
      BorderWidth     =   4
      Height          =   1695
      Left            =   113
      Top             =   90
      Width           =   6375
   End
End
Attribute VB_Name = "frmAccessGranted"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public IPAddress As String

Private Declare Function sndPlaySound Lib "winmm.dll" Alias "sndPlaySoundA" (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long
Private Const SND_ASYNC = &H1         '  play asynchronously
Private Const SND_MEMORY = &H4         '  lpszSoundName points to a memory file
Private Const SND_NODEFAULT = &H2  '  silence not default, if sound not found


Private Sub Form_KeyPress(KeyAscii As Integer)
Unload Me
End Sub

Private Sub Form_Load()
Me.Left = Screen.Width / 2 - Me.Width / 2
Me.Top = Screen.Height / 2 - Me.Height / 2

Dim flag As String
Dim flagchars(26) As Integer

' The flag is: cybears{furious_typing_intensifies} XORed with the password "I'm in!"
Dim i As Integer

flagchars(0) = 47
flagchars(1) = 82
flagchars(2) = 31
flagchars(3) = 73
flagchars(4) = 6
flagchars(5) = 27
flagchars(6) = 82
flagchars(7) = 22
flagchars(8) = 83
flagchars(9) = 20
flagchars(10) = 80
flagchars(11) = 0
flagchars(12) = 0
flagchars(13) = 70
flagchars(14) = 22
flagchars(15) = 78
flagchars(16) = 3
flagchars(17) = 84
flagchars(18) = 12
flagchars(19) = 0
flagchars(20) = 82
flagchars(21) = 32
flagchars(22) = 65
flagchars(23) = 4
flagchars(24) = 69
flagchars(25) = 26

For i = 0 To UBound(flagchars) - 1
    flag = flag + Chr$(flagchars(i) Xor Asc(Mid(IPAddress, (i Mod Len(IPAddress)) + 1, 1)))
Next i

lblFlag.Caption = "The flag is: cybears{" + flag + "}"

' Play sound from PE resource
Dim SoundBuffer As String
SoundBuffer = StrConv(LoadResData(101, "SOUND"), vbUnicode)
sndPlaySound SoundBuffer, SND_ASYNC Or SND_NODEFAULT Or SND_MEMORY

End Sub

Private Sub Timer1_Timer()
Shape1.Visible = Not Shape1.Visible
End Sub


