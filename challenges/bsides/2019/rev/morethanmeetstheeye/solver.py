# Rough solution for disguise
import hashlib

# Virtual Address
#CIPHERTEXT_OFFSET = 0x402108
# File Offset
CIPHERTEXT_OFFSET = 0x1108
CIPHERTEXT_SIZE = 0x1b

def rc4(key, plaintext):
    # initialise KSA
    S = list(range(256))

    j = 0
    for i in range(256):
        j = (j + S[i] + key[i % len(key)]) % 256

        temp = S[i]
        S[i] = S[j]
        S[j] = temp

    i = 0
    j = 0
    ciphertext = b""
    for character in plaintext:
        i = (i + 1) % 256
        j = (j + S[i]) % 256

        temp = S[i]
        S[i] = S[j]
        S[j] = temp

        K = S[(S[i] + S[j]) % 256]
        ciphertext += (character ^ K).to_bytes(1, 'big')

    return ciphertext

def encrypt(key, plaintext):
    sha = hashlib.sha256(key)

    return rc4(sha.digest(), plaintext)


#ciphertext = bytes([0x1c, 0x8f, 0x30, 0x16, 0x83, 0xcd, 0xf0, 0x2b, 0xea, 0xf9, 0x95, 0xf1, 0x53, 0x6e, 0x72, 0xfe, 0xf9, 0xb0, 0x1e, 0x9b, 0x96, 0xe3, 0xd5, 0x88, 0xdb, 0x48, 0x8])
with open('Release/MoreThanMeetsTheEye.exe', 'rb') as f:
    f.seek(CIPHERTEXT_OFFSET)
    ciphertext = f.read(CIPHERTEXT_SIZE)

print("Ciphertext: " + ' '.join(hex(b)[2:] for b in ciphertext))
print("Expected Key: {}".format(b"2.6.35.1"))
print("Decrypt with expected key: {}".format(encrypt(b"2.6.35.1", ciphertext)))

def brute_force(ciphertext):
    for major_version in range(2, 6):        # linux 2.0 -> 4.0
        for minor_version in range(0, 21):    # linux 2.0 -> linux 2.6
            for revision in range(0, 110):   # linux 2.0.0 -> linux 2.0.109
                for patch in range(0, 100):  # linux 2.0.0.0 -> linux 2.0.0.99
                    linux_version = "%d.%d.%d.%d" % (major_version, minor_version, revision, patch)
                    linux_version = linux_version[:8] # Max length is 8
                    linux_version = linux_version.encode()
                    result = encrypt(linux_version, ciphertext)
                    if result.startswith(b'cybears'):
                        return (linux_version, result)

    raise Exception("could not find flag")

key, flag = brute_force(ciphertext)
print("Found Key: {}".format(key))
print("Found Flag: {}".format(flag))

