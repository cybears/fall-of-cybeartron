/**
 * @brief Works On My Machine entrypoint
 **/

#include "worksonmymachine.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <windows.h>

// Compiler-specific stuff to get CPUID intrinsic
#ifdef _MSC_BUILD
#include <intrin.h>
#else
#include <cpuid.h>
#endif

// Gets CPU vendor string
bool GetCPUVendor(char * out_cpu_vendor, size_t cpu_vendor_size)
{
    uint32_t temp[4] = { 0 };

    if (cpu_vendor_size < 12)
        return 0;

#ifdef _MSC_BUILD
    __cpuid((int*) temp, 0);
#else
    __get_cpuid(0, &temp[0], &temp[1], &temp[2], &temp[3]);
#endif
    memcpy(out_cpu_vendor, &(temp[1]), 4);
    memcpy(out_cpu_vendor + 4, &(temp[3]), 4);
    memcpy(out_cpu_vendor + 8, &(temp[2]), 4);
    return 1;
}


// Install a custom crash handler
LONG WINAPI handle_unhandled_exception(LPEXCEPTION_POINTERS exc_info)
{
    switch (exc_info->ExceptionRecord->ExceptionCode)
    {
    case EXCEPTION_ACCESS_VIOLATION:
        printf("Access violation at %p\n", exc_info->ExceptionRecord->ExceptionAddress);
        break;
    case EXCEPTION_ILLEGAL_INSTRUCTION:
        printf("Illegal instruction at %p\n", exc_info->ExceptionRecord->ExceptionAddress);
        break;
    case EXCEPTION_BREAKPOINT:
        printf("Breakpoint at %p\n", exc_info->ExceptionRecord->ExceptionAddress);
        break;
    default:
        printf("Exception 0x%x at %p\n", exc_info->ExceptionRecord->ExceptionCode, exc_info->ExceptionRecord->ExceptionAddress);
        break;
    }
    ExitProcess(1);
}

int main(int argc, char* argv[])
{
    char cpu_vendor[13] = { 0 };
    char result[64] = { 0 };

    SetUnhandledExceptionFilter(handle_unhandled_exception);


    if (GetCPUVendor(cpu_vendor, sizeof(cpu_vendor))) 
    {
#ifdef CTF_DEBUG
            printf("CPU vendor reported: %s\n", cpu_vendor);
            memcpy(cpu_vendor, "GenuineTMx86", 12);
#endif
        if (DoDeobfuscation(".ctf", cpu_vendor, 12))
        {
            uint32_t vm_success = GenerateFlag(result, sizeof(result));
            if (vm_success)
            {
                printf("The flag is: %s\n", result);
            }
        }
    }
    return 0;
}