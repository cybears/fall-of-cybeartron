#ifndef WORKSONMYMACHINE_H
#define WORKSONMYMACHINE_H

#include <stdint.h>

// Enable debug output
// #define CTF_DEBUG

#ifdef CTF_DEBUG
#define CTF_PRINTF        printf
#else
#define CTF_PRINTF
#endif


// Deobfuscate PE section with XOR
bool DoDeobfuscation(const char * section_name, const char * key, const size_t key_size);

// Run the VM to generate the flag
uint32_t GenerateFlag(char * output_buffer, size_t output_buffer_size);

// Instructions
#define EXIT                    0        // no params
#define PUSH_U32                1        // param1: U32 value to push to stack
#define PUSH_REG                2        // param1: U8 register to push to stack
#define POP_REG                 3        // param1: U8 register to push to stack
#define XOR_REG_REG             4        // param1: U8 register dest, param2: U8 register src
#define MOVD_RESULT_OFFSET_REG  5        // param1: U8 offset from start of result buffer (dest), param2: U8 reg (src)
#define MOVD_REG_RESULT_OFFSET  6        // param1: U8 reg (dest), param2: U8 offset from start of result buffer (dest)


#endif