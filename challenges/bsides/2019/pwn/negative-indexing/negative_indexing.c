#include <float.h>
#include <inttypes.h>
#include <locale.h>
#include <limits.h>
#include <math.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

/******************************************************************************
 * Bug structure setup!
 *****************************************************************************/
/* This is so we can fiddle with how far the negative index bug can reach */
#define COUNT_ARRAY_TYPE    uint16_t
#define COUNT_ARRAY_ELEMS   (sizeof(char) * (1 << CHAR_BIT))
#define COUNT_ARRAY_SIZE    COUNT_ARRAY_ELEMS * sizeof(COUNT_ARRAY_TYPE)

/* Here we define the various strings and sizes we need to construct the victim
 * structure properly.
 */
/* `VICTIM` is arbitrary length */
#define VICTIM      L"-- 'There's more to them than meets the eye.' --"
/* `FLAG` can be up to `COUNT_ARRAY_SIZE / 2 - TAIL_SIZE` in length */
#ifndef FLAG
#define FLAG        L"WHY DID NOBODY PUT A FLAG HERE!?"
#endif
#define VICTIM_SIZE sizeof(VICTIM)
#define FLAG_SIZE   sizeof(FLAG)
/* We choose a value not divisible by `sizeof(wchar_t)` to misalign `target` */
#define TAIL_SIZE   23
#define PAD_SIZE    (COUNT_ARRAY_SIZE / 2) - FLAG_SIZE - TAIL_SIZE

struct __attribute__((packed)) {
    wchar_t   victim[(VICTIM_SIZE / sizeof(wchar_t)) - 1];
    char      pad[PAD_SIZE];
    wchar_t   target[FLAG_SIZE / sizeof(wchar_t)];
    char      tail [TAIL_SIZE];
    COUNT_ARRAY_TYPE counts[COUNT_ARRAY_ELEMS];
} _ __attribute__((section(".data"))) = {
    .victim = VICTIM,
    .target = FLAG,
};
/* Sanity check the structure layout at compile time to make sure we didn't
 * screw up the challenge somehow...
 */
_Static_assert(
    offsetof(typeof(_), pad) + COUNT_ARRAY_SIZE / 2
        == offsetof(typeof(_), counts),
    "The target structure has fucked up sizes"
);

void __attribute__((constructor)) setup_locale(void) {
    /* We set `LC_ALL` to nothing to make sure we dump out raw UTF-8 */
    setlocale(LC_ALL, "");
}

/******************************************************************************
 * This art was sourced from:
 * http://textart4u.blogspot.com/2014/06/transformers-autobot-symbol-copy-paste.html
 * Hopefully they didn't just steal it from somewhere!
 *****************************************************************************/
wchar_t __attribute__((section(".data"))) banner[] =
L"                                                   ▄▄▄▄▄▄▄▄▄              \n"
L"                                                ▄█████████████▄           \n"
L"                                         ▐███▌ █████████████████ ▐███▌    \n"
L"                                          ████▄ ▀███▄     ▄███▀ ▄████     \n"
L"                                          ▐█████▄ ▀███▄ ▄███▀ ▄█████▌     \n"
L"                                           ██▄▀███▄ ▀█████▀ ▄███▀▄██      \n"
L"                                           ▐█▀█▄▀███ ▄ ▀ ▄ ███▀▄█▀█▌      \n"
L"                                            ██▄▀█▄██ ██▄██ ██▄█▀▄██       \n"
L"                                             ▀██▄▀██ █████ ██▀▄██▀        \n"
L"                                            ▄  ▀████ █████ ████▀  ▄       \n"
L"                                            ██        ███        ██       \n"
L"                                            ██▄    ▄█ ███ █▄    ▄██       \n"
L"                                            ████ ▄███ ███ ███▄ ████       \n"
L"                                            ████ ████ ███ ████ ████       \n"
L"                                            ████ ████ ███ ████ ████       \n"
L"                                            ████ ████▄▄▄▄▄████ ████       \n"
L"        Cybears Technology Group            ▀███ █████████████ ███▀       \n"
L"                                              ▀█ ███ ▄▄▄▄▄ ███ █▀         \n"
L"      Decepticom Encryption Scanner              ▀█▌▐█████▌▐█▀            \n"
L"              by cykumamon                          ███████               \n"
L"                                                                          \n"
L"                                                                          \n"
L" Enter captured communications to detect traces of Decepticom language    \n"
L" obscured by the codes we know they use.                                  \n"
L"                                                                          \n"
L" Pass in EOF or press ^D to finalise input.                               \n"
L">> ";

/******************************************************************************
 * This is a statistic table of what we suggest that the Decepticom dialect of
 * Cybeartronian looks like. I generated this by doing this in Python:
 *
 * ```
 * >>> s = dict()
 * >>> remaining = 1024*1024
 * >>> for i in range(256):
 * ...     x = int(random.random() * 1024*1024*2 / 256)
 * ...     s[i] = x
 * ...     remaining -= x
 * ...
 * >>> for i in range(remaining):
 * ...     s[i%256] += 1
 * ...
 * >>> for i, v in enumerate(s.values()):
 * ...     print("\tPER1Mi(%i)," % v, end="")
 * ...     if (i+1) % 4 == 0: print()
 * ...
 * ```
 *
 * So kind of randomly consuming occurences from a 1Mi count and then spreading
 * whatever is left over "evenly". It's gross but it gives us numbers which are
 * enough to differentiate from uniform which is kind of the point.
 *
 * We shove this in `.data` to create more noise for the player to navigate
 * around. Hopefully it ends up being clear that this is just garbage once they
 * dismiss the chi squared code and that's the only code which references this
 * big chunk of data.
 *****************************************************************************/
#define PER1Mi(n) ((float) n / 1024 / 1024)
float __attribute__((section(".data"))) decept_stat[COUNT_ARRAY_ELEMS] = {
        PER1Mi(813),    PER1Mi(7260),   PER1Mi(538),    PER1Mi(8038),
        PER1Mi(1103),   PER1Mi(8010),   PER1Mi(2150),   PER1Mi(6488),
        PER1Mi(6761),   PER1Mi(2315),   PER1Mi(6687),   PER1Mi(5887),
        PER1Mi(3507),   PER1Mi(2180),   PER1Mi(5474),   PER1Mi(2848),
        PER1Mi(4681),   PER1Mi(4444),   PER1Mi(1986),   PER1Mi(6752),
        PER1Mi(5942),   PER1Mi(1368),   PER1Mi(7970),   PER1Mi(6192),
        PER1Mi(5768),   PER1Mi(391),    PER1Mi(3722),   PER1Mi(6924),
        PER1Mi(2778),   PER1Mi(908),    PER1Mi(7285),   PER1Mi(8179),
        PER1Mi(1288),   PER1Mi(3275),   PER1Mi(2565),   PER1Mi(7116),
        PER1Mi(382),    PER1Mi(6904),   PER1Mi(1810),   PER1Mi(4627),
        PER1Mi(4514),   PER1Mi(6831),   PER1Mi(5463),   PER1Mi(1445),
        PER1Mi(7370),   PER1Mi(2717),   PER1Mi(7973),   PER1Mi(1333),
        PER1Mi(7519),   PER1Mi(2251),   PER1Mi(7778),   PER1Mi(1002),
        PER1Mi(1514),   PER1Mi(5611),   PER1Mi(3690),   PER1Mi(7647),
        PER1Mi(566),    PER1Mi(6710),   PER1Mi(1621),   PER1Mi(3931),
        PER1Mi(3882),   PER1Mi(1436),   PER1Mi(2044),   PER1Mi(5424),
        PER1Mi(3409),   PER1Mi(2927),   PER1Mi(3319),   PER1Mi(5094),
        PER1Mi(4113),   PER1Mi(3711),   PER1Mi(1005),   PER1Mi(6825),
        PER1Mi(4980),   PER1Mi(563),    PER1Mi(6619),   PER1Mi(2325),
        PER1Mi(678),    PER1Mi(5219),   PER1Mi(6655),   PER1Mi(5042),
        PER1Mi(4076),   PER1Mi(5775),   PER1Mi(6804),   PER1Mi(3067),
        PER1Mi(3036),   PER1Mi(2339),   PER1Mi(174),    PER1Mi(2075),
        PER1Mi(7383),   PER1Mi(1275),   PER1Mi(8162),   PER1Mi(7867),
        PER1Mi(7960),   PER1Mi(610),    PER1Mi(4075),   PER1Mi(4631),
        PER1Mi(1898),   PER1Mi(4369),   PER1Mi(678),    PER1Mi(3568),
        PER1Mi(6500),   PER1Mi(3068),   PER1Mi(7491),   PER1Mi(235),
        PER1Mi(576),    PER1Mi(4326),   PER1Mi(2478),   PER1Mi(6192),
        PER1Mi(4069),   PER1Mi(1732),   PER1Mi(4498),   PER1Mi(6459),
        PER1Mi(2301),   PER1Mi(6048),   PER1Mi(3877),   PER1Mi(6541),
        PER1Mi(5284),   PER1Mi(1158),   PER1Mi(2973),   PER1Mi(7454),
        PER1Mi(2553),   PER1Mi(247),    PER1Mi(3417),   PER1Mi(224),
        PER1Mi(8070),   PER1Mi(6708),   PER1Mi(2009),   PER1Mi(3944),
        PER1Mi(3183),   PER1Mi(832),    PER1Mi(5551),   PER1Mi(4935),
        PER1Mi(1620),   PER1Mi(935),    PER1Mi(4006),   PER1Mi(6931),
        PER1Mi(7552),   PER1Mi(1576),   PER1Mi(7893),   PER1Mi(1710),
        PER1Mi(3148),   PER1Mi(6483),   PER1Mi(5193),   PER1Mi(7506),
        PER1Mi(5041),   PER1Mi(2176),   PER1Mi(4641),   PER1Mi(1527),
        PER1Mi(1356),   PER1Mi(2551),   PER1Mi(1971),   PER1Mi(8234),
        PER1Mi(5145),   PER1Mi(688),    PER1Mi(4789),   PER1Mi(975),
        PER1Mi(6532),   PER1Mi(571),    PER1Mi(4133),   PER1Mi(8261),
        PER1Mi(5173),   PER1Mi(5358),   PER1Mi(3401),   PER1Mi(7571),
        PER1Mi(8250),   PER1Mi(3538),   PER1Mi(6191),   PER1Mi(6531),
        PER1Mi(1570),   PER1Mi(4950),   PER1Mi(2617),   PER1Mi(2320),
        PER1Mi(1793),   PER1Mi(191),    PER1Mi(7080),   PER1Mi(4537),
        PER1Mi(973),    PER1Mi(820),    PER1Mi(6054),   PER1Mi(6385),
        PER1Mi(1544),   PER1Mi(4583),   PER1Mi(4766),   PER1Mi(3116),
        PER1Mi(209),    PER1Mi(743),    PER1Mi(7085),   PER1Mi(239),
        PER1Mi(3166),   PER1Mi(6548),   PER1Mi(4654),   PER1Mi(1459),
        PER1Mi(1209),   PER1Mi(313),    PER1Mi(5547),   PER1Mi(1753),
        PER1Mi(3107),   PER1Mi(2682),   PER1Mi(4321),   PER1Mi(1318),
        PER1Mi(2993),   PER1Mi(4970),   PER1Mi(4426),   PER1Mi(566),
        PER1Mi(1667),   PER1Mi(3629),   PER1Mi(7972),   PER1Mi(363),
        PER1Mi(7078),   PER1Mi(4626),   PER1Mi(7987),   PER1Mi(6282),
        PER1Mi(3807),   PER1Mi(6871),   PER1Mi(7626),   PER1Mi(7436),
        PER1Mi(2566),   PER1Mi(2868),   PER1Mi(8296),   PER1Mi(6483),
        PER1Mi(5227),   PER1Mi(5963),   PER1Mi(6031),   PER1Mi(2539),
        PER1Mi(7410),   PER1Mi(5729),   PER1Mi(3258),   PER1Mi(5607),
        PER1Mi(483),    PER1Mi(539),    PER1Mi(6191),   PER1Mi(970),
        PER1Mi(1162),   PER1Mi(8188),   PER1Mi(1314),   PER1Mi(2838),
        PER1Mi(6691),   PER1Mi(2005),   PER1Mi(5674),   PER1Mi(3616),
        PER1Mi(588),    PER1Mi(3059),   PER1Mi(2840),   PER1Mi(5731),
        PER1Mi(1744),   PER1Mi(7135),   PER1Mi(6130),   PER1Mi(1917),
        PER1Mi(6101),   PER1Mi(5091),   PER1Mi(6416),   PER1Mi(4492),
        PER1Mi(6641),   PER1Mi(1754),   PER1Mi(3480),   PER1Mi(7874),
};

/******************************************************************************
 * How about some actual code now?
 *****************************************************************************/
int main (void) {
    register signed char c;
    register COUNT_ARRAY_TYPE *counts = _.counts;

    /* We have some fluff to distract from the bug here - we're claiming this
     * is a detection algorithm for Decepticom encrypted messages and then we
     * run through all symbol rotations to find the one with the lowest chi
     * squared statistic.
     */
    wprintf(banner);
    fflush(stdout);

    /* Here's the bug! `c` is a signed char which causes us to throw away all
     * the good work that `fgetc` has done in casting the `unsigned char` value
     * of the byte it pulled off `stdin` into an `int`.
     */
    size_t bread = 0;
    while ((c = fgetc(stdin)) != EOF) {
        bread += 1;
        /* And here's the potentially negative index! */
        counts[c] += 1;
    }

    /* Now we bamboozle them by calculating chi squared statistics for a
     * presumed rotation based monoalphabetic cipher with an ordered alphabet
     * of all valid byte values, ie. a 256 symbol Caesar cipher. We have a made
     * up distribution which defines what the Decepticom dialect of the
     * Cybeartronian language looks like statistically.
     */
    int best_rotation = INT_MAX;
    float best_chi_sq = FLT_MAX;
    for (int rotation = 0; rotation < COUNT_ARRAY_ELEMS; ++rotation) {
        float chi_sq = 0;
        for (int offset = 0; offset < COUNT_ARRAY_ELEMS; ++offset) {
            float actual = (uint8_t) counts[(rotation + offset) % COUNT_ARRAY_ELEMS];
            float expected = decept_stat[offset] * bread;
            chi_sq += pow(actual - expected, 2) / expected;
        }
        if (chi_sq < best_chi_sq) {
            best_chi_sq = chi_sq;
            best_rotation = rotation;
        }
    }
    wprintf(
        L"\n\nBest candidate for cipher key: %i (%lf)\n\n",
        best_rotation, best_chi_sq
    );

    wprintf(L"%ls\n", _.victim);
    return EXIT_SUCCESS;
}
