#!/usr/bin/env python3
import pickle
import os
import socketserver
import yaml

class Handler(socketserver.BaseRequestHandler, socketserver.ForkingMixIn):
    def handle(self):
        os.dup2(self.request.fileno(), 1)
        os.dup2(self.request.fileno(), 2)
        self.request.sendall(pickle.dumps(
            yaml.load(self.request.recv(4096), Loader=yaml.Loader)
        ))

server = socketserver.TCPServer(('0.0.0.0', 2323), Handler)
server.serve_forever()
