#pragma once
#ifndef _QARMA_H
#define _QARMA_H
#if __cplusplus
extern "C" {
#endif


#include <stdint.h>
typedef union
{
    uint16_t row[4];
    uint32_t lrow[2];
    uint64_t llrow;
 } QarmaMatrix_t;


/*
 * rotate shift right n-bit on x(a 64-bit block)
 */
static inline uint64_t ror64(uint64_t x, int n) { return x>>n | x<<(64-n); }
static inline uint8_t rot8(uint8_t x, int n) { return ((x << n) | (x >> (4-n))) % 16; }

void xorBlock( uint8_t *a, uint8_t *b, uint8_t *out, uint8_t size);
void copy_matrix(uint8_t *in, uint8_t *out);

void subBytes( uint8_t *in, uint8_t *out, bool inverse, uint8_t size);

void permuteTweak( uint8_t *tweak, uint8_t *out, uint8_t size);
void permuteTweak2( uint8_t *tweak, uint8_t *out, uint8_t size);

void permuteState( uint8_t *state, uint8_t *out, bool inverse, uint8_t size);

void mixColums(QarmaMatrix_t *inMatrix);

void tweakLFSR(uint8_t *tweak, uint8_t *out, uint8_t size);

void calcTweak(uint8_t *tweakIn, uint8_t *out, uint8_t size, uint8_t range);

void calcRoundTweakKey(uint8_t *tweakIn, uint8_t *out, uint8_t size, uint8_t *key, uint8_t round, bool reverse);

void doRound(uint8_t *state, uint8_t *tweakIn, uint8_t *out, uint8_t size, uint8_t round, bool reverse);

void middleRound(uint8_t *state, uint8_t *k1, uint8_t *out, uint8_t size);

uint64_t qarma64( uint8_t plaintext[8], uint8_t tweak[8], uint8_t key[16], bool encrypt, uint8_t rounds);

#if __cplusplus
}
#endif

#endif //_QARMA_H
