#include "pac.h"
#include "string.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
uint64_t temp = 0;
uint64_t cipher_pointer = 0;
KEY_DATA key_data = {0};

#define GET_PAC_SYM(ptr, sym) \
    ptr = dlsym(libc, sym); \
    PACIA(&ptr);

#define min(x,y) ({ typeof(x) _x = x; typeof(y) _y = y; x < y ? x : y; })

static void random_bytes(char *buf, size_t len) {
    size_t check_buf_size = min(len, 4096);
    char check_buf[check_buf_size];
    int fd, i;
    size_t wanted;
    ssize_t bread;

    fd = open("/dev/urandom", O_RDONLY, 0);
    if (fd != -1) {
        for (wanted = len; wanted > 0; /* nothing */) {
            bread = read(fd, check_buf, min(wanted, sizeof(check_buf)));
            if (bread < 0)
                break;
            for (i = 0; i < bread; ++i) {
                if (check_buf[i]) {
                    buf[len - (wanted--)] = check_buf[i];
                }
            }
        }
        close(fd);
    }
    if(wanted > 0) {
        exit(-1);
    }
}
// Intitialises the crypto key, and sets up any functions used in the program with PAC.
void init() {
    void *libc;
    random_bytes((void*)&key_data.qarma_key, 16);
    random_bytes((void*)&key_data.qarma_tweak, 8);

    libc = dlopen("libc.so.6", RTLD_NOW);
    if (!libc) {
        fprintf(stderr, "%s\n", dlerror());
        exit(EXIT_FAILURE);
    }

    GET_PAC_SYM(_printf, "printf");
    GET_PAC_SYM(_fgets, "fgets");
    GET_PAC_SYM(__exit, "exit");
    GET_PAC_SYM(_puts, "puts");
    GET_PAC_SYM(xsetbuf, "setbuf");
    GET_PAC_SYM(_fputs, "fputs");
    GET_PAC_SYM(_scanf, "scanf");
    GET_PAC_SYM(_malloc, "malloc");
    GET_PAC_SYM(_free, "free");
    GET_PAC_SYM(_write, "write");
    GET_PAC_SYM(_read, "read");
}
