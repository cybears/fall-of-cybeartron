#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include "pac.h"
#include "mem.h"

// IMPORTANT, size allows for large negative array index to loop around and have structure close to overlapping but with name aligned to the pointer.
#define MAX_NAME 0x18
#define MAX_AISLES 30

typedef struct Item Item;

struct Item{
    char name[MAX_NAME];
    Item *prev;
    Item *next;
};

typedef struct {
    char name[MAX_NAME];
    Item *items;
} AISLE;

char *supermarket_name;
// aisles array will be attackable
AISLE aisles[MAX_AISLES];
int num_aisles = 0;


void freeAisles() {
    PACIASP();
    Item *item = NULL;
    Item *nextItem = NULL;
    for(int i=0; i < num_aisles; i++) {
        item = aisles[i].items;
        while(item != NULL) {
            DOPACTHING(item,
                nextItem = item->next;
            );
            pac_free(item);
            item = nextItem;
        }
    }
    AUTIASP();
}

void printAisle(long aisleIndex) {
    PACIASP();
    Item *item = NULL;
    Item *tempItem = NULL;

    AUTIAC(&_printf, "\nAisle %d - %s", aisleIndex, aisles[aisleIndex].name, stdout);
    item = aisles[aisleIndex].items;
    while(item != NULL) {
        tempItem = item;
        DOPACTHING(tempItem,
            AUTIAC(&_fputs, tempItem->name, stdout);
            item = tempItem->next;
        )
    }
    AUTIASP();
}

void print(void) {
    PACIASP();
    for(int i=0; i < num_aisles; i++) {
        printAisle(i);
    }
    AUTIASP();
}

void printName(void) {
    PACIASP();
    DOPACTHING(supermarket_name,
        AUTIAC(&_fputs, supermarket_name, stdout);
    )
    AUTIASP();
}

void renameSupermarket(void) {
    PACIASP();
    char *readString = NULL;
    AUTIAC(&_fputs, "New Supermarket Name > ", stdout);

    DOPACTHING(supermarket_name,
        AUTIAC_R(&_fgets, readString, supermarket_name, MAX_NAME, stdin);
    )
    AUTIASP();
}

bool setAisleNameIndex(long aisleIndex) {
    PACIASP();
    bool result = false;
    char *readString = NULL;
    AUTIAC(&_printf, "Name for aisle %d? > ", aisleIndex);

    AUTIAC_R(&_fgets, readString, aisles[aisleIndex].name, MAX_NAME+1, stdin);
    if(readString != NULL) {
        result = true;
    }
    AUTIASP(result);
}

bool setAisleName() {
    PACIASP();
    long aisleIndex = 0;
    bool result = false;
    char newLine;
    AUTIAC(&_fputs, "What Aisle name to set? ", stdout);
    AUTIAC(&_scanf, "%li", &aisleIndex);
    AUTIAC(&_scanf, "%c", &newLine);
    AUTIAC(&_printf, "Setting Aisle %ld", aisleIndex);
    AUTIAC(&_printf, "of %ld", num_aisles);

    // BUG BUG, signed comparison.
    if(aisleIndex < num_aisles) {
        result = setAisleNameIndex(aisleIndex);
    } else {
        AUTIAC(&_puts, "I dont have that many aisles!");
    }
    AUTIASP(result);
}


bool printAisleName() {
    PACIASP();
    long aisleIndex = 0;
    bool result = false;
    char newLine;
    AUTIAC(&_fputs, "What Aisle name to get? ", stdout);
    AUTIAC(&_scanf, "%li", &aisleIndex);
    AUTIAC(&_scanf, "%c", &newLine);

    // BUG BUG, signed comparison.
    if(aisleIndex < num_aisles) {
        AUTIAC(&_printf, "Aisle %d - %s", aisleIndex, aisles[aisleIndex].name, stdout);
    } else {
        AUTIAC(&_puts, "I dont have that many aisles!");
    }
    AUTIASP(result);
}

bool addItem(){
    PACIASP();
    char *readString = NULL;
    char newLine;
    bool result = false;
    long aisleIndex = 0;
    Item *newItem = pac_alloc(sizeof(Item));
    Item *nextItem = NULL;
    Item *prev = NULL;

    AUTIAC(&_printf, "Name for new Item? > ", stdout);
    DOPACTHING(newItem,
        AUTIAC_R(&_fgets, readString, newItem->name, MAX_NAME+1, stdin);
        for(int i = 0; i < MAX_NAME; i++) {
            if(newItem->name[i] == '%') {
                newItem->name[i] = '.';
            }
        }
    )
    if(readString) {

        AUTIAC(&_fputs, "What Aisle is it stored? ", stdout);
        AUTIAC(&_scanf, "%lu", &aisleIndex);
        AUTIAC(&_scanf, "%c", &newLine);
        if(aisleIndex < num_aisles) {
            //todo: add item to list betterer
            DOPACTHING(newItem,
                newItem->next = aisles[aisleIndex].items;
                prev = (Item*)&aisles[aisleIndex];
                PACIA(&prev);
                newItem->prev = prev;
                nextItem = aisles[aisleIndex].items;
            )
            if(nextItem != 0) {
                DOPACTHING(nextItem,
                    nextItem->prev = newItem;
                )
            }
            aisles[aisleIndex].items = newItem;
            result = true;
        } else {
            AUTIAC(&_puts, "You must enter a valid aisle!");
            pac_free(newItem);
        }
    }
    AUTIASP(result);
}

bool intro() {
    PACIASP();
    char *hello = "Welcome to the Shelf PACker!\n============================";
    char newLine;
    char *readString = NULL;
    AUTIAC(&_puts, hello);
    AUTIAC(&_puts, "Tell me about your supermarket.");
    AUTIAC(&_fputs, "Name? ", stdout);
    supermarket_name = pac_alloc(sizeof(char) * (MAX_NAME+1));

    DOPACTHING(supermarket_name,
        AUTIAC_R(&_fgets, readString, supermarket_name, MAX_NAME, stdin);
    )
    AUTIAC(&_fputs, "How many aisles? ", stdout);
    AUTIAC(&_scanf, "%u", &num_aisles);
    AUTIAC(&_scanf, "%c", &newLine);
    if(num_aisles > MAX_AISLES) {
       AUTIAC(&_puts, "Whoa there, thats a big store. I can't handle that.");
       AUTIASP(false);
    }
    if(num_aisles <= 0) {
       AUTIAC(&_puts, "Need at least one aisle to put stuff in.");
       AUTIASP(false);
    }

    for(int i=0; i < num_aisles; i++) {
        aisles[i].items = NULL;
        setAisleNameIndex(i);
    }

    AUTIASP(true);
}

void help() {
    PACIASP();
    AUTIAC(&_puts, "p\tPrint all Items");
    AUTIAC(&_puts, "a\tAdd new item");
    AUTIAC(&_puts, "s\tSet Aisle name");
    AUTIAC(&_puts, "g\tGet Aisle name");
    AUTIAC(&_puts, "n\tPrint supermarket name");
    AUTIAC(&_puts, "r\tRename supermarket");
    AUTIAC(&_puts, "q\tQuit");
    AUTIAC(&_puts, "h\tThis Help");
    AUTIASP();
}

void menu() {
    PACIASP();
    char input[4];
    char newline = '0';
    char *readString = NULL;
    char *menu = "menu > ";

    AUTIAC(&_printf, menu, stdout);

    AUTIAC_R(&_fgets, readString, input, 3, stdin);

    while (input[0] != EOF && input[0] != 'q') {

      switch(input[0]) {
          case 'p':
            print();
            break;
          case 'a':
            addItem();
            break;
          case 's':
            setAisleName();
            break;
          case 'g':
            printAisleName();
            break;
          case 'n':
            printName();
            break;
          case 'r':
            renameSupermarket();
            break;
          case 'h':
            help();
            break;
          default:
            break;
      }

      AUTIAC(&_printf, menu, stdout);
      AUTIAC_R(&_fgets, readString, input, 3, stdin);
    }
    AUTIASP();
}

int main(int argc, char **argv) {
    init();
    PACIASP();

    AUTIAC(&xsetbuf, stdin, NULL);
    AUTIAC(&xsetbuf, stdout, NULL);
    AUTIAC(&xsetbuf, stderr, NULL);

    if( intro()) {
      menu();
      freeAisles();
      pac_free(supermarket_name);
    }
    /* whilst we can protect the return here, the libc returns __start etc wont be protected
     * so we exit here to block that easy way to code execution */
    AUTIAC(&__exit, 0);
    AUTIASP(0);
}
