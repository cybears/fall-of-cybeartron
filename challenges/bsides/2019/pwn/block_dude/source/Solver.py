import itertools
import logging
import socket
import time
import sys
import traceback
from pathlib import Path

assert sys.version_info >= (3, 0)

class GameFinished(BaseException):
    def __init__(self, msg):
        self.msg = msg

class BlockDude:
    TILE_EMPTY  = b' '
    TILE_WALL   = b'w'
    TILE_LEFT   = b'l'
    TILE_RIGHT  = b'r'
    TILE_BLOCK  = b'b'
    TILE_DOOR   = b'd'

    TILES = [
        TILE_EMPTY,
        TILE_WALL,
        TILE_LEFT,
        TILE_RIGHT,
        TILE_BLOCK,
        TILE_DOOR
    ]

    GRID_SIZE = 18
    TILE_SIZE = 24

    MAP_X = 50
    MAP_Y = 18

    def __init__(self, hostname, port):

        # Initialize connection to server
        self.socket = socket.socket()
        self.socket.connect((hostname, port))

        # Initialize
        self.running = True

        # Initialize map
        self.map = [[BlockDude.TILE_EMPTY] * BlockDude.MAP_Y for _ in range(BlockDude.MAP_X)]
        self.goal = [[BlockDude.TILE_EMPTY] * BlockDude.MAP_Y for _ in range(BlockDude.MAP_X)]
        self.x = 2
        self.y = 1
        self.dir = 1
        self.b = False

        # Read initial map from server
        self.read_update_map()

    def __del__(self):
        self.socket.close()

    def read_update_map(self):

        # Read map from server
        data = b''
        while len(data) != BlockDude.GRID_SIZE * BlockDude.GRID_SIZE:
            d = self.socket.recv(BlockDude.GRID_SIZE * BlockDude.GRID_SIZE)
            # Break if socket closed
            if not d:
                break
            data += d

        # Verify map data
        if len(data) != BlockDude.GRID_SIZE * BlockDude.GRID_SIZE or set(data).issubset(BlockDude.TILES):
            if len(data) == 0:
                err = "Server closed connection"
            else:
                try:
                    data = data.decode('utf-8')
                except:
                    pass
                err = data
            self.running = False
            raise GameFinished(err)

        # Calculate window bounds
        hor = min(max(self.x - BlockDude.GRID_SIZE // 2, 0), BlockDude.MAP_X - BlockDude.GRID_SIZE)

        # Update display
        for i,t in enumerate(data):
            x = (i % BlockDude.GRID_SIZE)
            y = (i // BlockDude.GRID_SIZE)
            t = bytes([t])

            # Update map
            self.map[x+hor][y] = t

            if t == BlockDude.TILE_LEFT:
                self.y = y
                self.dir = 1
            elif t == BlockDude.TILE_RIGHT:
                self.y = y
                self.dir = -1

    def send_update_map(self, cmd):
        self.socket.send(cmd)
        self.read_update_map()

    def game_loop(self):

        # First wall near
        self.create_tower(11, 1, -1)

        # First wall far
        self.create_tower(13, 1, 1)

        # Second wall near
        self.create_tower(26, 2, -1)
        self.create_tower(28, 2, 1)

        # build initial towers
        self.run_auto_target()

        # Spawn leak boxes
        target = (0,1)
        self.b = False
        self.run(target, True)
        self.b = False
        self.run(target, True)
        self.b = False
        self.run(target, True)

        # Search for floating box - it should be in the region:
        #   3 <= x <= 17, 3 <= y <= 5
        for x, y in itertools.product(range(3, 18), range(3, 6)):
            if self.map[x][y] == BlockDude.TILE_BLOCK:
                # XXX: I think it's possible for both leak boxes to be found in
                # the region we're running through but since we're counting
                # "up" and "left" (ie. away from the box origin) we should find
                # the lower half of the leak first even if the second one is
                # present.
                leak = (x,y)
                print("leak: {}".format(leak))
                break
        else:
            raise Exception("No leak found!")

        # in case it makes the first wall taller
        if leak[1] == 3 and leak[0] >= 11 and leak[0] <= 14:
            self.create_tower(leak[0]-1, 2, -1)
            self.create_tower(leak[0]+1, 2, 1)

        # Build destination tower
        self.create_tower(leak[0] + 9, leak[1], -1)

        # Remove last block from destination tower
        self.goal[leak[0] + 9][leak[1]] = BlockDude.TILE_EMPTY
        self.run_auto_target()

        # Build source tower
        self.create_tower(leak[0] - 1, leak[1] - 1, -1)
        self.run_auto_target()

        # Set source as target
        target = (leak[0], leak[1])
        self.run(target, True)

        # Set dest as target
        target = (leak[0] + 9, leak[1])
        self.run(target, False)

        # Set out of bounds as target
        target = (255, 1)
        self.run(target, None)

        raise Exception("Game didn't finish!")

    def run_auto_target(self):
        while self.running:
            cmd = self.get_cmd(self.get_target())
            if cmd:
                self.send_update_map(cmd)
            else:
                break

    def run(self, target, block):
        while self.running:
            cmd = self.get_cmd(target, block)
            if cmd:
                self.send_update_map(cmd)
            else:
                break

    def create_tower(self, x, y, d):

        # Create tower with top at X and Y in direction D
        for h in range(y):
            for w in range(h+1):
                self.goal[x+w*d][y-h] = BlockDude.TILE_BLOCK

    def get_cmd(self, target, block=None):
        if not target:
            return None
        directions = { 1: b'W', -1: b'E' }

        # Get target direction
        direction = 1 if target[0] > self.x else -1

        # try not to move on top of target
        if self.x + direction == target[0] and self.dir != direction:
            direction = -direction

        # Check if reached target
        if self.x + direction == target[0]:
            # check if state reached
            if self.b == block:
                return None
            self.b = not self.b
            return b'S'

        # Check obstacle
        if self.map[self.x + direction][self.y] not in [BlockDude.TILE_EMPTY, BlockDude.TILE_DOOR]:
            if self.dir == direction:
                self.x += direction
                return b'N'
            else:
                # No x movement here -> just change direction on spot
                return directions[direction]

        # Move in correct direction
        self.x += direction
        return directions[direction]

    def get_tower_target(self):
        # Diff map and goal looking for next block
        for x in range(BlockDude.MAP_X):
            for y in range(1, BlockDude.MAP_Y):
                if self.map[x][y] == BlockDude.TILE_EMPTY and self.goal[x][y] == BlockDude.TILE_BLOCK:
                    return (x,y)
        return None

    def get_target(self):
        target = self.get_tower_target()
        if target and not self.b:
            # Need to go get block
            return (0,1)
        return target

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('hostname',
        type=str,
        help='Hostname of blockdude server')
    parser.add_argument('--port',
        type=int,
        default=31337,
        help='Port number of blockdude server')
    args = parser.parse_args()

    # The solver sometimes hits conditions which it doesn't solve correctly so
    # we'll just retry a few times if the game loop blows up
    for i in range(5):
        # Initialize and start game loop
        try:
            bd = BlockDude(**vars(args))
            bd.game_loop()
        except GameFinished as exc:
            print("Finished the game! Final message was %r" % exc.msg)
            if "cybears{i_5ti77_u53_my_ti-83}" not in exc.msg:
                print("Failed attempt %i - the flag wasn't extracted" % i)
            else:
                break
        except Exception as exc:
            print("Failed attempt %i: %s" % (i, exc))
            traceback.print_exc()
        # The server seeds its random address generator using `time(2)` so we
        # should just wait a second so we get a different address if we didn't
        # manage to get a solve this time
        time.sleep(1)
    else:
        raise Exception("Failed %i times!" % i)
