#!/usr/bin/env python3

import subprocess
import bottle
import os
import base64
import json
import shutil
import uuid

OUTDIR = './box'
if not os.path.isdir(OUTDIR):
    os.mkdir(OUTDIR)


@bottle.route('/run', method='POST')
def run():
    dest_dir = os.path.join(OUTDIR, str(uuid.uuid4()))
    try:
        dest_file_name = str(uuid.uuid4())
        dest_file = os.path.join(dest_dir, dest_file_name)
        binary_file = bottle.request.files.get('file')
        if not os.path.isdir(dest_dir):
            os.mkdir(dest_dir)
        binary_file.save(dest_file)
        os.chmod(dest_file, 0o500)
        p = subprocess.Popen(['/secelf-server/handout/loader', dest_file_name], cwd=dest_dir, stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate(timeout=120)
        return json.dumps({'stdout': base64.b64encode(stdout).decode('utf-8'), 'stderr': base64.b64encode(stderr).decode('utf-8')})
    finally:
        shutil.rmtree(dest_dir, ignore_errors=True)

if __name__ == '__main__':
    bottle.run(host='0.0.0.0', port=2323)
