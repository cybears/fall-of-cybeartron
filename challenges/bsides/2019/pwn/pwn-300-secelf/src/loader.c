/*
This file contains an ELF loader that loads
specially signed ELF files.
*/

#include <elf.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>

#include <sodium.h>

#include "key_pub.h"

#define DIE(...) fprintf(stderr, "ABORT " __FILE__ ":%d : ", __LINE__);fprintf(stderr, __VA_ARGS__); fflush(NULL); abort();
#define LOG(...) printf(__FILE__":%d\t", __LINE__); printf(__VA_ARGS__);

#define ALIGN(x, a)     __ALIGN_MASK(x, (typeof(x))(a)-1)
#define __ALIGN_MASK(x, mask)    (((x)+(mask))&~(mask))

int validate_elf(uint8_t* buff, size_t size) {
    if (size <= EI_NIDENT) {
        // Well this is too small for me to even identify it as an ELF...
        DIE("This ELF is smaller than the magic!");
        goto error;
    }
    if (size < sizeof(Elf64_Ehdr)) {
        // Technically possible for a valid ELF, but not for a SecElf
        DIE("This ELF is smaller than the ELF header!");
        goto error;
    }

    // Alright, the size is fine, but what's an ELF without magic?
    if (buff[0] == ELFMAG0 && buff[1] == ELFMAG1 && buff[2] == ELFMAG2 && buff[3] == ELFMAG3) {
        // Safe to cast this to an ELF struct and take a look
        Elf64_Ehdr* elf_header = (Elf64_Ehdr*)buff;
        LOG("ELF magic: %.4s\n", elf_header->e_ident);

        if (elf_header->e_shentsize != sizeof(Elf64_Shdr)) {
            DIE("Your section header size doesn't match mine. Yours: %d, mine: %d\n", elf_header->e_shentsize, sizeof(Elf64_Shdr));
        }

        if (elf_header->e_phentsize != sizeof(Elf64_Phdr)) {
            DIE("Your program header size doesn't match mine. Yours: %d, mine %d\n", elf_header->e_phentsize, sizeof(Elf64_Phdr));
        }

        if (elf_header->e_type == ET_EXEC) {
            // We only x86_64 support executables
            if (elf_header->e_machine == EM_X86_64) {
                return 0;   
            }
            else {
                DIE("We only support X86_64 files");
            }
        }
        else {
            DIE("We only support executable files.");
        }
    }
    else {
        DIE("We only support ELF files!");
    }

    error:
    return -1;
}

Elf64_Shdr* locate_section(char* elf, size_t elf_size, const char* name) {

    Elf64_Ehdr* elf_header = elf;
    if (elf_header->e_phoff > elf_size || elf_header->e_shoff > elf_size) {
        DIE("Program headers or section headers out of bounds!");
    }
    Elf64_Phdr* program_headers = elf + elf_header->e_phoff;
    Elf64_Shdr* section_headers = elf + elf_header->e_shoff;

    if (elf_header->e_shstrndx == SHN_UNDEF) {
        // No section name string table, bail
        DIE("No section name string table!");
    }

    LOG("Section %d/%d holds the section header names\n", elf_header->e_shstrndx + 1, elf_header->e_shnum);
    if (elf_header->e_shstrndx > elf_header->e_shnum) {
        DIE("Section header name table is too far! %d > %d", elf_header->e_shstrndx, elf_header->e_shnum);
    }

    if (elf_header->e_shstrndx > SHN_LORESERVE) {
        DIE("The section header name table is larger than SHN_LORESERVE. We don't support this ELF");
    }

    if (elf_header + elf_size < &(section_headers[elf_header->e_shstrndx])) {
        DIE("Section header name table is out of bounds!");
    }
    Elf64_Shdr string_section = section_headers[elf_header->e_shstrndx];
    if (string_section.sh_type != SHT_STRTAB) {
        // We have the wrong section!
        DIE("Invalid ELF file. Section header string table section is not a string table!");
    }

    if (string_section.sh_size <= 0) {
        DIE("No strings in the section header string table!");
    }

    LOG("Searching for code signature section...\n");
    for (Elf64_Section i = 0; i < elf_header->e_shnum; i++) {
        Elf64_Shdr section_header = section_headers[i];
        char* section_name = section_header.sh_name + (elf + string_section.sh_offset);
        if (section_header.sh_name >= string_section.sh_size) {
            // The name lies outside the string table, bad bad bad
            DIE("Section name lies outside the string table!\n");
        }
        LOG("Section name: %s\n", section_name);
        if (strncmp(name, section_name, strlen(name) + 1) == 0) {
            // FOUND IT! Return the address of this section header
            return &(section_headers[i]);
        }
    }
    // No code signature no run!
    DIE("No code signature found!");
}


Elf64_Shdr* locate_code_signature(char* elf, size_t elf_size) {
    Elf64_Shdr* section_header = locate_section(elf, elf_size, ".text.code_signature");
    char* code_signature_buff = elf + section_header->sh_offset;
    size_t code_signature_size = section_header->sh_size;
    LOG("Located %zu bytes of code signatures @ %p\n", code_signature_size, code_signature_buff);
    return section_header;
}

void display_hash(const unsigned char* hash) {
    for (int j = 0; j < crypto_hash_sha256_BYTES; j++) {
        printf("%.2hhX", hash[j]);
    }
}

int validate_section_code_signature(char* buff, size_t buff_size, void* main_addr, Elf64_Shdr* section, Elf64_Shdr* code_signature) {
    unsigned char* signature = buff + code_signature->sh_offset;
    unsigned char* entry_signature = signature + crypto_sign_BYTES;
    int* num_buckets = entry_signature + crypto_sign_BYTES;
    unsigned char* buckets = ((char*)num_buckets + 4 );
    LOG("Section @ %p\n", buff + section->sh_offset);
    LOG("Signature len: %d Hash len: %d Signature @ %p Entry Signature @ %p Num buckets @ %p Buckets @ %p\n", crypto_sign_BYTES, crypto_hash_sha256_BYTES, signature, entry_signature, num_buckets, buckets);
    LOG("%d buckets: %d bytes\n", *num_buckets, crypto_hash_sha256_BYTES * (*num_buckets));

    // This is just for debugging
    unsigned char full_section_hash[crypto_hash_sha256_BYTES];
    crypto_hash_sha256(full_section_hash, buff + section->sh_offset, section->sh_size);
    LOG("Section \t\t");
    display_hash(full_section_hash);
    printf("\n");

    // Actually do the verification
    unsigned char combined_hash[crypto_hash_sha256_BYTES];
    unsigned char main_hash[crypto_hash_sha256_BYTES];
    crypto_hash_sha256(combined_hash, buckets, crypto_hash_sha256_BYTES * (*num_buckets));
    crypto_hash_sha256(main_hash, &main_addr, 8);

    size_t remaining_section_size = section->sh_size; 

    for (int i = 0; i < *num_buckets; i++) {
        unsigned char* bucket = (buckets + (i*crypto_hash_sha256_BYTES)); 
        LOG("Bucket %.2d @ %p\t\t", i, bucket);
        display_hash(bucket);
        printf("\n");


        // Hash the i'th page of the section
        unsigned char* bucket_section = buff + section->sh_offset + (0x4000 * i);
        size_t bucket_section_size = 0x4000;
        if (remaining_section_size < 0x4000) {
            bucket_section_size = section->sh_size - (0x4000 * i) ;
            LOG("SHORT! 0x%x\n", bucket_section_size);
        }
        remaining_section_size = remaining_section_size - bucket_section_size;


        LOG("Covers: 0x%x bytes @ %p\n", bucket_section_size, bucket_section);
        unsigned char* section_hash[crypto_hash_sha256_BYTES];
        crypto_hash_sha256(section_hash, bucket_section, bucket_section_size);
        if (memcmp(bucket, section_hash, crypto_hash_sha256_BYTES) != 0) {
            LOG("Mismatch!\t\t\t\t");
            display_hash(section_hash);
            printf("\n");
            DIE("Bucket %d has been modified!\n", i);
        }
             
    }
    LOG("Combined hash: ");
    display_hash(combined_hash);
    printf("\n");

    LOG("Entry: %p\n", main_addr);
    LOG("Entry hash: ");
    display_hash(main_hash);
    printf("\n");
    if (crypto_sign_verify_detached(signature, combined_hash, crypto_hash_sha256_BYTES, key_pub) == 0) {
        if (crypto_sign_verify_detached(entry_signature, main_hash, crypto_hash_sha256_BYTES, key_pub) == 0) {
            LOG("Code signature is valid!\n");
        }
        else {
            DIE("Entry point validation error!\n");
        }
    }
    else {
        DIE("Signature validation error!\n");
    }

    return 0;
}

int load_elf(void* buff, size_t buff_size) {
    if (NULL == buff || buff_size == 0) {
        DIE("buff is NULL!");
    }

    if (0 == validate_elf(buff, buff_size)) {
        // First locate the code signature block
        Elf64_Shdr* code_signature_section = locate_code_signature(buff, buff_size);
        if (NULL == code_signature_section) {
            DIE("No code signature section!");
        }

        Elf64_Ehdr* elf_header = (Elf64_Ehdr*)buff;
        if (elf_header->e_phoff >= buff_size || elf_header->e_shoff >= buff_size) {
            DIE("Program or Section headers lie outside the ELF!");
        }
        Elf64_Phdr* program_headers = buff + elf_header->e_phoff;
        Elf64_Shdr* section_headers = buff + elf_header->e_shoff;

        // Next lets walk through each segment and allocate it
        for (int i = 0; i < elf_header->e_phnum; i++) {
            if ((&program_headers[i]) < buff + buff_size) {
                Elf64_Phdr program_header = program_headers[i];
                LOG("Program header found @ %p: Offset: 0x%x\n", &(program_headers[i]), program_header.p_offset);

                if (program_header.p_type == PT_LOAD && program_header.p_memsz > 0) {
                    LOG("vaddr: %p memsize: %zu, filesize: %zu\n", program_header.p_vaddr, program_header.p_memsz, program_header.p_filesz);
                    size_t aligned_size = ALIGN(program_header.p_memsz, 0x8000);
                    LOG("Aligned size: %zu\n", aligned_size);
                    void* mapping = mmap(program_header.p_vaddr, aligned_size, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, 0, 0);
                    if (mapping != NULL && mapping != MAP_FAILED) {
                        LOG("Mapped!\n");
                        if (program_header.p_vaddr == memcpy(program_header.p_vaddr, buff + program_header.p_offset, program_header.p_filesz)) {
                            LOG("Copied!\n");
                        }
                        else {
                            DIE("Copy failed!");
                        }
                    }
                    else {
                        DIE("Failed to map segment!");
                    }
                }
            }
            else {
                DIE("Refusing to load bad offset @ %p", &program_headers[i]);
            }
        }

        void* main_addr = NULL; 
        // Now that we have everything loaded, lets find the main function!
        for (Elf64_Section i = 0; i < elf_header->e_shnum; i++) {
            Elf64_Shdr section_header = section_headers[i];
            if (section_header.sh_type == SHT_SYMTAB) {
                LOG("Found symbol table\n");
                Elf64_Sym* symbol_table = buff + section_header.sh_offset;
                Elf64_Shdr* string_table_section = &(section_headers[section_header.sh_link]);
                char* string_table = buff + string_table_section->sh_offset;
                for (size_t i = 0; i < (section_header.sh_size/section_header.sh_entsize); i++) {
                    char* name = string_table + symbol_table[i].st_name;
                    LOG("name: %s addr %p\n", name, symbol_table[i].st_value);
                    if (strcmp("main", name) == 0) {
                        main_addr = symbol_table[i].st_value;
                    }
                }
                
            }
        }

        if (main_addr == NULL) {
            DIE("Didn't find main!");
        }

        // Now reprotect the memory
        for (int i = 0; i < elf_header->e_phnum; i++) {
            if ((&program_headers[i]) < buff + buff_size) {
                Elf64_Phdr program_header = program_headers[i];
                int mmap_prot = PROT_READ;
                if (program_header.p_flags & PF_R) {
                    mmap_prot |= PROT_READ;
                }
                if (program_header.p_flags & PF_W) {
                    mmap_prot |= PROT_WRITE;
                }
                if (program_header.p_flags & PF_X) {
                    // All executable memory must be signed!
                    if (0 != validate_section_code_signature(buff, buff_size, main_addr, locate_section(buff, buff_size, ".text"), code_signature_section)) {
                        DIE("Code signature is not valid!");
                    }
                    mmap_prot |= PROT_EXEC;
                }

                if (program_header.p_vaddr != NULL && program_header.p_memsz > 0) {
                    mprotect(program_header.p_vaddr, program_header.p_memsz, mmap_prot);
                    LOG("Re-protected!\n");
                }
            }
        }

        Elf64_Shdr* text_shdr = locate_section(buff, buff_size, ".text");
        if (NULL == text_shdr || main_addr < text_shdr->sh_addr || main_addr > text_shdr->sh_addr + text_shdr->sh_size) {
            DIE("main lies outside the .text section!");
        }

        LOG("ELF mapped, returning entry point @ %p\n", main_addr);
        return main_addr;
    }
}

int jump_to_main(void* entry, int argc, char** argv, char** envp) {
    // This is split into it's own method to keep control flow integrity happy
    void* (*entry_func)(int, char**, char*) = entry;
    int retval = entry_func(argc-1, argv+1, envp);
    fflush(NULL); // Make sure we don't buffer any output
    return retval;
}

int main(int argc, char** argv, char** envp) {
    LOG("Welcome to SECURE ELF Version: " __DATE__ " " __TIME__ "\n");
    if (argc != 2) {
        DIE("Usage: ./loader ./test.signed");
    }
    int elf_fd = open(argv[1], O_RDONLY);
    if (elf_fd >= 0) {
        off_t elf_size = lseek(elf_fd, 0, SEEK_END);
        lseek(elf_fd, 0, SEEK_SET);
        void* elf_buff = mmap(NULL, elf_size, PROT_READ, MAP_PRIVATE, elf_fd, 0);
        if (elf_buff != NULL && elf_buff != -1) {
            LOG("Loading ELF %s %d @ %p\n", argv[1], elf_size, elf_buff);
            void* (*entry)(int, char**, char*) = load_elf(elf_buff, elf_size);
            LOG("Calling entrypoint @ %p\nTHANKS FOR PLAYING!\n--------\n", entry);
            exit(jump_to_main(entry, argc, argv, envp));
        }
        else {
            DIE("mmap of ELF file failed!");
        }
    }
    else {
        DIE("Failed to open %s", argv[1]);
    }
    exit(1); 
}

