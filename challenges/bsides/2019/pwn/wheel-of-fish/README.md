# Wheel of Fish

## Infrastructure

Use the Dockerfile to build a container to host the challenge.

## Solution

In Wheel of Fish, the player gets three spins of the wheel of fish. If you get the Red Snapper, you have the option to keep it or trade it all in for what's in the box. If you're lucky, there's a flag in the box... otherwise you get nothing.

The game reads the player's name from the console into an eight byte stack buffer. However the scanf() call uses "%16s" as the format string, so you can overflow an extra eight bytes. This is not enough to get code execution: and even if it was, there is a stack cookie preventing you from overflowing the return address. However, you can overflow the local variable used to seed the random number generator. This can be used to fix the result of rand() so you can win the game.

A bruteforce solver is included in the repo. It will generate a name string to enter which will overflow the random seed such that the first spin gives you the Red Snapper, and then going for "what's in the box" will return the flag. It also ensures that the generated seed is printable so you can type it into the challenge.

(this challenge is heavily inspired by the movie UHF: https://www.youtube.com/watch?v=KezvwARhBIc )

## Flag

```
cybears{omg_thats_soooooooo_pseudorandom}
```
