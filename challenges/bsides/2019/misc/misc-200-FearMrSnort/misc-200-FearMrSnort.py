#!/usr/bin/python3

#Misc Challenge -

#python3 misc-200-FearMrSnort.py

#requirements

## SERVER
# socat TCP-LISTEN:1729,reuseaddr,fork EXEC:"python3 ./misc-200-FearMrSnort.py"

##CLIENT
# nc localhost 1729

import string

flag = "cybears{N0@m_3lk135_4nn0y1ng_4d@g3s}"

names = ['Optimus Prime', 'Bumblebee', 'Megatron', 'Starscream', 'Grimlock', 'Sideswipe', 'Ironhide', 'Ratchet', 'Soundwave', 'Jazz', 'Prowl', 'Barricade', 'Devastator', 'Ultra Magnus', 'Brawl', 'Drift', 'Thundercracker', 'Cliffjumper', 'Arcee', 'Mirage', 'Galvatron', 'Hound', 'Jetfire', 'Shockwave', 'Skids', 'Lockdown', 'Strafe', 'Blackout', 'Skywarp', 'Smokescreen', 'Strongarm', 'Cheetor', 'Ravage', 'Bonecrusher', 'Dead End', 'Mudflap', 'Snarl', 'Wheeljack', 'Blades', 'Bulkhead', 'Heatwave', 'Inferno', 'Optimus Primal', 'Scourge', 'Thrust', 'Ramjet', 'Silverbolt', 'Breakdown', 'Cyclonus', 'Jolt']

#no spaces and all uppercase, only letters A-Z
names_normalised = ['OPTIMUSPRIME', 'BUMBLEBEE', 'MEGATRON', 'STARSCREAM', 'GRIMLOCK', 'SIDESWIPE', 'IRONHIDE', 'RATCHET', 'SOUNDWAVE', 'JAZZ', 'PROWL', 'BARRICADE', 'DEVASTATOR', 'ULTRAMAGNUS', 'BRAWL', 'DRIFT', 'THUNDERCRACKER', 'CLIFFJUMPER', 'ARCEE', 'MIRAGE', 'GALVATRON', 'HOUND', 'JETFIRE', 'SHOCKWAVE', 'SKIDS', 'LOCKDOWN', 'STRAFE', 'BLACKOUT', 'SKYWARP', 'SMOKESCREEN', 'STRONGARM', 'CHEETOR', 'RAVAGE', 'BONECRUSHER', 'DEADEND', 'MUDFLAP', 'SNARL', 'WHEELJACK', 'BLADES', 'BULKHEAD', 'HEATWAVE', 'INFERNO', 'OPTIMUSPRIMAL', 'SCOURGE', 'THRUST', 'RAMJET', 'SILVERBOLT', 'BREAKDOWN', 'CYCLONUS', 'JOLT']

debug = False

def StringToVector(s):
        #remove spaces
        #make all upper case
        #freq count to vector

        FreqCount = [0]*26
        s = s.upper()
        for c in s:
                if c in string.ascii_uppercase:
                        index = ord(c) - ord('A')
                        FreqCount[index] += 1

        return FreqCount

if __name__ == "__main__":

    user_input = input("Please enter your anagram: \n")

    if (debug == True):
        print("DEBUG: {}".format(str(user_input)))

    #Check split in half with "="
    if (len(user_input.split("=")) != 2):
        print("ERROR: Needs = sign to separate LHS and RHS")
        exit()

    LHS = user_input.split("=")[0]
    RHS = user_input.split("=")[1]

    #normalise and split on "+"
    LHS = list(map(str.strip , LHS.upper().split("+")))
    RHS = list(map(str.strip , RHS.upper().split("+")))

    LHS = [s.replace(" ", "") for s in LHS ]
    RHS = [s.replace(" ", "") for s in RHS ]

    #check for uniqueness
    ALL = LHS + RHS
    ALL.sort()

    dupe = False
    for i in range(len(ALL)-1):
        if ALL[i] == ALL[i+1] :
            dupe = True

    if (dupe):
        print("ERROR: All elements need to be unique")
        exit()

    #check that the names are from the predetermined list

    in_list = True

    for a in ALL:
        if a not in names_normalised:
            in_list = False

    if (in_list == False):
        print("ERROR: All elements must be from Transformers.txt")
        exit()

    #check at least one side has at least 12 elements
    min_length = (len(LHS) >= 12) or (len(RHS) >=12)
    if (not min_length):
        print("ERROR: At least one side must have more than 12 elements")
        exit()

    #check that LHS anagrams to RHS
    anagram = (StringToVector("".join(LHS)) == StringToVector("".join(RHS)) )
    if(anagram == True):
        print("Congratulations! Here is your flag: {}".format(flag))
    else:
        print("ERROR: LHS does not anagram to RHS")
        exit()

