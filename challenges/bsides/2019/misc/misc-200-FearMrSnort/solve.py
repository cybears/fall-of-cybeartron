#fearmrsnort CI tester

## python3 fearmrsnort_ci_socket.py -n localhost -p 1729

import socket
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--hostname', type=str,  default="localhost", help='Hostname')
parser.add_argument('-p', '--port', default=1729, type=int, help='Remote port')
args = parser.parse_args()

hostname = args.hostname
port = args.port

SOLUTION = b"OPTIMUSPRIME+RATCHET+BARRICADE+ARCEE+HOUND+LOCKDOWN+STRAFE+SMOKESCREEN+RAVAGE+BLADES+THRUST+RAMJET+JOLT = STARSCREAM+DEVASTATOR+JETFIRE+BLACKOUT+STRONGARM+CHEETOR+BONECRUSHER+DEADEND+WHEELJACK+OPTIMUSPRIMAL"

FLAG = b'cybears{N0@m_3lk135_4nn0y1ng_4d@g3s}'

if __name__ == "__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((hostname, port))
        print("INFO: Connected")
        data = s.recv(1024)
        s.sendall(SOLUTION + b"\n\n")
        r = s.recv(1024)
        print("INFO: " + r.decode("utf-8").strip())
        assert(FLAG in r)
