# Not automated

Feel free to convert "run_solution.sh_using_dind.sh" to CI script for this project. I just don't know how they format of the gitlab-ci.yml files works (vs .gitlab-ci.yml).

# Log of running the solution script:

```

[user@ctf:~/.../cybeartron/challenges/misc/stevedore-level1]$ ./run_solution.sh_using_dind.sh
63c60a85950c1065c30aaa0f7c513e5d97716ed48d73640bfc96cf1ac9885b2b
/sbin/apk
fetch http://dl-cdn.alpinelinux.org/alpine/v3.9/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.9/community/x86_64/APKINDEX.tar.gz
(1/10) Installing libbz2 (1.0.6-r6)
(2/10) Installing expat (2.2.6-r0)
(3/10) Installing libffi (3.2.1-r6)
(4/10) Installing gdbm (1.13-r1)
(5/10) Installing ncurses-terminfo-base (6.1_p20190105-r0)
(6/10) Installing ncurses-terminfo (6.1_p20190105-r0)
(7/10) Installing ncurses-libs (6.1_p20190105-r0)
(8/10) Installing readline (7.0.003-r1)
(9/10) Installing sqlite-libs (3.26.0-r3)
(10/10) Installing python3 (3.6.8-r1)
Executing busybox-1.29.3-r10.trigger
OK: 81 MiB in 49 packages
/usr/bin/python3
Sending build context to Docker daemon  1.615MB
Step 1/5 : FROM scratch
 ---> 
Step 2/5 : COPY catflag/catflag /
 ---> 6dfc1921f8d4
Step 3/5 : CMD ["/catflag"]
 ---> Running in 6240c85d2777
Removing intermediate container 6240c85d2777
 ---> ba9367d42ca1
Step 4/5 : COPY flag.txt /
 ---> 78b18c294823
Step 5/5 : COPY empty.txt /flag.txt
 ---> d80b24cdcad0
Successfully built d80b24cdcad0
Successfully tagged level1a-tmp:latest

===
found level1a flag - copying to root
===

Sending build context to Docker daemon  49.66kB
Step 1/4 : FROM alpine
latest: Pulling from library/alpine
8e402f1a9c57: Pull complete 
Digest: sha256:644fcb1a676b5165371437feaa922943aaf7afcfa8bfee4472f6860aad1ef2a0
Status: Downloaded newer image for alpine:latest
 ---> 5cb3aa00f899
Step 2/4 : COPY flag.txt /
 ---> 738de31ead06
Step 3/4 : RUN rm /bin/cat && touch /bin/cat && chmod +x /bin/cat && echo -e '#!/bin/sh\necho "[This is an empty flag that the recipient will return through customs]"' > /bin/cat
 ---> Running in 24a1abe0ba80
Removing intermediate container 24a1abe0ba80
 ---> ac0b1651d5cb
Step 4/4 : CMD cat /flag.txt
 ---> Running in 52705d7d39a6
Removing intermediate container 52705d7d39a6
 ---> f7a83ea0f373
Successfully built f7a83ea0f373
Successfully tagged level1b-tmp:latest

===
Found level1b flag - copying to root
===


===
Flag is: cybears{tru$t_in_th3_c0nta!n3rz}
===

Untagged: level1a-tmp:latest
Deleted: sha256:d80b24cdcad0ec1fb5bbfb1af3743584df8dea1be3cc7385189acde382ab72dc
Deleted: sha256:78fa69ea659334266a9460467a4c0c2e76489928215ee4024402177868b63c9e
Deleted: sha256:78b18c294823d0ddc92747560fa7ea30fa466b5b87f0fedeca577593663931e6
Deleted: sha256:c5d3e7ea9b1cd072a4ebf89beeb635182a02f84b206c70bd8600a578b243080e
Deleted: sha256:ba9367d42ca1aa521fcab1346c4e77c378be9a642099bbb8ea7a194f410c41bd
Deleted: sha256:6dfc1921f8d447877f3a28fdc95b7ccdb54b59aa0a5a770802677b1bbdd57a34
Deleted: sha256:6dfb80b7088c50c9bf5e26a371b37907514b9dc9fb87ce96c24cd67193bbcd39
Untagged: level1b-tmp:latest
Deleted: sha256:f7a83ea0f373fbed41e51cff9c85c9bc8a648307cde39c8fc102eab711c0d501
Deleted: sha256:ac0b1651d5cb2162ef2c5ba1a52950e2232c2c8f232e9624da3320c578a591b7
Deleted: sha256:5736e9965a20446acb61e14a022ca05c903d21b3410b42b07b276b761d695e52
Deleted: sha256:738de31ead06d610e856022a07968e33d220fdcb6546bcd46091706537569869
Deleted: sha256:70f43aaa2c2b46abe2069db2c2f4eb6f3f6d1defffcb3bb69a35d1c44fdece6e
dind

```

