package main

import (
    "fmt"
    "io/ioutil"
)

func main() {
    b, err := ioutil.ReadFile("flag.txt") // just pass the file name
    if err != nil {
        fmt.Print(err)
    }

	str := string(b) // convert content to a 'string'
    fmt.Println(str) // print the content as a 'string'
}
