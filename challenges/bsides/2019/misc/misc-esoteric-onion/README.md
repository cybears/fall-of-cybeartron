# ESOTERIC ONION

## Handout

- chicken.zip
- FW_What_a_head_case.txt


## Running

None

<details>
<summary>SPOILERS</summary>
<p>

## Flag Notes / Building the handout

The flag embedded in the brainfuck + path code is manually done so is difficult to change (esoteric is my jam); so
it'd be easier to change flag params in CTFd

The other two are pretty easy to tweak, just change the flag at the top of 'src/02_path/r_and_d_notes_bad.path' and
'src/03_chicken/chn_flag.txt' then run the builder.py script again and it'll repackage the docs into the chicken.zip

## Premise

Esoteric languages are useless but they're also pretty funny.

This challenge is a russian doll or onion of esoteric languages whereby once
you figure out one esoteric language, you get hit with another esoteric
language... its about learning weird coding languages, and maybe having a
laugh on the way! (As well as getting some kind of exposure to some really
weird hobbies people have.)

The story is that an employee with the R&D division for cybears is looking at
different languages to try. However, the employee gets convinced that the
company is messing with the employee's head. As such there is a bit of crazy
talk about hiding stuff... what is hidden is the flag!

The chicken .js interpreter is also a work of art and people should find it:
```
    function chicken(CHICKEN, Chicken) {
        Chicken &&( chicken. chicken =[,
        CHICKEN, CHICKEN = Chicken = chicken.
        $Chicken =-( CHICKEN ==( chicken.
        Chicken = Chicken ))], chicken.
        chicken [Chicken++] = chicken. chicken, chicken.
        CHICKEN = ++Chicken, chicken (--Chicken), chicken.
        $Chicken = ++Chicken, chicken. CHICKEN++ );
        Chicken = chicken. Chicken [chicken.
        $Chicken++ ]; chicken. Chicken = CHICKEN? Chicken?
        '\012'== Chicken? chicken (++ CHICKEN, chicken.
        chicken [++ chicken. CHICKEN ]=
        CHICKEN - CHICKEN ): Chicken
        ==' '|'\015'== Chicken ||
        (Chicken   )== "c" &  chicken. Chicken [chicken.
        $Chicken++ ]== "h" &  chicken. Chicken [chicken.
        $Chicken++ ]== "i" &  chicken. Chicken [chicken.
        $Chicken++ ]== "c" &  chicken. Chicken [chicken.
        $Chicken++ ]== "k" &  chicken. Chicken [chicken.
        $Chicken++ ]== "e" &  chicken. Chicken [chicken.
        $Chicken++ ]== "n"&&++chicken. chicken [chicken.
        CHICKEN]? chicken (CHICKEN)
        :[ "Error on line "+CHICKEN+": expected 'chicken'",
           chicken. CHICKEN = CHICKEN ++- CHICKEN ]:
        chicken. chicken :( CHICKEN = chicken.
        Chicken[chicken.CHICKEN], Chicken? (Chicken =

        --Chicken? --Chicken? --Chicken? --Chicken? --Chicken?
        --Chicken? --Chicken? --Chicken? --Chicken?
        chicken. CHICKEN++ &&
        --Chicken :String.fromCharCode(CHICKEN): chicken.
        Chicken [chicken. Chicken [-- chicken. CHICKEN ]&&
        (chicken. $Chicken += CHICKEN), --chicken.
        CHICKEN ]: chicken. Chicken [chicken.
        Chicken [CHICKEN] = chicken. Chicken
        [-- chicken. CHICKEN ],-- chicken. CHICKEN ]:
        chicken. Chicken [chicken. Chicken [chicken.
        $Chicken++ ]] [CHICKEN]: CHICKEN == chicken.
        Chicken [-- chicken. CHICKEN ]:
        CHICKEN*chicken. Chicken [-- chicken.
        CHICKEN ]: chicken. Chicken [-- chicken.
        CHICKEN ]- CHICKEN: chicken. Chicken [-- chicken.
        CHICKEN ]+ CHICKEN: chicken.
        CHICKEN ++ && "chicken", chicken.
        Chicken [chicken. CHICKEN ]= Chicken, chicken
        ()): CHICKEN );

        return chicken.
        Chicken
    }
```
## Difficulty

This puzzle is a medium challenge:

- Its really weird.
- Nobody likes esoteric languages.
- The zip password should be pretty obvious if you read the email (but you
should still be able to get in anyway with a crack).
- The chicken is a massive hint on the language, and a hint tells you there
are only 5 op codes used; so you have to write a stack based interpreter with
only half the instructions (easy ones to implement).
- The resulting PATH document tells you its a path document, and has info
on how PATH works. You can realise that there is only one starting point in
the program (the '$' sign, everywhere else its been stripped) and follow it or
you can read the author's notes; run it with a verbose interpreter and find
the loop it gets stuck in (and the whitespace removal).
- Finally, brainfuck is brainfuck... it doesnt make sense; but there is only
one place its broken, which is an infinite looping input instruction. So
providing you can debug it, and understand that its looping on that forever
and blocking the rest of the print instructions; should be ok.

With the broken PATH and brainfuck programs; it teases you with an output which
helps you work out where it might be stuck as well...
</p>
</details>