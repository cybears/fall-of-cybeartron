From: Cybears-HR <cybears.hr@cybears.io>
To: "helpdesk" <helpdesk@cybears.io>
Subject: FW: What a head case!
Body:

Hi guys,

I have a bit of a weird one...

One of the employees we let go recently had their hard drive recovered.
The manager in charge said there was critical information hidden in the
employees work and so we're reaching out to see if you guys can get the
information from it.

Can you look at the attached email and find what Stuart is after?

Kind regards,

Mel,

Cybears Human Resourcing

> From: Stuart <stuart@cybears.io>
> To: Mel <mel@cybears.io>
> Subject: What a head case!
> Attachments: chicken.zip
> Body:
>
> Wow! I can't believe how cooked that meeting was!
>
> I had the meeting but I was really not prepared for the pandora's box of
> insanity that came out. Thanks for leaving me alone there, you owe me a
> drink!
>
> Anyway, we finally got through the meeting and the person is gone.
> We dont know what happened to all the work they were working on; but the
> techs did find the attached .zip file among some of the leftover data on
> the PC they left behind. There are three critical things we need to find
> in there but we can't seem to get in...
>
> As for the exit interview the employee started yelling stuff about chickens
> is a language and how the company was out to loop the brain or something.
> I don't think I've ever been that uncomfortable in my life!
> What an oddball! By the way, the techs said they found a post-it note with
> "chicken" written on it under the keyboard. Weird, huh?
>
> Also, it's your shout after work this time ;)
>
> TGIF,
>
> Stuart
>
> R&D Management Lead
> "No one knows what they dont know but they know what they know." - Gandhi
> Please consider the environment before printing this email.