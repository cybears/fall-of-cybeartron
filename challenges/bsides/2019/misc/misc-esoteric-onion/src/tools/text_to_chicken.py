import math

def get_instructions_chn(mult_a, mult_b, add, first=False, last=False):
    instructions = []

    # push first set of chickens
    instructions.append(' '.join(['chicken' for _ in range(10 + mult_a)]))
    instructions.append(' '.join(['chicken' for _ in range(10 + mult_b)]))
    instructions.append(' '.join(['chicken' for _ in range(4)]))

    if add > 0:
        instructions.append(' '.join(['chicken' for _ in range(10 + add)]))
        instructions.append(' '.join(['chicken' for _ in range(2)]))

    instructions.append(' '.join(['chicken' for _ in range(9)]))
    # instructions.append(' ')

    if not first:
        instructions.append(' '.join(['chicken' for _ in range(2)]))
        # instructions.append(' ')

    if last:
        instructions.append('')

    # instructions.append(f'push {mult_a}')
    # instructions.append(f'push {mult_b}')
    # instructions.append(f'multiply')
    #
    # if add > 0:
    #     instructions.append(f'push {add}')
    #     instructions.append(f'add')
    #
    # instructions.append('char')
    # instructions.append('')
    #
    # if not first:
    #     instructions.append('add')
    #     instructions.append('')
    #
    # if last:
    #     instructions.append('exit')
    #
    return instructions


def get_instructions(mult_a, mult_b, add, first=False, last=False):
    instructions = []

    instructions.append(f'push {mult_a}')
    instructions.append(f'push {mult_b}')
    instructions.append(f'multiply')

    if add > 0:
        instructions.append(f'push {add}')
        instructions.append(f'add')

    instructions.append('char')
    instructions.append('')

    if not first:
        instructions.append('add')
        instructions.append('')

    if last:
        instructions.append('exit')

    return instructions


def get_divisible_by_10(divisor, in_num):
    other_num = math.floor(in_num / divisor)
    remainder = in_num % divisor

    return other_num, remainder


def encode_to_chn(in_file, out_file):

    DIVISOR = 10

    # in_file = r'note'
    # out_file = 'note.chn'
    with open(in_file, 'r') as fp:
        all_text = fp.read()

    # all_text = 'Im only using 5 ops.'
    all_instructions = []

    for i in range(len(all_text)):

        c = all_text[i]

        if i == 0:
            first = True
        else:
            first = False

        if i == (len(all_text) - 1):
            last = True
        else:
            last = False

        other_num, remainder = get_divisible_by_10(DIVISOR, ord(c))
        all_instructions.extend(get_instructions_chn(DIVISOR,
                                                 other_num,
                                                 remainder,
                                                 first,
                                                 last))

    output_text = '\n'.join(all_instructions)

    # print(output_text)
    with open(out_file, 'w') as fp:
        fp.write(output_text)
