"""
This module is supposed to auto-magically pull the components together and
dump them into the handout folder.

There are 3 main kinda layers to this.

A flag gets encoded into brainfuck, which gets encoded into PATH which gets
encoded into chicken. There are two extra flags hidden in the higher layers
but the last flag is hidden in the brainfuck.

01_bf - manually encoded brainfuck file
    - flag_good.bf: the flag without a bug in it.
    - flag_bad.bf: the flag with a shitty bug in it.

02_path - manually encoded path file
    - r_and_d_notes_good.path: path encoded brainfuck code with shitty bug.
    - r_and_d_notes_bad.path: path encoded brainfuck code without shitty bug.

03_chicken - auto-generated chicken files + a broken chicken interpreter
    - chickn.py: a broken reduced instruction set interpreter for chicken
    - chn_flag.txt: a cleartext flag that is to be encoded into chicken

tools - scripts used to pull everything together / test the solution

NOTE: This builder needs 7-zip installed as it calls it via subprocess
"""
import os
import shutil
import subprocess
from src.tools import text_to_chicken

def build_challenge():

    # Set folders and file names
    current_folder = os.path.dirname(os.path.realpath(__file__))

    story_folder = os.path.join(current_folder, r'src/00_story_file')
    path_folder = os.path.join(current_folder, r'src/02_path')
    chicken_folder = os.path.join(current_folder, r'src/03_chicken')
    output_folder = os.path.join(current_folder, r'src/04_output')
    handout_folder = os.path.join(current_folder, r'handout')

    chicken_flag_in = 'chn_flag.txt'
    chicken_flag_out = 'flag.chn'

    path_bad_in = 'r_and_d_notes_bad.path'
    path_bad_out = 'r_and_d_notes.chn'

    broken_chn_interpreter = 'chickn.py'
    story_file = 'FW_What_a_head_case.txt'

    zip_file_name = 'chicken.zip'
    zip_password = 'chicken'

    # Encode the clear text chn_flag.txt file to chicken
    text_to_chicken.encode_to_chn(
        os.path.join(chicken_folder, chicken_flag_in),
        os.path.join(output_folder, chicken_flag_out))

    # Encode the r_and_d_notes_bad.path file to chicken
    text_to_chicken.encode_to_chn(
        os.path.join(path_folder, path_bad_in),
        os.path.join(output_folder, path_bad_out))

    # Add the chickn.py file into the output dir
    shutil.copy(
        os.path.join(chicken_folder, broken_chn_interpreter),
        os.path.join(output_folder, broken_chn_interpreter),
    )

    # Add the story email into the handout dir
    shutil.copy(
        os.path.join(story_folder, story_file),
        os.path.join(handout_folder, story_file),
    )

    # ZIP the contents of the output dir and put it in the handout
    zip_output = os.path.join(handout_folder, zip_file_name)
    rc = subprocess.call(['7z', 'a', f'-p{zip_password}', '-y', zip_output] +
                         [os.path.join(output_folder, chicken_flag_out),
                          os.path.join(output_folder, path_bad_out),
                          os.path.join(output_folder, broken_chn_interpreter)])

if __name__ == '__main__':
    build_challenge()
