#!/bin/sh
docker run --name dind --privileged -d --rm docker:dind
docker cp solution.sh dind:/
docker exec -it dind /solution.sh
docker stop dind
