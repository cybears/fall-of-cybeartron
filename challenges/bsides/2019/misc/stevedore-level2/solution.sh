#!/bin/sh
#
# Rules for hiding the flag:
#     directory owner OR group permissions will allow user to:
#         read (list files); and
#         execute (enter directory)
# Root user will always be able to see everything anyway, but will 
# really only have permissions for (randomly) half. CTF user will have
# permission for the other half.
#
# Other user permissions (not owner/group) will only be both read and 
# execute (ie. 5 or 7) for the final flag directory parts. This won't be 
# visually obvious, and since other users don't really exist in docker land 
# it should be hard to test.
#
# Since permissions aren't set in the script left in the docker container, it 
# should be apparent that the hide_flag.py alters directory permissions in 
# some meaningful way.
#

echo "cybears/level2 test: I would not recommend running this as part of CI since pulling "
echo "                     or deleting the level2 image takes several minutes on some setups."
echo ""
echo "Solve method is as follows:"
echo "0. Login to cybears on dockerhub (not required once challenge is live)"
echo "   $ docker login -u cybears -p hunter2"
docker login -u cybears -p Cy8earsCTFCy8earsCTF

echo ""
echo "1. Pull the cybears/level2 image using docker"
echo "   $ docker pull cybears/level2"
docker pull cybears/level2

echo ""
echo "2. Start the container as root user (rather than following the USER=ctf directive) to create an unpriv user"
echo "   $ docker run -d -it --rm --name cybearslevel2 -u root cybears/level2 /bin/sh -c 'adduser -D unpriv && /bin/sh'"
docker run -d -it --rm --name cybearslevel2 -u root cybears/level2 /bin/sh -c 'adduser -D unpriv && /bin/sh'

echo ""
echo "3. Exec as our unpriv user, a find command to locate the correct verify script to run"
echo "   $ SOLVER=\$(docker exec -t -u unpriv cybearslevel2 'find /ctf -type f -iname \"*.py\" 2>&1 | grep -v denied | grep -E \[1-9\])"
SOLVER=$(docker exec -t -u unpriv cybearslevel2 find /ctf -type f -iname "*.py" 2>&1 | grep -v denied | grep -E \[1-9\])
echo $SOLVER

echo ""
echo "4. Run the solver script as the unpriv user within the container:"
echo "   $ docker exec -t -u unpriv cybearslevel2 $(echo \$SOLVER | tr -d '\015')"
docker exec -t -u unpriv cybearslevel2 $(echo $SOLVER | tr -d '\015') # | cut -d" " -f6

echo ""
echo "4. Done! Stop the container we started. It will auto-delete."
echo "   $ docker stop cybearslevel2"
docker stop cybearslevel2
