# Stevedore Level2

* title: Stevedore Level 2
* difficulty: medium
* tags: misc docker forensics
* todo: Automate pushing level1a/level1b/level2 to bitbucket
* activate: [Make public](https://cloud.docker.com/repository/docker/cybears/level2/settings)

## WARNING

Because the image is going to be publically available from docker hub, the deploy script
can't run until the day of the competition.

## EXTRA WARNING

THIS IMAGE TAKES ABOUT AN HOUR TO BUILD ON DOCKER HUB! So, I wouldn't recommend attempting
to use the gitlab CI structure to test or build it! 

On my crappy laptop it takes around 15 minutes. Tweak the folder number in
create_flag_maze.py down from 59998 to 49998 to reduce it to abour 40 minutes on dockerhub. 

## Infrastructure

Push the level2 folder (including it's Dockerfile) to cybears bitbucket account
Docker hub detects the push, and auto-builds the container
Users visit docker hub to view the Dockerfile / image
Users pull the container and have to find the flags on their own disk

## Skills required

* docker installation
* pulling an image from docker hub
* understanding multi-stage builds enough to know the layers are gone
* spelunking folder permissions on unix

## How to test drive

1. `cd level2`
2. `docker build -t cybears/level2 .`
3. This could take a while to build - sorry! Appreciate any feedback if it fails.
4. Pretend you just pulled your local copy of cybears/level2 from docker hub
5. You can read the Dockerfile in `level2/Dockerfile`
6. Run the container: `docker run cybears/level2`
7. Or jump into a shell: `docker run cybears/level2 /bin/sh`
8. ???

## Solution

In this final docker challenge, there is a docker images created that contains several
import python scripts: `create_flag_maze.py`, `hide_flag.py`, and `verify_flag.py`.

Unfortunately, `flag.txt` and `hide_flag.py` are deleted before the user receives the image
and since a multi-stage build was used, the layers can't be recovered by forensically
examining the files on your disk.

So, the contents of `hide_flag.py` has to be inferred from what has been altered in the
image. The flag maze created is simply a series of random nested folders (single integers) 
each containing the `verify_flag.py` script. Running a particular script checks a PBKDF2
of it's location, and if it matches a the hardcoded hash then you've run the correct one.

Unfortunately, the script takes a while to run, and there are ~50,000 of them. (Originally
there were a lot more, but things take a while to build on docker hub with too many folders
and I don't want to explode someone's windows computer).

The folder permissions look weird (random?) in the final image, so the `hide_flag.py` must
have altered them in some meaningful way. Some hints are the creation of a new user (ctf) 
and the ownership of the files (root) being different to the group (ctf). Both of these
classes of users will have access to ~half the verify scripts.

An 'other' user however, will only have access to one script in the whole directory, 
controlled by a single path having both read and execute permissions on each folder along
the way. As such, the solve is as simple as being in the resulting filesystem as neither
root nor ctf and running:

`$ find . -type f 2>&1 | grep verify`
`$ python3 ./x/x/x/x/x/x/x/x/x/x/verify_flag.py`

The solution is a notable prime number, as is the total number of created folders, so 
there is a chance for particularly enthusiastic individuals to brute force a limited set
of possible solutions should they choose.

The biggest hint for the challenge is in the readme, where it states: "I'm sure if anyone 
can access the flag, then you can".

## Flag

cybears{2147483647}
