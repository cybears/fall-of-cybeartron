# Running a solution script is not advised on Gitlab CI

Essentially the docker container for this challenge is not only a bit too much for CI runners to build, but also to pull. I'd recommend manually running the solve scripts on a machine with some decent storage IO.

# Log of running the solution script:

```
[heef@slowtop:~]$ cd ~/code/ctf/cybeartron/challenges/misc/stevedore-level2

[heef@slowtop:~/.../cybeartron/challenges/misc/stevedore-level2]$ ./run_solution.sh_using_dind.sh 
Unable to find image 'docker:dind' locally
Trying to pull repository docker.io/library/docker ... 
sha256:ec353956a21300964a7eb2b620a742c2730f618f4df35f60609b30969cd83ce8: Pulling from docker.io/library/docker
8e402f1a9c57: Pull complete 
ce7779d8bfe3: Pull complete 
de1a1e452942: Pull complete 
2868e3d1f4ba: Pull complete 
d23f6cb1b8ba: Pull complete 
e43826924a95: Pull complete 
4770db71125a: Pull complete 
66b6f417d329: Pull complete 
643874589690: Pull complete 
81b7ef78ea7f: Pull complete 
Digest: sha256:ec353956a21300964a7eb2b620a742c2730f618f4df35f60609b30969cd83ce8
Status: Downloaded newer image for docker.io/docker:dind
bc55f9a9b87b905a6c587a31fe755529ea4549b975b7de5341b1bad4c674f430
cybears/level2 test: I would not recommend running this as part of CI since pulling 
                     or deleting the level2 image takes several minutes on some setups.

Solve method is as follows:
0. Login to cybears on dockerhub (not required once challenge is live)
   $ docker login -u cybears -p hunter2
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded

1. Pull the cybears/level2 image using docker
   $ docker pull cybears/level2
Using default tag: latest
latest: Pulling from cybears/level2
6c40cc604d8e: Pull complete 
eb28c72fd5c9: Pull complete 
8b7b7e8a3ec6: Pull complete 
07400c149ca6: Pull complete 
db5b3982c316: Pull complete 
89822a41a5ce: Pull complete 
b4a1dc09bd74: Pull complete 
60765f6de516: Pull complete 
b81ff414376b: Pull complete 
Digest: sha256:0ac5d812e097a08129007f1199dbc26c138863f41894ca29ba3053db0f249286
Status: Downloaded newer image for cybears/level2:latest

2. Start the container as root user (rather than following the USER=ctf directive) to create an unpriv user
   $ docker run -d -it --rm --name cybearslevel2 -u root cybears/level2 /bin/sh -c 'adduser -D unpriv && /bin/sh'
51d051f3c1c2e328dc3bb4d5dd5446f2fda6b4ceadacd3808413380b326ecf42

3. Exec as our unpriv user, a find command to locate the correct verify script to run
   $ SOLVER=$(docker exec -t -u unpriv cybearslevel2 'find /ctf -type f -iname "*.py" 2>&1 | grep -v denied | grep -E \[1-9\])
/ctf/2/1/4/7/4/8/3/6/4/7/verify_flag.py

4. Run the solver script as the unpriv user within the container:
   $ docker exec -t -u unpriv cybearslevel2 $SOLVER
Checking... 
Verified. You have the flag: cybears{2147483647}

4. Done! Stop the container we started. It will auto-delete.
   $ docker stop cybearslevel2
cybearslevel2
dind

```

