#!/bin/bash

# Check the code quality
pylint maximum_extract_challenge/ solution/ base_problem/

# Run the integration tests
pytest tests/test_integration.py
