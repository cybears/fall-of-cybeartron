"""
This module is the base server implementation to make some CTF challenges
easier to implement.
"""
import logging
import time
import socketserver
import socket
import os
import signal
import threading
from multiprocessing.pool import ThreadPool
from multiprocessing.context import TimeoutError as LoadTimeOutError
from base_problem.problem_framework import ChallengeClass
from server_settings import HOST_NAME, HOST_SOCKET

SERVER = None
BUFFER_SIZE = 4096
SCRIPT_DIRECTORY = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(os.path.join(SCRIPT_DIRECTORY, 'flag.txt')) as fp:
    CHALLENGE_FLAG = fp.read()

LOGGER = logging.getLogger()


# pylint: disable=no-member
class ChallengeHandler(socketserver.BaseRequestHandler):
    """
    This server is the handler for connections to a challenge.
    """

    def __init__(self, *args):
        self.logger = None
        super(ChallengeHandler, self).__init__(*args)

    # pylint: disable=too-many-statements
    def handle(self):
        """
        Handle the messages from the connected client.

        When the connection arrives, create a game, send the response and
        wait for the return feedback.

        :param client:
        :param address:
        :return:
        """
        listen_for_response = True
        client = self.request  # Get the client requester
        address = self.client_address  # Get the address details

        os.getpid()
        self.logger = logging.getLogger(f'{os.getpid()}:SERVER_{address[0]}'
                                        f':{address[1]}')

        # When a client connects; set up a new problem
        try:
            self._send_message(client, **{'message': '::CONNECTED::'})
            challenge = ChallengeClass(CHALLENGE_FLAG, address)
            self.logger.info(f'Challenge created for {address}')
        except TypeError:
            self.logger.error('Challenge failed to start. '
                              'Disconnecting client.')
            client.send(b'\nThere was a problem loading this CTF challenge.\n')
            client.send(b'Talk to the creator of this challenge maker...\n')
            client.close()
            raise

        # Send the splash screen to the connected client. (cool ASCII!)
        try:
            splash_text = challenge.get_splash_screen_text()
            self._send_message(client, **splash_text)
            time.sleep(challenge.splash_screen_wait)
        except NotImplementedError:
            # Log that there is no splash screen but don't do anything.
            self.logger.debug('Splash screen not implemented yet.')

        # Send the problem statement to the connected client.
        # This should tell them what they're supposed to be doing.
        try:
            problem_statement = challenge.get_problem_statement_text()
            self._send_message(client, **problem_statement)
            time.sleep(challenge.problem_statement_wait)
        except NotImplementedError:
            # Log that there is no splash screen but don't do anything.
            self.logger.debug('Problem statement not implemented yet.')

        # Setup the problem in another thread (and print dots while you wait)
        try:
            # Print the loading message
            msg = {
                'message': challenge.loading_message,
            }
            self._send_message(client, **msg)

            # Setup a pool worker and load
            pool = ThreadPool(processes=1)
            setup_complete = False
            # result = None
            async_result = pool.apply_async(challenge.setup_problem,
                                            args=())

            # Wait till the load is complete and then print the result
            while not setup_complete:
                try:
                    result = async_result.get(timeout=1)
                    setup_complete = True
                    self._send_message(client, **result)
                    time.sleep(challenge.problem_setup_wait)
                except LoadTimeOutError:
                    # Print dots while it waits for completion
                    msg = {
                        'message': '.',
                    }
                    self._send_message(client, **msg)
                except ValueError:
                    # This is when the problem doesn't want to be solved
                    self.logger.error('The problem could be solved '
                                      'via greedy even after 4 re-rolls.'
                                      ' Exiting.')
                    msg = {
                        'message': challenge.problem_setup_exception_message,
                    }
                    self._send_message(client, **msg)
                    listen_for_response = False  # Skip the listening bit
                    break

        except NotImplementedError:
            # Log that there is no splash screen but don't do anything.
            self.logger.debug('Problem statement not implemented yet.')

        # Listen for the answer
        if listen_for_response:
            try:
                client.settimeout(challenge.problem_timeout)
                client_solution = client.recv(BUFFER_SIZE)
                self.logger.info(f'{address} tried solution {client_solution}')
                result = challenge.check_client_solution(client_solution)
                self._send_message(client, **result)

            except socket.timeout:
                self.logger.info(f'{address} timed out before submitting a'
                                 f' solution.')
                msg = {
                    'message': challenge.connection_timeout_message,
                }
                self._send_message(client, **msg)
            except ConnectionAbortedError:
                self.logger.error(f'{address} aborted the connection')

    def _send_message(self, client, **kwargs):
        """
        Send a message to a connected client.

        If split_output is True, split message using the delimiter and send
        chunks to the output (this can simulate row-by-row printing).

        Otherwise just pump out the message direct.

        message, split_output=False,
                      delimiter='\n', row_delay=0
        :param client:
        :return:
        """
        message = kwargs.get('message', None)
        split_message = kwargs.get('split_message', False)
        delimiter = kwargs.get('delimiter', '\n')
        delay = kwargs.get('row_delay', 0)

        try:
            if split_message:
                output_list = message.split(delimiter)
                for row in output_list[:-1]:
                    out_row = row + delimiter
                    client.send(out_row.encode('utf-8'))
                    time.sleep(delay)
                client.send(output_list[-1].encode('utf-8'))
            else:
                client.send(message.encode('utf-8'))

        # pylint: disable=broad-except
        except Exception as ex:
            self.logger.error(f'Sending message to client failed {ex}')


class ConcurrentTCPServer(socketserver.ForkingMixIn, socketserver.TCPServer):
    """
    Simple socketserver Forking server with the ability for attributes
    """

    def __init__(self, *args):
        super(ConcurrentTCPServer, self).__init__(*args)


def shutdown_server():
    """
    Handle the shutdown of the server
    :return:
    """
    # pylint: disable=global-statement
    global SERVER

    try:
        SERVER.shutdown()
    # Don't care, we're ending.
    # pylint: disable=broad-except
    except Exception as ex:
        logging.error(f'{ex}')


def terminate(os_signal, frame):
    """
    Start a thread to terminate the server to avoid deadlocks
    :param os_signal:
    :param frame:
    :return:
    """
    del frame
    LOGGER.info(f'Received SIGNAL_CODE:{os_signal} stopping server...')
    terminator_thread = threading.Thread(target=shutdown_server,
                                         args=())
    terminator_thread.start()


def main():
    """
    Main
    :return:
    """
    # pylint: disable=global-statement
    global SERVER
    signal.signal(signal.SIGINT, terminate)
    signal.signal(signal.SIGTERM, terminate)

    # IP and port details
    host_ip = HOST_NAME
    port_num = HOST_SOCKET

    LOGGER.info(f'Starting server on {host_ip}:{port_num}')
    SERVER = ConcurrentTCPServer((host_ip, port_num),
                                 ChallengeHandler)
    SERVER.allow_reuse_address = True
    SERVER.max_children = 50
    with SERVER:
        SERVER.serve_forever()

    LOGGER.info(f'Server stopped.')


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
