#!/usr/bin/env python3
'''
pip install:
    crypyto
    base58
'''
import string
from crypyto.ciphers import Vigenere
import base64

key = 'mozart'
text = 'The people are real. The cases are real. The rulings are final. cybears{th1s_i5_judg3_judy}'

v = Vigenere(key)
ciphertext = v.encrypt(text)
encoded_text = base64.b32encode(ciphertext.encode('utf-8'))
# Ensure we can decode the text.
assert ciphertext == base64.b32decode(''.join([chr(l).upper() for l in encoded_text])).decode('utf-8')
nato = {'A':'Alpha', 'B':'Bravo','C':'Charlie', 'D':'Delta', 'E':'Echo', 'F':'Foxtrot', 'G':'Golf', 'H': 'Hotel', 'I':'India', 'J':'Juliet', 'K':'Kilo', 'L':'Lima', 'M':'Mike', 'N':'November', 'O':'Oscar', 'P':'Papa', 'Q':'Quebec', 'R':'Romeo', 'S':'Sierra', 'T':'Tango', 'U':'Uniform', 'V':'Victor', 'W':'Whiskey', 'X':'Xray', 'Y':'Yankee', 'Z':'Zulu', '9': 'Niner' }


final_text = ''
for letter in encoded_text:
    l = chr(letter).upper()
    try:
        word = nato[l]
    except:
        word = l
    final_text += word + ' '

print('Begin Begin Begin ' + final_text + "End End End")
