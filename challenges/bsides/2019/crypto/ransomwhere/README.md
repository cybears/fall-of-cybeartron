story-100-ransomwhere
=================

* _title_: Ransomwhere?
* _points_: 100
* _tags_: story, crypto

## Flags
* `cybears{l3t5_c@u53_s0m3_m@yh3m!}`

## Challenge Text
```markdown
We have managed to break into the DecepticomTSS core network. What
we've found is surprising - someone, or something, has beaten us here,
and they are very sophisticated. They've dropped some kind of
ransomware that has encrypted a number of core files. Two of the
encrypted files are of particular interest - one we think is a
description of this other threat actor, the other looks to be some kind
of intercepted cybears communications.

We've also managed to grab the compiled version of the ransomware.
You'll need to somehow break into the encrypted files...
```

## Attachments
* `ransom.pyc` : compile python bytecode of encryptor/decryptor
* `threat_intell.pdf.enc` : encrypted pdf
* `cybears_intercept.mp3.enc` : encrypted mp3 file

## Hints
* The encryption and decryption looks fine. Hopefully there is something wrong
  with the public key?

## References
* https://github.com/Ganapati/RsaCtfTool

## Notes
* Decompile .pyc file (eg with uncompyle6)
* Copy/paste RSA public key to `ransom.pub`
* Potential method to solve would be to look at the random number generation,
  AES encryption and then RSA encryption to realise that there don't seem to be
  any issues there
* Consider RSA vulnerabilities. Only a few target weak RSA key generation. The
  easiest thing to do would be to try all of the possible weak RSA key
  generation routines - RsaCtfTool does this for you, given a public key.
* It will hit on the "fermat" attack, where the two primes in an RSA key are
  too close together
* extract the primes, create a private key and then use the tool to decrypt the
  messages

* RsaCtfTool can automatically create a private key file from a broken public
  key. Players can then use this to decrypt using the .pyc file
* `python ./RsaCtfTool --publickey ransom.pub --private --attack all > random.priv`
* `python3 ransom.pyc -i test.txt.enc -v ./ransom.priv -m decrypt #will output test.txt.enc.dec`

* To encrypt files: `python3 ransom.{py|pyc} -i [inputfile]`
* To encrypt files: `python3 ransom.{py|pyc} -i [inputfile] -p ransom.pub`
* To decrypt files: `python3 ransom.{py|pyc} -i [inputfile.enc] -v ransom.priv -m decrypt`
