#!/usr/bin/python3

#crypto-150-VanityRSAII solution

from pwn import *
from Crypto.Util.number import isPrime, getPrime, GCD, inverse, getRandomNBitInteger
import Crypto.PublicKey.RSA as RSA
import os
import sys

def generatePrivateKey(bitlength=1024, e=65537, target=b'cybearsctf', debug=False):

	p = e
	while True :
		p = getPrime(512)
		if(debug==True):
			print("GCD: {0}".format(GCD(p,e)))
			print("bitlen: {0}".format(p.bit_length()))
		if (GCD(p,e) == 1) and (p.bit_length != 512):
			break

	q = e
	while True:
		putative_N = b'\xff' + os.urandom(1024//8 - len(target) - 2 ) + target + b'\xff'
		pN = int.from_bytes(putative_N, byteorder="big")
		q = inverse(p, 2**1024)*pN % 2**512
		if (isPrime(q) and GCD(q,e) == 1 and q.bit_length() == 512): 
			break

	N = p*q
	d = inverse(e, (p-1)*(q-1))
	R = RSA.construct((N,e,d))
	if(debug==True):
            print("p bitlen: {0}".format(R.p.bit_length()))
            print("q bitlen: {0}".format(R.q.bit_length()))            
            print("n bitlen: {0}".format(R.n.bit_length()))
            print("n size: {0}".format(R.size()))            

	return R.exportKey()


import argparse 

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--hostname', type=str,  default="localhost", help='Hostname')
parser.add_argument('-p', '--port', default=1414, type=int, help='Remote port')
args = parser.parse_args()

hostname = args.hostname
port = args.port

if __name__ == "__main__":

	conn = remote(hostname, port)
	prompt = conn.recvuntil("finish): ")

	#print("**" + str(prompt))
	log.info("Sending RSA key...")
	rsa_key = generatePrivateKey()
	#print("**" +  rsa_key.decode("utf-8") + "\n")

	#todo: why does this need two carriage returns? 
	conn.send(rsa_key.decode("utf-8") + "\n\n")

	#todo: why does this need two conn.recv()??
	resp2 = conn.recv()
	#print("**" + resp2.decode("utf-8"))
	#log.success("Flag is " + str(resp2))
	resp2 = conn.recv()
	#print("**" + resp2.decode("utf-8"))
	log.success("Flag is " + str(resp2))
	conn.close()
	assert 'cybears{M4yb3_w3_5h0uld_u53_3CC?}' in str(resp2)


