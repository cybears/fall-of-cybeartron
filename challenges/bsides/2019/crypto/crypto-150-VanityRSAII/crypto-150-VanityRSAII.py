#!/usr/bin/python3

#crypto-150-VanityRSAII
#Crypto Challenge - Vanity RSA II

# Cybears comms officer cipher has realised that the last RSA private key he told you to create is too easy to forge. 
# You will need to regenerate your private key again. This time you will need to generate a new RSA private key, ensuring that the string "cybearsctf" is embedded in the LOWER half of the RSA public modulus.  
#
# Hint:You will need to submit your answer in X.509 format (eg.
'''
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQD/VEVTVFRFU1Q2ovEr/Iaz006A38wDF1dvWScrRTWG0hdnKjjJ
efTEHFFib2gRjzRAtveEPZXPmCeQb+RQYo8clcJUBJFaii2UZXzyR6tnqAt7pdYJ
oNfs99BJvvyQhaTOqkw4rmlr8SBtTkzPXX6MI+ZpvpnCsksC8iL5KdzHMQIDAQAB
AoGBAIhOyc/Jpk94Q+4DIPNVlefsd+1vDG/eByyHkNSV1xSJljraHITa2aPUZlJB
nXI6XC/sPclYQ2EXHpAKW/3aEAGX1RtoSN8aBDXNWeXqlz4/CXbOMcNF2fbGVX6I
0CVyKBWHr4VX/ruyyUaNqJan3IdsdKjsNkFkanr9gDlz2xABAkEAuCDy+kvLN781
crcq8tQJRy2xkKms4aVsoukFySAEjOIXTGWhVvnuPMqkgxTX9RhvqoWds+XPL2eS
puiVlbqhAQJBAWL+BnPnvufBF0gPz2Qj1SGc7OCnrf2JUf2rCGmRT7qGjuQM4X7f
pBfLmapdPoh5quC1Tj+y3ScmFhUpQp1t9jECQCsVkqVcrN2LgU8pawRM9yrPl1f5
S/m0wpnQGsl4E3h/wuHeegUnEEbrR9lgPDQelqp4/3DD2loGSzuA+teBRwECQDTK
dgcyhW9NhbrPrxXDRmSzQ369MOCtVSYWEzAvzd19OS6sw7PsaiinvHhbWXtOLJ0y
GSrb3It/3HTVJ/Tlb2ECQFjeIrpXrLkvxncoMBF6gj172RuagsKfogfhQpPz70mx
f0kQf8kgqebINMlnEEQkKDxLGcn5vh1ZncWrp3SfKDA=
-----END RSA PRIVATE KEY-----
'''
#
# As an example the integer 1205757975063083732915455 contains the byte string "TESTTEST" (so does the above example modulus!)

#python3 crypto04.py

#requirements 
#pip3 install pycryptodome

## SERVER
# socat TCP-LISTEN:1414,reuseaddr,fork EXEC:"python3 ./crypto-150-VanityRSAII.py"

##CLIENT
# nc localhost 1414

from Crypto.Util.number import isPrime, getPrime, GCD, inverse
import Crypto.PublicKey.RSA as RSA
import os
import sys


flag = "cybears{M4yb3_w3_5h0uld_u53_3CC?}"

if __name__ == "__main__":
	#user_input = input("Enter your RSA private key: ").strip()
	print("Enter your RSA private key (Enter blank line to finish): ")
	
	buf_array = []
	user_input = "XXXX"
	while(user_input != ""):
		user_input = input().strip()
		buf_array.append(user_input)
	

	user_input = "\n".join(buf_array)

	#sys.stdin.buffer.flush()	
	#user_input = sys.stdin.buffer.read()
	
	#user_input = b'xx'
	##validate user input
	# length > 0?
	# begin and end with BEGIN/END RSA PRIVATE KEY

	#s = user_input.decode("utf-8")
	s = user_input
	
	try: 
		#print("DEBUG: {0}".format(str(s)))
		r = RSA.importKey(s)
	except (ValueError,IndexError,TypeError) as e: 
		print("Error importing RSA key")
		exit(0)
	
	##check key
	#p*q = N
	result = (r.p * r.q == r.n) 
	if not result: 
                print("Error, p*q != n")
                exit(0)

	#p is prime
	result = isPrime(r.p)
	if not result: 
		print("Error, p is not prime")
		exit(0)

	#q is prime
	result = isPrime(r.q)
	if not result: 
                print("Error, q is not prime")
                exit(0)

	#length of p,q >=512
	#length of N >= 1024
	result = (r.n.bit_length() >=1023) and (r.n.bit_length() <= 1025)
	if not result: 
                print("Error, n is not correct size (1024-bits)")
                exit(0)

	result = (r.p.bit_length() >= 511) and (r.p.bit_length() <= 513)
	if not result: 
                print("Error, p is not correct size (512-bits)")
                exit(0)

	result = (r.q.bit_length() >= 511) and (r.q.bit_length() <= 513)
	if not result: 
                print("Error, q is not correct size (512-bits)")
                exit(0)

	#b'cybearsctf' is in LOWER half of N
	index = r.n.to_bytes(1024//8, byteorder="big")[128//2:].find(b'cybearsctf')
	result = (index	>= 0)
	if not result:
                print("Error, 'cybearsctf' not found in modulus")
                exit(0)


	print("Congratulations! The flag is: " + flag)



