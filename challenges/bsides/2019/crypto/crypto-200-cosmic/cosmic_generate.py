#outputs messages.json and flag.txt.enc

import unittest.mock
unittest.mock.patch("time.clock", create=True).start()

import Crypto.Util.number as num
import Crypto.Signature.PKCS1_v1_5 as pkcs
from Crypto.Hash import SHA
import random
from binascii import *
import Crypto.PublicKey.RSA as rsa
import json
import os

# Python 3.6
from functools import reduce
def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod



def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1


'''
Generate an RSA signed message with PKCS1.5 padding
in: msg (bytes)
in: r (RSA Private key)
in: fault True|False to inject fault
out: sig (bytes)

Valiate with: 
h = SHA.new(M)
signer = pkcs.new(rsa)
signer.verify(h,ans)
'''
def generate_sig(msg, r, fault=False):
    asn1_sha = unhexlify('3021300906052b0e03021a05000414') #fixed
    h = SHA.new(msg)
    pad_len = r.n.bit_length()//8 -2 -1 - len(asn1_sha) - len(h.digest())
    build = b'\x00\x01' + b'\xff'*pad_len + b'\x00' + asn1_sha + h.digest()
    
    d_p = r.d % (r.p-1)
    d_q = r.d % (r.q-1)
    
    s1 = pow(int.from_bytes(build, byteorder="big"), d_p, r.p)
    s2 = pow(int.from_bytes(build, byteorder="big"), d_q, r.q)

    if(fault==True):
        #inject 'fault'
        #print("DEBUG: Injecting Fault into s2")
        s2 = s2^(2**random.randrange(128,1024))
        #s2 = s2+1

    primes = [r.p, r.q]
    partial_sigs = [s1,s2]

    S = chinese_remainder(primes, partial_sigs)
    #print("DEBUG: " + str(S))

    return S.to_bytes(2048//8, byteorder="big")


def gen_pkcs_pad(M, bitlen):
    asn1_sha = unhexlify('3021300906052b0e03021a05000414') #fixed
    h = SHA.new(M)
    pad_len = bitlen//8 -2 -1 - len(asn1_sha) - len(h.digest())
    build = b'\x00\x01' + b'\xff'*pad_len + b'\x00' + asn1_sha + h.digest()
    return build


def find_factor(M, e, n, S):
    pM = gen_pkcs_pad(M, n.bit_length())
    return num.GCD(pow(S, e, n) - int.from_bytes(pM, byteorder="big"), n)


def gen_random_message():
    out = b''
    for i in range(0,9):
        out = out + hexlify(os.urandom(1)) + b"-"
    out = out + hexlify(os.urandom(1))
    return out

if __name__ == '__main__':
    
    #generate random RSA key pair
    r = rsa.generate(2048)

    #create json
        #public modulus - r.n
        #public exponent - r.e
        #padding type - pkcsv1.5
        #hash type - SHA1
        # array - [ {message:m1,sig:s1}, ...]
 
    j = {'modulus':r.n, 'exponent':r.e, 'padding':'PKCSv1.5', 'hash':'SHA1', 'content':[]}

    #loop - generate standard messages
    for i in range(1,100):    
        t = gen_random_message()
        s = generate_sig(t, r, fault=False)
        sh = hexlify(s).decode("utf-8")
    
        j['content'].append({'message':t.decode("utf-8"), 'signature':sh})

    #generate fault
    t = gen_random_message()
    s = generate_sig(t, r, fault=True)
    sh = hexlify(s).decode("utf-8")    
    j['content'].append({'message':t.decode("utf-8"), 'signature':sh})

    for i in range(1,99): 
        t = gen_random_message()
        s = generate_sig(t, r, fault=False)
        sh = hexlify(s).decode("utf-8")
    
        j['content'].append({'message':t.decode("utf-8"), 'signature':sh})

    print("Writing json messages...")
    with  open("messages.json", "w") as f:
        f.write(json.dumps(j))


    #write encrypted flag
    flag = b'cybears{D@mn_th0s3_c0sm1c_r@yz}'

    enc = r.encrypt(flag, 0)
    
    print("Writing encrypted flag...")
    with open("flag.txt.enc", "wb") as g: 
        g.write(enc[0])

    print("Done!")



