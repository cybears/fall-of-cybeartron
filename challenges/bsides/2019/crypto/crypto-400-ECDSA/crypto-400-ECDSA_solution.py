#!/usr/bin/python3

#crypto-400-ECDSA solution

#challenge running locally as
## socat TCP-LISTEN:3141,reuseaddr,fork EXEC:"python3 ./crypto-400-ECDSA.py"
#solve as
## python3 crypto-400-ECDSA_solution.py

#https://crypto.stackexchange.com/questions/7904/attack-on-dsa-with-signatures-made-with-k-k1-k2

'''
You got three equations with two unknowns (k and x). You only need two signatures to solve the private key x:

s1k≡h1+xr1(modq)
s2k+s2≡h2+xr2(modq)
This might be solved using Gaussian elimination. Step 1:

s1k/r1≡h1/r1+x(modq) - Divide 0.1 by r1
s2k+s2−s1kr2/r1≡h2−h1r2/r1(modq) - Subtract 1.1 times r2 from 0.2
Step 2:

x≡s1k/r1−h1/r1(modq) - Swap terms of 1.1
k≡(h2−s2−h1r2/r1)/(s2−s1r2/r1)(modq) - Divide 1.1 by s2−s1r2/r1
Step 3:

x≡s1((h2−s2−h1r2/r1)/(s2−s1r2/r1))/r1−h1/r1(modq) - substitute k in 2.1 for right expression of 2.2

'''

import ecdsa
from pwn import *
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--hostname', type=str,  default="localhost", help='Hostname')
parser.add_argument('-p', '--port', default=3141, type=int, help='Remote port')
args = parser.parse_args()

hostname = args.hostname
port = args.port

if __name__ == "__main__":
	conn = remote(hostname, port)
	conn.recvuntil("value:")

	#get public key
	log.info("Requesting public parameters")
	conn.send("0\n")
	resp0 = conn.recv()
	resp0 = conn.recv()
	r0 = resp0.split(b'\n')[0]
	j0 = json.loads(r0.decode(encoding='utf-8'))

	g = ecdsa.ecdsa.generator_192
	n = g.order()

	#extract public key and curve info

	#get signatures
	log.info("Requesting signature #1")
	conn.send("1\n")
	resp1 = conn.recv()
	r1 = resp1.split(b'\n')[0]
	j1 = json.loads(r1.decode(encoding='utf-8'))

	log.info("Requesting signature #2")
	conn.send("1\n")
	resp2 = conn.recv()
	r2 = resp2.split(b'\n')[0]
	j2 = json.loads(r2.decode(encoding='utf-8'))

	#recover private key
	log.info("Recovering Private Key")
	h1 = j1["message"]
	r1 = j1["r"]
	s1 = j1["s"]

	h2 = j2["message"]
	r2 = j2["r"]
	s2 = j2["s"]

	r1_inv = ecdsa.ecdsa.numbertheory.inverse_mod(r1,n)
	# s1 *(A/B)/r1 - h1/r1
	A = h2-s2-(h1*r2*r1_inv)
	B = s2-s1*r2*r1_inv
	B_inv = ecdsa.ecdsa.numbertheory.inverse_mod(B,n)

	x = s1*A*B_inv*r1_inv - h1*r1_inv
	x = x % n

	pubkey = ecdsa.ecdsa.Public_key(g, g*x)
	privkey = ecdsa.ecdsa.Private_key(pubkey, x)

	#confirm
	#j0['x'] == pubkey.point.x()
	#j0['y'] == pubkey.point.y()

	#request signature
	conn.send("2\n")
	resp3 = conn.recv()

	start = resp3.find(b'{')
	end = resp3.find(b'}')

	r3 = resp3[start:end+1]
	j3 = json.loads(r3.decode(encoding='utf-8'))

	#create signature
	s = privkey.sign(j3["message"], 123)
	challenge_response = json.dumps({"r":s.r, "s":s.s})

	#send signature
	conn.send(challenge_response + "\n")
	flag = conn.recv()
	log.success(flag.decode("utf-8").strip())
	conn.close()
	assert 'cybears{T4k3_th4t_pl4yst4t10n}' in flag.decode("utf-8").strip()
