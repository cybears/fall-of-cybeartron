all: test

.deploy: clean
	python3 bin/manifest_to_k8s_deploy.py -o bin/.deploy --no-domain --no-port-reuse --no-domain --no-external-dns --config config/testing.tfvars.json

lib-unit-tests:
	pytest lib

bin-unit-tests:
	pytest bin

unit-test: bin-unit-tests lib-unit-tests

kubeval: .deploy
	docker run -it -v `pwd`/bin/.deploy:/test-deploy garethr/kubeval:latest -d /test-deploy/

kube-score: .deploy kubeval
	docker run -v `pwd`/:/project zegl/kube-score:v1.11.0 score \
	--ignore-test pod-networkpolicy \
	--ignore-test container-image-tag  \
	--enable-optional-test container-security-context-user-group-id \
    --enable-optional-test container-security-context-privileged \
    --enable-optional-test container-security-context-readonlyrootfilesystem \
    --ignore-test container-security-context \
	--ignore-test container-security-context-user-group-id \
	--ignore-test container-security-context-privileged \
	bin/.deploy/*.yaml
	rm -rf .deploy

test: kube-score unit-test

challenge-deploy-test:
	pytest bin --run-deploy-tests --config config/testing.tfvars.json

challenge-deploy-bsides-2019:
	pytest bin --run-deploy-tests --config config/bsides-2019.tfvars.json

challenge-deploy-bsides-2021:
	pytest bin --run-deploy-tests --config config/bsides-2021.tfvars.json

challenge-deploy-bsides-next:
	pytest bin --run-deploy-tests --config config/bsides-next.tfvars.json

clean:
	rm -rf .deploy
