# Contributing

Welcome to the Cybears CTF repository! So you want to contribute? This document
explains the high level goals and guidelines we apply to our development
process.

## Getting Involved

We're a predominantly real-world team who work together to develop challenges
for the Canberra CTF scene (and some day, the world!). We develop new
challenges in a walled garden repository open to our friends and collaborators,
so if you're reading this from our public repository, you might not see much
active development! We generally don't bring people into that group without
knowing them in real life, but we do appreciate input from Internet people on
the contents of our public repository where it's offered.

## Philosophy

We aim to keep a few things in mind in our development process:

  * We support the development of real-world skills through the puzzles we pose
  * We provide clear goals which don't require logical leaps to reach
  * We dogfood our work so that players don't have to deal with broken garbage
  * We verify and validate our work with scriptable solutions
  * We ensure we can deploy challenges with infrastructural resilience

When building a challenge, you should be thinking about these things to make
sure that your puzzle is the most fun it can be! Sometimes we don't get it
completely right, and that's okay as long as we still provide a good time and
don't cause too much hair to be torn out.

## Repository Structure

Our repository is broken down into the following directories which you might care
about:

```sh
$ tree -L 2
hello :)
├── bin
│   ├── // programs live here!
│   ├── foo.py
│   ├── bar.sh
│   └── tests
│       └── // tests for bin live here
├── challenges
│   ├── // Challenges are grouped into event directories
│   ├── event-name
│   └── something-else
├── config
│   ├── // Config json files here drive our infrastructure setup.
│   └── event-name.tfvars.json
├── infra
│   ├── builder
│       └── // images for challenge building live here
│   ├── challenges
│       └── // Infrastructure setup for challenge hosting live here
│   ├── ctfd
│       └── // Infrastructure setup for CTFd hosting live here
│   ├── dns
│       └── // Infrastructure for DNS setup live here
│   ├── kubernetes-deploy
│       └── // Infrastructure setup for kubernetes challenge hosting live here
│   ├── mon
│       └── // Infrastructure setup for monitoring hosting live here
│   ├── services
│       └── // base images for challenge hosting live here
│   ├── terraform_remote_state
│       └── // setup for terraform shared staate (s3 bucket) lives here
│   ├── tester
│       └── // base images for challenge testing live here
│   └── www
│       └── // Some random websites live here
├── lib
│   ├── // scripts and libs live here!
│   ├── foo.py
│   └── bar.sh
│   └── tests
│       └── // tests for lib live here
└── // There's more here but who cares?
```

# Challenge dev

It's most likely that you'll be interested in the `challenges/` structure if
you're going to be working on challenges (obviously). This tree is broken down
into a substructure of named "events", like `bsides/2019/` &c, each of which
include subdirectories for challenge categories, e.g. `pwn/` and `crypto/`.
Each challenge is typically contained in its own directory (there are
some exceptions) with a few key components which we'll cover in the next
section.

## Anatomy of a Challenge

We have a challenge skeleton which lives in `challenges/base/skel/` which
contains the following exemplar files:

```sh
$ tree skel
skel
├── gitlab-ci.yml
├── MANIFEST.yml
├── README.md
└── WALKTHROUGH.md
```

Each challenge is expected to include definitions for its continuous
integration (for build and solve validation) and a `MANIFEST` which helps
describe the challenge from a development and deployment perspective. For more
information on what we might expect to be in these files, it's better to peruse
the skeleton directory so we don't have to keep this document up to date! :)

You might note that there is also a `README` and a `WALKTHOUGH` in the skeleton.
While we encourage the inclusion of these files for each challenge, they're not
as essential as the `MANIFEST` and CI configuration files. We aim to include
high level information without spoilers in the `README` since it is generally
rendered automatically by most `git` repository web hosts, and include a more
fully formed `WALKTHROUGH` for our solution in that file for those who care to
read it.

You'll also notice that there is nothing else in the skeleton. That's because
our challenges span a very wide gamut and there's not necessarily an answer to
the question "How should I build my challenge?" We aim to write our supporting
scripts in Python (3 is strongly preferred) but other than that, it's entirely
up to you, as long as you don't make our lives extremely difficult. You are
free to use whatever build system seems sensible, although we do prefer for it
to be possible to containerise the build process and any deployable systems
which are required to support the challenge in production.

## Branches, Trees, Cybears, Oh My!

We have multiple "eternal branches" in our repository, and this is important
because I say so. Generally we stick to one key rule, `master` must always be
publicly releasable. This is so we can push fixes or small collections of
content for ad-hoc events without compromising the work for other upcoming
events. For all long-term work, e.g. BSides CTF, we maintain `staging/*`
branches which act as a target for all work on challenges for those
events/milestones. The default branch of our private repository may change over
time as we set specific milestones as our next major target.

If you are a `role` you should probably be branching from `branch`:

  * `challenge dev` -> `staging/<event>`
  * `designer` -> `staging/<event>` or some art specific branch
  * `infra dev` -> `master`

## Tracking Work

Now that we understand how to do work, we also need to have an idea of how we
would like to track progress over time. We aim to track work in a few ways to
make sure we actually complete it:

  * GitLab issues help us conceptualise ideas before we get to the point of
    actualising them (it's all very zen)
  * We track progress milestones for challenges in their `MANIFEST`s
  * We use GitLab merge requests to propose and improve code for challenges

We have historically approached challenges development as a very monolithic
process, where development, review, playtesting, and integration all needed to
take place prior to merging the work into an eternal branch. This resulted in
very long lived merge requests which didn't accurately reflect how work was
being done by our contributors. We now aim to reduce those barriers by adopting
an approach focused more on reaching one of our well-defined checkpoints for
progress without requiring the others be complete. This is intended to allow us
to merge work which is functional without having to block on slower processes
like playtesting.

## Time to Get Started

Rather than keep hammering you with philosophy and mechanics, it's now a good
time for you to start getting your hands dirty with writing or improving a
challenge.

  1. Check out the project issues to see if there's anything you'd like to work
     on, or to suggest a new challenge of your own.
  1. Clone the repository and take a look around. Try building some of the
     challenges which have already been merged into the default branch.
  1. Check out if any challenges need to be playtested, it's an easy way to
     start looking at how other people are approaching challenge design!

# Core development

If you are looking to get involved with core development (`bin/`, `lib/`, `infra/` etc.)
then you will need to understand the [CI](docs/CI-SYSTEM.md) system.

In addition the Makefile in the base directory will run a bunch of unit and integration
tests locally.

Run unit tests
```
make test
```

Run integration tests for test challenges
```
make challenge-deploy-test
```

Run integration tests for bsides 2019 challenges
```
make challenge-deploy-bsides-2019
```

Run integration tests for bsides 2021 challenges
```
make challenge-deploy-bsides-2021
```

Run integration tests for bsides-next challenges
```
make challenge-deploy-bsides-next
```