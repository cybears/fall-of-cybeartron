#!/usr/bin/env python3
import os
import json
import sys

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

BASE_DIR=os.path.join(SCRIPT_DIR, '..',)
LIB_DIR=os.path.join(BASE_DIR, 'lib')
sys.path.append(LIB_DIR)
from ctfd_api import CTFdAuthenticatedAPISession
import ctfd_admin_pass

CTFD_HOST = os.environ.get("CTFD_HOST", "http://localhost:8000/")

if __name__ == "__main__":
    admin_pass = ctfd_admin_pass.get_pass()
    api_session = CTFdAuthenticatedAPISession(CTFD_HOST, admin_pass)
    chall_get_r = api_session.get("challenges")
    assert chall_get_r.status_code == 200, chall_get_r.content
    response_data = json.loads(chall_get_r.content)
    assert response_data["success"] == True, response_data

    for challenge_data in response_data["data"]:
        print("Removing challenge: id:{} - {}".format(challenge_data["id"], challenge_data["name"]))
        chall_delete_r = api_session.delete("challenges/{}".format(challenge_data["id"]), json="")
        assert chall_delete_r.status_code == 200, chall_delete_r.content
