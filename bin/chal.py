#!/usr/bin/env python3

import argparse
import pathlib
import sys
import string
import subprocess
from subprocess import check_call
import os
import shutil
import logging

try:
    import yaml
except ImportError:
    logging.exception("Failed to load yaml. Install the pyyaml package from pip")
    sys.exit(1)

try:
    from pwn import *
except ImportError:
    logging.exception("Failed to import pwntools. Install the pwntools package from pip")
    sys.exit(1)

context.log_level = 'info'

SCRIPT_DIR = pathlib.Path(__file__).resolve().parent
BASE_DIR = SCRIPT_DIR.parent
CHAL_DIR = BASE_DIR / "challenges"
LIB_DIR = BASE_DIR / "lib"
sys.path.append(str(LIB_DIR))
from challenge import Challenge

# We hunt for events based on the presence of `.event_root` files
EVENT_ROOT_INDICATOR = ".event_root"

def find_event_roots(from_path=CHAL_DIR):
    for p, d, f in os.walk(from_path):
        if EVENT_ROOT_INDICATOR in f:
            # A YAML event root file can override this
            event_name = str((from_path / p).relative_to(CHAL_DIR)).replace("/", "-")
            event_root = from_path / p
            info = yaml.load(
                open(event_root / EVENT_ROOT_INDICATOR), Loader=yaml.SafeLoader
            )
            if info:
                if info.get("archived") is True:
                    # Archiving excludes nested events so we'll skip dirs here
                    d[:] = []
                    continue
                event_name = info.get("name", event_name)
            yield event_name, event_root

VALID_EVENTS = {n: p for (n, p) in find_event_roots()}
DEFAULT_EVENT = "bsides-2020"
assert DEFAULT_EVENT in VALID_EVENTS

def prompt(p):
    s = input(p)
    if isinstance(s, str):
        return s.strip()
    if isinstance(s, bytes):
        s = s.decode('utf-8').strip()
        return s
    raise TypeError("Input is neither bytes nor a string!")

def merge_directories(root_src_dir, root_dst_dir):
    p = log.progress('Merging {} -> {}'.format(root_src_dir, root_dst_dir))
    for src_dir, dirs, files in os.walk(root_src_dir):
        dst_dir = pathlib.Path(
            src_dir.replace(str(root_src_dir), str(root_dst_dir), 1)
        )
        dst_dir.mkdir(parents=True, exist_ok=True)
        for file_ in files:
            src_file = os.path.join(src_dir, file_)
            dst_file = os.path.join(dst_dir, file_)
            if os.path.exists(dst_file):
                os.remove(dst_file)
            shutil.copy(src_file, dst_dir)
            p.status("%s", file_)
    p.success('Merged!')

def do_build(args, manifest):
    if args.local:
        manifest.try_build()
    else:
        check_call('make docker-build', shell=True)
    log.success("Built {}".format(manifest.name))

def do_test(args, manifest):
    if args.local:
        check_call('make test', shell=True)
    else:
        check_call('make docker-test', shell=True) 
    log.success("Tested {}!".format(manifest.name))

def do_run(args, manifest):
    if args.local:
        check_call('make run', shell=True)
    else:
        check_call('make docker-run', shell=True)

def do_info(args, manifest):
    print(manifest)


def do_create(args, manifest):
    event = args.event
    log.success("Creating a challenge in %s", event)
    base_challenge_dir = BASE_DIR.joinpath('challenges')
    event_dir = VALID_EVENTS[event]
    # This must be a directory
    log.debug("Event dir: {}".format(event_dir))
    assert event_dir.is_dir()
    # Load event info if the root indicator is a YAML file
    event_info = yaml.load(
        open(event_dir / EVENT_ROOT_INDICATOR), Loader=yaml.SafeLoader
    )
    if event_info is None:
        event_info = dict()

    if event_info.get("categorised", True):
        categories = []
        for item in event_dir.iterdir():
            if item.is_dir():
                categories.append(item.name)
        categories.append('New category')

        while True:
            try:
                selection = ui.options("What category?", categories)
            except IndexError:
                log.warning("Invalid selection")
                continue
            log.debug("You selected %s of %s", selection+1, len(categories))

            if selection == len(categories)-1:
                category = prompt("New category name: ")
                # Refuse to re-add an extant category - loops again
                if category in categories[:-1]:
                    log.warning("{!r} already exists".format(category))
                    continue
                if ui.yesno("Is '{}' correct?".format(category), True):
                    p = event_dir.joinpath(category)
                    p.mkdir(parents=True, exist_ok=True)
                    with open(event_dir.joinpath('gitlab-ci.yml'), 'a') as f:
                        f.write('    - local: "/{}"\n'.format(
                            p.joinpath('gitlab-ci.yml').relative_to(BASE_DIR))
                        )
                    break
            else:
                category = categories[selection]
                break
        category_dir = event_dir.joinpath(category)
        log.info("Category directory is %s", category_dir)
    else:
        log.debug("Event is not categorised")
        category_dir = event_dir
        while True:
            category = prompt("New challenge category: ")
            if ui.yesno("Is '{}' correct?".format(category), True):
                break

    # We now have the category, what is the name?
    log.info("Now we need the challenge name")
    while True:
        challenge_name = prompt("New challenge name: ")
        if ui.yesno("Is '{}' correct?".format(challenge_name), True):
            break
    log.success("Challenge name is %s", challenge_name)

    allowed_chars = string.ascii_lowercase + string.digits + '-'
    display_name = challenge_name
    challenge_name = challenge_name.lower()
    challenge_name = challenge_name.replace(' ', '-')
    challenge_name = ''.join([x for x in challenge_name if x in allowed_chars])

    challenge_path = category_dir.joinpath(challenge_name)
    if challenge_path.exists():
        # Log and die
        relpath = challenge_path.relative_to(BASE_DIR)
        log.error(
            "A directory already exists at {!r}- aborting"
            .format(str(relpath))
        )

    base_dir = BASE_DIR.joinpath("challenges", "base")
    skeleton_dir = BASE_DIR.joinpath(base_dir, "skel")
    project_type_dir = base_dir.joinpath("project_types")

    # Select a base project
    base_projects = []
    for d in project_type_dir.iterdir():
        if d.is_dir():
            base_projects.append(d.name)
    base_projects.append('None')
    while True:
        try:
            selection = ui.options("Select a template to use", base_projects)
        except IndexError:
            log.warning("Invalid selection")
            continue
        base_project = base_projects[selection]
        if ui.yesno("Is '{}' correct?".format(base_project), True):
            break
    if selection != len(base_projects)-1:
        # We've selected a base project
        base_project_path = project_type_dir.joinpath(base_project)
    else:
        base_project_path = None

    # Add a flag if you already know what you want it to be
    while True:
        flag = prompt('Enter your flag, leave blank for a placeholder: ')
        if not flag:
            flag = 'cybears{th1s_1s_a_t3st}'
        if ui.yesno("Is '{}' correct?".format(flag), True):
            break
    
    log.info("OK that should be enough to get started!")
    log.info("I'm going to create a new challenge directory for you:")
    log.indented(str(challenge_path))
    log.info("I'll place a skeleton challenge in that directory for you")

    log.success("Copying skeleton: %s -> %s", skeleton_dir, challenge_path)
    merge_directories(skeleton_dir, challenge_path)

    if base_project_path:
        log.success("Copying project template: %s -> %s", base_project_path, challenge_path)
        merge_directories(base_project_path, challenge_path)

    # Now we fill in any templated stuff
    replacements = {
        '<Put your challenge name here>': challenge_name,
        '<category>': category,
        '<name>': challenge_name,
        '<display_name>': display_name,
        '<flag goes here>': flag,
    }
    # Right now templated stuff is just in the root of the challenge directory
    patch_progress = log.progress('Patching skeleton files')
    for item in challenge_path.iterdir():
        if item.is_file():
            try:
                with open(item, 'r') as f:
                    content = f.read()
            except UnicodeDecodeError:
                continue
            patch_progress.status("Patching %s", item)
            for r, v in replacements.items():
                content = content.replace(r, v)
            with open(item, 'w') as f:
                f.write(content)

    category_ci_yaml = category_dir.joinpath('gitlab-ci.yml')
    if not category_ci_yaml.is_file():
        patch_progress.status('Creating category CI yaml')
        # We need to create a blank one...
        with open(category_ci_yaml, 'w') as f:
            f.write('---\n')
    with open(category_ci_yaml, 'a') as f:
        patch_progress.status('Adding challenge to CI')
        f.write(challenge_name + ':\n')
        f.write('    trigger:\n')
        f.write('        strategy: depend\n')
        f.write('        include:\n')
        f.write('            - local: "/{}"\n'.format(
            challenge_path.joinpath('gitlab-ci.yml').relative_to(BASE_DIR))
        )
    patch_progress.success('All files patched!')

    print('\n\n')
    log.success("Challenge created! '{}'".format(challenge_path))

def get_manifest():
    log.debug("Searching for manifest @ {}".format('./MANIFEST.yml'))
    manifest = None
    if os.path.isfile('MANIFEST.yml'):
        manifest = next(Challenge.from_manifest('MANIFEST.yml'))
    return manifest

def main(argv=sys.argv):

    parser = argparse.ArgumentParser(description='The cybears challenge management utility!')
    # Run without docker
    parser.add_argument('--local', '-l', action='store_true', help='Run without docker')
    # Debug and verbose are currently the same...
    parser.add_argument('--debug', action='store_true', help='Enable debug logging') 
    parser.add_argument('--verbose', '-v', action='store_true', help='Enable verbose logging')

    subparsers = parser.add_subparsers()

    # Create comes first as it's probably the most popular command
    create = subparsers.add_parser('create', help='Create a new challenge')
    create.add_argument('--event', default=DEFAULT_EVENT, choices=VALID_EVENTS,
                        help='The event to add the challenge to')
    create.set_defaults(func=do_create)

    # Then info
    info = subparsers.add_parser('info', help='Print info about this challenge')
    info.set_defaults(func=do_info)

    # Build
    build = subparsers.add_parser('build', help='Build the challenge')
    build.set_defaults(func=do_build)

    # Run
    run = subparsers.add_parser('run', help='Run the challenge')
    run.set_defaults(func=do_run)

    # and Test
    test = subparsers.add_parser('test', help='Run the challenge and the solver')
    test.set_defaults(func=do_test)

    args = parser.parse_args(argv[1:])

    if args.debug or args.verbose:
        context.log_level = 'debug'

    manifest = get_manifest() 
    log.debug("Args: {}".format(args))

    try:
        func = args.func
    except AttributeError:
        log.failure("No subcommand provided")
        log.info("If you're creating a challenge you probably want the 'create' subcommand")
        print("")
        parser.print_help()
        sys.exit(1)

    # Execute the subcommand function
    func(args, manifest)

if __name__ == '__main__':
    main()
