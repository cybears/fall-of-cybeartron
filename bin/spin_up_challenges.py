#!/usr/bin/env python3
import argparse
import importlib
import os
import pathlib
import logging
import sys
import json
import manifest_to_k8s_deploy
import shutil

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BASE_DIR=os.path.normpath(os.path.join(SCRIPT_DIR, '..'))
DEPLOY_DIR=os.path.join(BASE_DIR,'.deploy')
LIB_DIR=os.path.join(BASE_DIR,'lib')
sys.path.append(LIB_DIR)
import utils

DOCKER_REGISTRY_BASE = 'registry.gitlab.com'

def setup_deploy_dir(config, deploy_dir, reuse_ports):

    # clean out deploy directory from any previous runs.
    try:
        shutil.rmtree(deploy_dir)
    except OSError as e:
        pass
 
    # Spin up challenges
    docker_registry = f"{DOCKER_REGISTRY_BASE}/{config['gitlab_group']}/{config['gitlab_project']}/"

    return manifest_to_k8s_deploy.manifest_to_k8s_deploy(challenge_dirs=config['challenge_directories'],
                                                  registry=docker_registry,
                                                  dns=config['chal_domain'],
                                                  no_externaldns=config['no_externaldns'],
                                                  force=True,
                                                  output_dir=deploy_dir,
                                                  reuse_ports=reuse_ports,
                                                  start_port=3000,
                                                  no_horizontal_autoscaling=config['no_horizontal_autoscaler'],
                                                  no_windows_support=config['no_windows_support'])

def start_challenges(platform, config, verbose):
    # By default we include externaldns suport. If a platform does not support this
    # They should set this to True in start_challenges
    config['no_externaldns'] = False
    # By default we include horizontal autoscaling suport. If a platform does not support this
    # They should set this to True in start_challenges
    config['no_horizontal_autoscaler'] = False
    # By default we include windows suport. If a platform does not support this
    # They should set this to True in start_challenges
    config['no_windows_support'] = False

    # Spin up the challenge infra using the platform specific module
    platform_module = importlib.import_module(platform)
    kubectl = platform_module.kubectl
    platform_module.start_challenges(config, verbose)

    setup_deploy_dir(config, DEPLOY_DIR, platform_module.reuse_ports)
    kubectl.apply(DEPLOY_DIR, can_fail=True)

def stop_challenges(platform, config, verbose):
    platform_module = importlib.import_module(platform)
    kubectl = platform_module.kubectl
    kubectl.delete(DEPLOY_DIR)
    platform_module.stop_challenges(config)

def restart_challenges(platform, config, verbose):
    stop_challenges(platform, config, verbose)
    start_challenges(platform, config, verbose)

def state_challenges(platform, config, verbose):
    # By default we include externaldns suport. If a platform does not support this
    # They should set this to True in start_challenges
    config['no_externaldns'] = False

    platform_module = importlib.import_module(platform)
    platform_module.state_challenges(config)
    setup_deploy_dir(config, DEPLOY_DIR, platform_module.reuse_ports)

def parse_args():
    # Top level parser
    parser = argparse.ArgumentParser()
    parser.set_defaults(action=None)
    parser.set_defaults(platform=None)
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Verbose logging')
    parser.add_argument('--hostname', nargs=1, default=None, help='Hostname to use in challenge descriptions. Useful for local deployments')
    parser.add_argument(
        "--config", action="store",
        help="JSON configuration file for the ctf",
        type=argparse.FileType('r'),
        required=True
    )
    parser.add_argument(
        "--var", action=utils.kv_dict_append_action,
        metavar="'VAR=VALUE'",
        help='set or override individual config variables'
    )
    # We add top level subparsers to define which platform we're using
    sps = parser.add_subparsers()
    k3s_sp = sps.add_parser("k3s", help="Use k3s kubectl")
    k3s_sp.set_defaults(platform="k3s")
    k8s_sp = sps.add_parser("k8s", help="Use k8s")
    k8s_sp.set_defaults(platform="k8s")
    k8s_sp.add_argument(
        "--k8s-config", action="store", metavar="KUBECONFIG",
        help="Use a specific config (defaults to environment KUBECONFIG)",
        type=pathlib.Path, default=os.environ.get("KUBECONFIG"),
    )
    aws_sp = sps.add_parser("aws", help="Use aws via terraform")
    aws_sp.set_defaults(platform="aws")
    
    # And then add action subparsers to each of those so that we have
    # reasonable natural feeling `./script <platform> <action>` command lines
    for platform in (k3s_sp, k8s_sp, aws_sp):
        # Subparsers for the actual actions we support
        sps = platform.add_subparsers()
        start_sp = sps.add_parser("start", help="Start challenges",)
        start_sp.set_defaults(action=start_challenges)
        stop_sp = sps.add_parser("stop", help="stop challenges")
        stop_sp.set_defaults(action=stop_challenges)
        restart_sp = sps.add_parser("restart", help="Restart challenges",)
        restart_sp.set_defaults(action=restart_challenges)
        state_sp = sps.add_parser("state", help="Get the CTFd config form the remote state. Only useful for aws(terraform)")
        state_sp.set_defaults(action=state_challenges)
    # Now we parse the arguments and try to print subparser aware help if we're
    # missing one of our expected arguments using a janky append of `-h`
    args = parser.parse_args()
    if not args.action or not args.platform:
        # This will exit the script after printing the usage
        parser.parse_args(sys.argv[1:] + ["-h", ])

    config_filename = args.config.name
    args.config = json.loads(args.config.read())
    args.config['filename'] = os.path.join(os.getcwd(), config_filename)
    # Put the extra command line variables (--var FOO=BAR) in their own dict for passing to terraform
    args.config['vars'] = args.var if args.var else {}
    # But also overwrite any existing configs so they take preference
    args.config.update(args.config['vars'])

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    return args

if __name__ == "__main__":
    args = parse_args()
    try:
        args.action(args.platform, args.config, args.verbose)
    except ModuleNotFoundError as exc:
        raise ModuleNotFoundError(
            "{} platform is not supported".format(args.platform)
        )
