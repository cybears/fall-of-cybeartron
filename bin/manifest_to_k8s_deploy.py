#!/usr/bin/env python3
import os
import os.path
import logging
import itertools
import argparse
import json
import shutil
import sys
import yaml
from string import Template

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
REPO_BASE = os.path.normpath(os.path.join(SCRIPT_DIR, ".."))
LIB_DIR = os.path.join(REPO_BASE, "lib")
sys.path.append(LIB_DIR)

from challenge import Challenge
from utils import discover_manifests

DEPLOYMENT_NAME = "deployment.yaml"
DEFAULT_START_PORT = 3000
next_port = itertools.count(DEFAULT_START_PORT).__next__


def create_k8s_deploy(
    challenge,
    registry,
    dns,
    no_externaldns,
    reuse_ports=True,
    no_horizontal_autoscaling=False,
):
    with open(challenge.deployment.template) as template_file:
        deployment_docs = list(
            yaml.load_all(template_file.read(), Loader=yaml.SafeLoader)
        )
        hpa_doc = None
        for doc in deployment_docs:
            if doc["kind"] == "Service":
                service_doc = doc
            elif doc["kind"] == "HorizontalPodAutoscaler":
                hpa_doc = doc
            elif doc["kind"] == "Deployment":
                deployment_doc = doc
                for container in doc["spec"]["template"]["spec"]["containers"]:
                    if container["name"] == "$name":
                        deployment_container = container
                    elif container["name"] == "$name-test":
                        healthcheck_container = container

        assert healthcheck_container
        assert deployment_container
        assert service_doc
        assert deployment_doc
        target_port = None

        deployment_container["ports"] = []
        service_doc["spec"]["ports"] = []
        for port in challenge.deployment.target_port:
            loadbalancer_port = port
            if not reuse_ports:
                loadbalancer_port = next_port()
            # set the first port to target_port
            if not target_port:
                target_port = port
            deployment_container["ports"].append(
                {"containerPort": port, "protocol": "TCP"}
            )

            service_doc["spec"]["ports"].append(
                {
                    "port": loadbalancer_port,
                    "protocol": "TCP",
                    "targetPort": port,
                    "name": f"tcp-{loadbalancer_port}-{port}",
                }
            )

        full_domain = challenge.deployment.target_dns_subdomain
        if dns:
            full_domain = full_domain + "." + dns
        patched_solve_script = Template(challenge.deployment.solve_script).substitute(
            target_port=target_port,
            target_dns=full_domain,
        )
        if challenge.deployment.container_image_os == "windows":
            healthcheck_container["args"] = [
                "powershell",
                "-Command",
                "while ($$true) {start-sleep -seconds 1}",
            ]
            healthcheck_container["livenessProbe"] = {
                "exec": {"command": ["powershell", "-Command", patched_solve_script]}
            }
        else:
            healthcheck_container["args"] = [
                "/bin/sh",
                "-c",
                "while true; do sleep 5; done",
            ]
            healthcheck_container["livenessProbe"] = {
                "exec": {"command": ["/bin/sh", "-c", patched_solve_script]}
            }
        healthcheck_container["livenessProbe"]["initialDelaySeconds"] = 10
        healthcheck_container["livenessProbe"]["periodSeconds"] = challenge.deployment.healthcheck_interval
        healthcheck_container["livenessProbe"]["timeoutSeconds"] = 30

        # Remove the external-dns reference if we are not using dns. Also a hint for CTFd spin up
        # that we are a local deployment
        if no_externaldns:
            try:
                service_doc["metadata"]["annotations"].pop(
                    "external-dns.alpha.kubernetes.io/hostname"
                )
            except KeyError:
                pass

        # Remove the resource requests and horizonalautoscaling if we don't want it.
        # the set static replica counts
        if no_horizontal_autoscaling:
            if hpa_doc:
                deployment_docs.remove(hpa_doc)
            if "resources" in deployment_container:
                deployment_container.pop("resources")
            if "resources" in healthcheck_container:
                healthcheck_container.pop("resources")
            deployment_doc["spec"]["replicas"] = challenge.deployment.min_replicas

        template_file = yaml.dump_all(deployment_docs)
        template = Template(template_file)

        server_container = (
            challenge.deployment.container_image
            if "/" in challenge.deployment.container_image
            else registry + challenge.deployment.container_image
        )
        healthcheck_container = (
            challenge.deployment.healthcheck_image
            if "/" in challenge.deployment.healthcheck_image
            else registry + challenge.deployment.healthcheck_image
        )

        deployment = template.substitute(
            name=challenge.deploy_name,
            category=challenge.category,
            server_container=server_container,
            loadbalancer_port=loadbalancer_port,
            target_dns=full_domain,
            healthcheck_container=healthcheck_container,
            min_replicas=challenge.deployment.min_replicas,
            max_replicas=challenge.deployment.max_replicas,
            replica_target_cpu_percentage=challenge.deployment.replica_target_cpu_percentage,
            registry=registry,
            node_os=challenge.deployment.container_image_os,
            cpu_request=challenge.deployment.cpu_request,
            mem_request=challenge.deployment.mem_request,
            cpu_limit=challenge.deployment.cpu_limit,
            mem_limit=challenge.deployment.mem_limit,
            healthcheck_cpu_request=challenge.deployment.healthcheck_cpu_request,
            healthcheck_mem_request=challenge.deployment.healthcheck_mem_request,
            healthcheck_cpu_limit=challenge.deployment.healthcheck_cpu_limit,
            healthcheck_mem_limit=challenge.deployment.healthcheck_mem_limit,
            privileged=challenge.deployment.privileged,
            isolation=challenge.deployment.isolation,
        )
        return list(yaml.load_all(deployment, Loader=yaml.SafeLoader))


def manifest_to_k8s_deploy(
    challenge_dirs,
    registry,
    dns,
    no_externaldns,
    force,
    output_dir,
    reuse_ports=True,
    start_port=DEFAULT_START_PORT,
    no_horizontal_autoscaling=False,
    no_windows_support=False,
):
    global next_port
    next_port = itertools.count(start_port).__next__
    os.makedirs(output_dir, exist_ok=True)
    for chal_obj in discover_manifests(
        challenge_dirs, challenge_obj=True, need_handouts=False
    ):
        if chal_obj.deployment:
            deploy_script_path = os.path.normpath(
                os.path.join(output_dir, chal_obj.deploy_name + "." + DEPLOYMENT_NAME)
            )

            logging.info("Creating deploy script for {}..".format(chal_obj.name))
            if not chal_obj.use and not force:
                logging.info(
                    "Skipping %r since it opts out of use and we weren't forced",
                    chal_obj.name,
                )
                continue
            if os.path.isfile(deploy_script_path) and not force:
                logging.info("Skipping existing file {}".format(deploy_script_path))
                continue
            if (
                no_windows_support
                and chal_obj.deployment.container_image_os == "windows"
            ):
                logging.info(
                    "Skipping %r since it's windows based and we don't support it",
                    chal_obj.name,
                )
                continue
            try:
                deployment_docs = create_k8s_deploy(
                    chal_obj,
                    registry=registry,
                    dns=dns,
                    no_externaldns=no_externaldns,
                    reuse_ports=reuse_ports,
                    no_horizontal_autoscaling=no_horizontal_autoscaling,
                )
                logging.info(f"Writing deploy script to {deploy_script_path}")
                with open(deploy_script_path, "w", encoding="utf-8") as deployment_file:
                    logging.info(f"Writing deploy script to {deploy_script_path}")
                    yaml.dump_all(
                        deployment_docs,
                        deployment_file,
                        default_flow_style=False,
                        explicit_start=True,
                    )
                yield deployment_docs

            except (ValueError,) as exc:
                logging.error("Skipping %s: %s", chal_obj, exc)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, help="Verbose logging"
    )
    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        default=False,
        help="Force creation of deployment scripts, will overwrite deployment.yaml files if they already exist",
    )
    parser.add_argument(
        "-r",
        "--registry",
        default="registry.gitlab.com/cybears/fall-of-cybeartron/",
        help="Base container registry location",
    )
    parser.add_argument(
        "-o",
        "--output-dir",
        default=os.path.join(REPO_BASE, ".deploy"),
        help="Directory to place the deployment yamls in. Defaults to .deploy in the repo root.",
    )
    parser.add_argument(
        "--config",
        action="store",
        required=True,
        help="JSON configuration file for the ctf",
        type=argparse.FileType("r"),
    )

    # Local testing stuff
    parser.add_argument(
        "--no-port-reuse",
        action="store_true",
        help="Don't reuse ports for loadbalancers. Useful when deploying locally on minikube or k3s, etc",
    )
    parser.add_argument(
        "--no-domain",
        action="store_true",
        help="Don't set a domain for loadbalancers, just uses subdomain. Useful when deploying locally on minikube or k3s, etc",
    )
    parser.add_argument(
        "--start-port",
        type=int,
        default=3000,
        help="The port to start with when assigning ports to the load balancers. Only useful when  the --no-port-reuse flag is specified.",
    )
    parser.add_argument(
        "--no-horizontal-autoscaling",
        action="store_true",
        help="Don't do horizontal autoscaling",
    )
    parser.add_argument(
        "--no-external-dns",
        action="store_true",
        help="Don't use externaldns to set dns entries for challenge domains",
    )
    parser.add_argument(
        "--no-windows-support",
        action="store_true",
        help="Don't support windows deployments",
    )

    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig()

    config = json.loads(args.config.read())
    if args.no_domain:
        args.dns = None
    else:
        args.dns = config["chal_domain"]
    for deployment in manifest_to_k8s_deploy(
        config["challenge_directories"],
        registry=args.registry,
        dns=args.dns,
        no_externaldns=args.no_external_dns,
        force=args.force,
        output_dir=args.output_dir,
        reuse_ports=not args.no_port_reuse,
        start_port=args.start_port,
        no_horizontal_autoscaling=args.no_horizontal_autoscaling,
        no_windows_support=args.no_windows_support,
    ):
        pass
