#!/usr/bin/env python3
import argparse
import distutils
import importlib
import logging
import os
import os.path
import pathlib
import sys
import tempfile
import requests
import json
import urllib.parse
import zipfile

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BASE_DIR=os.path.normpath(os.path.join(SCRIPT_DIR, '..'))
LIB_DIR=os.path.join(BASE_DIR,'lib')
sys.path.append(LIB_DIR)
import ctfd_config
import ctfd_upload
import ctfd_admin_pass
import cybears_gitlab
import utils

def prompt(query):
   sys.stdout.write('%s [y/n]: ' % query)
   val = input()
   try:
       ret = distutils.util.strtobool(val)
   except ValueError:
       sys.stdout.write('Please answer with a y/n\n')
       return prompt(query)
   return ret

def start_ctfd(platform, ctfd_path, args):
    # Extract the CTFd config from the args object
    config = args.config
    # If we have artifacts to download, do that now and reconfigure the challenge dir to the artifacts
    token = os.environ.get("GITLAB_ACCESS_TOKEN")
    try:
        gl = cybears_gitlab.Gitlab('https://gitlab.com', f"{config['gitlab_group']}/{config['gitlab_project']}", token)
        latest_pipeline = gl.get_latest_pipeline(config['gitlab_branch'])
    except:
        raise Exception("Failed to find latest successful pipeline")

    try:
        if 'ctfd_bundle_job' in config:
            logging.info("Retrieving custom CTFd overlay")
            ctfd_bundle = gl.get_artifact_from_job(latest_pipeline, config['ctfd_bundle_job'])
            temp_ctfd_dir = tempfile.TemporaryDirectory()
            z = zipfile.ZipFile(ctfd_bundle)
            # artifact should only have one file within it, which is a gzip tar.
            # By getting a zipinfo this way we don't rely on the name needing to be standardised.
            overlay_zipinfo = z.infolist()[0]
            z.extract(overlay_zipinfo, path=temp_ctfd_dir.name)
            # config['vars'] is a way to parse extra variables to terraform.
            config['vars']['ctfd_overlay'] = os.path.join(temp_ctfd_dir.name, overlay_zipinfo.filename)
    except:
        raise Exception("Failed to get ctfd_bundle_job")

    # For the moment we'll pull down the challenge artifacts regardless of if
    # we intend to push them to CTFd or not
    try:
        if 'artifact_bundle_job' in config:
            logging.info("Retrieving challenge artifacts")
            temp_challenge_dir = tempfile.TemporaryDirectory()
            challenge_artifacts = gl.get_artifact_from_job(latest_pipeline, config['artifact_bundle_job'])
            utils.unzip_unzips(challenge_artifacts, temp_challenge_dir)
            config['challenge_directories'] = [temp_challenge_dir.name]
    except:
        raise Exception("Failed to get challenge artifacts")

    # Add a CTFd secret key if one doesn't already exist
    with open(
        os.path.join(ctfd_path, ".ctfd_secret_key"), "ab+"
    ) as secretfile:
        if not secretfile.read():
            secretfile.write(os.urandom(64))
    # Spin up the CTFd infra using the platform specific module
    platform_module = importlib.import_module(platform)
    ctfd_host = platform_module.start_ctfd(
        ctfd_path, config, verbose=args.verbose
    )
    # The CTFd may have an alternate name which we need to use instead of the
    # service name to get TLS validation
    logging.info("The service name is {!r}".format(ctfd_host))
    ctfd_host = os.environ.get("REAL_CTFD_HOST", ctfd_host)

    # Now we can run the setup actions to make sure it's fully configured
    hostname = config.get("hostname")
    ctfd_config.do_config(
        ctfd_host, SCRIPT_DIR, config, force=args.force, hostname=hostname,
    )
    # And finally upload challenges unless we've been told not to
    if args.push_challenges:
        ctfd_config.push_chals(
            ctfd_host, config["challenge_directories"], hostname=hostname,
        )
    else:
        logging.warning("Skipping challenge upload")

    logging.info("CTFd admin password=%r" % ctfd_admin_pass.get_pass())
    # And spit out a line which a shell can eval
    logging.info("export CTFD_HOST=%r" % ctfd_host)

def stop_ctfd(platform, ctfd_path, args):
    platform_module = importlib.import_module(platform)
    platform_module.stop_ctfd(ctfd_path, args.config, verbose=args.verbose)

def restart_ctfd(platform, ctfd_path, args):
    stop_ctfd(platform, ctfd_path, args)
    start_ctfd(platform, ctfd_path, args)

def purge_ctfd(platform, ctfd_path, args):
    platform_module = importlib.import_module(platform)
    platform_module.purge_ctfd(ctfd_path, args.config, verbose=args.verbose)

def state_ctfd(platform, ctfd_path, args):
    platform_module = importlib.import_module(platform)
    try:
        platform_module.state_ctfd(ctfd_path, args.config, verbose=args.verbose)
    except AttributeError as exc:
        logging.error(exc)


def parse_args():
    # Top level parser
    parser = argparse.ArgumentParser()
    parser.set_defaults(action=None)
    parser.set_defaults(platform=None)
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Verbose logging')
    parser.add_argument('-f', '--force', action='store_true', default=False, help='Force CTFd setup and challenge upload')
    parser.add_argument('--hostname', nargs=1, default=None, help='Hostname to use in challenge descriptions. Useful for local deployments')
    parser.add_argument(
        "--config", action="store",
        help="JSON configuration file for the ctf",
        type=argparse.FileType('r'),
        required=True
    )
    parser.add_argument(
        "--var", action=utils.kv_dict_append_action,
        metavar="'VAR=VALUE'",
        help='set or override individual config variables'
    )
    # We add top level subparsers to define which platform we're using
    sps = parser.add_subparsers()
    compose_sp = sps.add_parser("compose", help="Use docker-compose")
    compose_sp.set_defaults(platform="compose")
    k3s_sp = sps.add_parser("k3s", help="Use k3s kubectl")
    k3s_sp.set_defaults(platform="k3s")
    k8s_sp = sps.add_parser("k8s", help="Use k8s")
    k8s_sp.set_defaults(platform="k8s")
    k8s_sp.add_argument(
        "--k8s-config", action="store", metavar="KUBECONFIG",
        help="Use a specific config (defaults to environment KUBECONFIG)",
        type=pathlib.Path, default=os.environ.get("KUBECONFIG"),
    )
    aws_sp = sps.add_parser("aws", help="Use aws via terraform")
    aws_sp.set_defaults(platform="aws")
    none_sp = sps.add_parser("none", help="Do not deploy a CTFd")
    none_sp.set_defaults(platform="none")

    # And then add action subparsers to each of those so that we have
    # reasonable natural feeling `./script <platform> <action>` command lines
    for platform in (compose_sp, k3s_sp, k8s_sp, aws_sp, none_sp):
        # Subparsers for the actual actions we support
        sps = platform.add_subparsers()
        # Start a CTFd
        start_sp = sps.add_parser("start", help="Start CTFd",)
        start_sp.set_defaults(action=start_ctfd)
        start_sp.add_argument(
            "--skip-challenges", dest="push_challenges", action="store_false",
            help="Skip uploading of challenges to CTFd",
        )
        # Stop a CTFd
        stop_sp = sps.add_parser("stop", help="Stop CTFd")
        stop_sp.set_defaults(action=stop_ctfd)
        # ReStart a CTFd
        restart_sp = sps.add_parser("restart", help="Restart CTFd",)
        restart_sp.set_defaults(action=start_ctfd)
        restart_sp.add_argument(
            "--skip-challenges", dest="push_challenges", action="store_false",
            help="Skip uploading of challenges to CTFd",
        )
        # Purge/destroy a CTFd
        purge_sp = sps.add_parser("purge", help="Stop CTFd and purge state")
        purge_sp.set_defaults(action=purge_ctfd)
        # Get/check state of an existing CTFd
        state_sp = sps.add_parser("state", help="Get the CTFd config form the remote state. Only useful for aws(terraform)")
        state_sp.set_defaults(action=state_ctfd)

    # Now we parse the arguments and try to print subparser aware help if we're
    # missing one of our expected arguments using a janky append of `-h`
    args = parser.parse_args()
    if not args.action or not args.platform:
        # This will exit the script after printing the usage
        parser.parse_args(sys.argv[1:] + ["-h", ])

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    config_filename = args.config.name
    args.config = json.loads(args.config.read())
    if args.hostname:
        args.config['hostname'] = args.hostname[0]
    args.config['filename'] = os.path.join(os.getcwd(), config_filename)
    # Put the extra command line variables (--var FOO=BAR) in their own dict for passing to terraform
    args.config['vars'] = args.var if args.var else {}
    # But also overwrite any existing configs so they take preference
    args.config.update(args.config['vars'])
    return args

if __name__ == "__main__":
    args = parse_args()
    ctfd_path = os.path.join(BASE_DIR, args.config['ctfd_dir'])
    try:
        args.action(args.platform, ctfd_path, args)
    except ModuleNotFoundError as exc:
        raise ModuleNotFoundError(
            "{} platform is not supported".format(args.platform)
        )
