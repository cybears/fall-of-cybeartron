#!/usr/bin/env python3
import argparse
import collections
import datetime
import gitlab
import gitlab.exceptions
import glob
import logging
import os
import pathlib
import sys

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BASE_DIR=os.path.normpath(os.path.join(SCRIPT_DIR, '..'))
LIB_DIR=os.path.join(BASE_DIR,'lib')
sys.path.append(LIB_DIR)
import challenge
from utils import discover_manifests

# Set of tags we define as "core" which will always be shown in the heads up
# Markdown table format
TABLE_FIELDS = (
    "name", "tags", "wf_concept", "wf_workup", "wf_playtest",
    "wf_ci", "wf_deploy", "wf_review", "wf_theme", "path",
)
TABLE_FMT = "| " + " | ".join(
    "{{{}}}".format(field) for field in TABLE_FIELDS
) + " |\n"

def find_encapsulating_event(chal_obj):
    # We have `.challenge_root` and `.event_root` files to help us work out the
    # semantic structure of the repo. We just walk up from the MANIFEST file.
    # We add a stub to the end since challenge objects only keep a reference to
    # their root directory and we'll handle the sentinel files being in the
    # same directory as the MANIFEST so we don't get surprised in future.
    check_path = pathlib.Path(chal_obj._root_path) / "stub.foo"
    while check_path != check_path.parent:
        if (check_path.parent / ".event_root").is_file():
            event_path = check_path.parent
            break
        check_path = check_path.parent
    else:
        raise FileNotFoundError("No .event_root file found")
    while check_path != check_path.parent:
        if (check_path.parent / ".challenge_root").is_file():
            chal_root_path = check_path.parent
            break
        check_path = check_path.parent
    else:
        raise FileNotFoundError("No .challenge_root file found")
    # Get the relative path for the event from the challenge root and take the
    # POSIX path  as the event name (the wiki can slugify this)
    event_name = event_path.relative_to(chal_root_path).as_posix()
    if event_name == ".":
        event_name = "miscellaneous"
    chal_relpath = pathlib.Path(chal_obj._root_path).relative_to(event_path)
    return event_name, chal_relpath


def run(args):
    # Find the project using CI variables
    foc_proj = None
    if "CI_SERVER_URL" in os.environ:
        gl_sess = gitlab.Gitlab(
            os.environ["CI_SERVER_URL"],
            private_token=os.environ["SECRET_PA_TOKEN"],
        )
        foc_proj = gl_sess.projects.get(os.environ["CI_PROJECT_ID"])
    # Scan each event path provided and push a page to the wiki
    event_row_kwargs = collections.defaultdict(list)
    # Now make a row for each challenge object we find
    total_chals, tag_counts = 0, collections.defaultdict(int)
    for chal_obj in discover_manifests(args.scan_path, challenge_obj=True, need_handouts=False):
        try:
            event_name, chal_relpath = find_encapsulating_event(chal_obj)
        except FileNotFoundError:
            logging.warning(
                "Skipping %r which is not in an event or lost",
                chal_obj.name
            )
            continue
        row_kwargs = {
            "name": chal_obj.name,
            "category": chal_obj.category,  # For sorting only
            "tags": ", ".join(chal_obj.tags),
            "path": chal_relpath,
        }
        row_kwargs.update(**{
            field: (
                ":white_check_mark:"
                if getattr(chal_obj, field) else ":x:"
            )
            # Add any columns not already set as Ex's and Oh's
            for field in set(TABLE_FIELDS) - set(row_kwargs)
        })
        # Add the kwargs for this row to the event's list
        event_row_kwargs[event_name].append(row_kwargs)
        # We also count the challenge toward the event total and increment the
        # count of the unique tags set for the challenges
        total_chals += 1
        for tag in set(chal_obj.tags):
            tag_counts[tag] += 1
    # Now we handle each event we found and collected data for
    for event_name, row_kwargs_items in event_row_kwargs.items():
        # Add a heading for the workflow report
        wf_markdown = "# {} Workflow Report\n\nGenerated {}\n\n".format(
            event_name,
            datetime.datetime.now(datetime.timezone.utc).isoformat(),
        )
        # Add the heads up view for tags and the total challenge count
        wf_markdown += "| Tag | Count |\n| --- | ----- |\n"
        for tag, count in sorted(tag_counts.items(), key=lambda t: -t[1]):
            if count > 5 or tag in challenge.challenge_schema["category"]["allowed"]:
                wf_markdown += "| {} | {} |\n".format(tag, count)
        wf_markdown += "| **Total challenges** | {} |\n\n".format(total_chals)
        # Make a table header and separator
        wf_markdown += TABLE_FMT.format(**{
            field: field.replace("wf_", "") for field in TABLE_FIELDS
        })
        wf_markdown += TABLE_FMT.format(**{
            field: "-" * len(field.replace("wf_", ""))
            for field in TABLE_FIELDS
        })
        # Render each of the rows after sorting them
        for row_kwargs in sorted(
            row_kwargs_items, key=lambda r: r["category"] + r["name"]
        ):
            wf_markdown += TABLE_FMT.format(**row_kwargs)
        logging.info(event_name)
        logging.info(wf_markdown)
        # Finally update or create the wiki page for this event report. We need
        # to use slugs without slashes in them because the GitLab wikis API
        # doesn't seem to handle updating them properly ):
        wiki_slug = "workflow-" + str(event_name).replace("/", "-")
        if foc_proj:
            try:
                wiki_page = foc_proj.wikis.get(wiki_slug)
            except gitlab.exceptions.GitlabGetError:
                foc_proj.wikis.create({
                    "title": wiki_slug, "content": wf_markdown
                })
            else:
                wiki_page.content = wf_markdown
                wiki_page.save()

def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose", action="count", default=0, help="Increase verbosity"
    )
    parser.add_argument("scan_path", nargs="+", help="Event path to scan")
    args = parser.parse_args(args)
    # Handle logging verbosity here
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    delattr(args, "verbose")
    return args

if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    run(args)
