import pytest
import os
import sys
import json
import argparse

from . import BASE_DIR, CONFIG_DIR
from spin_up_challenges import setup_deploy_dir
TEST_DEPLOY_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), '.test-deploy')

def _test_config(config_file):
    config_file.seek(0, 0)
    c = json.load(config_file)
    c['no_externaldns'] = True
    c['no_horizontal_autoscaler'] = True
    c['no_windows_support'] = True
    c["filename"] = os.path.join(BASE_DIR, config_file.name)
    c["vars"] = {}
    c["no_monitoring"] = True
    return c

@pytest.fixture(scope="session")
def test_deploy_dir():
    return TEST_DEPLOY_DIR

@pytest.fixture(scope="session")
def config(config_file):
    return _test_config(config_file)

@pytest.fixture(scope="session")
def kubectl():
    import k3s
    return k3s.kubectl

def deployments(config_file):
    import k3s
    kubectl = k3s.kubectl
    config = _test_config(config_file)
    k3s.start_challenges(config, verbose=False)

    deployments = setup_deploy_dir(config, TEST_DEPLOY_DIR, k3s.reuse_ports)

    for deployment in deployments:
        for doc in deployment:
            if doc['kind'] == 'Deployment':
                yield doc['metadata']['name']

def pytest_addoption(parser):
    parser.addoption(
        "--run-deploy-tests", action="store_true", default=False, help="run k3s deployment tests"
    )
    parser.addoption(
        "--runtime", default=13*60, type=int, help="How many seconds to run deployments before testing."
    )
    parser.addoption(
        "--config", action="store",
        help="JSON configuration file for the ctf",
        type=argparse.FileType('r'),
        default=os.path.join(CONFIG_DIR, "bsides-2021.tfvars.json")
    )

def pytest_configure(config):
    config.addinivalue_line("markers", "deploy: mark test as a deploy test")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--run-deploy-tests"):
        # disable all the non deploy tests
        skip_non_deploy = pytest.mark.skip(reason="Skipping unit tests during deploy tests")
        for item in items:
            if "deploy" not in item.keywords:
                item.add_marker(skip_non_deploy)
    else:    
        # disable all the deploy tests
        skip_deploy = pytest.mark.skip(reason="need --run-deploy-tests option to run")
        for item in items:
            if "deploy" in item.keywords:
                item.add_marker(skip_deploy)
    

def pytest_generate_tests(metafunc):
    if metafunc.config.option.run_deploy_tests and "deployment" in metafunc.fixturenames:
        metafunc.parametrize("deployment", list(deployments(metafunc.config.option.config)))
    if "runtime" in metafunc.fixturenames:
        metafunc.parametrize("runtime", [metafunc.config.option.runtime], scope="session")
    if "config_file" in metafunc.fixturenames:
        metafunc.parametrize("config_file", [metafunc.config.option.config], scope="session")