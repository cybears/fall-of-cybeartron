import pytest
import os
from string import Template
from utils import discover_manifests
from . import BSIDES_CHALLENGE_DIR, TEST_CHALLENGE_DIR

BSIDES_2019 = os.path.join(BSIDES_CHALLENGE_DIR, "2019")
BSIDES_2021 = os.path.join(BSIDES_CHALLENGE_DIR, "2020")
BSIDES_NEXT = os.path.join(BSIDES_CHALLENGE_DIR, "next")
CHAL_DEPLOYMENT_LIST = [
    x
    for x in list(
        discover_manifests(
            [TEST_CHALLENGE_DIR, BSIDES_2019, BSIDES_2021, BSIDES_NEXT],
            challenge_obj=True,
            need_handouts=False,
        )
    )
    if x.deployment
]
KNOWN_NO_CI_LIST = ["Oort Cloud"]
KNOWN_DIFFERENT_CI_TESTS_LIST = ["Fixie Bike Website"]


def get_ci_client_test(ci_doc):
    tests = [k for k, v in ci_doc.items() if k.startswith("test:")]
    if len(tests) > 1:
        tests = [k for k in tests if k.endswith("healthcheck") or k.endswith("client")]
    assert len(tests) == 1
    return ci_doc[tests[0]]


def get_ci_solve_script(ci_doc):
    challenge_host = ""
    challenge_port = ""
    try:
        challenge_host = ci_doc["variables"]["CHALLENGE_HOST"]
        challenge_port = ci_doc["variables"]["CHALLENGE_PORT"]
    except KeyError:
        pass
    client_test = get_ci_client_test(ci_doc)
    return (
        client_test["script"][0]
        .replace("${CHALLENGE_HOST}", challenge_host)
        .replace("${CHALLENGE_PORT}", challenge_port)
    )


def get_healthcheck_image(ci_doc):
    healthcheck_image_tag = ""
    try:
        healthcheck_image_tag = ci_doc["variables"]["HEALTHCHECK_IMAGE_TAG"]
    except KeyError:
        pass
    client_test = get_ci_client_test(ci_doc)
    return (
        client_test["image"]
        .replace("${CI_REGISTRY_IMAGE}/", "")
        .replace("${HEALTHCHECK_IMAGE_TAG}", healthcheck_image_tag)
    )


@pytest.mark.parametrize("chal_obj", CHAL_DEPLOYMENT_LIST)
def test_chal_has_ci(chal_obj):
    if chal_obj.name not in KNOWN_NO_CI_LIST:
        ci_doc = chal_obj.ci()
        assert ci_doc
        get_ci_client_test(ci_doc)


@pytest.mark.parametrize("chal_obj", CHAL_DEPLOYMENT_LIST)
def test_chal_solve(chal_obj):
    if chal_obj.name not in KNOWN_NO_CI_LIST + KNOWN_DIFFERENT_CI_TESTS_LIST:
        ci = chal_obj.ci()
        ci_solve_script = get_ci_solve_script(ci)

        patched_solve_script = Template(chal_obj.deployment.solve_script).substitute(
            target_port=chal_obj.deployment.target_port[0],
            target_dns=chal_obj.deployment.target_dns_subdomain + ".ctf.cybears.io",
        )
        # chal skeleton generated
        if ci_solve_script.startswith("${CHALLENGE_MANAGER}"):
            assert patched_solve_script.startswith("python3")
            assert patched_solve_script.endswith(
                f"-r {chal_obj.deployment.target_dns_subdomain}.ctf.cybears.io:{chal_obj.deployment.target_port[0]}"
            )
        else:
            assert ci_solve_script == patched_solve_script


@pytest.mark.parametrize("chal_obj", CHAL_DEPLOYMENT_LIST)
def test_chal_healthcheck_image(chal_obj):
    if chal_obj.name not in KNOWN_NO_CI_LIST + KNOWN_DIFFERENT_CI_TESTS_LIST:
        ci = chal_obj.ci()
        ci_healthcheck_image = get_healthcheck_image(ci)
        assert ci_healthcheck_image == chal_obj.deployment.healthcheck_image
