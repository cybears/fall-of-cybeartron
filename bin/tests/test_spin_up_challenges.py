import pytest
import logging
import time

@pytest.mark.deploy
@pytest.fixture(scope="session", autouse=True)
def setup(kubectl, config, test_deploy_dir, runtime):
    import k3s
    kubectl.apply(test_deploy_dir)
    time.sleep(runtime)
    yield
    kubectl.delete(test_deploy_dir)
    k3s.stop_challenges(config, verbose=False)

@pytest.mark.deploy
def test_deployments(deployment, kubectl):
    for d in kubectl.get_deployments(name=deployment, ns="challenges"):
        if 'unavailableReplicas' in d['status']:
            pytest.fail(f"{deployment} has {d['status']['unavailableReplicas']} unavailableReplicas")
        if 'availableReplicas' not in d['status']:
            pytest.fail(f"{deployment} doesn't have any availableReplicas")
        elif d['status']['availableReplicas'] != d['status']['replicas']:
            pytest.fail(f"{deployment} doesn't have expected replicas available({d['status']['replicas']})")

@pytest.mark.deploy
def test_pods(deployment, kubectl, setup):
    for pod in kubectl.get_deployment_pods(name=deployment, ns="challenges"):
        for container in pod['status']['containerStatuses']:
            if not container['ready']:
                pytest.fail(f"{container['name']} in {deployment} is not ready")
            if container['restartCount'] != 0:
                pytest.fail(f"{container['name']} in {deployment} has restartCount of {container['restartCount']}")