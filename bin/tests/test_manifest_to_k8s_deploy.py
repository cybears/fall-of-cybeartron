import glob
import pytest
import os
import yaml
from . import SCRIPT_DIR, TEST_CHALLENGE_DIR

from challenge_fixtures import complete_challenge, required_deployment, required_challenge, temp_files, valid_challenge_template
from challenge import Challenge, InvalidChallengeManifest
from manifest_to_k8s_deploy import create_k8s_deploy, manifest_to_k8s_deploy

@pytest.fixture
def complete_challenge_obj(complete_challenge):
    complete_challenge['deployment'].pop('template')
    complete_challenge['deployment']['target_port'] = 1337
    return Challenge(SCRIPT_DIR, attrs=complete_challenge)

@pytest.fixture(scope="session")
def deploy_dir(tmp_path_factory):
    fn = tmp_path_factory.mktemp("deploy")
    return fn

def test_good(complete_challenge_obj, deploy_dir):
    k8s_deploy = create_k8s_deploy(complete_challenge_obj,
                                   registry='registry.gitlab.com',
                                   dns='ctf.cybears.io',
                                   no_externaldns=False,
                                   reuse_ports=True,
                                   no_horizontal_autoscaling=False)
    assert k8s_deploy
    for doc in k8s_deploy:
        if doc['kind'] == 'Deployment':
            for c in doc['spec']['template']['spec']['containers']:
                assert c['image'].startswith('registry.gitlab.com')
                if 'livenessProbe' in c:
                    assert c['livenessProbe']["initialDelaySeconds"]
                    assert c['livenessProbe']["periodSeconds"]
                    assert c['livenessProbe']["timeoutSeconds"]
        
        elif doc['kind'] == 'Service':
            assert doc['metadata']['annotations']['cybears_dns'].endswith('.ctf.cybears.io')


def test_no_externaldns(complete_challenge_obj, deploy_dir):
    k8s_deploy = create_k8s_deploy(complete_challenge_obj,
                                   registry='registry.gitlab.com',
                                   dns='ctf.cybears.io',
                                   no_externaldns=True,
                                   reuse_ports=True,
                                   no_horizontal_autoscaling=False)
    assert k8s_deploy
    for doc in k8s_deploy:
        if 'annotations' in doc['metadata']:
            assert 'external-dns.alpha.kubernetes.io/hostname' not in doc['metadata']['annotations']

def test_no_dns(complete_challenge_obj, deploy_dir):
    k8s_deploy = create_k8s_deploy(complete_challenge_obj,
                                   registry='registry.gitlab.com',
                                   dns=None,
                                   no_externaldns=False,
                                   reuse_ports=True,
                                   no_horizontal_autoscaling=False)
    assert k8s_deploy
    for doc in k8s_deploy:
        if doc['kind'] == 'Service':
            assert 'ctf.cybears.io' not in doc['metadata']['annotations']['cybears_dns']
            

def test_no_hpa(complete_challenge_obj, deploy_dir):
    k8s_deploy = create_k8s_deploy(complete_challenge_obj,
                                   registry='registry.gitlab.com',
                                   dns='ctf.cybears.io',
                                   no_externaldns=False,
                                   reuse_ports=True,
                                   no_horizontal_autoscaling=True)
    assert k8s_deploy
    for doc in k8s_deploy:
        assert doc['kind'] != 'HorizontalPodAutoscaler'
        if doc['kind'] == 'Deployment':
            for container in doc['spec']['template']['spec']['containers']:
                assert 'resources' not in container
            assert 'replicas' in doc['spec']

def test_no_reuse_port(complete_challenge_obj, deploy_dir):
    k8s_deploy = create_k8s_deploy(complete_challenge_obj,
                                   registry='registry.gitlab.com',
                                   dns='ctf.cybears.io',
                                   no_externaldns=False,
                                   reuse_ports=False,
                                   no_horizontal_autoscaling=False)
    assert k8s_deploy
    for doc in k8s_deploy:
        if doc['kind'] == 'Service':
            assert doc['spec']['ports'][0]['port'] not in complete_challenge_obj.deployment.target_port


def test_multiple_ports(valid_challenge_template, deploy_dir):
    valid_challenge_template['deployment']['target_port'] = [1337,1338]
    complete_challenge_obj = Challenge(SCRIPT_DIR, attrs=valid_challenge_template)
    k8s_deploy = create_k8s_deploy(complete_challenge_obj,
                                   registry='registry.gitlab.com',
                                   dns='ctf.cybears.io',
                                   no_externaldns=False,
                                   reuse_ports=False,
                                   no_horizontal_autoscaling=False)
    assert k8s_deploy
    for doc in k8s_deploy:
        if doc['kind'] == 'Service':
            ports = [p['port'] for p in doc['spec']['ports']]
            assert len(set(ports)) == len(ports)

def test_multiple_deployments_no_reuse_ports(valid_challenge_template, deploy_dir):
    valid_challenge_template['deployment']['target_port'] = [1337,1338]
    complete_challenge_obj = Challenge(SCRIPT_DIR, attrs=valid_challenge_template)
    k8s_deploy = create_k8s_deploy(complete_challenge_obj,
                                   registry='registry.gitlab.com',
                                   dns='ctf.cybears.io',
                                   no_externaldns=False,
                                   reuse_ports=False,
                                   no_horizontal_autoscaling=False)
    assert k8s_deploy
    for doc in k8s_deploy:
        if doc['kind'] == 'Service':
            first_ports = [p['port'] for p in doc['spec']['ports']]
    k8s_deploy = create_k8s_deploy(complete_challenge_obj,
                                   registry='registry.gitlab.com',
                                   dns='ctf.cybears.io',
                                   no_externaldns=False,
                                   reuse_ports=False,
                                   no_horizontal_autoscaling=False)
    assert k8s_deploy
    for doc in k8s_deploy:
        if doc['kind'] == 'Service':
            second_ports = [p['port'] for p in doc['spec']['ports']]
            assert len(set(first_ports + second_ports)) == len(first_ports) + len(second_ports)
 

def test_linux(valid_challenge_template, deploy_dir):
    valid_challenge_template['deployment']['container_image_os'] = 'linux'
    complete_challenge_obj = Challenge(SCRIPT_DIR, attrs=valid_challenge_template)
    k8s_deploy = create_k8s_deploy(complete_challenge_obj,
                                   registry='registry.gitlab.com',
                                   dns='ctf.cybears.io',
                                   no_externaldns=False,
                                   reuse_ports=False,
                                   no_horizontal_autoscaling=False)
    assert k8s_deploy
    for doc in k8s_deploy:
        if doc['kind'] == 'Deployment':
            assert doc['spec']['template']['spec']['nodeSelector']['kubernetes.io/os'] == 'linux'
            for container in doc['spec']['template']['spec']['containers']:
                if 'args' in container:
                    assert '/bin/sh' in container['args']
                if 'livenessProbe' in container and 'exec' in container['livenessProbe']:
                    assert '/bin/sh' in container['livenessProbe']['exec']['command']

def test_good_deploy_dir(deploy_dir):
    deployments = manifest_to_k8s_deploy([TEST_CHALLENGE_DIR],
                            registry='registry.gitlab.com',
                            dns='ctf.cybears.io',
                            no_externaldns=False,
                            force=True,
                            output_dir=deploy_dir,
                            reuse_ports=True,
                            no_horizontal_autoscaling=False)
    ports = []
    for deployment in deployments:
        for doc in deployment:
            # test dns='ctf.cybears.io'
            if doc['kind'] == 'Service':
                assert 'ctf.cybears.io' in doc['metadata']['annotations']['cybears_dns']
            # test no_externaldns=False
            if 'annotations' in doc['metadata']:
                assert 'external-dns.alpha.kubernetes.io/hostname' in doc['metadata']['annotations']
            # test no_horizontal_autoscaling=False
            if doc['kind'] == 'Deployment':
                for container in doc['spec']['template']['spec']['containers']:
                    assert 'resources' in container
                assert 'replicas' not in doc['spec']
            elif doc['kind'] == 'Service':
                ports += [p['port'] for p in doc['spec']['ports']]
    # reuse_ports=True
    assert len(set(ports)) < len(ports)
    
def test_good_deploy_dir_no_reuse_ports(deploy_dir):
    deployments = manifest_to_k8s_deploy([TEST_CHALLENGE_DIR],
                                        registry='registry.gitlab.com',
                                        dns='ctf.cybears.io',
                                        no_externaldns=False,
                                        force=True,
                                        output_dir=deploy_dir,
                                        reuse_ports=False,
                                        no_horizontal_autoscaling=False)  
    ports = []
    for deployment in deployments:
        for doc in deployment:
            # test dns='ctf.cybears.io'
            if doc['kind'] == 'Service':
                assert 'ctf.cybears.io' in doc['metadata']['annotations']['cybears_dns']
            # test no_externaldns=False
            if 'annotations' in doc['metadata']:
                assert 'external-dns.alpha.kubernetes.io/hostname' in doc['metadata']['annotations']
            # test no_horizontal_autoscaling=False
            if doc['kind'] == 'Deployment':
                for container in doc['spec']['template']['spec']['containers']:
                    assert 'resources' in container
                assert 'replicas' not in doc['spec']
            elif doc['kind'] == 'Service':
                ports += [p['port'] for p in doc['spec']['ports']]
    # test reuse_ports=False
    assert len(ports)
    assert len(set(ports)) == len(ports)

def test_good_deploy_dir_no_hpa(deploy_dir):
    deployments = manifest_to_k8s_deploy([TEST_CHALLENGE_DIR],
                            registry='registry.gitlab.com',
                            dns='ctf.cybears.io',
                            no_externaldns=False,
                            force=True,
                            output_dir=deploy_dir,
                            reuse_ports=True,
                            no_horizontal_autoscaling=True)
    ports = []
    for deployment in deployments:
        for doc in deployment:
            # test dns='ctf.cybears.io'
            if doc['kind'] == 'Service':
                assert 'ctf.cybears.io' in doc['metadata']['annotations']['cybears_dns']
            # test no_externaldns=False
            if 'annotations' in doc['metadata']:
                assert 'external-dns.alpha.kubernetes.io/hostname' in doc['metadata']['annotations']
            # test no_horizontal_autoscaling=True
            assert doc['kind'] != 'HorizontalPodAutoscaler'
            if doc['kind'] == 'Deployment':
                for container in doc['spec']['template']['spec']['containers']:
                    assert 'resources' not in container
                assert 'replicas'  in doc['spec']
            elif doc['kind'] == 'Service':
                ports += [p['port'] for p in doc['spec']['ports']]
    # test reuse_ports=True
    assert len(set(ports)) < len(ports)
    
def test_good_deploy_dir_no_hpa(deploy_dir):
    deployments = manifest_to_k8s_deploy([TEST_CHALLENGE_DIR],
                            registry='registry.gitlab.com',
                            dns='ctf.cybears.io',
                            no_externaldns=True,
                            force=True,
                            output_dir=deploy_dir,
                            reuse_ports=True,
                            no_horizontal_autoscaling=True)
    ports = []
    for deployment in deployments:
        for doc in deployment:
            # test dns='ctf.cybears.io'
            if doc['kind'] == 'Service':
                assert 'ctf.cybears.io' in doc['metadata']['annotations']['cybears_dns']
            # test no_externaldns=True
            if 'annotations' in doc['metadata']:
                assert 'external-dns.alpha.kubernetes.io/hostname' not in doc['metadata']['annotations']
            # test no_horizontal_autoscaling=True
            assert doc['kind'] != 'HorizontalPodAutoscaler'
            if doc['kind'] == 'Deployment':
                for container in doc['spec']['template']['spec']['containers']:
                    assert 'resources' not in container
                assert 'replicas'  in doc['spec']
            elif doc['kind'] == 'Service':
                ports += [p['port'] for p in doc['spec']['ports']]
    # test reuse_ports=True
    assert len(set(ports)) < len(ports)


def test_good_deploy_dir_no_dns(deploy_dir):
    deployments = manifest_to_k8s_deploy([TEST_CHALLENGE_DIR],
                            registry='registry.gitlab.com',
                            dns=None,
                            no_externaldns=True,
                            force=True,
                            output_dir=deploy_dir,
                            reuse_ports=True,
                            no_horizontal_autoscaling=True)
    ports = []
    for deployment in deployments:
        for doc in deployment:
            # test dns=None
            if doc['kind'] == 'Service':
                assert 'ctf.cybears.io' not in doc['metadata']['annotations']['cybears_dns']
            # test no_externaldns=True
            if 'annotations' in doc['metadata']:
                assert 'external-dns.alpha.kubernetes.io/hostname' not in doc['metadata']['annotations']
            # test no_horizontal_autoscaling=True
            assert doc['kind'] != 'HorizontalPodAutoscaler'
            if doc['kind'] == 'Deployment':
                for container in doc['spec']['template']['spec']['containers']:
                    assert 'resources' not in container
                assert 'replicas'  in doc['spec']
            elif doc['kind'] == 'Service':
                ports += [p['port'] for p in doc['spec']['ports']]
    # test reuse_ports=True
    assert len(set(ports)) < len(ports)


def test_good_deploy_dir_registry(deploy_dir):
    deployments = manifest_to_k8s_deploy([TEST_CHALLENGE_DIR],
                            registry='registry3.gitlab.com',
                            dns=None,
                            no_externaldns=True,
                            force=True,
                            output_dir=deploy_dir,
                            reuse_ports=True,
                            no_horizontal_autoscaling=True)
    ports = []
    for deployment in deployments:
        for doc in deployment:
            # test dns=None
            if doc['kind'] == 'Service':
                assert 'ctf.cybears.io' not in doc['metadata']['annotations']['cybears_dns']
            # test no_externaldns=True
            if 'annotations' in doc['metadata']:
                assert 'external-dns.alpha.kubernetes.io/hostname' not in doc['metadata']['annotations']
            # test no_horizontal_autoscaling=True
            assert doc['kind'] != 'HorizontalPodAutoscaler'
            if doc['kind'] == 'Deployment':
                for container in doc['spec']['template']['spec']['containers']:
                    assert container['image'].startswith('registry3.gitlab.com')
                    assert 'resources' not in container
                assert 'replicas'  in doc['spec']
            elif doc['kind'] == 'Service':
                ports += [p['port'] for p in doc['spec']['ports']]
    # test reuse_ports=True
    assert len(set(ports)) < len(ports)

def test_no_windows(deploy_dir):
    deployments = manifest_to_k8s_deploy([TEST_CHALLENGE_DIR],
                            registry='registry.gitlab.com',
                            dns='ctf.cybears.io',
                            no_externaldns=False,
                            force=True,
                            output_dir=deploy_dir,
                            reuse_ports=True,
                            no_horizontal_autoscaling=False,
                            no_windows_support=True)

    for deployment in deployments:
        for doc in deployment:
            # test windows_support=False
            if doc['kind'] == 'Deployment':
                assert doc['spec']['template']['spec']['nodeSelector']['kubernetes.io/os'] != 'windows'