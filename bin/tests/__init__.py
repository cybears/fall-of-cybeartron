import os
import sys

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BIN_DIR = os.path.normpath(os.path.join(SCRIPT_DIR, ".."))
BASE_DIR = os.path.normpath(os.path.join(BIN_DIR, ".."))
LIB_DIR = os.path.normpath(os.path.join(BASE_DIR, "lib"))
CONFIG_DIR = os.path.normpath(os.path.join(BASE_DIR, "config"))
LIB_TEST_DIR = os.path.normpath(os.path.join(LIB_DIR, "tests"))
CHALLENGE_DIR = os.path.normpath(os.path.join(BIN_DIR, "..", "challenges"))
TEST_CHALLENGE_DIR = os.path.normpath(os.path.join(CHALLENGE_DIR, "test"))
BSIDES_CHALLENGE_DIR = os.path.normpath(os.path.join(CHALLENGE_DIR, "bsides"))
sys.path.append(BIN_DIR)
sys.path.append(LIB_DIR)
sys.path.append(LIB_TEST_DIR)
