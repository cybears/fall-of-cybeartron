![Cybears logo](/.gitlab/cybears-150.png)

# Cybears CTF

This repo contains the CTF challenges developed by the Cybears CTF team. Check out our [Contribution guide](CONTRIBUTING.md) to
get started with development.

## Creating a new challenge

Creating a challenge is easy with the Cybears Challenge Wizard! 🧙‍♂️

```bash
pip3 install pwntools pyyaml
./bin/chal.py create
# ./bin/chal.py has a bunch of useful commands, try `./bin/chal.py --help`
```

[chal.py](/bin/chal.py) will walk you through creating a challenge and set you up with a dummy challenge
that includes:
- A build system
- CI
- All the required files
- A README.md explaining what the wizard just did.

You should read the README.md the wizard puts in your challenge directory and
the [contribution guide](CONTRIBUTING.md) for more info.

This should get you all set up with a nice base for your challenge. If it doesn't
suit your needs, drop an issue in the issue tracker and tag it with `skeleton` or
`wizard`.

# Other documentation

- [Infrastructure](INFRASTRUCTURE.md)
